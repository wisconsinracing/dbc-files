function msg = WR225E_dbc(msgname)
%msg = WR225E_dbc(msgname);

msg = struct;

switch msgname

%===================================================================
case 'AUX_AIN'
WR225E.AUX_AIN = struct;
WR225E.AUX_AIN.name = 'AUX_AIN';
WR225E.AUX_AIN.description = 'AUX_AIN';
WR225E.AUX_AIN.protocol = 'WR225E';
WR225E.AUX_AIN.id = hex2dec('00000160');
WR225E.AUX_AIN.idext = 'STANDARD';
WR225E.AUX_AIN.idinherit = 0;
WR225E.AUX_AIN.payload_size = 8;
WR225E.AUX_AIN.payload_value = [];
WR225E.AUX_AIN.payload_mask = [];
WR225E.AUX_AIN.interval = 50;

WR225E.AUX_AIN.fields{1}.name = 'AUX_BPD_FAULT_V';
WR225E.AUX_AIN.fields{1}.units = 'V';
WR225E.AUX_AIN.fields{1}.start_bit = 0;
WR225E.AUX_AIN.fields{1}.bit_length = 8;
WR225E.AUX_AIN.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{1}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{1}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{1}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{2}.name = 'AUX_AIR_COIL_V';
WR225E.AUX_AIN.fields{2}.units = 'V';
WR225E.AUX_AIN.fields{2}.start_bit = 8;
WR225E.AUX_AIN.fields{2}.bit_length = 8;
WR225E.AUX_AIN.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{2}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{2}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{2}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{3}.name = 'AUX_IMD_FAULT_V';
WR225E.AUX_AIN.fields{3}.units = 'V';
WR225E.AUX_AIN.fields{3}.start_bit = 16;
WR225E.AUX_AIN.fields{3}.bit_length = 8;
WR225E.AUX_AIN.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{3}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{3}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{3}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{4}.name = 'AUX_CURRENT_THRESH';
WR225E.AUX_AIN.fields{4}.units = 'V';
WR225E.AUX_AIN.fields{4}.start_bit = 24;
WR225E.AUX_AIN.fields{4}.bit_length = 8;
WR225E.AUX_AIN.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{4}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{4}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{4}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{5}.name = 'AUX_BRK_THRESH';
WR225E.AUX_AIN.fields{5}.units = 'V';
WR225E.AUX_AIN.fields{5}.start_bit = 32;
WR225E.AUX_AIN.fields{5}.bit_length = 8;
WR225E.AUX_AIN.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{5}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{5}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{5}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{6}.name = 'AUX_BRK_PRES1_V';
WR225E.AUX_AIN.fields{6}.units = 'V';
WR225E.AUX_AIN.fields{6}.start_bit = 40;
WR225E.AUX_AIN.fields{6}.bit_length = 8;
WR225E.AUX_AIN.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{6}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{6}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{6}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{7}.name = 'AUX_SDC_SUPPLY_V';
WR225E.AUX_AIN.fields{7}.units = 'V';
WR225E.AUX_AIN.fields{7}.start_bit = 48;
WR225E.AUX_AIN.fields{7}.bit_length = 8;
WR225E.AUX_AIN.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{7}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{7}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{7}.offset = 0.000000e+00;

WR225E.AUX_AIN.fields{8}.name = 'AUX_GLV_V';
WR225E.AUX_AIN.fields{8}.units = 'V';
WR225E.AUX_AIN.fields{8}.start_bit = 56;
WR225E.AUX_AIN.fields{8}.bit_length = 8;
WR225E.AUX_AIN.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_AIN.fields{8}.data_type = 'UNSIGNED';
WR225E.AUX_AIN.fields{8}.scale = 1.000000e-01;
WR225E.AUX_AIN.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'AUX_FAULTS'
WR225E.AUX_FAULTS = struct;
WR225E.AUX_FAULTS.name = 'AUX_FAULTS';
WR225E.AUX_FAULTS.description = 'AUX_FAULTS';
WR225E.AUX_FAULTS.protocol = 'WR225E';
WR225E.AUX_FAULTS.id = hex2dec('00000060');
WR225E.AUX_FAULTS.idext = 'STANDARD';
WR225E.AUX_FAULTS.idinherit = 0;
WR225E.AUX_FAULTS.payload_size = 1;
WR225E.AUX_FAULTS.payload_value = [];
WR225E.AUX_FAULTS.payload_mask = [];
WR225E.AUX_FAULTS.interval = 50;

WR225E.AUX_FAULTS.fields{1}.name = 'AUX_IMD_UNDERVOLTAGE';
WR225E.AUX_FAULTS.fields{1}.units = '';
WR225E.AUX_FAULTS.fields{1}.start_bit = 58;
WR225E.AUX_FAULTS.fields{1}.bit_length = 1;
WR225E.AUX_FAULTS.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{1}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{1}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{1}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{2}.name = 'AUX_IMD_SST';
WR225E.AUX_FAULTS.fields{2}.units = '';
WR225E.AUX_FAULTS.fields{2}.start_bit = 59;
WR225E.AUX_FAULTS.fields{2}.bit_length = 1;
WR225E.AUX_FAULTS.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{2}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{2}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{2}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{3}.name = 'AUX_IMD_SHORT_CIRCUIT';
WR225E.AUX_FAULTS.fields{3}.units = '';
WR225E.AUX_FAULTS.fields{3}.start_bit = 62;
WR225E.AUX_FAULTS.fields{3}.bit_length = 1;
WR225E.AUX_FAULTS.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{3}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{3}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{3}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{4}.name = 'AUX_IMD_NORMAL';
WR225E.AUX_FAULTS.fields{4}.units = '';
WR225E.AUX_FAULTS.fields{4}.start_bit = 57;
WR225E.AUX_FAULTS.fields{4}.bit_length = 1;
WR225E.AUX_FAULTS.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{4}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{4}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{4}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{5}.name = 'AUX_IMD_KI31_FAULT';
WR225E.AUX_FAULTS.fields{5}.units = '';
WR225E.AUX_FAULTS.fields{5}.start_bit = 61;
WR225E.AUX_FAULTS.fields{5}.bit_length = 1;
WR225E.AUX_FAULTS.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{5}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{5}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{5}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{6}.name = 'AUX_IMD_DEVICE_ERROR';
WR225E.AUX_FAULTS.fields{6}.units = '';
WR225E.AUX_FAULTS.fields{6}.start_bit = 60;
WR225E.AUX_FAULTS.fields{6}.bit_length = 1;
WR225E.AUX_FAULTS.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{6}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{6}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{6}.offset = 0.000000e+00;

WR225E.AUX_FAULTS.fields{7}.name = 'AUX_AIRS_CLOSED';
WR225E.AUX_FAULTS.fields{7}.units = '';
WR225E.AUX_FAULTS.fields{7}.start_bit = 56;
WR225E.AUX_FAULTS.fields{7}.bit_length = 1;
WR225E.AUX_FAULTS.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_FAULTS.fields{7}.data_type = 'UNSIGNED';
WR225E.AUX_FAULTS.fields{7}.scale = 1.000000e+00;
WR225E.AUX_FAULTS.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'AUX_Flags'
WR225E.AUX_Flags = struct;
WR225E.AUX_Flags.name = 'AUX_Flags';
WR225E.AUX_Flags.description = 'AUX_Flags';
WR225E.AUX_Flags.protocol = 'WR225E';
WR225E.AUX_Flags.id = hex2dec('00000030');
WR225E.AUX_Flags.idext = 'STANDARD';
WR225E.AUX_Flags.idinherit = 0;
WR225E.AUX_Flags.payload_size = 2;
WR225E.AUX_Flags.payload_value = [];
WR225E.AUX_Flags.payload_mask = [];
WR225E.AUX_Flags.interval = 50;

WR225E.AUX_Flags.fields{1}.name = 'AUX_IMD_FAULT';
WR225E.AUX_Flags.fields{1}.units = 'Flag';
WR225E.AUX_Flags.fields{1}.start_bit = 59;
WR225E.AUX_Flags.fields{1}.bit_length = 1;
WR225E.AUX_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{1}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{1}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{1}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{2}.name = 'AUX_SDC_RETURN';
WR225E.AUX_Flags.fields{2}.units = '';
WR225E.AUX_Flags.fields{2}.start_bit = 60;
WR225E.AUX_Flags.fields{2}.bit_length = 1;
WR225E.AUX_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{2}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{2}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{2}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{3}.name = 'AUX_SDC_SUPPLY';
WR225E.AUX_Flags.fields{3}.units = 'Flag';
WR225E.AUX_Flags.fields{3}.start_bit = 61;
WR225E.AUX_Flags.fields{3}.bit_length = 1;
WR225E.AUX_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{3}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{3}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{3}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{4}.name = 'AUX_MUX_DCDC_VALID';
WR225E.AUX_Flags.fields{4}.units = '';
WR225E.AUX_Flags.fields{4}.start_bit = 56;
WR225E.AUX_Flags.fields{4}.bit_length = 1;
WR225E.AUX_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{4}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{4}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{4}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{5}.name = 'AUX_MUX_BATT_VALID';
WR225E.AUX_Flags.fields{5}.units = '';
WR225E.AUX_Flags.fields{5}.start_bit = 57;
WR225E.AUX_Flags.fields{5}.bit_length = 1;
WR225E.AUX_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{5}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{5}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{5}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{6}.name = 'AUX_BPD_FAULT';
WR225E.AUX_Flags.fields{6}.units = 'Flag';
WR225E.AUX_Flags.fields{6}.start_bit = 58;
WR225E.AUX_Flags.fields{6}.bit_length = 1;
WR225E.AUX_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{6}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{6}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{6}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{7}.name = 'AUX_ECU_FAULT';
WR225E.AUX_Flags.fields{7}.units = '';
WR225E.AUX_Flags.fields{7}.start_bit = 62;
WR225E.AUX_Flags.fields{7}.bit_length = 1;
WR225E.AUX_Flags.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{7}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{7}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{7}.offset = 0.000000e+00;

WR225E.AUX_Flags.fields{8}.name = 'AUX_AIR_COIL';
WR225E.AUX_Flags.fields{8}.units = 'Flag';
WR225E.AUX_Flags.fields{8}.start_bit = 63;
WR225E.AUX_Flags.fields{8}.bit_length = 1;
WR225E.AUX_Flags.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR225E.AUX_Flags.fields{8}.data_type = 'UNSIGNED';
WR225E.AUX_Flags.fields{8}.scale = 1.000000e+00;
WR225E.AUX_Flags.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'BMS_Vitals'
WR225E.BMS_Vitals = struct;
WR225E.BMS_Vitals.name = 'BMS_Vitals';
WR225E.BMS_Vitals.description = 'BMS_Vitals';
WR225E.BMS_Vitals.protocol = 'WR225E';
WR225E.BMS_Vitals.id = hex2dec('000000A0');
WR225E.BMS_Vitals.idext = 'STANDARD';
WR225E.BMS_Vitals.idinherit = 0;
WR225E.BMS_Vitals.payload_size = 8;
WR225E.BMS_Vitals.payload_value = [];
WR225E.BMS_Vitals.payload_mask = [];
WR225E.BMS_Vitals.interval = 50;

WR225E.BMS_Vitals.fields{1}.name = 'BMS_PowerStageClosed';
WR225E.BMS_Vitals.fields{1}.units = '';
WR225E.BMS_Vitals.fields{1}.start_bit = 56;
WR225E.BMS_Vitals.fields{1}.bit_length = 1;
WR225E.BMS_Vitals.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.BMS_Vitals.fields{1}.data_type = 'UNSIGNED';
WR225E.BMS_Vitals.fields{1}.scale = 1.000000e+00;
WR225E.BMS_Vitals.fields{1}.offset = 0.000000e+00;

WR225E.BMS_Vitals.fields{2}.name = 'BMS_ChargeCurrentLimit';
WR225E.BMS_Vitals.fields{2}.units = 'A';
WR225E.BMS_Vitals.fields{2}.start_bit = 32;
WR225E.BMS_Vitals.fields{2}.bit_length = 16;
WR225E.BMS_Vitals.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.BMS_Vitals.fields{2}.data_type = 'UNSIGNED';
WR225E.BMS_Vitals.fields{2}.maximum = 2.550000e+02;
WR225E.BMS_Vitals.fields{2}.minimum = 0.000000e+00;
WR225E.BMS_Vitals.fields{2}.scale = 1.000000e+00;
WR225E.BMS_Vitals.fields{2}.offset = 0.000000e+00;

WR225E.BMS_Vitals.fields{3}.name = 'BMS_DischargeCurrentLimit';
WR225E.BMS_Vitals.fields{3}.units = 'A';
WR225E.BMS_Vitals.fields{3}.start_bit = 48;
WR225E.BMS_Vitals.fields{3}.bit_length = 16;
WR225E.BMS_Vitals.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.BMS_Vitals.fields{3}.data_type = 'UNSIGNED';
WR225E.BMS_Vitals.fields{3}.maximum = 6.553500e+04;
WR225E.BMS_Vitals.fields{3}.minimum = 0.000000e+00;
WR225E.BMS_Vitals.fields{3}.scale = 1.000000e+00;
WR225E.BMS_Vitals.fields{3}.offset = 0.000000e+00;

WR225E.BMS_Vitals.fields{4}.name = 'BMS_BalancingActive';
WR225E.BMS_Vitals.fields{4}.units = '';
WR225E.BMS_Vitals.fields{4}.start_bit = 57;
WR225E.BMS_Vitals.fields{4}.bit_length = 1;
WR225E.BMS_Vitals.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.BMS_Vitals.fields{4}.data_type = 'UNSIGNED';
WR225E.BMS_Vitals.fields{4}.scale = 1.000000e+00;
WR225E.BMS_Vitals.fields{4}.offset = 0.000000e+00;

WR225E.BMS_Vitals.fields{5}.name = 'BMS_NumberCellsBalancing';
WR225E.BMS_Vitals.fields{5}.units = '';
WR225E.BMS_Vitals.fields{5}.start_bit = 16;
WR225E.BMS_Vitals.fields{5}.bit_length = 8;
WR225E.BMS_Vitals.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.BMS_Vitals.fields{5}.data_type = 'UNSIGNED';
WR225E.BMS_Vitals.fields{5}.scale = 1.000000e+00;
WR225E.BMS_Vitals.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Ax'
WR225E.Bosch_Ax = struct;
WR225E.Bosch_Ax.name = 'Bosch_Ax';
WR225E.Bosch_Ax.description = 'Bosch_Ax';
WR225E.Bosch_Ax.protocol = 'WR225E';
WR225E.Bosch_Ax.id = hex2dec('00000178');
WR225E.Bosch_Ax.idext = 'STANDARD';
WR225E.Bosch_Ax.idinherit = 0;
WR225E.Bosch_Ax.payload_size = 8;
WR225E.Bosch_Ax.payload_value = [];
WR225E.Bosch_Ax.payload_mask = [];
WR225E.Bosch_Ax.interval = 50;

WR225E.Bosch_Ax.fields{1}.name = 'RolRate_Degs';
WR225E.Bosch_Ax.fields{1}.units = '';
WR225E.Bosch_Ax.fields{1}.start_bit = 56;
WR225E.Bosch_Ax.fields{1}.bit_length = 16;
WR225E.Bosch_Ax.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Ax.fields{1}.data_type = 'UNSIGNED';
WR225E.Bosch_Ax.fields{1}.maximum = 1.638350e+02;
WR225E.Bosch_Ax.fields{1}.minimum = -1.638400e+02;
WR225E.Bosch_Ax.fields{1}.scale = 5.000000e-03;
WR225E.Bosch_Ax.fields{1}.offset = -1.638400e+02;

WR225E.Bosch_Ax.fields{2}.name = 'Bosch_ax_g';
WR225E.Bosch_Ax.fields{2}.units = 'g';
WR225E.Bosch_Ax.fields{2}.start_bit = 24;
WR225E.Bosch_Ax.fields{2}.bit_length = 16;
WR225E.Bosch_Ax.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Ax.fields{2}.data_type = 'UNSIGNED';
WR225E.Bosch_Ax.fields{2}.maximum = 4.176500e+00;
WR225E.Bosch_Ax.fields{2}.minimum = -4.176800e+00;
WR225E.Bosch_Ax.fields{2}.scale = 1.274650e-04;
WR225E.Bosch_Ax.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Ay'
WR225E.Bosch_Ay = struct;
WR225E.Bosch_Ay.name = 'Bosch_Ay';
WR225E.Bosch_Ay.description = 'Bosch_Ay';
WR225E.Bosch_Ay.protocol = 'WR225E';
WR225E.Bosch_Ay.id = hex2dec('00000174');
WR225E.Bosch_Ay.idext = 'STANDARD';
WR225E.Bosch_Ay.idinherit = 0;
WR225E.Bosch_Ay.payload_size = 8;
WR225E.Bosch_Ay.payload_value = [];
WR225E.Bosch_Ay.payload_mask = [];
WR225E.Bosch_Ay.interval = 50;

WR225E.Bosch_Ay.fields{1}.name = 'YawRate_Deg';
WR225E.Bosch_Ay.fields{1}.units = '';
WR225E.Bosch_Ay.fields{1}.start_bit = 56;
WR225E.Bosch_Ay.fields{1}.bit_length = 16;
WR225E.Bosch_Ay.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Ay.fields{1}.data_type = 'UNSIGNED';
WR225E.Bosch_Ay.fields{1}.maximum = 1.638300e+02;
WR225E.Bosch_Ay.fields{1}.minimum = -1.638400e+02;
WR225E.Bosch_Ay.fields{1}.scale = 5.000000e-03;
WR225E.Bosch_Ay.fields{1}.offset = -1.638400e+02;

WR225E.Bosch_Ay.fields{2}.name = 'Bosch_ay_g';
WR225E.Bosch_Ay.fields{2}.units = 'g';
WR225E.Bosch_Ay.fields{2}.start_bit = 24;
WR225E.Bosch_Ay.fields{2}.bit_length = 16;
WR225E.Bosch_Ay.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Ay.fields{2}.data_type = 'UNSIGNED';
WR225E.Bosch_Ay.fields{2}.maximum = 4.176500e+00;
WR225E.Bosch_Ay.fields{2}.minimum = -4.176800e+00;
WR225E.Bosch_Ay.fields{2}.scale = 1.274650e-04;
WR225E.Bosch_Ay.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Az'
WR225E.Bosch_Az = struct;
WR225E.Bosch_Az.name = 'Bosch_Az';
WR225E.Bosch_Az.description = 'Bosch_Az';
WR225E.Bosch_Az.protocol = 'WR225E';
WR225E.Bosch_Az.id = hex2dec('0000017C');
WR225E.Bosch_Az.idext = 'STANDARD';
WR225E.Bosch_Az.idinherit = 0;
WR225E.Bosch_Az.payload_size = 8;
WR225E.Bosch_Az.payload_value = [];
WR225E.Bosch_Az.payload_mask = [];
WR225E.Bosch_Az.interval = 50;

WR225E.Bosch_Az.fields{1}.name = 'AZ_STAT';
WR225E.Bosch_Az.fields{1}.units = '';
WR225E.Bosch_Az.fields{1}.start_bit = 12;
WR225E.Bosch_Az.fields{1}.bit_length = 4;
WR225E.Bosch_Az.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Az.fields{1}.data_type = 'UNSIGNED';
WR225E.Bosch_Az.fields{1}.scale = 1.000000e+00;
WR225E.Bosch_Az.fields{1}.offset = 0.000000e+00;

WR225E.Bosch_Az.fields{2}.name = 'Bosch_az_g';
WR225E.Bosch_Az.fields{2}.units = 'g';
WR225E.Bosch_Az.fields{2}.start_bit = 24;
WR225E.Bosch_Az.fields{2}.bit_length = 16;
WR225E.Bosch_Az.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Az.fields{2}.data_type = 'UNSIGNED';
WR225E.Bosch_Az.fields{2}.maximum = 4.176500e+00;
WR225E.Bosch_Az.fields{2}.minimum = -4.176800e+00;
WR225E.Bosch_Az.fields{2}.scale = 1.274650e-04;
WR225E.Bosch_Az.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Steering_Angle'
WR225E.Bosch_Steering_Angle = struct;
WR225E.Bosch_Steering_Angle.name = 'Bosch_Steering_Angle';
WR225E.Bosch_Steering_Angle.description = 'Bosch_Steering_Angle';
WR225E.Bosch_Steering_Angle.protocol = 'WR225E';
WR225E.Bosch_Steering_Angle.id = hex2dec('000002B0');
WR225E.Bosch_Steering_Angle.idext = 'STANDARD';
WR225E.Bosch_Steering_Angle.idinherit = 0;
WR225E.Bosch_Steering_Angle.payload_size = 5;
WR225E.Bosch_Steering_Angle.payload_value = [];
WR225E.Bosch_Steering_Angle.payload_mask = [];
WR225E.Bosch_Steering_Angle.interval = 50;

WR225E.Bosch_Steering_Angle.fields{1}.name = 'Trim';
WR225E.Bosch_Steering_Angle.fields{1}.units = '';
WR225E.Bosch_Steering_Angle.fields{1}.start_bit = 34;
WR225E.Bosch_Steering_Angle.fields{1}.bit_length = 1;
WR225E.Bosch_Steering_Angle.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle.fields{1}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle.fields{1}.maximum = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{1}.minimum = 0.000000e+00;
WR225E.Bosch_Steering_Angle.fields{1}.scale = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{1}.offset = 0.000000e+00;

WR225E.Bosch_Steering_Angle.fields{2}.name = 'Cal';
WR225E.Bosch_Steering_Angle.fields{2}.units = '';
WR225E.Bosch_Steering_Angle.fields{2}.start_bit = 33;
WR225E.Bosch_Steering_Angle.fields{2}.bit_length = 1;
WR225E.Bosch_Steering_Angle.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle.fields{2}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle.fields{2}.maximum = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{2}.minimum = 0.000000e+00;
WR225E.Bosch_Steering_Angle.fields{2}.scale = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{2}.offset = 0.000000e+00;

WR225E.Bosch_Steering_Angle.fields{3}.name = 'Ok';
WR225E.Bosch_Steering_Angle.fields{3}.units = '';
WR225E.Bosch_Steering_Angle.fields{3}.start_bit = 32;
WR225E.Bosch_Steering_Angle.fields{3}.bit_length = 1;
WR225E.Bosch_Steering_Angle.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle.fields{3}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle.fields{3}.maximum = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{3}.minimum = 0.000000e+00;
WR225E.Bosch_Steering_Angle.fields{3}.scale = 1.000000e+00;
WR225E.Bosch_Steering_Angle.fields{3}.offset = 0.000000e+00;

WR225E.Bosch_Steering_Angle.fields{4}.name = 'SAS_Speed';
WR225E.Bosch_Steering_Angle.fields{4}.units = '';
WR225E.Bosch_Steering_Angle.fields{4}.start_bit = 40;
WR225E.Bosch_Steering_Angle.fields{4}.bit_length = 8;
WR225E.Bosch_Steering_Angle.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle.fields{4}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle.fields{4}.maximum = 1.016000e+03;
WR225E.Bosch_Steering_Angle.fields{4}.minimum = 0.000000e+00;
WR225E.Bosch_Steering_Angle.fields{4}.scale = 4.000000e+00;
WR225E.Bosch_Steering_Angle.fields{4}.offset = 0.000000e+00;

WR225E.Bosch_Steering_Angle.fields{5}.name = 'SAS_Angle';
WR225E.Bosch_Steering_Angle.fields{5}.units = '';
WR225E.Bosch_Steering_Angle.fields{5}.start_bit = 56;
WR225E.Bosch_Steering_Angle.fields{5}.bit_length = 16;
WR225E.Bosch_Steering_Angle.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle.fields{5}.data_type = 'SIGNED';
WR225E.Bosch_Steering_Angle.fields{5}.maximum = 7.800000e+02;
WR225E.Bosch_Steering_Angle.fields{5}.minimum = -7.800000e+02;
WR225E.Bosch_Steering_Angle.fields{5}.scale = 1.000000e-01;
WR225E.Bosch_Steering_Angle.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Config'
WR225E.Bosch_Steering_Angle_Config = struct;
WR225E.Bosch_Steering_Angle_Config.name = 'Bosch_Steering_Angle_Config';
WR225E.Bosch_Steering_Angle_Config.description = 'Bosch_Steering_Angle_Config';
WR225E.Bosch_Steering_Angle_Config.protocol = 'WR225E';
WR225E.Bosch_Steering_Angle_Config.id = hex2dec('000007C0');
WR225E.Bosch_Steering_Angle_Config.idext = 'STANDARD';
WR225E.Bosch_Steering_Angle_Config.idinherit = 0;
WR225E.Bosch_Steering_Angle_Config.payload_size = 2;
WR225E.Bosch_Steering_Angle_Config.payload_value = [];
WR225E.Bosch_Steering_Angle_Config.payload_mask = [];
WR225E.Bosch_Steering_Angle_Config.interval = 50;

WR225E.Bosch_Steering_Angle_Config.fields{1}.name = 'Reset';
WR225E.Bosch_Steering_Angle_Config.fields{1}.units = '';
WR225E.Bosch_Steering_Angle_Config.fields{1}.start_bit = 60;
WR225E.Bosch_Steering_Angle_Config.fields{1}.bit_length = 12;
WR225E.Bosch_Steering_Angle_Config.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle_Config.fields{1}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle_Config.fields{1}.scale = 1.000000e+00;
WR225E.Bosch_Steering_Angle_Config.fields{1}.offset = 0.000000e+00;

WR225E.Bosch_Steering_Angle_Config.fields{2}.name = 'Cal_Zero';
WR225E.Bosch_Steering_Angle_Config.fields{2}.units = '';
WR225E.Bosch_Steering_Angle_Config.fields{2}.start_bit = 56;
WR225E.Bosch_Steering_Angle_Config.fields{2}.bit_length = 4;
WR225E.Bosch_Steering_Angle_Config.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Bosch_Steering_Angle_Config.fields{2}.data_type = 'UNSIGNED';
WR225E.Bosch_Steering_Angle_Config.fields{2}.scale = 1.000000e+00;
WR225E.Bosch_Steering_Angle_Config.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'Brake_Rotor_Sensor_Temp'
WR225E.Brake_Rotor_Sensor_Temp = struct;
WR225E.Brake_Rotor_Sensor_Temp.name = 'Brake_Rotor_Sensor_Temp';
WR225E.Brake_Rotor_Sensor_Temp.description = 'Brake_Rotor_Sensor_Temp';
WR225E.Brake_Rotor_Sensor_Temp.protocol = 'WR225E';
WR225E.Brake_Rotor_Sensor_Temp.id = hex2dec('000004C8');
WR225E.Brake_Rotor_Sensor_Temp.idext = 'STANDARD';
WR225E.Brake_Rotor_Sensor_Temp.idinherit = 0;
WR225E.Brake_Rotor_Sensor_Temp.payload_size = 2;
WR225E.Brake_Rotor_Sensor_Temp.payload_value = [];
WR225E.Brake_Rotor_Sensor_Temp.payload_mask = [];
WR225E.Brake_Rotor_Sensor_Temp.interval = 50;

WR225E.Brake_Rotor_Sensor_Temp.fields{1}.name = 'Brake_Sensor_Temp';
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.units = 'C';
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.start_bit = 48;
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.bit_length = 16;
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Sensor_Temp.fields{1}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp1'
WR225E.Brake_Rotor_Temp1 = struct;
WR225E.Brake_Rotor_Temp1.name = 'Brake_Rotor_Temp1';
WR225E.Brake_Rotor_Temp1.description = 'Brake_Rotor_Temp1';
WR225E.Brake_Rotor_Temp1.protocol = 'WR225E';
WR225E.Brake_Rotor_Temp1.id = hex2dec('000004C4');
WR225E.Brake_Rotor_Temp1.idext = 'STANDARD';
WR225E.Brake_Rotor_Temp1.idinherit = 0;
WR225E.Brake_Rotor_Temp1.payload_size = 8;
WR225E.Brake_Rotor_Temp1.payload_value = [];
WR225E.Brake_Rotor_Temp1.payload_mask = [];
WR225E.Brake_Rotor_Temp1.interval = 50;

WR225E.Brake_Rotor_Temp1.fields{1}.name = 'Rotor_Temp_1';
WR225E.Brake_Rotor_Temp1.fields{1}.units = 'C';
WR225E.Brake_Rotor_Temp1.fields{1}.start_bit = 48;
WR225E.Brake_Rotor_Temp1.fields{1}.bit_length = 16;
WR225E.Brake_Rotor_Temp1.fields{1}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp1.fields{1}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp1.fields{1}.maximum = 6.452000e+03;
WR225E.Brake_Rotor_Temp1.fields{1}.minimum = 0.000000e+00;
WR225E.Brake_Rotor_Temp1.fields{1}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp1.fields{1}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp1.fields{2}.name = 'Rotor_Temp_2';
WR225E.Brake_Rotor_Temp1.fields{2}.units = 'C';
WR225E.Brake_Rotor_Temp1.fields{2}.start_bit = 32;
WR225E.Brake_Rotor_Temp1.fields{2}.bit_length = 16;
WR225E.Brake_Rotor_Temp1.fields{2}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp1.fields{2}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp1.fields{2}.maximum = 6.452000e+03;
WR225E.Brake_Rotor_Temp1.fields{2}.minimum = 0.000000e+00;
WR225E.Brake_Rotor_Temp1.fields{2}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp1.fields{2}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp1.fields{3}.name = 'Rotor_Temp_3';
WR225E.Brake_Rotor_Temp1.fields{3}.units = 'C';
WR225E.Brake_Rotor_Temp1.fields{3}.start_bit = 16;
WR225E.Brake_Rotor_Temp1.fields{3}.bit_length = 16;
WR225E.Brake_Rotor_Temp1.fields{3}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp1.fields{3}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp1.fields{3}.maximum = 6.452000e+03;
WR225E.Brake_Rotor_Temp1.fields{3}.minimum = 0.000000e+00;
WR225E.Brake_Rotor_Temp1.fields{3}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp1.fields{3}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp1.fields{4}.name = 'Rotor_Temp_4';
WR225E.Brake_Rotor_Temp1.fields{4}.units = 'C';
WR225E.Brake_Rotor_Temp1.fields{4}.start_bit = 0;
WR225E.Brake_Rotor_Temp1.fields{4}.bit_length = 16;
WR225E.Brake_Rotor_Temp1.fields{4}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp1.fields{4}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp1.fields{4}.maximum = 6.452000e+03;
WR225E.Brake_Rotor_Temp1.fields{4}.minimum = 0.000000e+00;
WR225E.Brake_Rotor_Temp1.fields{4}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp1.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp2'
WR225E.Brake_Rotor_Temp2 = struct;
WR225E.Brake_Rotor_Temp2.name = 'Brake_Rotor_Temp2';
WR225E.Brake_Rotor_Temp2.description = 'Brake_Rotor_Temp2';
WR225E.Brake_Rotor_Temp2.protocol = 'WR225E';
WR225E.Brake_Rotor_Temp2.id = hex2dec('000004C5');
WR225E.Brake_Rotor_Temp2.idext = 'STANDARD';
WR225E.Brake_Rotor_Temp2.idinherit = 0;
WR225E.Brake_Rotor_Temp2.payload_size = 8;
WR225E.Brake_Rotor_Temp2.payload_value = [];
WR225E.Brake_Rotor_Temp2.payload_mask = [];
WR225E.Brake_Rotor_Temp2.interval = 50;

WR225E.Brake_Rotor_Temp2.fields{1}.name = 'Rotor_Temp_5';
WR225E.Brake_Rotor_Temp2.fields{1}.units = 'C';
WR225E.Brake_Rotor_Temp2.fields{1}.start_bit = 48;
WR225E.Brake_Rotor_Temp2.fields{1}.bit_length = 16;
WR225E.Brake_Rotor_Temp2.fields{1}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp2.fields{1}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp2.fields{1}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp2.fields{1}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp2.fields{2}.name = 'Rotor_Temp_6';
WR225E.Brake_Rotor_Temp2.fields{2}.units = 'C';
WR225E.Brake_Rotor_Temp2.fields{2}.start_bit = 32;
WR225E.Brake_Rotor_Temp2.fields{2}.bit_length = 16;
WR225E.Brake_Rotor_Temp2.fields{2}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp2.fields{2}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp2.fields{2}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp2.fields{2}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp2.fields{3}.name = 'Rotor_Temp_7';
WR225E.Brake_Rotor_Temp2.fields{3}.units = 'C';
WR225E.Brake_Rotor_Temp2.fields{3}.start_bit = 16;
WR225E.Brake_Rotor_Temp2.fields{3}.bit_length = 16;
WR225E.Brake_Rotor_Temp2.fields{3}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp2.fields{3}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp2.fields{3}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp2.fields{3}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp2.fields{4}.name = 'Rotor_Temp_8';
WR225E.Brake_Rotor_Temp2.fields{4}.units = 'C';
WR225E.Brake_Rotor_Temp2.fields{4}.start_bit = 0;
WR225E.Brake_Rotor_Temp2.fields{4}.bit_length = 16;
WR225E.Brake_Rotor_Temp2.fields{4}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp2.fields{4}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp2.fields{4}.scale = 1.000000e-01;
WR225E.Brake_Rotor_Temp2.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp3'
WR225E.Brake_Rotor_Temp3 = struct;
WR225E.Brake_Rotor_Temp3.name = 'Brake_Rotor_Temp3';
WR225E.Brake_Rotor_Temp3.description = 'Brake_Rotor_Temp3';
WR225E.Brake_Rotor_Temp3.protocol = 'WR225E';
WR225E.Brake_Rotor_Temp3.id = hex2dec('000004C6');
WR225E.Brake_Rotor_Temp3.idext = 'STANDARD';
WR225E.Brake_Rotor_Temp3.idinherit = 0;
WR225E.Brake_Rotor_Temp3.payload_size = 8;
WR225E.Brake_Rotor_Temp3.payload_value = [];
WR225E.Brake_Rotor_Temp3.payload_mask = [];
WR225E.Brake_Rotor_Temp3.interval = 50;

WR225E.Brake_Rotor_Temp3.fields{1}.name = 'Rotor_Temp_9';
WR225E.Brake_Rotor_Temp3.fields{1}.units = 'C';
WR225E.Brake_Rotor_Temp3.fields{1}.start_bit = 48;
WR225E.Brake_Rotor_Temp3.fields{1}.bit_length = 16;
WR225E.Brake_Rotor_Temp3.fields{1}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp3.fields{1}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp3.fields{1}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp3.fields{1}.offset = 0.000000e+00;

WR225E.Brake_Rotor_Temp3.fields{2}.name = 'Rotor_Temp_10';
WR225E.Brake_Rotor_Temp3.fields{2}.units = 'C';
WR225E.Brake_Rotor_Temp3.fields{2}.start_bit = 32;
WR225E.Brake_Rotor_Temp3.fields{2}.bit_length = 16;
WR225E.Brake_Rotor_Temp3.fields{2}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp3.fields{2}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp3.fields{2}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp3.fields{2}.offset = 0.000000e+00;

WR225E.Brake_Rotor_Temp3.fields{3}.name = 'Rotor_Temp_11';
WR225E.Brake_Rotor_Temp3.fields{3}.units = 'C';
WR225E.Brake_Rotor_Temp3.fields{3}.start_bit = 16;
WR225E.Brake_Rotor_Temp3.fields{3}.bit_length = 16;
WR225E.Brake_Rotor_Temp3.fields{3}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp3.fields{3}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp3.fields{3}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp3.fields{3}.offset = 0.000000e+00;

WR225E.Brake_Rotor_Temp3.fields{4}.name = 'Rotor_Temp_12';
WR225E.Brake_Rotor_Temp3.fields{4}.units = 'C';
WR225E.Brake_Rotor_Temp3.fields{4}.start_bit = 0;
WR225E.Brake_Rotor_Temp3.fields{4}.bit_length = 16;
WR225E.Brake_Rotor_Temp3.fields{4}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp3.fields{4}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp3.fields{4}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp3.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Brake_Rotor_Temp4'
WR225E.Brake_Rotor_Temp4 = struct;
WR225E.Brake_Rotor_Temp4.name = 'Brake_Rotor_Temp4';
WR225E.Brake_Rotor_Temp4.description = 'Brake_Rotor_Temp4';
WR225E.Brake_Rotor_Temp4.protocol = 'WR225E';
WR225E.Brake_Rotor_Temp4.id = hex2dec('000004C7');
WR225E.Brake_Rotor_Temp4.idext = 'STANDARD';
WR225E.Brake_Rotor_Temp4.idinherit = 0;
WR225E.Brake_Rotor_Temp4.payload_size = 8;
WR225E.Brake_Rotor_Temp4.payload_value = [];
WR225E.Brake_Rotor_Temp4.payload_mask = [];
WR225E.Brake_Rotor_Temp4.interval = 50;

WR225E.Brake_Rotor_Temp4.fields{1}.name = 'Rotor_Temp_13';
WR225E.Brake_Rotor_Temp4.fields{1}.units = 'C';
WR225E.Brake_Rotor_Temp4.fields{1}.start_bit = 48;
WR225E.Brake_Rotor_Temp4.fields{1}.bit_length = 16;
WR225E.Brake_Rotor_Temp4.fields{1}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp4.fields{1}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp4.fields{1}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp4.fields{1}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp4.fields{2}.name = 'Rotor_Temp_14';
WR225E.Brake_Rotor_Temp4.fields{2}.units = 'C';
WR225E.Brake_Rotor_Temp4.fields{2}.start_bit = 32;
WR225E.Brake_Rotor_Temp4.fields{2}.bit_length = 16;
WR225E.Brake_Rotor_Temp4.fields{2}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp4.fields{2}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp4.fields{2}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp4.fields{2}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp4.fields{3}.name = 'Rotor_Temp_15';
WR225E.Brake_Rotor_Temp4.fields{3}.units = 'C';
WR225E.Brake_Rotor_Temp4.fields{3}.start_bit = 16;
WR225E.Brake_Rotor_Temp4.fields{3}.bit_length = 16;
WR225E.Brake_Rotor_Temp4.fields{3}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp4.fields{3}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp4.fields{3}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp4.fields{3}.offset = -1.000000e+02;

WR225E.Brake_Rotor_Temp4.fields{4}.name = 'Rotor_Temp_16';
WR225E.Brake_Rotor_Temp4.fields{4}.units = 'C';
WR225E.Brake_Rotor_Temp4.fields{4}.start_bit = 0;
WR225E.Brake_Rotor_Temp4.fields{4}.bit_length = 16;
WR225E.Brake_Rotor_Temp4.fields{4}.byte_order = 'BIG_ENDIAN';
WR225E.Brake_Rotor_Temp4.fields{4}.data_type = 'UNSIGNED';
WR225E.Brake_Rotor_Temp4.fields{4}.scale = 1.000000e+00;
WR225E.Brake_Rotor_Temp4.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Coolant_Temps'
WR225E.Coolant_Temps = struct;
WR225E.Coolant_Temps.name = 'Coolant_Temps';
WR225E.Coolant_Temps.description = 'Coolant_Temps';
WR225E.Coolant_Temps.protocol = 'WR225E';
WR225E.Coolant_Temps.id = hex2dec('00000336');
WR225E.Coolant_Temps.idext = 'STANDARD';
WR225E.Coolant_Temps.idinherit = 0;
WR225E.Coolant_Temps.payload_size = 8;
WR225E.Coolant_Temps.payload_value = [];
WR225E.Coolant_Temps.payload_mask = [];
WR225E.Coolant_Temps.interval = 50;

WR225E.Coolant_Temps.fields{1}.name = 'Coolant_Temp_L';
WR225E.Coolant_Temps.fields{1}.units = '';
WR225E.Coolant_Temps.fields{1}.start_bit = 56;
WR225E.Coolant_Temps.fields{1}.bit_length = 16;
WR225E.Coolant_Temps.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Coolant_Temps.fields{1}.data_type = 'UNSIGNED';
WR225E.Coolant_Temps.fields{1}.scale = 1.000000e-01;
WR225E.Coolant_Temps.fields{1}.offset = 0.000000e+00;

WR225E.Coolant_Temps.fields{2}.name = 'Coolant_Temp_R';
WR225E.Coolant_Temps.fields{2}.units = '';
WR225E.Coolant_Temps.fields{2}.start_bit = 40;
WR225E.Coolant_Temps.fields{2}.bit_length = 16;
WR225E.Coolant_Temps.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Coolant_Temps.fields{2}.data_type = 'UNSIGNED';
WR225E.Coolant_Temps.fields{2}.scale = 1.000000e-01;
WR225E.Coolant_Temps.fields{2}.offset = 0.000000e+00;

WR225E.Coolant_Temps.fields{3}.name = 'Temp_1';
WR225E.Coolant_Temps.fields{3}.units = '';
WR225E.Coolant_Temps.fields{3}.start_bit = 24;
WR225E.Coolant_Temps.fields{3}.bit_length = 16;
WR225E.Coolant_Temps.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Coolant_Temps.fields{3}.data_type = 'UNSIGNED';
WR225E.Coolant_Temps.fields{3}.scale = 1.000000e-01;
WR225E.Coolant_Temps.fields{3}.offset = 0.000000e+00;

WR225E.Coolant_Temps.fields{4}.name = 'Temp_2';
WR225E.Coolant_Temps.fields{4}.units = '';
WR225E.Coolant_Temps.fields{4}.start_bit = 8;
WR225E.Coolant_Temps.fields{4}.bit_length = 16;
WR225E.Coolant_Temps.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Coolant_Temps.fields{4}.data_type = 'UNSIGNED';
WR225E.Coolant_Temps.fields{4}.scale = 1.000000e-01;
WR225E.Coolant_Temps.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Driver_Inputs'
WR225E.Driver_Inputs = struct;
WR225E.Driver_Inputs.name = 'Driver_Inputs';
WR225E.Driver_Inputs.description = 'driver inputs that arent from the steering wheel microcontroller';
WR225E.Driver_Inputs.protocol = 'WR225E';
WR225E.Driver_Inputs.id = hex2dec('00000001');
WR225E.Driver_Inputs.idext = 'STANDARD';
WR225E.Driver_Inputs.idinherit = 0;
WR225E.Driver_Inputs.payload_size = 8;
WR225E.Driver_Inputs.payload_value = [];
WR225E.Driver_Inputs.payload_mask = [];
WR225E.Driver_Inputs.interval = 50;

WR225E.Driver_Inputs.fields{1}.name = 'Brake_Pedal_Pressure_Front';
WR225E.Driver_Inputs.fields{1}.units = 'Bar';
WR225E.Driver_Inputs.fields{1}.start_bit = 56;
WR225E.Driver_Inputs.fields{1}.bit_length = 11;
WR225E.Driver_Inputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{1}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{1}.maximum = 2.000000e+02;
WR225E.Driver_Inputs.fields{1}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{1}.scale = 1.000000e-01;
WR225E.Driver_Inputs.fields{1}.offset = 0.000000e+00;

WR225E.Driver_Inputs.fields{2}.name = 'Accel_Pedal_Position_Arb';
WR225E.Driver_Inputs.fields{2}.units = '%';
WR225E.Driver_Inputs.fields{2}.start_bit = 51;
WR225E.Driver_Inputs.fields{2}.bit_length = 10;
WR225E.Driver_Inputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{2}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{2}.maximum = 1.000000e+02;
WR225E.Driver_Inputs.fields{2}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{2}.scale = 1.000000e-01;
WR225E.Driver_Inputs.fields{2}.offset = 0.000000e+00;

WR225E.Driver_Inputs.fields{3}.name = 'Accel_Pedal_Position_1';
WR225E.Driver_Inputs.fields{3}.units = '%';
WR225E.Driver_Inputs.fields{3}.start_bit = 45;
WR225E.Driver_Inputs.fields{3}.bit_length = 10;
WR225E.Driver_Inputs.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{3}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{3}.maximum = 1.000000e+02;
WR225E.Driver_Inputs.fields{3}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{3}.scale = 1.000000e-01;
WR225E.Driver_Inputs.fields{3}.offset = 0.000000e+00;

WR225E.Driver_Inputs.fields{4}.name = 'Accel_Pedal_Position_2';
WR225E.Driver_Inputs.fields{4}.units = '%';
WR225E.Driver_Inputs.fields{4}.start_bit = 39;
WR225E.Driver_Inputs.fields{4}.bit_length = 10;
WR225E.Driver_Inputs.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{4}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{4}.maximum = 1.000000e+02;
WR225E.Driver_Inputs.fields{4}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{4}.scale = 1.000000e-01;
WR225E.Driver_Inputs.fields{4}.offset = 0.000000e+00;

WR225E.Driver_Inputs.fields{5}.name = 'Brake_Flag';
WR225E.Driver_Inputs.fields{5}.units = 'flag';
WR225E.Driver_Inputs.fields{5}.start_bit = 17;
WR225E.Driver_Inputs.fields{5}.bit_length = 1;
WR225E.Driver_Inputs.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{5}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{5}.maximum = 1.000000e+00;
WR225E.Driver_Inputs.fields{5}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{5}.scale = 1.000000e+00;
WR225E.Driver_Inputs.fields{5}.offset = 0.000000e+00;

WR225E.Driver_Inputs.fields{6}.name = 'Brake_Pedal_Pressure_Rear';
WR225E.Driver_Inputs.fields{6}.units = 'Bar';
WR225E.Driver_Inputs.fields{6}.start_bit = 18;
WR225E.Driver_Inputs.fields{6}.bit_length = 11;
WR225E.Driver_Inputs.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.Driver_Inputs.fields{6}.data_type = 'UNSIGNED';
WR225E.Driver_Inputs.fields{6}.maximum = 2.000000e+02;
WR225E.Driver_Inputs.fields{6}.minimum = 0.000000e+00;
WR225E.Driver_Inputs.fields{6}.scale = 1.000000e-01;
WR225E.Driver_Inputs.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Controls'
WR225E.IFL_Controls = struct;
WR225E.IFL_Controls.name = 'IFL_Controls';
WR225E.IFL_Controls.description = 'IFL_Controls';
WR225E.IFL_Controls.protocol = 'WR225E';
WR225E.IFL_Controls.id = hex2dec('000000C4');
WR225E.IFL_Controls.idext = 'STANDARD';
WR225E.IFL_Controls.idinherit = 0;
WR225E.IFL_Controls.payload_size = 8;
WR225E.IFL_Controls.payload_value = [];
WR225E.IFL_Controls.payload_mask = [];
WR225E.IFL_Controls.interval = 50;

WR225E.IFL_Controls.fields{1}.name = 'IFL_T_IGBT';
WR225E.IFL_Controls.fields{1}.units = 'C';
WR225E.IFL_Controls.fields{1}.start_bit = 0;
WR225E.IFL_Controls.fields{1}.bit_length = 8;
WR225E.IFL_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Controls.fields{1}.data_type = 'UNSIGNED';
WR225E.IFL_Controls.fields{1}.maximum = 2.160000e+02;
WR225E.IFL_Controls.fields{1}.minimum = 0.000000e+00;
WR225E.IFL_Controls.fields{1}.scale = 1.000000e+00;
WR225E.IFL_Controls.fields{1}.offset = -4.000000e+01;

WR225E.IFL_Controls.fields{2}.name = 'IFL_T_winding';
WR225E.IFL_Controls.fields{2}.units = 'C';
WR225E.IFL_Controls.fields{2}.start_bit = 8;
WR225E.IFL_Controls.fields{2}.bit_length = 8;
WR225E.IFL_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Controls.fields{2}.data_type = 'UNSIGNED';
WR225E.IFL_Controls.fields{2}.maximum = 2.160000e+02;
WR225E.IFL_Controls.fields{2}.minimum = 0.000000e+00;
WR225E.IFL_Controls.fields{2}.scale = 1.000000e+00;
WR225E.IFL_Controls.fields{2}.offset = -4.000000e+01;

WR225E.IFL_Controls.fields{3}.name = 'IFL_I_s_amplitude';
WR225E.IFL_Controls.fields{3}.units = 'A';
WR225E.IFL_Controls.fields{3}.start_bit = 24;
WR225E.IFL_Controls.fields{3}.bit_length = 16;
WR225E.IFL_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Controls.fields{3}.data_type = 'SIGNED';
WR225E.IFL_Controls.fields{3}.maximum = 6.550000e+02;
WR225E.IFL_Controls.fields{3}.minimum = -6.550000e+02;
WR225E.IFL_Controls.fields{3}.scale = 2.000000e-02;
WR225E.IFL_Controls.fields{3}.offset = 0.000000e+00;

WR225E.IFL_Controls.fields{4}.name = 'IFL_P_ac';
WR225E.IFL_Controls.fields{4}.units = 'W';
WR225E.IFL_Controls.fields{4}.start_bit = 40;
WR225E.IFL_Controls.fields{4}.bit_length = 16;
WR225E.IFL_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Controls.fields{4}.data_type = 'SIGNED';
WR225E.IFL_Controls.fields{4}.maximum = 6.400000e+04;
WR225E.IFL_Controls.fields{4}.minimum = -6.400000e+04;
WR225E.IFL_Controls.fields{4}.scale = 2.000000e+00;
WR225E.IFL_Controls.fields{4}.offset = 0.000000e+00;

WR225E.IFL_Controls.fields{5}.name = 'IFL_MotorSpeed';
WR225E.IFL_Controls.fields{5}.units = 'RPM';
WR225E.IFL_Controls.fields{5}.start_bit = 56;
WR225E.IFL_Controls.fields{5}.bit_length = 16;
WR225E.IFL_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Controls.fields{5}.data_type = 'SIGNED';
WR225E.IFL_Controls.fields{5}.maximum = 3.270000e+04;
WR225E.IFL_Controls.fields{5}.minimum = -3.270000e+04;
WR225E.IFL_Controls.fields{5}.scale = 1.000000e+00;
WR225E.IFL_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Debug'
WR225E.IFL_Debug = struct;
WR225E.IFL_Debug.name = 'IFL_Debug';
WR225E.IFL_Debug.description = 'IFL_Debug';
WR225E.IFL_Debug.protocol = 'WR225E';
WR225E.IFL_Debug.id = hex2dec('000001D9');
WR225E.IFL_Debug.idext = 'STANDARD';
WR225E.IFL_Debug.idinherit = 0;
WR225E.IFL_Debug.payload_size = 8;
WR225E.IFL_Debug.payload_value = [];
WR225E.IFL_Debug.payload_mask = [];
WR225E.IFL_Debug.interval = 50;

WR225E.IFL_Debug.fields{1}.name = 'Debug_Value4';
WR225E.IFL_Debug.fields{1}.units = '';
WR225E.IFL_Debug.fields{1}.start_bit = 8;
WR225E.IFL_Debug.fields{1}.bit_length = 16;
WR225E.IFL_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Debug.fields{1}.data_type = 'SIGNED';
WR225E.IFL_Debug.fields{1}.scale = 1.000000e-02;
WR225E.IFL_Debug.fields{1}.offset = 0.000000e+00;

WR225E.IFL_Debug.fields{2}.name = 'Debug_Value3';
WR225E.IFL_Debug.fields{2}.units = '';
WR225E.IFL_Debug.fields{2}.start_bit = 24;
WR225E.IFL_Debug.fields{2}.bit_length = 16;
WR225E.IFL_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Debug.fields{2}.data_type = 'SIGNED';
WR225E.IFL_Debug.fields{2}.scale = 1.000000e-02;
WR225E.IFL_Debug.fields{2}.offset = 0.000000e+00;

WR225E.IFL_Debug.fields{3}.name = 'Debug_Value2';
WR225E.IFL_Debug.fields{3}.units = '';
WR225E.IFL_Debug.fields{3}.start_bit = 40;
WR225E.IFL_Debug.fields{3}.bit_length = 16;
WR225E.IFL_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Debug.fields{3}.data_type = 'SIGNED';
WR225E.IFL_Debug.fields{3}.scale = 1.000000e-02;
WR225E.IFL_Debug.fields{3}.offset = 0.000000e+00;

WR225E.IFL_Debug.fields{4}.name = 'Debug_Value1';
WR225E.IFL_Debug.fields{4}.units = '';
WR225E.IFL_Debug.fields{4}.start_bit = 56;
WR225E.IFL_Debug.fields{4}.bit_length = 16;
WR225E.IFL_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Debug.fields{4}.data_type = 'SIGNED';
WR225E.IFL_Debug.fields{4}.scale = 1.000000e-02;
WR225E.IFL_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_FaultCode'
WR225E.IFL_FaultCode = struct;
WR225E.IFL_FaultCode.name = 'IFL_FaultCode';
WR225E.IFL_FaultCode.description = 'IFL_FaultCode';
WR225E.IFL_FaultCode.protocol = 'WR225E';
WR225E.IFL_FaultCode.id = hex2dec('000000D1');
WR225E.IFL_FaultCode.idext = 'STANDARD';
WR225E.IFL_FaultCode.idinherit = 0;
WR225E.IFL_FaultCode.payload_size = 5;
WR225E.IFL_FaultCode.payload_value = [];
WR225E.IFL_FaultCode.payload_mask = [];
WR225E.IFL_FaultCode.interval = 50;

WR225E.IFL_FaultCode.fields{1}.name = 'IFL_SDCControl';
WR225E.IFL_FaultCode.fields{1}.units = '';
WR225E.IFL_FaultCode.fields{1}.start_bit = 24;
WR225E.IFL_FaultCode.fields{1}.bit_length = 8;
WR225E.IFL_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_FaultCode.fields{1}.data_type = 'UNSIGNED';
WR225E.IFL_FaultCode.fields{1}.scale = 1.000000e+00;
WR225E.IFL_FaultCode.fields{1}.offset = 0.000000e+00;

WR225E.IFL_FaultCode.fields{2}.name = 'IFL_DiagnosticInfo';
WR225E.IFL_FaultCode.fields{2}.units = '';
WR225E.IFL_FaultCode.fields{2}.start_bit = 40;
WR225E.IFL_FaultCode.fields{2}.bit_length = 16;
WR225E.IFL_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_FaultCode.fields{2}.data_type = 'UNSIGNED';
WR225E.IFL_FaultCode.fields{2}.maximum = 6.553600e+04;
WR225E.IFL_FaultCode.fields{2}.minimum = 0.000000e+00;
WR225E.IFL_FaultCode.fields{2}.scale = 1.000000e+00;
WR225E.IFL_FaultCode.fields{2}.offset = 0.000000e+00;

WR225E.IFL_FaultCode.fields{3}.name = 'IFL_FaultCode';
WR225E.IFL_FaultCode.fields{3}.units = '';
WR225E.IFL_FaultCode.fields{3}.start_bit = 56;
WR225E.IFL_FaultCode.fields{3}.bit_length = 16;
WR225E.IFL_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_FaultCode.fields{3}.data_type = 'UNSIGNED';
WR225E.IFL_FaultCode.fields{3}.maximum = 6.553600e+04;
WR225E.IFL_FaultCode.fields{3}.minimum = 0.000000e+00;
WR225E.IFL_FaultCode.fields{3}.scale = 1.000000e+00;
WR225E.IFL_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Measured_Vdq'
WR225E.IFL_Measured_Vdq = struct;
WR225E.IFL_Measured_Vdq.name = 'IFL_Measured_Vdq';
WR225E.IFL_Measured_Vdq.description = 'IFL_Measured_Vdq';
WR225E.IFL_Measured_Vdq.protocol = 'WR225E';
WR225E.IFL_Measured_Vdq.id = hex2dec('000000DB');
WR225E.IFL_Measured_Vdq.idext = 'STANDARD';
WR225E.IFL_Measured_Vdq.idinherit = 0;
WR225E.IFL_Measured_Vdq.payload_size = 8;
WR225E.IFL_Measured_Vdq.payload_value = [];
WR225E.IFL_Measured_Vdq.payload_mask = [];
WR225E.IFL_Measured_Vdq.interval = 50;

WR225E.IFL_Measured_Vdq.fields{1}.name = 'IFL_elecAngle';
WR225E.IFL_Measured_Vdq.fields{1}.units = 'Degrees';
WR225E.IFL_Measured_Vdq.fields{1}.start_bit = 8;
WR225E.IFL_Measured_Vdq.fields{1}.bit_length = 16;
WR225E.IFL_Measured_Vdq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Measured_Vdq.fields{1}.data_type = 'SIGNED';
WR225E.IFL_Measured_Vdq.fields{1}.scale = 1.000000e+00;
WR225E.IFL_Measured_Vdq.fields{1}.offset = 0.000000e+00;

WR225E.IFL_Measured_Vdq.fields{2}.name = 'IFL_mechAngle';
WR225E.IFL_Measured_Vdq.fields{2}.units = 'Degrees';
WR225E.IFL_Measured_Vdq.fields{2}.start_bit = 24;
WR225E.IFL_Measured_Vdq.fields{2}.bit_length = 16;
WR225E.IFL_Measured_Vdq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Measured_Vdq.fields{2}.data_type = 'UNSIGNED';
WR225E.IFL_Measured_Vdq.fields{2}.scale = 1.000000e+00;
WR225E.IFL_Measured_Vdq.fields{2}.offset = 0.000000e+00;

WR225E.IFL_Measured_Vdq.fields{3}.name = 'IFL_meas_Vq';
WR225E.IFL_Measured_Vdq.fields{3}.units = 'V';
WR225E.IFL_Measured_Vdq.fields{3}.start_bit = 40;
WR225E.IFL_Measured_Vdq.fields{3}.bit_length = 16;
WR225E.IFL_Measured_Vdq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Measured_Vdq.fields{3}.data_type = 'SIGNED';
WR225E.IFL_Measured_Vdq.fields{3}.scale = 2.000000e-02;
WR225E.IFL_Measured_Vdq.fields{3}.offset = 0.000000e+00;

WR225E.IFL_Measured_Vdq.fields{4}.name = 'IFL_meas_Vd';
WR225E.IFL_Measured_Vdq.fields{4}.units = 'V';
WR225E.IFL_Measured_Vdq.fields{4}.start_bit = 56;
WR225E.IFL_Measured_Vdq.fields{4}.bit_length = 16;
WR225E.IFL_Measured_Vdq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Measured_Vdq.fields{4}.data_type = 'SIGNED';
WR225E.IFL_Measured_Vdq.fields{4}.scale = 1.000000e+00;
WR225E.IFL_Measured_Vdq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_MotorParams'
WR225E.IFL_MotorParams = struct;
WR225E.IFL_MotorParams.name = 'IFL_MotorParams';
WR225E.IFL_MotorParams.description = 'IFL_MotorParams';
WR225E.IFL_MotorParams.protocol = 'WR225E';
WR225E.IFL_MotorParams.id = hex2dec('000000C5');
WR225E.IFL_MotorParams.idext = 'STANDARD';
WR225E.IFL_MotorParams.idinherit = 0;
WR225E.IFL_MotorParams.payload_size = 8;
WR225E.IFL_MotorParams.payload_value = [];
WR225E.IFL_MotorParams.payload_mask = [];
WR225E.IFL_MotorParams.interval = 50;

WR225E.IFL_MotorParams.fields{1}.name = 'IFL_Vdc';
WR225E.IFL_MotorParams.fields{1}.units = 'V';
WR225E.IFL_MotorParams.fields{1}.start_bit = 8;
WR225E.IFL_MotorParams.fields{1}.bit_length = 16;
WR225E.IFL_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{1}.data_type = 'SIGNED';
WR225E.IFL_MotorParams.fields{1}.maximum = 3.270000e+02;
WR225E.IFL_MotorParams.fields{1}.minimum = -3.270000e+02;
WR225E.IFL_MotorParams.fields{1}.scale = 1.000000e-02;
WR225E.IFL_MotorParams.fields{1}.offset = 0.000000e+00;

WR225E.IFL_MotorParams.fields{2}.name = 'IFL_Torque_est';
WR225E.IFL_MotorParams.fields{2}.units = 'Nm';
WR225E.IFL_MotorParams.fields{2}.start_bit = 24;
WR225E.IFL_MotorParams.fields{2}.bit_length = 16;
WR225E.IFL_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{2}.data_type = 'SIGNED';
WR225E.IFL_MotorParams.fields{2}.scale = 2.000000e-03;
WR225E.IFL_MotorParams.fields{2}.offset = 0.000000e+00;

WR225E.IFL_MotorParams.fields{3}.name = 'IFL_R_winding';
WR225E.IFL_MotorParams.fields{3}.units = 'mOhm';
WR225E.IFL_MotorParams.fields{3}.start_bit = 32;
WR225E.IFL_MotorParams.fields{3}.bit_length = 8;
WR225E.IFL_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{3}.data_type = 'UNSIGNED';
WR225E.IFL_MotorParams.fields{3}.maximum = 5.120000e+01;
WR225E.IFL_MotorParams.fields{3}.minimum = 0.000000e+00;
WR225E.IFL_MotorParams.fields{3}.scale = 2.000000e-01;
WR225E.IFL_MotorParams.fields{3}.offset = 0.000000e+00;

WR225E.IFL_MotorParams.fields{4}.name = 'IFL_K_t';
WR225E.IFL_MotorParams.fields{4}.units = 'Nm/A';
WR225E.IFL_MotorParams.fields{4}.start_bit = 40;
WR225E.IFL_MotorParams.fields{4}.bit_length = 8;
WR225E.IFL_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{4}.data_type = 'UNSIGNED';
WR225E.IFL_MotorParams.fields{4}.maximum = 5.120000e-01;
WR225E.IFL_MotorParams.fields{4}.minimum = 0.000000e+00;
WR225E.IFL_MotorParams.fields{4}.scale = 2.000000e-03;
WR225E.IFL_MotorParams.fields{4}.offset = 0.000000e+00;

WR225E.IFL_MotorParams.fields{5}.name = 'IFL_L_q';
WR225E.IFL_MotorParams.fields{5}.units = 'uH';
WR225E.IFL_MotorParams.fields{5}.start_bit = 48;
WR225E.IFL_MotorParams.fields{5}.bit_length = 8;
WR225E.IFL_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{5}.data_type = 'UNSIGNED';
WR225E.IFL_MotorParams.fields{5}.maximum = 1.280000e+02;
WR225E.IFL_MotorParams.fields{5}.minimum = 0.000000e+00;
WR225E.IFL_MotorParams.fields{5}.scale = 5.000000e-01;
WR225E.IFL_MotorParams.fields{5}.offset = 0.000000e+00;

WR225E.IFL_MotorParams.fields{6}.name = 'IFL_L_d';
WR225E.IFL_MotorParams.fields{6}.units = 'uH';
WR225E.IFL_MotorParams.fields{6}.start_bit = 56;
WR225E.IFL_MotorParams.fields{6}.bit_length = 8;
WR225E.IFL_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_MotorParams.fields{6}.data_type = 'UNSIGNED';
WR225E.IFL_MotorParams.fields{6}.maximum = 1.280000e+02;
WR225E.IFL_MotorParams.fields{6}.minimum = 0.000000e+00;
WR225E.IFL_MotorParams.fields{6}.scale = 5.000000e-01;
WR225E.IFL_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Observer'
WR225E.IFL_Observer = struct;
WR225E.IFL_Observer.name = 'IFL_Observer';
WR225E.IFL_Observer.description = 'IFL_Observer';
WR225E.IFL_Observer.protocol = 'WR225E';
WR225E.IFL_Observer.id = hex2dec('000001D1');
WR225E.IFL_Observer.idext = 'STANDARD';
WR225E.IFL_Observer.idinherit = 0;
WR225E.IFL_Observer.payload_size = 8;
WR225E.IFL_Observer.payload_value = [];
WR225E.IFL_Observer.payload_mask = [];
WR225E.IFL_Observer.interval = 50;

WR225E.IFL_Observer.fields{1}.name = 'IFL_Iq_ref';
WR225E.IFL_Observer.fields{1}.units = 'A';
WR225E.IFL_Observer.fields{1}.start_bit = 40;
WR225E.IFL_Observer.fields{1}.bit_length = 16;
WR225E.IFL_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Observer.fields{1}.data_type = 'SIGNED';
WR225E.IFL_Observer.fields{1}.scale = 2.000000e-02;
WR225E.IFL_Observer.fields{1}.offset = 0.000000e+00;

WR225E.IFL_Observer.fields{2}.name = 'IFL_Id_ref';
WR225E.IFL_Observer.fields{2}.units = 'A';
WR225E.IFL_Observer.fields{2}.start_bit = 56;
WR225E.IFL_Observer.fields{2}.bit_length = 16;
WR225E.IFL_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Observer.fields{2}.data_type = 'SIGNED';
WR225E.IFL_Observer.fields{2}.scale = 2.000000e-02;
WR225E.IFL_Observer.fields{2}.offset = 0.000000e+00;

WR225E.IFL_Observer.fields{3}.name = 'IFL_flux_q';
WR225E.IFL_Observer.fields{3}.units = 'VpHz';
WR225E.IFL_Observer.fields{3}.start_bit = 8;
WR225E.IFL_Observer.fields{3}.bit_length = 16;
WR225E.IFL_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Observer.fields{3}.data_type = 'SIGNED';
WR225E.IFL_Observer.fields{3}.scale = 3.333333e-04;
WR225E.IFL_Observer.fields{3}.offset = 0.000000e+00;

WR225E.IFL_Observer.fields{4}.name = 'IFL_flux_d';
WR225E.IFL_Observer.fields{4}.units = 'VpHz';
WR225E.IFL_Observer.fields{4}.start_bit = 24;
WR225E.IFL_Observer.fields{4}.bit_length = 16;
WR225E.IFL_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Observer.fields{4}.data_type = 'SIGNED';
WR225E.IFL_Observer.fields{4}.scale = 3.333333e-04;
WR225E.IFL_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Offsets'
WR225E.IFL_Offsets = struct;
WR225E.IFL_Offsets.name = 'IFL_Offsets';
WR225E.IFL_Offsets.description = 'IFL_Offsets';
WR225E.IFL_Offsets.protocol = 'WR225E';
WR225E.IFL_Offsets.id = hex2dec('000000C7');
WR225E.IFL_Offsets.idext = 'STANDARD';
WR225E.IFL_Offsets.idinherit = 0;
WR225E.IFL_Offsets.payload_size = 6;
WR225E.IFL_Offsets.payload_value = [];
WR225E.IFL_Offsets.payload_mask = [];
WR225E.IFL_Offsets.interval = 50;

WR225E.IFL_Offsets.fields{1}.name = 'IFL_V_c_offset';
WR225E.IFL_Offsets.fields{1}.units = 'ADC';
WR225E.IFL_Offsets.fields{1}.start_bit = 16;
WR225E.IFL_Offsets.fields{1}.bit_length = 8;
WR225E.IFL_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{1}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{1}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{1}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{1}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{1}.offset = 0.000000e+00;

WR225E.IFL_Offsets.fields{2}.name = 'IFL_V_b_offset';
WR225E.IFL_Offsets.fields{2}.units = 'ADC';
WR225E.IFL_Offsets.fields{2}.start_bit = 24;
WR225E.IFL_Offsets.fields{2}.bit_length = 8;
WR225E.IFL_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{2}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{2}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{2}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{2}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{2}.offset = 0.000000e+00;

WR225E.IFL_Offsets.fields{3}.name = 'IFL_V_a_offset';
WR225E.IFL_Offsets.fields{3}.units = 'ADC';
WR225E.IFL_Offsets.fields{3}.start_bit = 32;
WR225E.IFL_Offsets.fields{3}.bit_length = 8;
WR225E.IFL_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{3}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{3}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{3}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{3}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{3}.offset = 0.000000e+00;

WR225E.IFL_Offsets.fields{4}.name = 'IFL_I_c_offset';
WR225E.IFL_Offsets.fields{4}.units = 'ADC';
WR225E.IFL_Offsets.fields{4}.start_bit = 40;
WR225E.IFL_Offsets.fields{4}.bit_length = 8;
WR225E.IFL_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{4}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{4}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{4}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{4}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{4}.offset = 0.000000e+00;

WR225E.IFL_Offsets.fields{5}.name = 'IFL_I_b_offset';
WR225E.IFL_Offsets.fields{5}.units = 'ADC';
WR225E.IFL_Offsets.fields{5}.start_bit = 48;
WR225E.IFL_Offsets.fields{5}.bit_length = 8;
WR225E.IFL_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{5}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{5}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{5}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{5}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{5}.offset = 0.000000e+00;

WR225E.IFL_Offsets.fields{6}.name = 'IFL_I_a_offset';
WR225E.IFL_Offsets.fields{6}.units = 'ADC';
WR225E.IFL_Offsets.fields{6}.start_bit = 56;
WR225E.IFL_Offsets.fields{6}.bit_length = 8;
WR225E.IFL_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_Offsets.fields{6}.data_type = 'SIGNED';
WR225E.IFL_Offsets.fields{6}.maximum = 1.280000e+02;
WR225E.IFL_Offsets.fields{6}.minimum = -1.270000e+02;
WR225E.IFL_Offsets.fields{6}.scale = 1.000000e+00;
WR225E.IFL_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFL_dq'
WR225E.IFL_dq = struct;
WR225E.IFL_dq.name = 'IFL_dq';
WR225E.IFL_dq.description = 'IFL_dq';
WR225E.IFL_dq.protocol = 'WR225E';
WR225E.IFL_dq.id = hex2dec('000000C6');
WR225E.IFL_dq.idext = 'STANDARD';
WR225E.IFL_dq.idinherit = 0;
WR225E.IFL_dq.payload_size = 8;
WR225E.IFL_dq.payload_value = [];
WR225E.IFL_dq.payload_mask = [];
WR225E.IFL_dq.interval = 50;

WR225E.IFL_dq.fields{1}.name = 'IFL_I_q';
WR225E.IFL_dq.fields{1}.units = 'A';
WR225E.IFL_dq.fields{1}.start_bit = 8;
WR225E.IFL_dq.fields{1}.bit_length = 16;
WR225E.IFL_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_dq.fields{1}.data_type = 'SIGNED';
WR225E.IFL_dq.fields{1}.maximum = 6.540000e+02;
WR225E.IFL_dq.fields{1}.minimum = -6.540000e+02;
WR225E.IFL_dq.fields{1}.scale = 2.000000e-02;
WR225E.IFL_dq.fields{1}.offset = 0.000000e+00;

WR225E.IFL_dq.fields{2}.name = 'IFL_I_d';
WR225E.IFL_dq.fields{2}.units = 'A';
WR225E.IFL_dq.fields{2}.start_bit = 24;
WR225E.IFL_dq.fields{2}.bit_length = 16;
WR225E.IFL_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_dq.fields{2}.data_type = 'SIGNED';
WR225E.IFL_dq.fields{2}.maximum = 6.540000e+02;
WR225E.IFL_dq.fields{2}.minimum = -6.540000e+02;
WR225E.IFL_dq.fields{2}.scale = 2.000000e-02;
WR225E.IFL_dq.fields{2}.offset = 0.000000e+00;

WR225E.IFL_dq.fields{3}.name = 'IFL_V_q';
WR225E.IFL_dq.fields{3}.units = 'V';
WR225E.IFL_dq.fields{3}.start_bit = 40;
WR225E.IFL_dq.fields{3}.bit_length = 16;
WR225E.IFL_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_dq.fields{3}.data_type = 'SIGNED';
WR225E.IFL_dq.fields{3}.maximum = 6.540000e+02;
WR225E.IFL_dq.fields{3}.minimum = -6.540000e+02;
WR225E.IFL_dq.fields{3}.scale = 2.000000e-02;
WR225E.IFL_dq.fields{3}.offset = 0.000000e+00;

WR225E.IFL_dq.fields{4}.name = 'IFL_V_d';
WR225E.IFL_dq.fields{4}.units = 'V';
WR225E.IFL_dq.fields{4}.start_bit = 56;
WR225E.IFL_dq.fields{4}.bit_length = 16;
WR225E.IFL_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFL_dq.fields{4}.data_type = 'SIGNED';
WR225E.IFL_dq.fields{4}.maximum = 6.540000e+02;
WR225E.IFL_dq.fields{4}.minimum = -6.540000e+02;
WR225E.IFL_dq.fields{4}.scale = 2.000000e-02;
WR225E.IFL_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFLrx_Bootloader'
WR225E.IFLrx_Bootloader = struct;
WR225E.IFLrx_Bootloader.name = 'IFLrx_Bootloader';
WR225E.IFLrx_Bootloader.description = 'IFLrx_Bootloader';
WR225E.IFLrx_Bootloader.protocol = 'WR225E';
WR225E.IFLrx_Bootloader.id = hex2dec('000000B2');
WR225E.IFLrx_Bootloader.idext = 'STANDARD';
WR225E.IFLrx_Bootloader.idinherit = 0;
WR225E.IFLrx_Bootloader.payload_size = 8;
WR225E.IFLrx_Bootloader.payload_value = [];
WR225E.IFLrx_Bootloader.payload_mask = [];
WR225E.IFLrx_Bootloader.interval = 50;

%===================================================================
case 'IFR_Controls'
WR225E.IFR_Controls = struct;
WR225E.IFR_Controls.name = 'IFR_Controls';
WR225E.IFR_Controls.description = 'IFR_Controls';
WR225E.IFR_Controls.protocol = 'WR225E';
WR225E.IFR_Controls.id = hex2dec('000000C0');
WR225E.IFR_Controls.idext = 'STANDARD';
WR225E.IFR_Controls.idinherit = 0;
WR225E.IFR_Controls.payload_size = 8;
WR225E.IFR_Controls.payload_value = [];
WR225E.IFR_Controls.payload_mask = [];
WR225E.IFR_Controls.interval = 50;

WR225E.IFR_Controls.fields{1}.name = 'IFR_T_IGBT';
WR225E.IFR_Controls.fields{1}.units = 'C';
WR225E.IFR_Controls.fields{1}.start_bit = 0;
WR225E.IFR_Controls.fields{1}.bit_length = 8;
WR225E.IFR_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Controls.fields{1}.data_type = 'UNSIGNED';
WR225E.IFR_Controls.fields{1}.maximum = 2.160000e+02;
WR225E.IFR_Controls.fields{1}.minimum = 0.000000e+00;
WR225E.IFR_Controls.fields{1}.scale = 1.000000e+00;
WR225E.IFR_Controls.fields{1}.offset = -4.000000e+01;

WR225E.IFR_Controls.fields{2}.name = 'IFR_T_winding';
WR225E.IFR_Controls.fields{2}.units = 'C';
WR225E.IFR_Controls.fields{2}.start_bit = 8;
WR225E.IFR_Controls.fields{2}.bit_length = 8;
WR225E.IFR_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Controls.fields{2}.data_type = 'UNSIGNED';
WR225E.IFR_Controls.fields{2}.maximum = 2.160000e+02;
WR225E.IFR_Controls.fields{2}.minimum = 0.000000e+00;
WR225E.IFR_Controls.fields{2}.scale = 1.000000e+00;
WR225E.IFR_Controls.fields{2}.offset = -4.000000e+01;

WR225E.IFR_Controls.fields{3}.name = 'IFR_I_s_amplitude';
WR225E.IFR_Controls.fields{3}.units = 'A';
WR225E.IFR_Controls.fields{3}.start_bit = 24;
WR225E.IFR_Controls.fields{3}.bit_length = 16;
WR225E.IFR_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Controls.fields{3}.data_type = 'SIGNED';
WR225E.IFR_Controls.fields{3}.maximum = 6.550000e+02;
WR225E.IFR_Controls.fields{3}.minimum = -6.550000e+02;
WR225E.IFR_Controls.fields{3}.scale = 2.000000e-02;
WR225E.IFR_Controls.fields{3}.offset = 0.000000e+00;

WR225E.IFR_Controls.fields{4}.name = 'IFR_P_ac';
WR225E.IFR_Controls.fields{4}.units = 'W';
WR225E.IFR_Controls.fields{4}.start_bit = 40;
WR225E.IFR_Controls.fields{4}.bit_length = 16;
WR225E.IFR_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Controls.fields{4}.data_type = 'SIGNED';
WR225E.IFR_Controls.fields{4}.maximum = 6.400000e+04;
WR225E.IFR_Controls.fields{4}.minimum = -6.400000e+04;
WR225E.IFR_Controls.fields{4}.scale = 2.000000e+00;
WR225E.IFR_Controls.fields{4}.offset = 0.000000e+00;

WR225E.IFR_Controls.fields{5}.name = 'IFR_MotorSpeed';
WR225E.IFR_Controls.fields{5}.units = 'RPM';
WR225E.IFR_Controls.fields{5}.start_bit = 56;
WR225E.IFR_Controls.fields{5}.bit_length = 16;
WR225E.IFR_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Controls.fields{5}.data_type = 'SIGNED';
WR225E.IFR_Controls.fields{5}.maximum = 3.270000e+04;
WR225E.IFR_Controls.fields{5}.minimum = -3.270000e+04;
WR225E.IFR_Controls.fields{5}.scale = 1.000000e+00;
WR225E.IFR_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Debug'
WR225E.IFR_Debug = struct;
WR225E.IFR_Debug.name = 'IFR_Debug';
WR225E.IFR_Debug.description = 'IFR_Debug';
WR225E.IFR_Debug.protocol = 'WR225E';
WR225E.IFR_Debug.id = hex2dec('000001D8');
WR225E.IFR_Debug.idext = 'STANDARD';
WR225E.IFR_Debug.idinherit = 0;
WR225E.IFR_Debug.payload_size = 8;
WR225E.IFR_Debug.payload_value = [];
WR225E.IFR_Debug.payload_mask = [];
WR225E.IFR_Debug.interval = 50;

WR225E.IFR_Debug.fields{1}.name = 'Debug_Value4';
WR225E.IFR_Debug.fields{1}.units = '';
WR225E.IFR_Debug.fields{1}.start_bit = 8;
WR225E.IFR_Debug.fields{1}.bit_length = 16;
WR225E.IFR_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Debug.fields{1}.data_type = 'SIGNED';
WR225E.IFR_Debug.fields{1}.scale = 1.000000e-02;
WR225E.IFR_Debug.fields{1}.offset = 0.000000e+00;

WR225E.IFR_Debug.fields{2}.name = 'Debug_Value3';
WR225E.IFR_Debug.fields{2}.units = '';
WR225E.IFR_Debug.fields{2}.start_bit = 24;
WR225E.IFR_Debug.fields{2}.bit_length = 16;
WR225E.IFR_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Debug.fields{2}.data_type = 'SIGNED';
WR225E.IFR_Debug.fields{2}.scale = 1.000000e-02;
WR225E.IFR_Debug.fields{2}.offset = 0.000000e+00;

WR225E.IFR_Debug.fields{3}.name = 'Debug_Value2';
WR225E.IFR_Debug.fields{3}.units = '';
WR225E.IFR_Debug.fields{3}.start_bit = 40;
WR225E.IFR_Debug.fields{3}.bit_length = 16;
WR225E.IFR_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Debug.fields{3}.data_type = 'SIGNED';
WR225E.IFR_Debug.fields{3}.scale = 1.000000e-02;
WR225E.IFR_Debug.fields{3}.offset = 0.000000e+00;

WR225E.IFR_Debug.fields{4}.name = 'Debug_Value1';
WR225E.IFR_Debug.fields{4}.units = '';
WR225E.IFR_Debug.fields{4}.start_bit = 56;
WR225E.IFR_Debug.fields{4}.bit_length = 16;
WR225E.IFR_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Debug.fields{4}.data_type = 'SIGNED';
WR225E.IFR_Debug.fields{4}.scale = 1.000000e-02;
WR225E.IFR_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_FaultCode'
WR225E.IFR_FaultCode = struct;
WR225E.IFR_FaultCode.name = 'IFR_FaultCode';
WR225E.IFR_FaultCode.description = 'IFR_FaultCode';
WR225E.IFR_FaultCode.protocol = 'WR225E';
WR225E.IFR_FaultCode.id = hex2dec('000000D0');
WR225E.IFR_FaultCode.idext = 'STANDARD';
WR225E.IFR_FaultCode.idinherit = 0;
WR225E.IFR_FaultCode.payload_size = 5;
WR225E.IFR_FaultCode.payload_value = [];
WR225E.IFR_FaultCode.payload_mask = [];
WR225E.IFR_FaultCode.interval = 50;

WR225E.IFR_FaultCode.fields{1}.name = 'IFR_SDCControl';
WR225E.IFR_FaultCode.fields{1}.units = '';
WR225E.IFR_FaultCode.fields{1}.start_bit = 24;
WR225E.IFR_FaultCode.fields{1}.bit_length = 8;
WR225E.IFR_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_FaultCode.fields{1}.data_type = 'UNSIGNED';
WR225E.IFR_FaultCode.fields{1}.scale = 1.000000e+00;
WR225E.IFR_FaultCode.fields{1}.offset = 0.000000e+00;

WR225E.IFR_FaultCode.fields{2}.name = 'IFR_DiagnosticInfo';
WR225E.IFR_FaultCode.fields{2}.units = '';
WR225E.IFR_FaultCode.fields{2}.start_bit = 40;
WR225E.IFR_FaultCode.fields{2}.bit_length = 16;
WR225E.IFR_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_FaultCode.fields{2}.data_type = 'UNSIGNED';
WR225E.IFR_FaultCode.fields{2}.maximum = 6.553600e+04;
WR225E.IFR_FaultCode.fields{2}.minimum = 0.000000e+00;
WR225E.IFR_FaultCode.fields{2}.scale = 1.000000e+00;
WR225E.IFR_FaultCode.fields{2}.offset = 0.000000e+00;

WR225E.IFR_FaultCode.fields{3}.name = 'IFR_FaultCode';
WR225E.IFR_FaultCode.fields{3}.units = '';
WR225E.IFR_FaultCode.fields{3}.start_bit = 56;
WR225E.IFR_FaultCode.fields{3}.bit_length = 16;
WR225E.IFR_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_FaultCode.fields{3}.data_type = 'UNSIGNED';
WR225E.IFR_FaultCode.fields{3}.maximum = 6.553600e+04;
WR225E.IFR_FaultCode.fields{3}.minimum = 0.000000e+00;
WR225E.IFR_FaultCode.fields{3}.scale = 1.000000e+00;
WR225E.IFR_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Measured_Vdq'
WR225E.IFR_Measured_Vdq = struct;
WR225E.IFR_Measured_Vdq.name = 'IFR_Measured_Vdq';
WR225E.IFR_Measured_Vdq.description = 'IFR_Measured_Vdq';
WR225E.IFR_Measured_Vdq.protocol = 'WR225E';
WR225E.IFR_Measured_Vdq.id = hex2dec('000000DA');
WR225E.IFR_Measured_Vdq.idext = 'STANDARD';
WR225E.IFR_Measured_Vdq.idinherit = 0;
WR225E.IFR_Measured_Vdq.payload_size = 8;
WR225E.IFR_Measured_Vdq.payload_value = [];
WR225E.IFR_Measured_Vdq.payload_mask = [];
WR225E.IFR_Measured_Vdq.interval = 50;

WR225E.IFR_Measured_Vdq.fields{1}.name = 'IFR_elecAngle';
WR225E.IFR_Measured_Vdq.fields{1}.units = 'Degrees';
WR225E.IFR_Measured_Vdq.fields{1}.start_bit = 8;
WR225E.IFR_Measured_Vdq.fields{1}.bit_length = 16;
WR225E.IFR_Measured_Vdq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Measured_Vdq.fields{1}.data_type = 'SIGNED';
WR225E.IFR_Measured_Vdq.fields{1}.scale = 1.000000e+00;
WR225E.IFR_Measured_Vdq.fields{1}.offset = 0.000000e+00;

WR225E.IFR_Measured_Vdq.fields{2}.name = 'IFR_mechAngle';
WR225E.IFR_Measured_Vdq.fields{2}.units = 'Degrees';
WR225E.IFR_Measured_Vdq.fields{2}.start_bit = 24;
WR225E.IFR_Measured_Vdq.fields{2}.bit_length = 16;
WR225E.IFR_Measured_Vdq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Measured_Vdq.fields{2}.data_type = 'UNSIGNED';
WR225E.IFR_Measured_Vdq.fields{2}.scale = 1.000000e+00;
WR225E.IFR_Measured_Vdq.fields{2}.offset = 0.000000e+00;

WR225E.IFR_Measured_Vdq.fields{3}.name = 'IFR_meas_Vd';
WR225E.IFR_Measured_Vdq.fields{3}.units = 'V';
WR225E.IFR_Measured_Vdq.fields{3}.start_bit = 56;
WR225E.IFR_Measured_Vdq.fields{3}.bit_length = 16;
WR225E.IFR_Measured_Vdq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Measured_Vdq.fields{3}.data_type = 'SIGNED';
WR225E.IFR_Measured_Vdq.fields{3}.scale = 1.000000e+00;
WR225E.IFR_Measured_Vdq.fields{3}.offset = 0.000000e+00;

WR225E.IFR_Measured_Vdq.fields{4}.name = 'IFR_meas_Vq';
WR225E.IFR_Measured_Vdq.fields{4}.units = 'V';
WR225E.IFR_Measured_Vdq.fields{4}.start_bit = 40;
WR225E.IFR_Measured_Vdq.fields{4}.bit_length = 16;
WR225E.IFR_Measured_Vdq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Measured_Vdq.fields{4}.data_type = 'SIGNED';
WR225E.IFR_Measured_Vdq.fields{4}.scale = 2.000000e-02;
WR225E.IFR_Measured_Vdq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_MotorParams'
WR225E.IFR_MotorParams = struct;
WR225E.IFR_MotorParams.name = 'IFR_MotorParams';
WR225E.IFR_MotorParams.description = 'IFR_MotorParams';
WR225E.IFR_MotorParams.protocol = 'WR225E';
WR225E.IFR_MotorParams.id = hex2dec('000000C1');
WR225E.IFR_MotorParams.idext = 'STANDARD';
WR225E.IFR_MotorParams.idinherit = 0;
WR225E.IFR_MotorParams.payload_size = 8;
WR225E.IFR_MotorParams.payload_value = [];
WR225E.IFR_MotorParams.payload_mask = [];
WR225E.IFR_MotorParams.interval = 50;

WR225E.IFR_MotorParams.fields{1}.name = 'IFR_Vdc';
WR225E.IFR_MotorParams.fields{1}.units = 'V';
WR225E.IFR_MotorParams.fields{1}.start_bit = 8;
WR225E.IFR_MotorParams.fields{1}.bit_length = 16;
WR225E.IFR_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{1}.data_type = 'SIGNED';
WR225E.IFR_MotorParams.fields{1}.maximum = 3.270000e+02;
WR225E.IFR_MotorParams.fields{1}.minimum = -3.270000e+02;
WR225E.IFR_MotorParams.fields{1}.scale = 1.000000e-02;
WR225E.IFR_MotorParams.fields{1}.offset = 0.000000e+00;

WR225E.IFR_MotorParams.fields{2}.name = 'IFR_Torque_est';
WR225E.IFR_MotorParams.fields{2}.units = 'Nm';
WR225E.IFR_MotorParams.fields{2}.start_bit = 24;
WR225E.IFR_MotorParams.fields{2}.bit_length = 16;
WR225E.IFR_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{2}.data_type = 'SIGNED';
WR225E.IFR_MotorParams.fields{2}.scale = 2.000000e-03;
WR225E.IFR_MotorParams.fields{2}.offset = 0.000000e+00;

WR225E.IFR_MotorParams.fields{3}.name = 'IFR_R_winding';
WR225E.IFR_MotorParams.fields{3}.units = 'mOhm';
WR225E.IFR_MotorParams.fields{3}.start_bit = 32;
WR225E.IFR_MotorParams.fields{3}.bit_length = 8;
WR225E.IFR_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{3}.data_type = 'UNSIGNED';
WR225E.IFR_MotorParams.fields{3}.maximum = 5.120000e+01;
WR225E.IFR_MotorParams.fields{3}.minimum = 0.000000e+00;
WR225E.IFR_MotorParams.fields{3}.scale = 2.000000e-01;
WR225E.IFR_MotorParams.fields{3}.offset = 0.000000e+00;

WR225E.IFR_MotorParams.fields{4}.name = 'IFR_K_t';
WR225E.IFR_MotorParams.fields{4}.units = 'Nm/A';
WR225E.IFR_MotorParams.fields{4}.start_bit = 40;
WR225E.IFR_MotorParams.fields{4}.bit_length = 8;
WR225E.IFR_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{4}.data_type = 'UNSIGNED';
WR225E.IFR_MotorParams.fields{4}.maximum = 5.120000e-01;
WR225E.IFR_MotorParams.fields{4}.minimum = 0.000000e+00;
WR225E.IFR_MotorParams.fields{4}.scale = 2.000000e-03;
WR225E.IFR_MotorParams.fields{4}.offset = 0.000000e+00;

WR225E.IFR_MotorParams.fields{5}.name = 'IFR_L_q';
WR225E.IFR_MotorParams.fields{5}.units = 'uH';
WR225E.IFR_MotorParams.fields{5}.start_bit = 48;
WR225E.IFR_MotorParams.fields{5}.bit_length = 8;
WR225E.IFR_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{5}.data_type = 'UNSIGNED';
WR225E.IFR_MotorParams.fields{5}.maximum = 1.280000e+02;
WR225E.IFR_MotorParams.fields{5}.minimum = 0.000000e+00;
WR225E.IFR_MotorParams.fields{5}.scale = 5.000000e-01;
WR225E.IFR_MotorParams.fields{5}.offset = 0.000000e+00;

WR225E.IFR_MotorParams.fields{6}.name = 'IFR_L_d';
WR225E.IFR_MotorParams.fields{6}.units = 'uH';
WR225E.IFR_MotorParams.fields{6}.start_bit = 56;
WR225E.IFR_MotorParams.fields{6}.bit_length = 8;
WR225E.IFR_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_MotorParams.fields{6}.data_type = 'UNSIGNED';
WR225E.IFR_MotorParams.fields{6}.maximum = 1.280000e+02;
WR225E.IFR_MotorParams.fields{6}.minimum = 0.000000e+00;
WR225E.IFR_MotorParams.fields{6}.scale = 5.000000e-01;
WR225E.IFR_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Observer'
WR225E.IFR_Observer = struct;
WR225E.IFR_Observer.name = 'IFR_Observer';
WR225E.IFR_Observer.description = 'IFR_Observer';
WR225E.IFR_Observer.protocol = 'WR225E';
WR225E.IFR_Observer.id = hex2dec('000001D0');
WR225E.IFR_Observer.idext = 'STANDARD';
WR225E.IFR_Observer.idinherit = 0;
WR225E.IFR_Observer.payload_size = 8;
WR225E.IFR_Observer.payload_value = [];
WR225E.IFR_Observer.payload_mask = [];
WR225E.IFR_Observer.interval = 50;

WR225E.IFR_Observer.fields{1}.name = 'IFR_Iq_ref';
WR225E.IFR_Observer.fields{1}.units = 'A';
WR225E.IFR_Observer.fields{1}.start_bit = 40;
WR225E.IFR_Observer.fields{1}.bit_length = 16;
WR225E.IFR_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Observer.fields{1}.data_type = 'SIGNED';
WR225E.IFR_Observer.fields{1}.scale = 2.000000e-02;
WR225E.IFR_Observer.fields{1}.offset = 0.000000e+00;

WR225E.IFR_Observer.fields{2}.name = 'IFR_Id_ref';
WR225E.IFR_Observer.fields{2}.units = 'A';
WR225E.IFR_Observer.fields{2}.start_bit = 56;
WR225E.IFR_Observer.fields{2}.bit_length = 16;
WR225E.IFR_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Observer.fields{2}.data_type = 'SIGNED';
WR225E.IFR_Observer.fields{2}.scale = 2.000000e-02;
WR225E.IFR_Observer.fields{2}.offset = 0.000000e+00;

WR225E.IFR_Observer.fields{3}.name = 'IFR_flux_q';
WR225E.IFR_Observer.fields{3}.units = 'VpHz';
WR225E.IFR_Observer.fields{3}.start_bit = 8;
WR225E.IFR_Observer.fields{3}.bit_length = 16;
WR225E.IFR_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Observer.fields{3}.data_type = 'SIGNED';
WR225E.IFR_Observer.fields{3}.scale = 3.333333e-04;
WR225E.IFR_Observer.fields{3}.offset = 0.000000e+00;

WR225E.IFR_Observer.fields{4}.name = 'IFR_flux_d';
WR225E.IFR_Observer.fields{4}.units = 'VpHz';
WR225E.IFR_Observer.fields{4}.start_bit = 24;
WR225E.IFR_Observer.fields{4}.bit_length = 16;
WR225E.IFR_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Observer.fields{4}.data_type = 'SIGNED';
WR225E.IFR_Observer.fields{4}.scale = 3.333333e-04;
WR225E.IFR_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Offsets'
WR225E.IFR_Offsets = struct;
WR225E.IFR_Offsets.name = 'IFR_Offsets';
WR225E.IFR_Offsets.description = 'IFR_Offsets';
WR225E.IFR_Offsets.protocol = 'WR225E';
WR225E.IFR_Offsets.id = hex2dec('000000C3');
WR225E.IFR_Offsets.idext = 'STANDARD';
WR225E.IFR_Offsets.idinherit = 0;
WR225E.IFR_Offsets.payload_size = 6;
WR225E.IFR_Offsets.payload_value = [];
WR225E.IFR_Offsets.payload_mask = [];
WR225E.IFR_Offsets.interval = 50;

WR225E.IFR_Offsets.fields{1}.name = 'IFR_V_c_offset';
WR225E.IFR_Offsets.fields{1}.units = 'ADC';
WR225E.IFR_Offsets.fields{1}.start_bit = 16;
WR225E.IFR_Offsets.fields{1}.bit_length = 8;
WR225E.IFR_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{1}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{1}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{1}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{1}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{1}.offset = 0.000000e+00;

WR225E.IFR_Offsets.fields{2}.name = 'IFR_V_b_offset';
WR225E.IFR_Offsets.fields{2}.units = 'ADC';
WR225E.IFR_Offsets.fields{2}.start_bit = 24;
WR225E.IFR_Offsets.fields{2}.bit_length = 8;
WR225E.IFR_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{2}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{2}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{2}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{2}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{2}.offset = 0.000000e+00;

WR225E.IFR_Offsets.fields{3}.name = 'IFR_V_a_offset';
WR225E.IFR_Offsets.fields{3}.units = 'ADC';
WR225E.IFR_Offsets.fields{3}.start_bit = 32;
WR225E.IFR_Offsets.fields{3}.bit_length = 8;
WR225E.IFR_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{3}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{3}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{3}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{3}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{3}.offset = 0.000000e+00;

WR225E.IFR_Offsets.fields{4}.name = 'IFR_I_c_offset';
WR225E.IFR_Offsets.fields{4}.units = 'ADC';
WR225E.IFR_Offsets.fields{4}.start_bit = 40;
WR225E.IFR_Offsets.fields{4}.bit_length = 8;
WR225E.IFR_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{4}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{4}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{4}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{4}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{4}.offset = 0.000000e+00;

WR225E.IFR_Offsets.fields{5}.name = 'IFR_I_b_offset';
WR225E.IFR_Offsets.fields{5}.units = 'ADC';
WR225E.IFR_Offsets.fields{5}.start_bit = 48;
WR225E.IFR_Offsets.fields{5}.bit_length = 8;
WR225E.IFR_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{5}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{5}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{5}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{5}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{5}.offset = 0.000000e+00;

WR225E.IFR_Offsets.fields{6}.name = 'IFR_I_a_offset';
WR225E.IFR_Offsets.fields{6}.units = 'ADC';
WR225E.IFR_Offsets.fields{6}.start_bit = 56;
WR225E.IFR_Offsets.fields{6}.bit_length = 8;
WR225E.IFR_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_Offsets.fields{6}.data_type = 'SIGNED';
WR225E.IFR_Offsets.fields{6}.maximum = 1.280000e+02;
WR225E.IFR_Offsets.fields{6}.minimum = -1.270000e+02;
WR225E.IFR_Offsets.fields{6}.scale = 1.000000e+00;
WR225E.IFR_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFR_dq'
WR225E.IFR_dq = struct;
WR225E.IFR_dq.name = 'IFR_dq';
WR225E.IFR_dq.description = 'IFR_dq';
WR225E.IFR_dq.protocol = 'WR225E';
WR225E.IFR_dq.id = hex2dec('000000C2');
WR225E.IFR_dq.idext = 'STANDARD';
WR225E.IFR_dq.idinherit = 0;
WR225E.IFR_dq.payload_size = 8;
WR225E.IFR_dq.payload_value = [];
WR225E.IFR_dq.payload_mask = [];
WR225E.IFR_dq.interval = 50;

WR225E.IFR_dq.fields{1}.name = 'IFR_I_q';
WR225E.IFR_dq.fields{1}.units = 'A';
WR225E.IFR_dq.fields{1}.start_bit = 8;
WR225E.IFR_dq.fields{1}.bit_length = 16;
WR225E.IFR_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_dq.fields{1}.data_type = 'SIGNED';
WR225E.IFR_dq.fields{1}.maximum = 6.540000e+02;
WR225E.IFR_dq.fields{1}.minimum = -6.540000e+02;
WR225E.IFR_dq.fields{1}.scale = 2.000000e-02;
WR225E.IFR_dq.fields{1}.offset = 0.000000e+00;

WR225E.IFR_dq.fields{2}.name = 'IFR_I_d';
WR225E.IFR_dq.fields{2}.units = 'A';
WR225E.IFR_dq.fields{2}.start_bit = 24;
WR225E.IFR_dq.fields{2}.bit_length = 16;
WR225E.IFR_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_dq.fields{2}.data_type = 'SIGNED';
WR225E.IFR_dq.fields{2}.maximum = 6.540000e+02;
WR225E.IFR_dq.fields{2}.minimum = -6.540000e+02;
WR225E.IFR_dq.fields{2}.scale = 2.000000e-02;
WR225E.IFR_dq.fields{2}.offset = 0.000000e+00;

WR225E.IFR_dq.fields{3}.name = 'IFR_V_q';
WR225E.IFR_dq.fields{3}.units = 'V';
WR225E.IFR_dq.fields{3}.start_bit = 40;
WR225E.IFR_dq.fields{3}.bit_length = 16;
WR225E.IFR_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_dq.fields{3}.data_type = 'SIGNED';
WR225E.IFR_dq.fields{3}.maximum = 6.540000e+02;
WR225E.IFR_dq.fields{3}.minimum = -6.540000e+02;
WR225E.IFR_dq.fields{3}.scale = 2.000000e-02;
WR225E.IFR_dq.fields{3}.offset = 0.000000e+00;

WR225E.IFR_dq.fields{4}.name = 'IFR_V_d';
WR225E.IFR_dq.fields{4}.units = 'V';
WR225E.IFR_dq.fields{4}.start_bit = 56;
WR225E.IFR_dq.fields{4}.bit_length = 16;
WR225E.IFR_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IFR_dq.fields{4}.data_type = 'SIGNED';
WR225E.IFR_dq.fields{4}.maximum = 6.540000e+02;
WR225E.IFR_dq.fields{4}.minimum = -6.540000e+02;
WR225E.IFR_dq.fields{4}.scale = 2.000000e-02;
WR225E.IFR_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFRrx_Bootloader'
WR225E.IFRrx_Bootloader = struct;
WR225E.IFRrx_Bootloader.name = 'IFRrx_Bootloader';
WR225E.IFRrx_Bootloader.description = 'IFRrx_Bootloader';
WR225E.IFRrx_Bootloader.protocol = 'WR225E';
WR225E.IFRrx_Bootloader.id = hex2dec('000000B1');
WR225E.IFRrx_Bootloader.idext = 'STANDARD';
WR225E.IFRrx_Bootloader.idinherit = 0;
WR225E.IFRrx_Bootloader.payload_size = 8;
WR225E.IFRrx_Bootloader.payload_value = [];
WR225E.IFRrx_Bootloader.payload_mask = [];
WR225E.IFRrx_Bootloader.interval = 50;

%===================================================================
case 'INVrx_Bootloader'
WR225E.INVrx_Bootloader = struct;
WR225E.INVrx_Bootloader.name = 'INVrx_Bootloader';
WR225E.INVrx_Bootloader.description = 'INVrx_Bootloader';
WR225E.INVrx_Bootloader.protocol = 'WR225E';
WR225E.INVrx_Bootloader.id = hex2dec('000000B0');
WR225E.INVrx_Bootloader.idext = 'STANDARD';
WR225E.INVrx_Bootloader.idinherit = 0;
WR225E.INVrx_Bootloader.payload_size = 8;
WR225E.INVrx_Bootloader.payload_value = [];
WR225E.INVrx_Bootloader.payload_mask = [];
WR225E.INVrx_Bootloader.interval = 50;

%===================================================================
case 'IRL_Controls'
WR225E.IRL_Controls = struct;
WR225E.IRL_Controls.name = 'IRL_Controls';
WR225E.IRL_Controls.description = 'IRL_Controls';
WR225E.IRL_Controls.protocol = 'WR225E';
WR225E.IRL_Controls.id = hex2dec('000000C8');
WR225E.IRL_Controls.idext = 'STANDARD';
WR225E.IRL_Controls.idinherit = 0;
WR225E.IRL_Controls.payload_size = 8;
WR225E.IRL_Controls.payload_value = [];
WR225E.IRL_Controls.payload_mask = [];
WR225E.IRL_Controls.interval = 50;

WR225E.IRL_Controls.fields{1}.name = 'IRL_T_IGBT';
WR225E.IRL_Controls.fields{1}.units = 'C';
WR225E.IRL_Controls.fields{1}.start_bit = 0;
WR225E.IRL_Controls.fields{1}.bit_length = 8;
WR225E.IRL_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Controls.fields{1}.data_type = 'UNSIGNED';
WR225E.IRL_Controls.fields{1}.maximum = 2.160000e+02;
WR225E.IRL_Controls.fields{1}.minimum = 0.000000e+00;
WR225E.IRL_Controls.fields{1}.scale = 1.000000e+00;
WR225E.IRL_Controls.fields{1}.offset = -4.000000e+01;

WR225E.IRL_Controls.fields{2}.name = 'IRL_T_winding';
WR225E.IRL_Controls.fields{2}.units = 'C';
WR225E.IRL_Controls.fields{2}.start_bit = 8;
WR225E.IRL_Controls.fields{2}.bit_length = 8;
WR225E.IRL_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Controls.fields{2}.data_type = 'UNSIGNED';
WR225E.IRL_Controls.fields{2}.maximum = 2.160000e+02;
WR225E.IRL_Controls.fields{2}.minimum = 0.000000e+00;
WR225E.IRL_Controls.fields{2}.scale = 1.000000e+00;
WR225E.IRL_Controls.fields{2}.offset = -4.000000e+01;

WR225E.IRL_Controls.fields{3}.name = 'IRL_I_s_amplitude';
WR225E.IRL_Controls.fields{3}.units = 'A';
WR225E.IRL_Controls.fields{3}.start_bit = 24;
WR225E.IRL_Controls.fields{3}.bit_length = 16;
WR225E.IRL_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Controls.fields{3}.data_type = 'SIGNED';
WR225E.IRL_Controls.fields{3}.maximum = 6.550000e+02;
WR225E.IRL_Controls.fields{3}.minimum = -6.550000e+02;
WR225E.IRL_Controls.fields{3}.scale = 2.000000e-02;
WR225E.IRL_Controls.fields{3}.offset = 0.000000e+00;

WR225E.IRL_Controls.fields{4}.name = 'IRL_P_ac';
WR225E.IRL_Controls.fields{4}.units = 'W';
WR225E.IRL_Controls.fields{4}.start_bit = 40;
WR225E.IRL_Controls.fields{4}.bit_length = 16;
WR225E.IRL_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Controls.fields{4}.data_type = 'SIGNED';
WR225E.IRL_Controls.fields{4}.maximum = 6.400000e+04;
WR225E.IRL_Controls.fields{4}.minimum = -6.400000e+04;
WR225E.IRL_Controls.fields{4}.scale = 2.000000e+00;
WR225E.IRL_Controls.fields{4}.offset = 0.000000e+00;

WR225E.IRL_Controls.fields{5}.name = 'IRL_MotorSpeed';
WR225E.IRL_Controls.fields{5}.units = 'RPM';
WR225E.IRL_Controls.fields{5}.start_bit = 56;
WR225E.IRL_Controls.fields{5}.bit_length = 16;
WR225E.IRL_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Controls.fields{5}.data_type = 'SIGNED';
WR225E.IRL_Controls.fields{5}.maximum = 3.270000e+04;
WR225E.IRL_Controls.fields{5}.minimum = -3.270000e+04;
WR225E.IRL_Controls.fields{5}.scale = 1.000000e+00;
WR225E.IRL_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Debug'
WR225E.IRL_Debug = struct;
WR225E.IRL_Debug.name = 'IRL_Debug';
WR225E.IRL_Debug.description = 'IRL_Debug';
WR225E.IRL_Debug.protocol = 'WR225E';
WR225E.IRL_Debug.id = hex2dec('000001DA');
WR225E.IRL_Debug.idext = 'STANDARD';
WR225E.IRL_Debug.idinherit = 0;
WR225E.IRL_Debug.payload_size = 8;
WR225E.IRL_Debug.payload_value = [];
WR225E.IRL_Debug.payload_mask = [];
WR225E.IRL_Debug.interval = 50;

WR225E.IRL_Debug.fields{1}.name = 'Debug_Value4';
WR225E.IRL_Debug.fields{1}.units = '';
WR225E.IRL_Debug.fields{1}.start_bit = 8;
WR225E.IRL_Debug.fields{1}.bit_length = 16;
WR225E.IRL_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Debug.fields{1}.data_type = 'SIGNED';
WR225E.IRL_Debug.fields{1}.scale = 1.000000e-02;
WR225E.IRL_Debug.fields{1}.offset = 0.000000e+00;

WR225E.IRL_Debug.fields{2}.name = 'Debug_Value3';
WR225E.IRL_Debug.fields{2}.units = '';
WR225E.IRL_Debug.fields{2}.start_bit = 24;
WR225E.IRL_Debug.fields{2}.bit_length = 16;
WR225E.IRL_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Debug.fields{2}.data_type = 'SIGNED';
WR225E.IRL_Debug.fields{2}.scale = 1.000000e-02;
WR225E.IRL_Debug.fields{2}.offset = 0.000000e+00;

WR225E.IRL_Debug.fields{3}.name = 'Debug_Value2';
WR225E.IRL_Debug.fields{3}.units = '';
WR225E.IRL_Debug.fields{3}.start_bit = 40;
WR225E.IRL_Debug.fields{3}.bit_length = 16;
WR225E.IRL_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Debug.fields{3}.data_type = 'SIGNED';
WR225E.IRL_Debug.fields{3}.scale = 1.000000e-02;
WR225E.IRL_Debug.fields{3}.offset = 0.000000e+00;

WR225E.IRL_Debug.fields{4}.name = 'Debug_Value1';
WR225E.IRL_Debug.fields{4}.units = '';
WR225E.IRL_Debug.fields{4}.start_bit = 56;
WR225E.IRL_Debug.fields{4}.bit_length = 16;
WR225E.IRL_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Debug.fields{4}.data_type = 'SIGNED';
WR225E.IRL_Debug.fields{4}.scale = 1.000000e-02;
WR225E.IRL_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_FaultCode'
WR225E.IRL_FaultCode = struct;
WR225E.IRL_FaultCode.name = 'IRL_FaultCode';
WR225E.IRL_FaultCode.description = 'IRL_FaultCode';
WR225E.IRL_FaultCode.protocol = 'WR225E';
WR225E.IRL_FaultCode.id = hex2dec('000000D2');
WR225E.IRL_FaultCode.idext = 'STANDARD';
WR225E.IRL_FaultCode.idinherit = 0;
WR225E.IRL_FaultCode.payload_size = 5;
WR225E.IRL_FaultCode.payload_value = [];
WR225E.IRL_FaultCode.payload_mask = [];
WR225E.IRL_FaultCode.interval = 50;

WR225E.IRL_FaultCode.fields{1}.name = 'IRL_SDCControl';
WR225E.IRL_FaultCode.fields{1}.units = '';
WR225E.IRL_FaultCode.fields{1}.start_bit = 24;
WR225E.IRL_FaultCode.fields{1}.bit_length = 8;
WR225E.IRL_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_FaultCode.fields{1}.data_type = 'UNSIGNED';
WR225E.IRL_FaultCode.fields{1}.scale = 1.000000e+00;
WR225E.IRL_FaultCode.fields{1}.offset = 0.000000e+00;

WR225E.IRL_FaultCode.fields{2}.name = 'IRL_DiagnosticInfo';
WR225E.IRL_FaultCode.fields{2}.units = '';
WR225E.IRL_FaultCode.fields{2}.start_bit = 40;
WR225E.IRL_FaultCode.fields{2}.bit_length = 16;
WR225E.IRL_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_FaultCode.fields{2}.data_type = 'UNSIGNED';
WR225E.IRL_FaultCode.fields{2}.maximum = 6.553600e+04;
WR225E.IRL_FaultCode.fields{2}.minimum = 0.000000e+00;
WR225E.IRL_FaultCode.fields{2}.scale = 1.000000e+00;
WR225E.IRL_FaultCode.fields{2}.offset = 0.000000e+00;

WR225E.IRL_FaultCode.fields{3}.name = 'IRL_FaultCode';
WR225E.IRL_FaultCode.fields{3}.units = '';
WR225E.IRL_FaultCode.fields{3}.start_bit = 56;
WR225E.IRL_FaultCode.fields{3}.bit_length = 16;
WR225E.IRL_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_FaultCode.fields{3}.data_type = 'UNSIGNED';
WR225E.IRL_FaultCode.fields{3}.maximum = 6.553600e+04;
WR225E.IRL_FaultCode.fields{3}.minimum = 0.000000e+00;
WR225E.IRL_FaultCode.fields{3}.scale = 1.000000e+00;
WR225E.IRL_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Flags'
WR225E.IRL_Flags = struct;
WR225E.IRL_Flags.name = 'IRL_Flags';
WR225E.IRL_Flags.description = 'IRL_Flags';
WR225E.IRL_Flags.protocol = 'WR225E';
WR225E.IRL_Flags.id = hex2dec('000001D6');
WR225E.IRL_Flags.idext = 'STANDARD';
WR225E.IRL_Flags.idinherit = 0;
WR225E.IRL_Flags.payload_size = 1;
WR225E.IRL_Flags.payload_value = [];
WR225E.IRL_Flags.payload_mask = [];
WR225E.IRL_Flags.interval = 50;

WR225E.IRL_Flags.fields{1}.name = 'IRL_flag_run';
WR225E.IRL_Flags.fields{1}.units = 'Flag';
WR225E.IRL_Flags.fields{1}.start_bit = 56;
WR225E.IRL_Flags.fields{1}.bit_length = 1;
WR225E.IRL_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{1}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{1}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{1}.offset = 0.000000e+00;

WR225E.IRL_Flags.fields{2}.name = 'IRL_hw_run';
WR225E.IRL_Flags.fields{2}.units = '';
WR225E.IRL_Flags.fields{2}.start_bit = 57;
WR225E.IRL_Flags.fields{2}.bit_length = 1;
WR225E.IRL_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{2}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{2}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{2}.offset = 0.000000e+00;

WR225E.IRL_Flags.fields{3}.name = 'IRL_ecu_run';
WR225E.IRL_Flags.fields{3}.units = 'Flag';
WR225E.IRL_Flags.fields{3}.start_bit = 58;
WR225E.IRL_Flags.fields{3}.bit_length = 1;
WR225E.IRL_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{3}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{3}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{3}.offset = 0.000000e+00;

WR225E.IRL_Flags.fields{4}.name = 'IRL_gate_deadzon';
WR225E.IRL_Flags.fields{4}.units = '';
WR225E.IRL_Flags.fields{4}.start_bit = 59;
WR225E.IRL_Flags.fields{4}.bit_length = 1;
WR225E.IRL_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{4}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{4}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{4}.offset = 0.000000e+00;

WR225E.IRL_Flags.fields{5}.name = 'IRL_ecu_shutdown';
WR225E.IRL_Flags.fields{5}.units = '';
WR225E.IRL_Flags.fields{5}.start_bit = 60;
WR225E.IRL_Flags.fields{5}.bit_length = 1;
WR225E.IRL_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{5}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{5}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{5}.offset = 0.000000e+00;

WR225E.IRL_Flags.fields{6}.name = 'IRL_Offset_cal';
WR225E.IRL_Flags.fields{6}.units = 'Flag';
WR225E.IRL_Flags.fields{6}.start_bit = 61;
WR225E.IRL_Flags.fields{6}.bit_length = 1;
WR225E.IRL_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Flags.fields{6}.data_type = 'UNSIGNED';
WR225E.IRL_Flags.fields{6}.scale = 1.000000e+00;
WR225E.IRL_Flags.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_MotorParams'
WR225E.IRL_MotorParams = struct;
WR225E.IRL_MotorParams.name = 'IRL_MotorParams';
WR225E.IRL_MotorParams.description = 'IRL_MotorParams';
WR225E.IRL_MotorParams.protocol = 'WR225E';
WR225E.IRL_MotorParams.id = hex2dec('000000C9');
WR225E.IRL_MotorParams.idext = 'STANDARD';
WR225E.IRL_MotorParams.idinherit = 0;
WR225E.IRL_MotorParams.payload_size = 8;
WR225E.IRL_MotorParams.payload_value = [];
WR225E.IRL_MotorParams.payload_mask = [];
WR225E.IRL_MotorParams.interval = 50;

WR225E.IRL_MotorParams.fields{1}.name = 'IRL_Vdc';
WR225E.IRL_MotorParams.fields{1}.units = 'V';
WR225E.IRL_MotorParams.fields{1}.start_bit = 8;
WR225E.IRL_MotorParams.fields{1}.bit_length = 16;
WR225E.IRL_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{1}.data_type = 'SIGNED';
WR225E.IRL_MotorParams.fields{1}.maximum = 3.270000e+02;
WR225E.IRL_MotorParams.fields{1}.minimum = -3.270000e+02;
WR225E.IRL_MotorParams.fields{1}.scale = 1.000000e-02;
WR225E.IRL_MotorParams.fields{1}.offset = 0.000000e+00;

WR225E.IRL_MotorParams.fields{2}.name = 'IRL_Torque_est';
WR225E.IRL_MotorParams.fields{2}.units = 'Nm';
WR225E.IRL_MotorParams.fields{2}.start_bit = 24;
WR225E.IRL_MotorParams.fields{2}.bit_length = 16;
WR225E.IRL_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{2}.data_type = 'SIGNED';
WR225E.IRL_MotorParams.fields{2}.scale = 2.000000e-03;
WR225E.IRL_MotorParams.fields{2}.offset = 0.000000e+00;

WR225E.IRL_MotorParams.fields{3}.name = 'IRL_R_winding';
WR225E.IRL_MotorParams.fields{3}.units = 'mOhm';
WR225E.IRL_MotorParams.fields{3}.start_bit = 32;
WR225E.IRL_MotorParams.fields{3}.bit_length = 8;
WR225E.IRL_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{3}.data_type = 'UNSIGNED';
WR225E.IRL_MotorParams.fields{3}.maximum = 5.120000e+01;
WR225E.IRL_MotorParams.fields{3}.minimum = 0.000000e+00;
WR225E.IRL_MotorParams.fields{3}.scale = 2.000000e-01;
WR225E.IRL_MotorParams.fields{3}.offset = 0.000000e+00;

WR225E.IRL_MotorParams.fields{4}.name = 'IRL_K_t';
WR225E.IRL_MotorParams.fields{4}.units = 'Nm/A';
WR225E.IRL_MotorParams.fields{4}.start_bit = 40;
WR225E.IRL_MotorParams.fields{4}.bit_length = 8;
WR225E.IRL_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{4}.data_type = 'UNSIGNED';
WR225E.IRL_MotorParams.fields{4}.maximum = 5.120000e-01;
WR225E.IRL_MotorParams.fields{4}.minimum = 0.000000e+00;
WR225E.IRL_MotorParams.fields{4}.scale = 2.000000e-03;
WR225E.IRL_MotorParams.fields{4}.offset = 0.000000e+00;

WR225E.IRL_MotorParams.fields{5}.name = 'IRL_L_q';
WR225E.IRL_MotorParams.fields{5}.units = 'uH';
WR225E.IRL_MotorParams.fields{5}.start_bit = 48;
WR225E.IRL_MotorParams.fields{5}.bit_length = 8;
WR225E.IRL_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{5}.data_type = 'UNSIGNED';
WR225E.IRL_MotorParams.fields{5}.maximum = 1.280000e+02;
WR225E.IRL_MotorParams.fields{5}.minimum = 0.000000e+00;
WR225E.IRL_MotorParams.fields{5}.scale = 5.000000e-01;
WR225E.IRL_MotorParams.fields{5}.offset = 0.000000e+00;

WR225E.IRL_MotorParams.fields{6}.name = 'IRL_L_d';
WR225E.IRL_MotorParams.fields{6}.units = 'uH';
WR225E.IRL_MotorParams.fields{6}.start_bit = 56;
WR225E.IRL_MotorParams.fields{6}.bit_length = 8;
WR225E.IRL_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_MotorParams.fields{6}.data_type = 'UNSIGNED';
WR225E.IRL_MotorParams.fields{6}.maximum = 1.280000e+02;
WR225E.IRL_MotorParams.fields{6}.minimum = 0.000000e+00;
WR225E.IRL_MotorParams.fields{6}.scale = 5.000000e-01;
WR225E.IRL_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Observer'
WR225E.IRL_Observer = struct;
WR225E.IRL_Observer.name = 'IRL_Observer';
WR225E.IRL_Observer.description = 'IRL_Observer';
WR225E.IRL_Observer.protocol = 'WR225E';
WR225E.IRL_Observer.id = hex2dec('000001D2');
WR225E.IRL_Observer.idext = 'STANDARD';
WR225E.IRL_Observer.idinherit = 0;
WR225E.IRL_Observer.payload_size = 8;
WR225E.IRL_Observer.payload_value = [];
WR225E.IRL_Observer.payload_mask = [];
WR225E.IRL_Observer.interval = 50;

WR225E.IRL_Observer.fields{1}.name = 'IRL_Iq_ref';
WR225E.IRL_Observer.fields{1}.units = 'A';
WR225E.IRL_Observer.fields{1}.start_bit = 40;
WR225E.IRL_Observer.fields{1}.bit_length = 16;
WR225E.IRL_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Observer.fields{1}.data_type = 'SIGNED';
WR225E.IRL_Observer.fields{1}.scale = 2.000000e-02;
WR225E.IRL_Observer.fields{1}.offset = 0.000000e+00;

WR225E.IRL_Observer.fields{2}.name = 'IRL_Id_ref';
WR225E.IRL_Observer.fields{2}.units = 'A';
WR225E.IRL_Observer.fields{2}.start_bit = 56;
WR225E.IRL_Observer.fields{2}.bit_length = 16;
WR225E.IRL_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Observer.fields{2}.data_type = 'SIGNED';
WR225E.IRL_Observer.fields{2}.scale = 2.000000e-02;
WR225E.IRL_Observer.fields{2}.offset = 0.000000e+00;

WR225E.IRL_Observer.fields{3}.name = 'IRL_flux_q';
WR225E.IRL_Observer.fields{3}.units = 'VpHz';
WR225E.IRL_Observer.fields{3}.start_bit = 8;
WR225E.IRL_Observer.fields{3}.bit_length = 16;
WR225E.IRL_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Observer.fields{3}.data_type = 'SIGNED';
WR225E.IRL_Observer.fields{3}.scale = 3.333333e-04;
WR225E.IRL_Observer.fields{3}.offset = 0.000000e+00;

WR225E.IRL_Observer.fields{4}.name = 'IRL_flux_d';
WR225E.IRL_Observer.fields{4}.units = 'VpHz';
WR225E.IRL_Observer.fields{4}.start_bit = 24;
WR225E.IRL_Observer.fields{4}.bit_length = 16;
WR225E.IRL_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Observer.fields{4}.data_type = 'SIGNED';
WR225E.IRL_Observer.fields{4}.scale = 3.333333e-04;
WR225E.IRL_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Offsets'
WR225E.IRL_Offsets = struct;
WR225E.IRL_Offsets.name = 'IRL_Offsets';
WR225E.IRL_Offsets.description = 'IRL_Offsets';
WR225E.IRL_Offsets.protocol = 'WR225E';
WR225E.IRL_Offsets.id = hex2dec('000000CB');
WR225E.IRL_Offsets.idext = 'STANDARD';
WR225E.IRL_Offsets.idinherit = 0;
WR225E.IRL_Offsets.payload_size = 6;
WR225E.IRL_Offsets.payload_value = [];
WR225E.IRL_Offsets.payload_mask = [];
WR225E.IRL_Offsets.interval = 50;

WR225E.IRL_Offsets.fields{1}.name = 'IRL_V_c_offset';
WR225E.IRL_Offsets.fields{1}.units = 'ADC';
WR225E.IRL_Offsets.fields{1}.start_bit = 16;
WR225E.IRL_Offsets.fields{1}.bit_length = 8;
WR225E.IRL_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{1}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{1}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{1}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{1}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{1}.offset = 0.000000e+00;

WR225E.IRL_Offsets.fields{2}.name = 'IRL_V_b_offset';
WR225E.IRL_Offsets.fields{2}.units = 'ADC';
WR225E.IRL_Offsets.fields{2}.start_bit = 24;
WR225E.IRL_Offsets.fields{2}.bit_length = 8;
WR225E.IRL_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{2}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{2}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{2}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{2}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{2}.offset = 0.000000e+00;

WR225E.IRL_Offsets.fields{3}.name = 'IRL_V_a_offset';
WR225E.IRL_Offsets.fields{3}.units = 'ADC';
WR225E.IRL_Offsets.fields{3}.start_bit = 32;
WR225E.IRL_Offsets.fields{3}.bit_length = 8;
WR225E.IRL_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{3}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{3}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{3}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{3}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{3}.offset = 0.000000e+00;

WR225E.IRL_Offsets.fields{4}.name = 'IRL_I_c_offset';
WR225E.IRL_Offsets.fields{4}.units = 'ADC';
WR225E.IRL_Offsets.fields{4}.start_bit = 40;
WR225E.IRL_Offsets.fields{4}.bit_length = 8;
WR225E.IRL_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{4}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{4}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{4}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{4}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{4}.offset = 0.000000e+00;

WR225E.IRL_Offsets.fields{5}.name = 'IRL_I_b_offset';
WR225E.IRL_Offsets.fields{5}.units = 'ADC';
WR225E.IRL_Offsets.fields{5}.start_bit = 48;
WR225E.IRL_Offsets.fields{5}.bit_length = 8;
WR225E.IRL_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{5}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{5}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{5}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{5}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{5}.offset = 0.000000e+00;

WR225E.IRL_Offsets.fields{6}.name = 'IRL_I_a_offset';
WR225E.IRL_Offsets.fields{6}.units = 'ADC';
WR225E.IRL_Offsets.fields{6}.start_bit = 56;
WR225E.IRL_Offsets.fields{6}.bit_length = 8;
WR225E.IRL_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_Offsets.fields{6}.data_type = 'SIGNED';
WR225E.IRL_Offsets.fields{6}.maximum = 1.280000e+02;
WR225E.IRL_Offsets.fields{6}.minimum = -1.270000e+02;
WR225E.IRL_Offsets.fields{6}.scale = 1.000000e+00;
WR225E.IRL_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_SensorData'
WR225E.IRL_SensorData = struct;
WR225E.IRL_SensorData.name = 'IRL_SensorData';
WR225E.IRL_SensorData.description = 'IRL_SensorData';
WR225E.IRL_SensorData.protocol = 'WR225E';
WR225E.IRL_SensorData.id = hex2dec('000000DC');
WR225E.IRL_SensorData.idext = 'STANDARD';
WR225E.IRL_SensorData.idinherit = 0;
WR225E.IRL_SensorData.payload_size = 8;
WR225E.IRL_SensorData.payload_value = [];
WR225E.IRL_SensorData.payload_mask = [];
WR225E.IRL_SensorData.interval = 50;

WR225E.IRL_SensorData.fields{1}.name = 'IRL_Ib';
WR225E.IRL_SensorData.fields{1}.units = 'A';
WR225E.IRL_SensorData.fields{1}.start_bit = 8;
WR225E.IRL_SensorData.fields{1}.bit_length = 16;
WR225E.IRL_SensorData.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_SensorData.fields{1}.data_type = 'SIGNED';
WR225E.IRL_SensorData.fields{1}.scale = 1.000000e-02;
WR225E.IRL_SensorData.fields{1}.offset = 0.000000e+00;

WR225E.IRL_SensorData.fields{2}.name = 'IRL_Ia';
WR225E.IRL_SensorData.fields{2}.units = 'A';
WR225E.IRL_SensorData.fields{2}.start_bit = 24;
WR225E.IRL_SensorData.fields{2}.bit_length = 16;
WR225E.IRL_SensorData.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_SensorData.fields{2}.data_type = 'SIGNED';
WR225E.IRL_SensorData.fields{2}.scale = 1.000000e-02;
WR225E.IRL_SensorData.fields{2}.offset = 0.000000e+00;

WR225E.IRL_SensorData.fields{3}.name = 'IRL_MechSpeed';
WR225E.IRL_SensorData.fields{3}.units = 'RPM';
WR225E.IRL_SensorData.fields{3}.start_bit = 40;
WR225E.IRL_SensorData.fields{3}.bit_length = 16;
WR225E.IRL_SensorData.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_SensorData.fields{3}.data_type = 'SIGNED';
WR225E.IRL_SensorData.fields{3}.scale = 1.000000e+00;
WR225E.IRL_SensorData.fields{3}.offset = 0.000000e+00;

WR225E.IRL_SensorData.fields{4}.name = 'IRL_MechAngle';
WR225E.IRL_SensorData.fields{4}.units = 'Degrees';
WR225E.IRL_SensorData.fields{4}.start_bit = 56;
WR225E.IRL_SensorData.fields{4}.bit_length = 16;
WR225E.IRL_SensorData.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_SensorData.fields{4}.data_type = 'SIGNED';
WR225E.IRL_SensorData.fields{4}.maximum = 6.553400e+01;
WR225E.IRL_SensorData.fields{4}.minimum = -6.553600e+01;
WR225E.IRL_SensorData.fields{4}.scale = 2.000000e-03;
WR225E.IRL_SensorData.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_dq'
WR225E.IRL_dq = struct;
WR225E.IRL_dq.name = 'IRL_dq';
WR225E.IRL_dq.description = 'IRL_dq';
WR225E.IRL_dq.protocol = 'WR225E';
WR225E.IRL_dq.id = hex2dec('000000CA');
WR225E.IRL_dq.idext = 'STANDARD';
WR225E.IRL_dq.idinherit = 0;
WR225E.IRL_dq.payload_size = 8;
WR225E.IRL_dq.payload_value = [];
WR225E.IRL_dq.payload_mask = [];
WR225E.IRL_dq.interval = 50;

WR225E.IRL_dq.fields{1}.name = 'IRL_I_q';
WR225E.IRL_dq.fields{1}.units = 'A';
WR225E.IRL_dq.fields{1}.start_bit = 8;
WR225E.IRL_dq.fields{1}.bit_length = 16;
WR225E.IRL_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_dq.fields{1}.data_type = 'SIGNED';
WR225E.IRL_dq.fields{1}.maximum = 6.540000e+02;
WR225E.IRL_dq.fields{1}.minimum = -6.540000e+02;
WR225E.IRL_dq.fields{1}.scale = 2.000000e-02;
WR225E.IRL_dq.fields{1}.offset = 0.000000e+00;

WR225E.IRL_dq.fields{2}.name = 'IRL_I_d';
WR225E.IRL_dq.fields{2}.units = 'A';
WR225E.IRL_dq.fields{2}.start_bit = 24;
WR225E.IRL_dq.fields{2}.bit_length = 16;
WR225E.IRL_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_dq.fields{2}.data_type = 'SIGNED';
WR225E.IRL_dq.fields{2}.maximum = 6.540000e+02;
WR225E.IRL_dq.fields{2}.minimum = -6.540000e+02;
WR225E.IRL_dq.fields{2}.scale = 2.000000e-02;
WR225E.IRL_dq.fields{2}.offset = 0.000000e+00;

WR225E.IRL_dq.fields{3}.name = 'IRL_V_q';
WR225E.IRL_dq.fields{3}.units = 'V';
WR225E.IRL_dq.fields{3}.start_bit = 40;
WR225E.IRL_dq.fields{3}.bit_length = 16;
WR225E.IRL_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_dq.fields{3}.data_type = 'SIGNED';
WR225E.IRL_dq.fields{3}.maximum = 6.540000e+02;
WR225E.IRL_dq.fields{3}.minimum = -6.540000e+02;
WR225E.IRL_dq.fields{3}.scale = 2.000000e-02;
WR225E.IRL_dq.fields{3}.offset = 0.000000e+00;

WR225E.IRL_dq.fields{4}.name = 'IRL_V_d';
WR225E.IRL_dq.fields{4}.units = 'V';
WR225E.IRL_dq.fields{4}.start_bit = 56;
WR225E.IRL_dq.fields{4}.bit_length = 16;
WR225E.IRL_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRL_dq.fields{4}.data_type = 'SIGNED';
WR225E.IRL_dq.fields{4}.maximum = 6.540000e+02;
WR225E.IRL_dq.fields{4}.minimum = -6.540000e+02;
WR225E.IRL_dq.fields{4}.scale = 2.000000e-02;
WR225E.IRL_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRLrx_Bootloader'
WR225E.IRLrx_Bootloader = struct;
WR225E.IRLrx_Bootloader.name = 'IRLrx_Bootloader';
WR225E.IRLrx_Bootloader.description = 'IRLrx_Bootloader';
WR225E.IRLrx_Bootloader.protocol = 'WR225E';
WR225E.IRLrx_Bootloader.id = hex2dec('000000B3');
WR225E.IRLrx_Bootloader.idext = 'STANDARD';
WR225E.IRLrx_Bootloader.idinherit = 0;
WR225E.IRLrx_Bootloader.payload_size = 8;
WR225E.IRLrx_Bootloader.payload_value = [];
WR225E.IRLrx_Bootloader.payload_mask = [];
WR225E.IRLrx_Bootloader.interval = 50;

%===================================================================
case 'IRR_Controls'
WR225E.IRR_Controls = struct;
WR225E.IRR_Controls.name = 'IRR_Controls';
WR225E.IRR_Controls.description = 'IRR_Controls';
WR225E.IRR_Controls.protocol = 'WR225E';
WR225E.IRR_Controls.id = hex2dec('000000CC');
WR225E.IRR_Controls.idext = 'STANDARD';
WR225E.IRR_Controls.idinherit = 0;
WR225E.IRR_Controls.payload_size = 8;
WR225E.IRR_Controls.payload_value = [];
WR225E.IRR_Controls.payload_mask = [];
WR225E.IRR_Controls.interval = 50;

WR225E.IRR_Controls.fields{1}.name = 'IRR_T_IGBT';
WR225E.IRR_Controls.fields{1}.units = 'C';
WR225E.IRR_Controls.fields{1}.start_bit = 0;
WR225E.IRR_Controls.fields{1}.bit_length = 8;
WR225E.IRR_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Controls.fields{1}.data_type = 'UNSIGNED';
WR225E.IRR_Controls.fields{1}.maximum = 2.160000e+02;
WR225E.IRR_Controls.fields{1}.minimum = 0.000000e+00;
WR225E.IRR_Controls.fields{1}.scale = 1.000000e+00;
WR225E.IRR_Controls.fields{1}.offset = -4.000000e+01;

WR225E.IRR_Controls.fields{2}.name = 'IRR_T_winding';
WR225E.IRR_Controls.fields{2}.units = 'C';
WR225E.IRR_Controls.fields{2}.start_bit = 8;
WR225E.IRR_Controls.fields{2}.bit_length = 8;
WR225E.IRR_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Controls.fields{2}.data_type = 'UNSIGNED';
WR225E.IRR_Controls.fields{2}.maximum = 2.160000e+02;
WR225E.IRR_Controls.fields{2}.minimum = 0.000000e+00;
WR225E.IRR_Controls.fields{2}.scale = 1.000000e+00;
WR225E.IRR_Controls.fields{2}.offset = -4.000000e+01;

WR225E.IRR_Controls.fields{3}.name = 'IRR_I_s_amplitude';
WR225E.IRR_Controls.fields{3}.units = 'A';
WR225E.IRR_Controls.fields{3}.start_bit = 24;
WR225E.IRR_Controls.fields{3}.bit_length = 16;
WR225E.IRR_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Controls.fields{3}.data_type = 'SIGNED';
WR225E.IRR_Controls.fields{3}.maximum = 6.550000e+02;
WR225E.IRR_Controls.fields{3}.minimum = -6.550000e+02;
WR225E.IRR_Controls.fields{3}.scale = 2.000000e-02;
WR225E.IRR_Controls.fields{3}.offset = 0.000000e+00;

WR225E.IRR_Controls.fields{4}.name = 'IRR_P_ac';
WR225E.IRR_Controls.fields{4}.units = 'W';
WR225E.IRR_Controls.fields{4}.start_bit = 40;
WR225E.IRR_Controls.fields{4}.bit_length = 16;
WR225E.IRR_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Controls.fields{4}.data_type = 'SIGNED';
WR225E.IRR_Controls.fields{4}.maximum = 6.400000e+04;
WR225E.IRR_Controls.fields{4}.minimum = -6.400000e+04;
WR225E.IRR_Controls.fields{4}.scale = 2.000000e+00;
WR225E.IRR_Controls.fields{4}.offset = 0.000000e+00;

WR225E.IRR_Controls.fields{5}.name = 'IRR_MotorSpeed';
WR225E.IRR_Controls.fields{5}.units = 'RPM';
WR225E.IRR_Controls.fields{5}.start_bit = 56;
WR225E.IRR_Controls.fields{5}.bit_length = 16;
WR225E.IRR_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Controls.fields{5}.data_type = 'SIGNED';
WR225E.IRR_Controls.fields{5}.maximum = 3.270000e+04;
WR225E.IRR_Controls.fields{5}.minimum = -3.270000e+04;
WR225E.IRR_Controls.fields{5}.scale = 1.000000e+00;
WR225E.IRR_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Debug'
WR225E.IRR_Debug = struct;
WR225E.IRR_Debug.name = 'IRR_Debug';
WR225E.IRR_Debug.description = 'IRR_Debug';
WR225E.IRR_Debug.protocol = 'WR225E';
WR225E.IRR_Debug.id = hex2dec('000001DB');
WR225E.IRR_Debug.idext = 'STANDARD';
WR225E.IRR_Debug.idinherit = 0;
WR225E.IRR_Debug.payload_size = 8;
WR225E.IRR_Debug.payload_value = [];
WR225E.IRR_Debug.payload_mask = [];
WR225E.IRR_Debug.interval = 50;

WR225E.IRR_Debug.fields{1}.name = 'Debug_Value4';
WR225E.IRR_Debug.fields{1}.units = '';
WR225E.IRR_Debug.fields{1}.start_bit = 8;
WR225E.IRR_Debug.fields{1}.bit_length = 16;
WR225E.IRR_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Debug.fields{1}.data_type = 'SIGNED';
WR225E.IRR_Debug.fields{1}.scale = 1.000000e-02;
WR225E.IRR_Debug.fields{1}.offset = 0.000000e+00;

WR225E.IRR_Debug.fields{2}.name = 'Debug_Value3';
WR225E.IRR_Debug.fields{2}.units = '';
WR225E.IRR_Debug.fields{2}.start_bit = 24;
WR225E.IRR_Debug.fields{2}.bit_length = 16;
WR225E.IRR_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Debug.fields{2}.data_type = 'SIGNED';
WR225E.IRR_Debug.fields{2}.scale = 1.000000e-02;
WR225E.IRR_Debug.fields{2}.offset = 0.000000e+00;

WR225E.IRR_Debug.fields{3}.name = 'Debug_Value2';
WR225E.IRR_Debug.fields{3}.units = '';
WR225E.IRR_Debug.fields{3}.start_bit = 40;
WR225E.IRR_Debug.fields{3}.bit_length = 16;
WR225E.IRR_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Debug.fields{3}.data_type = 'SIGNED';
WR225E.IRR_Debug.fields{3}.scale = 1.000000e-02;
WR225E.IRR_Debug.fields{3}.offset = 0.000000e+00;

WR225E.IRR_Debug.fields{4}.name = 'Debug_Value1';
WR225E.IRR_Debug.fields{4}.units = '';
WR225E.IRR_Debug.fields{4}.start_bit = 56;
WR225E.IRR_Debug.fields{4}.bit_length = 16;
WR225E.IRR_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Debug.fields{4}.data_type = 'SIGNED';
WR225E.IRR_Debug.fields{4}.scale = 1.000000e-02;
WR225E.IRR_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_FaultCode'
WR225E.IRR_FaultCode = struct;
WR225E.IRR_FaultCode.name = 'IRR_FaultCode';
WR225E.IRR_FaultCode.description = 'IRR_FaultCode';
WR225E.IRR_FaultCode.protocol = 'WR225E';
WR225E.IRR_FaultCode.id = hex2dec('000000D3');
WR225E.IRR_FaultCode.idext = 'STANDARD';
WR225E.IRR_FaultCode.idinherit = 0;
WR225E.IRR_FaultCode.payload_size = 5;
WR225E.IRR_FaultCode.payload_value = [];
WR225E.IRR_FaultCode.payload_mask = [];
WR225E.IRR_FaultCode.interval = 50;

WR225E.IRR_FaultCode.fields{1}.name = 'IRR_SDCControl';
WR225E.IRR_FaultCode.fields{1}.units = '';
WR225E.IRR_FaultCode.fields{1}.start_bit = 24;
WR225E.IRR_FaultCode.fields{1}.bit_length = 8;
WR225E.IRR_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_FaultCode.fields{1}.data_type = 'UNSIGNED';
WR225E.IRR_FaultCode.fields{1}.scale = 1.000000e+00;
WR225E.IRR_FaultCode.fields{1}.offset = 0.000000e+00;

WR225E.IRR_FaultCode.fields{2}.name = 'IRR_DiagnosticInfo';
WR225E.IRR_FaultCode.fields{2}.units = '';
WR225E.IRR_FaultCode.fields{2}.start_bit = 40;
WR225E.IRR_FaultCode.fields{2}.bit_length = 16;
WR225E.IRR_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_FaultCode.fields{2}.data_type = 'UNSIGNED';
WR225E.IRR_FaultCode.fields{2}.maximum = 6.553600e+04;
WR225E.IRR_FaultCode.fields{2}.minimum = 0.000000e+00;
WR225E.IRR_FaultCode.fields{2}.scale = 1.000000e+00;
WR225E.IRR_FaultCode.fields{2}.offset = 0.000000e+00;

WR225E.IRR_FaultCode.fields{3}.name = 'IRR_FaultCode';
WR225E.IRR_FaultCode.fields{3}.units = '';
WR225E.IRR_FaultCode.fields{3}.start_bit = 56;
WR225E.IRR_FaultCode.fields{3}.bit_length = 16;
WR225E.IRR_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_FaultCode.fields{3}.data_type = 'UNSIGNED';
WR225E.IRR_FaultCode.fields{3}.maximum = 6.553600e+04;
WR225E.IRR_FaultCode.fields{3}.minimum = 0.000000e+00;
WR225E.IRR_FaultCode.fields{3}.scale = 1.000000e+00;
WR225E.IRR_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Flags'
WR225E.IRR_Flags = struct;
WR225E.IRR_Flags.name = 'IRR_Flags';
WR225E.IRR_Flags.description = 'IRR_Flags';
WR225E.IRR_Flags.protocol = 'WR225E';
WR225E.IRR_Flags.id = hex2dec('000001D7');
WR225E.IRR_Flags.idext = 'STANDARD';
WR225E.IRR_Flags.idinherit = 0;
WR225E.IRR_Flags.payload_size = 1;
WR225E.IRR_Flags.payload_value = [];
WR225E.IRR_Flags.payload_mask = [];
WR225E.IRR_Flags.interval = 50;

WR225E.IRR_Flags.fields{1}.name = 'IRR_flag_run';
WR225E.IRR_Flags.fields{1}.units = 'Flag';
WR225E.IRR_Flags.fields{1}.start_bit = 56;
WR225E.IRR_Flags.fields{1}.bit_length = 1;
WR225E.IRR_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{1}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{1}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{1}.offset = 0.000000e+00;

WR225E.IRR_Flags.fields{2}.name = 'IRR_hw_run';
WR225E.IRR_Flags.fields{2}.units = '';
WR225E.IRR_Flags.fields{2}.start_bit = 57;
WR225E.IRR_Flags.fields{2}.bit_length = 1;
WR225E.IRR_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{2}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{2}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{2}.offset = 0.000000e+00;

WR225E.IRR_Flags.fields{3}.name = 'IRR_ecu_run';
WR225E.IRR_Flags.fields{3}.units = 'Flag';
WR225E.IRR_Flags.fields{3}.start_bit = 58;
WR225E.IRR_Flags.fields{3}.bit_length = 1;
WR225E.IRR_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{3}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{3}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{3}.offset = 0.000000e+00;

WR225E.IRR_Flags.fields{4}.name = 'IRR_gate_deadzon';
WR225E.IRR_Flags.fields{4}.units = '';
WR225E.IRR_Flags.fields{4}.start_bit = 59;
WR225E.IRR_Flags.fields{4}.bit_length = 1;
WR225E.IRR_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{4}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{4}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{4}.offset = 0.000000e+00;

WR225E.IRR_Flags.fields{5}.name = 'IRR_ecu_shutdown';
WR225E.IRR_Flags.fields{5}.units = '';
WR225E.IRR_Flags.fields{5}.start_bit = 60;
WR225E.IRR_Flags.fields{5}.bit_length = 1;
WR225E.IRR_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{5}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{5}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{5}.offset = 0.000000e+00;

WR225E.IRR_Flags.fields{6}.name = 'IRR_Offset_cal';
WR225E.IRR_Flags.fields{6}.units = 'Flag';
WR225E.IRR_Flags.fields{6}.start_bit = 61;
WR225E.IRR_Flags.fields{6}.bit_length = 1;
WR225E.IRR_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Flags.fields{6}.data_type = 'UNSIGNED';
WR225E.IRR_Flags.fields{6}.scale = 1.000000e+00;
WR225E.IRR_Flags.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_MotorParams'
WR225E.IRR_MotorParams = struct;
WR225E.IRR_MotorParams.name = 'IRR_MotorParams';
WR225E.IRR_MotorParams.description = 'IRR_MotorParams';
WR225E.IRR_MotorParams.protocol = 'WR225E';
WR225E.IRR_MotorParams.id = hex2dec('000000CD');
WR225E.IRR_MotorParams.idext = 'STANDARD';
WR225E.IRR_MotorParams.idinherit = 0;
WR225E.IRR_MotorParams.payload_size = 8;
WR225E.IRR_MotorParams.payload_value = [];
WR225E.IRR_MotorParams.payload_mask = [];
WR225E.IRR_MotorParams.interval = 50;

WR225E.IRR_MotorParams.fields{1}.name = 'IRR_Vdc';
WR225E.IRR_MotorParams.fields{1}.units = 'V';
WR225E.IRR_MotorParams.fields{1}.start_bit = 8;
WR225E.IRR_MotorParams.fields{1}.bit_length = 16;
WR225E.IRR_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{1}.data_type = 'SIGNED';
WR225E.IRR_MotorParams.fields{1}.maximum = 3.270000e+02;
WR225E.IRR_MotorParams.fields{1}.minimum = -3.270000e+02;
WR225E.IRR_MotorParams.fields{1}.scale = 1.000000e-02;
WR225E.IRR_MotorParams.fields{1}.offset = 0.000000e+00;

WR225E.IRR_MotorParams.fields{2}.name = 'IRR_Torque_est';
WR225E.IRR_MotorParams.fields{2}.units = 'Nm';
WR225E.IRR_MotorParams.fields{2}.start_bit = 24;
WR225E.IRR_MotorParams.fields{2}.bit_length = 16;
WR225E.IRR_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{2}.data_type = 'SIGNED';
WR225E.IRR_MotorParams.fields{2}.scale = 2.000000e-03;
WR225E.IRR_MotorParams.fields{2}.offset = 0.000000e+00;

WR225E.IRR_MotorParams.fields{3}.name = 'IRR_R_winding';
WR225E.IRR_MotorParams.fields{3}.units = 'mOhm';
WR225E.IRR_MotorParams.fields{3}.start_bit = 32;
WR225E.IRR_MotorParams.fields{3}.bit_length = 8;
WR225E.IRR_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{3}.data_type = 'UNSIGNED';
WR225E.IRR_MotorParams.fields{3}.maximum = 5.120000e+01;
WR225E.IRR_MotorParams.fields{3}.minimum = 0.000000e+00;
WR225E.IRR_MotorParams.fields{3}.scale = 2.000000e-01;
WR225E.IRR_MotorParams.fields{3}.offset = 0.000000e+00;

WR225E.IRR_MotorParams.fields{4}.name = 'IRR_K_t';
WR225E.IRR_MotorParams.fields{4}.units = 'Nm/A';
WR225E.IRR_MotorParams.fields{4}.start_bit = 40;
WR225E.IRR_MotorParams.fields{4}.bit_length = 8;
WR225E.IRR_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{4}.data_type = 'UNSIGNED';
WR225E.IRR_MotorParams.fields{4}.maximum = 5.120000e-01;
WR225E.IRR_MotorParams.fields{4}.minimum = 0.000000e+00;
WR225E.IRR_MotorParams.fields{4}.scale = 2.000000e-03;
WR225E.IRR_MotorParams.fields{4}.offset = 0.000000e+00;

WR225E.IRR_MotorParams.fields{5}.name = 'IRR_L_q';
WR225E.IRR_MotorParams.fields{5}.units = 'uH';
WR225E.IRR_MotorParams.fields{5}.start_bit = 48;
WR225E.IRR_MotorParams.fields{5}.bit_length = 8;
WR225E.IRR_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{5}.data_type = 'UNSIGNED';
WR225E.IRR_MotorParams.fields{5}.maximum = 1.280000e+02;
WR225E.IRR_MotorParams.fields{5}.minimum = 0.000000e+00;
WR225E.IRR_MotorParams.fields{5}.scale = 5.000000e-01;
WR225E.IRR_MotorParams.fields{5}.offset = 0.000000e+00;

WR225E.IRR_MotorParams.fields{6}.name = 'IRR_L_d';
WR225E.IRR_MotorParams.fields{6}.units = 'uH';
WR225E.IRR_MotorParams.fields{6}.start_bit = 56;
WR225E.IRR_MotorParams.fields{6}.bit_length = 8;
WR225E.IRR_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_MotorParams.fields{6}.data_type = 'UNSIGNED';
WR225E.IRR_MotorParams.fields{6}.maximum = 1.280000e+02;
WR225E.IRR_MotorParams.fields{6}.minimum = 0.000000e+00;
WR225E.IRR_MotorParams.fields{6}.scale = 5.000000e-01;
WR225E.IRR_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Observer'
WR225E.IRR_Observer = struct;
WR225E.IRR_Observer.name = 'IRR_Observer';
WR225E.IRR_Observer.description = 'IRR_Observer';
WR225E.IRR_Observer.protocol = 'WR225E';
WR225E.IRR_Observer.id = hex2dec('000001D3');
WR225E.IRR_Observer.idext = 'STANDARD';
WR225E.IRR_Observer.idinherit = 0;
WR225E.IRR_Observer.payload_size = 8;
WR225E.IRR_Observer.payload_value = [];
WR225E.IRR_Observer.payload_mask = [];
WR225E.IRR_Observer.interval = 50;

WR225E.IRR_Observer.fields{1}.name = 'IRR_Iq_ref';
WR225E.IRR_Observer.fields{1}.units = 'A';
WR225E.IRR_Observer.fields{1}.start_bit = 40;
WR225E.IRR_Observer.fields{1}.bit_length = 16;
WR225E.IRR_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Observer.fields{1}.data_type = 'SIGNED';
WR225E.IRR_Observer.fields{1}.scale = 2.000000e-02;
WR225E.IRR_Observer.fields{1}.offset = 0.000000e+00;

WR225E.IRR_Observer.fields{2}.name = 'IRR_Id_ref';
WR225E.IRR_Observer.fields{2}.units = 'A';
WR225E.IRR_Observer.fields{2}.start_bit = 56;
WR225E.IRR_Observer.fields{2}.bit_length = 16;
WR225E.IRR_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Observer.fields{2}.data_type = 'SIGNED';
WR225E.IRR_Observer.fields{2}.scale = 2.000000e-02;
WR225E.IRR_Observer.fields{2}.offset = 0.000000e+00;

WR225E.IRR_Observer.fields{3}.name = 'IRR_flux_q';
WR225E.IRR_Observer.fields{3}.units = 'VpHz';
WR225E.IRR_Observer.fields{3}.start_bit = 8;
WR225E.IRR_Observer.fields{3}.bit_length = 16;
WR225E.IRR_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Observer.fields{3}.data_type = 'SIGNED';
WR225E.IRR_Observer.fields{3}.scale = 3.333333e-04;
WR225E.IRR_Observer.fields{3}.offset = 0.000000e+00;

WR225E.IRR_Observer.fields{4}.name = 'IRR_flux_d';
WR225E.IRR_Observer.fields{4}.units = 'VpHz';
WR225E.IRR_Observer.fields{4}.start_bit = 24;
WR225E.IRR_Observer.fields{4}.bit_length = 16;
WR225E.IRR_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Observer.fields{4}.data_type = 'SIGNED';
WR225E.IRR_Observer.fields{4}.scale = 3.333333e-04;
WR225E.IRR_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Offsets'
WR225E.IRR_Offsets = struct;
WR225E.IRR_Offsets.name = 'IRR_Offsets';
WR225E.IRR_Offsets.description = 'IRR_Offsets';
WR225E.IRR_Offsets.protocol = 'WR225E';
WR225E.IRR_Offsets.id = hex2dec('000000CF');
WR225E.IRR_Offsets.idext = 'STANDARD';
WR225E.IRR_Offsets.idinherit = 0;
WR225E.IRR_Offsets.payload_size = 6;
WR225E.IRR_Offsets.payload_value = [];
WR225E.IRR_Offsets.payload_mask = [];
WR225E.IRR_Offsets.interval = 50;

WR225E.IRR_Offsets.fields{1}.name = 'IRR_V_c_offset';
WR225E.IRR_Offsets.fields{1}.units = 'ADC';
WR225E.IRR_Offsets.fields{1}.start_bit = 16;
WR225E.IRR_Offsets.fields{1}.bit_length = 8;
WR225E.IRR_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{1}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{1}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{1}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{1}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{1}.offset = 0.000000e+00;

WR225E.IRR_Offsets.fields{2}.name = 'IRR_V_b_offset';
WR225E.IRR_Offsets.fields{2}.units = 'ADC';
WR225E.IRR_Offsets.fields{2}.start_bit = 24;
WR225E.IRR_Offsets.fields{2}.bit_length = 8;
WR225E.IRR_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{2}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{2}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{2}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{2}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{2}.offset = 0.000000e+00;

WR225E.IRR_Offsets.fields{3}.name = 'IRR_V_a_offset';
WR225E.IRR_Offsets.fields{3}.units = 'ADC';
WR225E.IRR_Offsets.fields{3}.start_bit = 32;
WR225E.IRR_Offsets.fields{3}.bit_length = 8;
WR225E.IRR_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{3}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{3}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{3}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{3}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{3}.offset = 0.000000e+00;

WR225E.IRR_Offsets.fields{4}.name = 'IRR_I_c_offset';
WR225E.IRR_Offsets.fields{4}.units = 'ADC';
WR225E.IRR_Offsets.fields{4}.start_bit = 40;
WR225E.IRR_Offsets.fields{4}.bit_length = 8;
WR225E.IRR_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{4}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{4}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{4}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{4}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{4}.offset = 0.000000e+00;

WR225E.IRR_Offsets.fields{5}.name = 'IRR_I_b_offset';
WR225E.IRR_Offsets.fields{5}.units = 'ADC';
WR225E.IRR_Offsets.fields{5}.start_bit = 48;
WR225E.IRR_Offsets.fields{5}.bit_length = 8;
WR225E.IRR_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{5}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{5}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{5}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{5}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{5}.offset = 0.000000e+00;

WR225E.IRR_Offsets.fields{6}.name = 'IRR_I_a_offset';
WR225E.IRR_Offsets.fields{6}.units = 'ADC';
WR225E.IRR_Offsets.fields{6}.start_bit = 56;
WR225E.IRR_Offsets.fields{6}.bit_length = 8;
WR225E.IRR_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_Offsets.fields{6}.data_type = 'SIGNED';
WR225E.IRR_Offsets.fields{6}.maximum = 1.280000e+02;
WR225E.IRR_Offsets.fields{6}.minimum = -1.270000e+02;
WR225E.IRR_Offsets.fields{6}.scale = 1.000000e+00;
WR225E.IRR_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_SensorData'
WR225E.IRR_SensorData = struct;
WR225E.IRR_SensorData.name = 'IRR_SensorData';
WR225E.IRR_SensorData.description = 'IRR_SensorData';
WR225E.IRR_SensorData.protocol = 'WR225E';
WR225E.IRR_SensorData.id = hex2dec('000000DD');
WR225E.IRR_SensorData.idext = 'STANDARD';
WR225E.IRR_SensorData.idinherit = 0;
WR225E.IRR_SensorData.payload_size = 8;
WR225E.IRR_SensorData.payload_value = [];
WR225E.IRR_SensorData.payload_mask = [];
WR225E.IRR_SensorData.interval = 50;

WR225E.IRR_SensorData.fields{1}.name = 'IRR_Ib';
WR225E.IRR_SensorData.fields{1}.units = 'A';
WR225E.IRR_SensorData.fields{1}.start_bit = 8;
WR225E.IRR_SensorData.fields{1}.bit_length = 16;
WR225E.IRR_SensorData.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_SensorData.fields{1}.data_type = 'SIGNED';
WR225E.IRR_SensorData.fields{1}.scale = 1.000000e-02;
WR225E.IRR_SensorData.fields{1}.offset = 0.000000e+00;

WR225E.IRR_SensorData.fields{2}.name = 'IRR_Ia';
WR225E.IRR_SensorData.fields{2}.units = 'A';
WR225E.IRR_SensorData.fields{2}.start_bit = 24;
WR225E.IRR_SensorData.fields{2}.bit_length = 16;
WR225E.IRR_SensorData.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_SensorData.fields{2}.data_type = 'SIGNED';
WR225E.IRR_SensorData.fields{2}.scale = 1.000000e-02;
WR225E.IRR_SensorData.fields{2}.offset = 0.000000e+00;

WR225E.IRR_SensorData.fields{3}.name = 'IRR_MechSpeed';
WR225E.IRR_SensorData.fields{3}.units = 'RPM';
WR225E.IRR_SensorData.fields{3}.start_bit = 40;
WR225E.IRR_SensorData.fields{3}.bit_length = 16;
WR225E.IRR_SensorData.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_SensorData.fields{3}.data_type = 'SIGNED';
WR225E.IRR_SensorData.fields{3}.scale = 1.000000e+00;
WR225E.IRR_SensorData.fields{3}.offset = 0.000000e+00;

WR225E.IRR_SensorData.fields{4}.name = 'IRR_MechAngle';
WR225E.IRR_SensorData.fields{4}.units = 'Degrees';
WR225E.IRR_SensorData.fields{4}.start_bit = 56;
WR225E.IRR_SensorData.fields{4}.bit_length = 16;
WR225E.IRR_SensorData.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_SensorData.fields{4}.data_type = 'SIGNED';
WR225E.IRR_SensorData.fields{4}.maximum = 6.553400e+01;
WR225E.IRR_SensorData.fields{4}.minimum = -6.553600e+01;
WR225E.IRR_SensorData.fields{4}.scale = 2.000000e-03;
WR225E.IRR_SensorData.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_dq'
WR225E.IRR_dq = struct;
WR225E.IRR_dq.name = 'IRR_dq';
WR225E.IRR_dq.description = 'IRR_dq';
WR225E.IRR_dq.protocol = 'WR225E';
WR225E.IRR_dq.id = hex2dec('000000CE');
WR225E.IRR_dq.idext = 'STANDARD';
WR225E.IRR_dq.idinherit = 0;
WR225E.IRR_dq.payload_size = 8;
WR225E.IRR_dq.payload_value = [];
WR225E.IRR_dq.payload_mask = [];
WR225E.IRR_dq.interval = 50;

WR225E.IRR_dq.fields{1}.name = 'IRR_I_q';
WR225E.IRR_dq.fields{1}.units = 'A';
WR225E.IRR_dq.fields{1}.start_bit = 8;
WR225E.IRR_dq.fields{1}.bit_length = 16;
WR225E.IRR_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_dq.fields{1}.data_type = 'SIGNED';
WR225E.IRR_dq.fields{1}.maximum = 6.540000e+02;
WR225E.IRR_dq.fields{1}.minimum = -6.540000e+02;
WR225E.IRR_dq.fields{1}.scale = 2.000000e-02;
WR225E.IRR_dq.fields{1}.offset = 0.000000e+00;

WR225E.IRR_dq.fields{2}.name = 'IRR_I_d';
WR225E.IRR_dq.fields{2}.units = 'A';
WR225E.IRR_dq.fields{2}.start_bit = 24;
WR225E.IRR_dq.fields{2}.bit_length = 16;
WR225E.IRR_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_dq.fields{2}.data_type = 'SIGNED';
WR225E.IRR_dq.fields{2}.maximum = 6.540000e+02;
WR225E.IRR_dq.fields{2}.minimum = -6.540000e+02;
WR225E.IRR_dq.fields{2}.scale = 2.000000e-02;
WR225E.IRR_dq.fields{2}.offset = 0.000000e+00;

WR225E.IRR_dq.fields{3}.name = 'IRR_V_q';
WR225E.IRR_dq.fields{3}.units = 'V';
WR225E.IRR_dq.fields{3}.start_bit = 40;
WR225E.IRR_dq.fields{3}.bit_length = 16;
WR225E.IRR_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_dq.fields{3}.data_type = 'SIGNED';
WR225E.IRR_dq.fields{3}.maximum = 6.540000e+02;
WR225E.IRR_dq.fields{3}.minimum = -6.540000e+02;
WR225E.IRR_dq.fields{3}.scale = 2.000000e-02;
WR225E.IRR_dq.fields{3}.offset = 0.000000e+00;

WR225E.IRR_dq.fields{4}.name = 'IRR_V_d';
WR225E.IRR_dq.fields{4}.units = 'V';
WR225E.IRR_dq.fields{4}.start_bit = 56;
WR225E.IRR_dq.fields{4}.bit_length = 16;
WR225E.IRR_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.IRR_dq.fields{4}.data_type = 'SIGNED';
WR225E.IRR_dq.fields{4}.maximum = 6.540000e+02;
WR225E.IRR_dq.fields{4}.minimum = -6.540000e+02;
WR225E.IRR_dq.fields{4}.scale = 2.000000e-02;
WR225E.IRR_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRRrx_Bootloader'
WR225E.IRRrx_Bootloader = struct;
WR225E.IRRrx_Bootloader.name = 'IRRrx_Bootloader';
WR225E.IRRrx_Bootloader.description = 'IRRrx_Bootloader';
WR225E.IRRrx_Bootloader.protocol = 'WR225E';
WR225E.IRRrx_Bootloader.id = hex2dec('000000B4');
WR225E.IRRrx_Bootloader.idext = 'STANDARD';
WR225E.IRRrx_Bootloader.idinherit = 0;
WR225E.IRRrx_Bootloader.payload_size = 8;
WR225E.IRRrx_Bootloader.payload_value = [];
WR225E.IRRrx_Bootloader.payload_mask = [];
WR225E.IRRrx_Bootloader.interval = 50;

%===================================================================
case 'Inverter_Command'
WR225E.Inverter_Command = struct;
WR225E.Inverter_Command.name = 'Inverter_Command';
WR225E.Inverter_Command.description = 'Commands sent to the inverters. Is addressed with Target. This is not in the normal CAN DAQ section in the ECU code, it is right by the inverter startup stateflow since it doesnt have any other effect than commands to and from the inverter and ECU';
WR225E.Inverter_Command.protocol = 'WR225E';
WR225E.Inverter_Command.id = hex2dec('00000006');
WR225E.Inverter_Command.idext = 'STANDARD';
WR225E.Inverter_Command.idinherit = 0;
WR225E.Inverter_Command.payload_size = 8;
WR225E.Inverter_Command.payload_value = [];
WR225E.Inverter_Command.payload_mask = [];
WR225E.Inverter_Command.interval = 50;

WR225E.Inverter_Command.fields{1}.name = 'Inverter_Command_Target';
WR225E.Inverter_Command.fields{1}.units = '';
WR225E.Inverter_Command.fields{1}.start_bit = 56;
WR225E.Inverter_Command.fields{1}.bit_length = 16;
WR225E.Inverter_Command.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Inverter_Command.fields{1}.data_type = 'UNSIGNED';
WR225E.Inverter_Command.fields{1}.scale = 1.000000e+00;
WR225E.Inverter_Command.fields{1}.offset = 0.000000e+00;

WR225E.Inverter_Command.fields{2}.name = 'Inverter_Command';
WR225E.Inverter_Command.fields{2}.units = '';
WR225E.Inverter_Command.fields{2}.start_bit = 40;
WR225E.Inverter_Command.fields{2}.bit_length = 16;
WR225E.Inverter_Command.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Inverter_Command.fields{2}.data_type = 'UNSIGNED';
WR225E.Inverter_Command.fields{2}.scale = 1.000000e+00;
WR225E.Inverter_Command.fields{2}.offset = 0.000000e+00;

WR225E.Inverter_Command.fields{3}.name = 'Inverter_Command_Value';
WR225E.Inverter_Command.fields{3}.units = '';
WR225E.Inverter_Command.fields{3}.start_bit = 24;
WR225E.Inverter_Command.fields{3}.bit_length = 32;
WR225E.Inverter_Command.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Inverter_Command.fields{3}.data_type = 'UNSIGNED';
WR225E.Inverter_Command.fields{3}.scale = 1.000000e+00;
WR225E.Inverter_Command.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'Motor_Power'
WR225E.Motor_Power = struct;
WR225E.Motor_Power.name = 'Motor_Power';
WR225E.Motor_Power.description = 'How much power the motors are using';
WR225E.Motor_Power.protocol = 'WR225E';
WR225E.Motor_Power.id = hex2dec('00000008');
WR225E.Motor_Power.idext = 'STANDARD';
WR225E.Motor_Power.idinherit = 0;
WR225E.Motor_Power.payload_size = 8;
WR225E.Motor_Power.payload_value = [];
WR225E.Motor_Power.payload_mask = [];
WR225E.Motor_Power.interval = 50;

WR225E.Motor_Power.fields{1}.name = 'Motor_Power_Rear_Right';
WR225E.Motor_Power.fields{1}.units = 'W';
WR225E.Motor_Power.fields{1}.start_bit = 56;
WR225E.Motor_Power.fields{1}.bit_length = 10;
WR225E.Motor_Power.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Motor_Power.fields{1}.data_type = 'SIGNED';
WR225E.Motor_Power.fields{1}.maximum = 4.000000e+01;
WR225E.Motor_Power.fields{1}.minimum = -1.000000e+01;
WR225E.Motor_Power.fields{1}.scale = 1.000000e-01;
WR225E.Motor_Power.fields{1}.offset = 0.000000e+00;

WR225E.Motor_Power.fields{2}.name = 'Motor_Power_Rear_Left';
WR225E.Motor_Power.fields{2}.units = 'W';
WR225E.Motor_Power.fields{2}.start_bit = 50;
WR225E.Motor_Power.fields{2}.bit_length = 10;
WR225E.Motor_Power.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Motor_Power.fields{2}.data_type = 'SIGNED';
WR225E.Motor_Power.fields{2}.maximum = 4.000000e+01;
WR225E.Motor_Power.fields{2}.minimum = -1.000000e+01;
WR225E.Motor_Power.fields{2}.scale = 1.000000e-01;
WR225E.Motor_Power.fields{2}.offset = 0.000000e+00;

WR225E.Motor_Power.fields{3}.name = 'Motor_Power_Front_Right';
WR225E.Motor_Power.fields{3}.units = 'W';
WR225E.Motor_Power.fields{3}.start_bit = 44;
WR225E.Motor_Power.fields{3}.bit_length = 10;
WR225E.Motor_Power.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Motor_Power.fields{3}.data_type = 'SIGNED';
WR225E.Motor_Power.fields{3}.maximum = 4.000000e+01;
WR225E.Motor_Power.fields{3}.minimum = -1.000000e+01;
WR225E.Motor_Power.fields{3}.scale = 1.000000e-01;
WR225E.Motor_Power.fields{3}.offset = 0.000000e+00;

WR225E.Motor_Power.fields{4}.name = 'Motor_Power_Front_Left';
WR225E.Motor_Power.fields{4}.units = 'W';
WR225E.Motor_Power.fields{4}.start_bit = 38;
WR225E.Motor_Power.fields{4}.bit_length = 10;
WR225E.Motor_Power.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Motor_Power.fields{4}.data_type = 'SIGNED';
WR225E.Motor_Power.fields{4}.maximum = 4.000000e+01;
WR225E.Motor_Power.fields{4}.minimum = -1.000000e+01;
WR225E.Motor_Power.fields{4}.scale = 1.000000e-01;
WR225E.Motor_Power.fields{4}.offset = 0.000000e+00;

WR225E.Motor_Power.fields{5}.name = 'Total_Motor_Power_Estimated';
WR225E.Motor_Power.fields{5}.units = 'W';
WR225E.Motor_Power.fields{5}.start_bit = 16;
WR225E.Motor_Power.fields{5}.bit_length = 11;
WR225E.Motor_Power.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.Motor_Power.fields{5}.data_type = 'SIGNED';
WR225E.Motor_Power.fields{5}.maximum = 8.000000e+01;
WR225E.Motor_Power.fields{5}.minimum = -4.000000e+01;
WR225E.Motor_Power.fields{5}.scale = 1.000000e-01;
WR225E.Motor_Power.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_States'
WR225E.PCM_States = struct;
WR225E.PCM_States.name = 'PCM_States';
WR225E.PCM_States.description = 'All of the states that the PCM is ';
WR225E.PCM_States.protocol = 'WR225E';
WR225E.PCM_States.id = hex2dec('00000003');
WR225E.PCM_States.idext = 'STANDARD';
WR225E.PCM_States.idinherit = 0;
WR225E.PCM_States.payload_size = 8;
WR225E.PCM_States.payload_value = [];
WR225E.PCM_States.payload_mask = [];
WR225E.PCM_States.interval = 50;

WR225E.PCM_States.fields{1}.name = 'Pump_PWM';
WR225E.PCM_States.fields{1}.units = '%';
WR225E.PCM_States.fields{1}.start_bit = 56;
WR225E.PCM_States.fields{1}.bit_length = 10;
WR225E.PCM_States.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{1}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{1}.maximum = 1.000000e+02;
WR225E.PCM_States.fields{1}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{1}.scale = 1.000000e-01;
WR225E.PCM_States.fields{1}.offset = 0.000000e+00;

WR225E.PCM_States.fields{2}.name = 'Current_Offset_State';
WR225E.PCM_States.fields{2}.units = 'index';
WR225E.PCM_States.fields{2}.start_bit = 50;
WR225E.PCM_States.fields{2}.bit_length = 4;
WR225E.PCM_States.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{2}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{2}.maximum = 1.000000e+01;
WR225E.PCM_States.fields{2}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{2}.scale = 1.000000e+00;
WR225E.PCM_States.fields{2}.offset = 0.000000e+00;

WR225E.PCM_States.fields{3}.name = 'Regen_Enabled';
WR225E.PCM_States.fields{3}.units = 'flag';
WR225E.PCM_States.fields{3}.start_bit = 54;
WR225E.PCM_States.fields{3}.bit_length = 1;
WR225E.PCM_States.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{3}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{3}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{3}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{3}.scale = 1.000000e+00;
WR225E.PCM_States.fields{3}.offset = 0.000000e+00;

WR225E.PCM_States.fields{4}.name = 'Motors_Enabled';
WR225E.PCM_States.fields{4}.units = 'flag';
WR225E.PCM_States.fields{4}.start_bit = 55;
WR225E.PCM_States.fields{4}.bit_length = 1;
WR225E.PCM_States.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{4}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{4}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{4}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{4}.scale = 1.000000e+00;
WR225E.PCM_States.fields{4}.offset = 0.000000e+00;

WR225E.PCM_States.fields{5}.name = 'RTD_Enabled';
WR225E.PCM_States.fields{5}.units = 'flag';
WR225E.PCM_States.fields{5}.start_bit = 40;
WR225E.PCM_States.fields{5}.bit_length = 1;
WR225E.PCM_States.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{5}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{5}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{5}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{5}.scale = 1.000000e+00;
WR225E.PCM_States.fields{5}.offset = 0.000000e+00;

WR225E.PCM_States.fields{6}.name = 'SDC_Break';
WR225E.PCM_States.fields{6}.units = 'flag';
WR225E.PCM_States.fields{6}.start_bit = 41;
WR225E.PCM_States.fields{6}.bit_length = 1;
WR225E.PCM_States.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{6}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{6}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{6}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{6}.scale = 1.000000e+00;
WR225E.PCM_States.fields{6}.offset = 0.000000e+00;

WR225E.PCM_States.fields{7}.name = 'Speed_Limit_Enabled';
WR225E.PCM_States.fields{7}.units = 'flag';
WR225E.PCM_States.fields{7}.start_bit = 42;
WR225E.PCM_States.fields{7}.bit_length = 1;
WR225E.PCM_States.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{7}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{7}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{7}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{7}.scale = 1.000000e+00;
WR225E.PCM_States.fields{7}.offset = 0.000000e+00;

WR225E.PCM_States.fields{8}.name = 'Limp_Mode_Enabled';
WR225E.PCM_States.fields{8}.units = 'flag';
WR225E.PCM_States.fields{8}.start_bit = 43;
WR225E.PCM_States.fields{8}.bit_length = 1;
WR225E.PCM_States.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{8}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{8}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{8}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{8}.scale = 1.000000e+00;
WR225E.PCM_States.fields{8}.offset = 0.000000e+00;

WR225E.PCM_States.fields{9}.name = 'Current_Offset_Timeout';
WR225E.PCM_States.fields{9}.units = 'flag';
WR225E.PCM_States.fields{9}.start_bit = 44;
WR225E.PCM_States.fields{9}.bit_length = 1;
WR225E.PCM_States.fields{9}.byte_order = 'LITTLE_ENDIAN';
WR225E.PCM_States.fields{9}.data_type = 'UNSIGNED';
WR225E.PCM_States.fields{9}.maximum = 1.000000e+00;
WR225E.PCM_States.fields{9}.minimum = 0.000000e+00;
WR225E.PCM_States.fields{9}.scale = 1.000000e+00;
WR225E.PCM_States.fields{9}.offset = 0.000000e+00;

%===================================================================
case 'Power_Limits'
WR225E.Power_Limits = struct;
WR225E.Power_Limits.name = 'Power_Limits';
WR225E.Power_Limits.description = 'Limits from the PCM on power, torque, etc. not including traction control stuff since there would be too much. also not flags, just the actual limits';
WR225E.Power_Limits.protocol = 'WR225E';
WR225E.Power_Limits.id = hex2dec('00000007');
WR225E.Power_Limits.idext = 'STANDARD';
WR225E.Power_Limits.idinherit = 0;
WR225E.Power_Limits.payload_size = 8;
WR225E.Power_Limits.payload_value = [];
WR225E.Power_Limits.payload_mask = [];
WR225E.Power_Limits.interval = 50;

WR225E.Power_Limits.fields{1}.name = 'Charge_Power_Limit';
WR225E.Power_Limits.fields{1}.units = 'kW';
WR225E.Power_Limits.fields{1}.start_bit = 56;
WR225E.Power_Limits.fields{1}.bit_length = 10;
WR225E.Power_Limits.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Power_Limits.fields{1}.data_type = 'UNSIGNED';
WR225E.Power_Limits.fields{1}.maximum = 4.000000e+01;
WR225E.Power_Limits.fields{1}.minimum = 0.000000e+00;
WR225E.Power_Limits.fields{1}.scale = 1.000000e-01;
WR225E.Power_Limits.fields{1}.offset = 0.000000e+00;

WR225E.Power_Limits.fields{2}.name = 'Discharge_Power_Limit';
WR225E.Power_Limits.fields{2}.units = 'kW';
WR225E.Power_Limits.fields{2}.start_bit = 50;
WR225E.Power_Limits.fields{2}.bit_length = 10;
WR225E.Power_Limits.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Power_Limits.fields{2}.data_type = 'UNSIGNED';
WR225E.Power_Limits.fields{2}.maximum = 8.000000e+01;
WR225E.Power_Limits.fields{2}.minimum = 0.000000e+00;
WR225E.Power_Limits.fields{2}.scale = 1.000000e-01;
WR225E.Power_Limits.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Acceleration'
WR225E.SBG_Acceleration = struct;
WR225E.SBG_Acceleration.name = 'SBG_Acceleration';
WR225E.SBG_Acceleration.description = 'SBG_Acceleration';
WR225E.SBG_Acceleration.protocol = 'WR225E';
WR225E.SBG_Acceleration.id = hex2dec('00000121');
WR225E.SBG_Acceleration.idext = 'STANDARD';
WR225E.SBG_Acceleration.idinherit = 0;
WR225E.SBG_Acceleration.payload_size = 7;
WR225E.SBG_Acceleration.payload_value = [];
WR225E.SBG_Acceleration.payload_mask = [];
WR225E.SBG_Acceleration.interval = 50;

WR225E.SBG_Acceleration.fields{1}.name = 'SBG_AccelZ';
WR225E.SBG_Acceleration.fields{1}.units = 'm/s2';
WR225E.SBG_Acceleration.fields{1}.start_bit = 24;
WR225E.SBG_Acceleration.fields{1}.bit_length = 16;
WR225E.SBG_Acceleration.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Acceleration.fields{1}.data_type = 'SIGNED';
WR225E.SBG_Acceleration.fields{1}.maximum = 1.000000e+02;
WR225E.SBG_Acceleration.fields{1}.minimum = 0.000000e+00;
WR225E.SBG_Acceleration.fields{1}.scale = 1.000000e-02;
WR225E.SBG_Acceleration.fields{1}.offset = 0.000000e+00;

WR225E.SBG_Acceleration.fields{2}.name = 'SBG_AccelY';
WR225E.SBG_Acceleration.fields{2}.units = 'm/s2';
WR225E.SBG_Acceleration.fields{2}.start_bit = 40;
WR225E.SBG_Acceleration.fields{2}.bit_length = 16;
WR225E.SBG_Acceleration.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Acceleration.fields{2}.data_type = 'SIGNED';
WR225E.SBG_Acceleration.fields{2}.maximum = 1.000000e+02;
WR225E.SBG_Acceleration.fields{2}.minimum = 0.000000e+00;
WR225E.SBG_Acceleration.fields{2}.scale = 1.000000e-02;
WR225E.SBG_Acceleration.fields{2}.offset = 0.000000e+00;

WR225E.SBG_Acceleration.fields{3}.name = 'SBG_AccelX';
WR225E.SBG_Acceleration.fields{3}.units = 'm/s2';
WR225E.SBG_Acceleration.fields{3}.start_bit = 56;
WR225E.SBG_Acceleration.fields{3}.bit_length = 16;
WR225E.SBG_Acceleration.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Acceleration.fields{3}.data_type = 'SIGNED';
WR225E.SBG_Acceleration.fields{3}.maximum = 1.000000e+02;
WR225E.SBG_Acceleration.fields{3}.minimum = 0.000000e+00;
WR225E.SBG_Acceleration.fields{3}.scale = 1.000000e-02;
WR225E.SBG_Acceleration.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Body_Vel'
WR225E.SBG_Body_Vel = struct;
WR225E.SBG_Body_Vel.name = 'SBG_Body_Vel';
WR225E.SBG_Body_Vel.description = 'SBG_Body_Vel';
WR225E.SBG_Body_Vel.protocol = 'WR225E';
WR225E.SBG_Body_Vel.id = hex2dec('00000139');
WR225E.SBG_Body_Vel.idext = 'STANDARD';
WR225E.SBG_Body_Vel.idinherit = 0;
WR225E.SBG_Body_Vel.payload_size = 6;
WR225E.SBG_Body_Vel.payload_value = [];
WR225E.SBG_Body_Vel.payload_mask = [];
WR225E.SBG_Body_Vel.interval = 50;

WR225E.SBG_Body_Vel.fields{1}.name = 'Vel_X';
WR225E.SBG_Body_Vel.fields{1}.units = 'm/s';
WR225E.SBG_Body_Vel.fields{1}.start_bit = 56;
WR225E.SBG_Body_Vel.fields{1}.bit_length = 16;
WR225E.SBG_Body_Vel.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Body_Vel.fields{1}.data_type = 'SIGNED';
WR225E.SBG_Body_Vel.fields{1}.maximum = 3.270000e+02;
WR225E.SBG_Body_Vel.fields{1}.minimum = -3.270000e+02;
WR225E.SBG_Body_Vel.fields{1}.scale = 1.000000e-02;
WR225E.SBG_Body_Vel.fields{1}.offset = 0.000000e+00;

WR225E.SBG_Body_Vel.fields{2}.name = 'Vel_Y';
WR225E.SBG_Body_Vel.fields{2}.units = 'm/s';
WR225E.SBG_Body_Vel.fields{2}.start_bit = 40;
WR225E.SBG_Body_Vel.fields{2}.bit_length = 16;
WR225E.SBG_Body_Vel.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Body_Vel.fields{2}.data_type = 'SIGNED';
WR225E.SBG_Body_Vel.fields{2}.maximum = 3.270000e+02;
WR225E.SBG_Body_Vel.fields{2}.minimum = -3.270000e+02;
WR225E.SBG_Body_Vel.fields{2}.scale = 1.000000e-02;
WR225E.SBG_Body_Vel.fields{2}.offset = 0.000000e+00;

WR225E.SBG_Body_Vel.fields{3}.name = 'Vel_z';
WR225E.SBG_Body_Vel.fields{3}.units = 'm/s';
WR225E.SBG_Body_Vel.fields{3}.start_bit = 24;
WR225E.SBG_Body_Vel.fields{3}.bit_length = 16;
WR225E.SBG_Body_Vel.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Body_Vel.fields{3}.data_type = 'SIGNED';
WR225E.SBG_Body_Vel.fields{3}.maximum = 3.270000e+02;
WR225E.SBG_Body_Vel.fields{3}.minimum = -3.270000e+02;
WR225E.SBG_Body_Vel.fields{3}.scale = 1.000000e-02;
WR225E.SBG_Body_Vel.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFAltitude'
WR225E.SBG_EKFAltitude = struct;
WR225E.SBG_EKFAltitude.name = 'SBG_EKFAltitude';
WR225E.SBG_EKFAltitude.description = 'SBG_EKFAltitude';
WR225E.SBG_EKFAltitude.protocol = 'WR225E';
WR225E.SBG_EKFAltitude.id = hex2dec('00000135');
WR225E.SBG_EKFAltitude.idext = 'STANDARD';
WR225E.SBG_EKFAltitude.idinherit = 0;
WR225E.SBG_EKFAltitude.payload_size = 6;
WR225E.SBG_EKFAltitude.payload_value = [];
WR225E.SBG_EKFAltitude.payload_mask = [];
WR225E.SBG_EKFAltitude.interval = 50;

WR225E.SBG_EKFAltitude.fields{1}.name = 'SBG_Undulation';
WR225E.SBG_EKFAltitude.fields{1}.units = '';
WR225E.SBG_EKFAltitude.fields{1}.start_bit = 24;
WR225E.SBG_EKFAltitude.fields{1}.bit_length = 16;
WR225E.SBG_EKFAltitude.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFAltitude.fields{1}.data_type = 'SIGNED';
WR225E.SBG_EKFAltitude.fields{1}.scale = 5.000000e-03;
WR225E.SBG_EKFAltitude.fields{1}.offset = 0.000000e+00;

WR225E.SBG_EKFAltitude.fields{2}.name = 'SBG_Altitude';
WR225E.SBG_EKFAltitude.fields{2}.units = 'm';
WR225E.SBG_EKFAltitude.fields{2}.start_bit = 56;
WR225E.SBG_EKFAltitude.fields{2}.bit_length = 32;
WR225E.SBG_EKFAltitude.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFAltitude.fields{2}.data_type = 'SIGNED';
WR225E.SBG_EKFAltitude.fields{2}.scale = 1.000000e-03;
WR225E.SBG_EKFAltitude.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFPosition'
WR225E.SBG_EKFPosition = struct;
WR225E.SBG_EKFPosition.name = 'SBG_EKFPosition';
WR225E.SBG_EKFPosition.description = 'SBG_EKFPosition';
WR225E.SBG_EKFPosition.protocol = 'WR225E';
WR225E.SBG_EKFPosition.id = hex2dec('00000134');
WR225E.SBG_EKFPosition.idext = 'STANDARD';
WR225E.SBG_EKFPosition.idinherit = 0;
WR225E.SBG_EKFPosition.payload_size = 8;
WR225E.SBG_EKFPosition.payload_value = [];
WR225E.SBG_EKFPosition.payload_mask = [];
WR225E.SBG_EKFPosition.interval = 50;

WR225E.SBG_EKFPosition.fields{1}.name = 'SBG_EKFLongitude';
WR225E.SBG_EKFPosition.fields{1}.units = '';
WR225E.SBG_EKFPosition.fields{1}.start_bit = 24;
WR225E.SBG_EKFPosition.fields{1}.bit_length = 32;
WR225E.SBG_EKFPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFPosition.fields{1}.data_type = 'SIGNED';
WR225E.SBG_EKFPosition.fields{1}.scale = 1.000000e-07;
WR225E.SBG_EKFPosition.fields{1}.offset = 0.000000e+00;

WR225E.SBG_EKFPosition.fields{2}.name = 'SBG_EKFLatitude';
WR225E.SBG_EKFPosition.fields{2}.units = '';
WR225E.SBG_EKFPosition.fields{2}.start_bit = 56;
WR225E.SBG_EKFPosition.fields{2}.bit_length = 32;
WR225E.SBG_EKFPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFPosition.fields{2}.data_type = 'SIGNED';
WR225E.SBG_EKFPosition.fields{2}.scale = 1.000000e-07;
WR225E.SBG_EKFPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFVelocity'
WR225E.SBG_EKFVelocity = struct;
WR225E.SBG_EKFVelocity.name = 'SBG_EKFVelocity';
WR225E.SBG_EKFVelocity.description = 'SBG_EKFVelocity';
WR225E.SBG_EKFVelocity.protocol = 'WR225E';
WR225E.SBG_EKFVelocity.id = hex2dec('00000137');
WR225E.SBG_EKFVelocity.idext = 'STANDARD';
WR225E.SBG_EKFVelocity.idinherit = 0;
WR225E.SBG_EKFVelocity.payload_size = 6;
WR225E.SBG_EKFVelocity.payload_value = [];
WR225E.SBG_EKFVelocity.payload_mask = [];
WR225E.SBG_EKFVelocity.interval = 50;

WR225E.SBG_EKFVelocity.fields{1}.name = 'SBG_VelocityZ';
WR225E.SBG_EKFVelocity.fields{1}.units = 'm/s';
WR225E.SBG_EKFVelocity.fields{1}.start_bit = 24;
WR225E.SBG_EKFVelocity.fields{1}.bit_length = 16;
WR225E.SBG_EKFVelocity.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFVelocity.fields{1}.data_type = 'SIGNED';
WR225E.SBG_EKFVelocity.fields{1}.maximum = 3.270000e+02;
WR225E.SBG_EKFVelocity.fields{1}.minimum = -3.270000e+02;
WR225E.SBG_EKFVelocity.fields{1}.scale = 1.000000e-02;
WR225E.SBG_EKFVelocity.fields{1}.offset = 0.000000e+00;

WR225E.SBG_EKFVelocity.fields{2}.name = 'SBG_VelocityY';
WR225E.SBG_EKFVelocity.fields{2}.units = 'm/s';
WR225E.SBG_EKFVelocity.fields{2}.start_bit = 40;
WR225E.SBG_EKFVelocity.fields{2}.bit_length = 16;
WR225E.SBG_EKFVelocity.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFVelocity.fields{2}.data_type = 'SIGNED';
WR225E.SBG_EKFVelocity.fields{2}.maximum = 3.270000e+02;
WR225E.SBG_EKFVelocity.fields{2}.minimum = -3.270000e+02;
WR225E.SBG_EKFVelocity.fields{2}.scale = 1.000000e-02;
WR225E.SBG_EKFVelocity.fields{2}.offset = 0.000000e+00;

WR225E.SBG_EKFVelocity.fields{3}.name = 'SBG_VelocityX';
WR225E.SBG_EKFVelocity.fields{3}.units = 'm/s';
WR225E.SBG_EKFVelocity.fields{3}.start_bit = 56;
WR225E.SBG_EKFVelocity.fields{3}.bit_length = 16;
WR225E.SBG_EKFVelocity.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_EKFVelocity.fields{3}.data_type = 'SIGNED';
WR225E.SBG_EKFVelocity.fields{3}.maximum = 3.270000e+02;
WR225E.SBG_EKFVelocity.fields{3}.minimum = -3.270000e+02;
WR225E.SBG_EKFVelocity.fields{3}.scale = 1.000000e-02;
WR225E.SBG_EKFVelocity.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_GPSPosition'
WR225E.SBG_GPSPosition = struct;
WR225E.SBG_GPSPosition.name = 'SBG_GPSPosition';
WR225E.SBG_GPSPosition.description = 'SBG_GPSPosition';
WR225E.SBG_GPSPosition.protocol = 'WR225E';
WR225E.SBG_GPSPosition.id = hex2dec('00000175');
WR225E.SBG_GPSPosition.idext = 'STANDARD';
WR225E.SBG_GPSPosition.idinherit = 0;
WR225E.SBG_GPSPosition.payload_size = 8;
WR225E.SBG_GPSPosition.payload_value = [];
WR225E.SBG_GPSPosition.payload_mask = [];
WR225E.SBG_GPSPosition.interval = 50;

WR225E.SBG_GPSPosition.fields{1}.name = 'SBG_GPSLongitude';
WR225E.SBG_GPSPosition.fields{1}.units = '';
WR225E.SBG_GPSPosition.fields{1}.start_bit = 24;
WR225E.SBG_GPSPosition.fields{1}.bit_length = 32;
WR225E.SBG_GPSPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_GPSPosition.fields{1}.data_type = 'SIGNED';
WR225E.SBG_GPSPosition.fields{1}.scale = 1.000000e-07;
WR225E.SBG_GPSPosition.fields{1}.offset = 0.000000e+00;

WR225E.SBG_GPSPosition.fields{2}.name = 'SBG_GPSLatitude';
WR225E.SBG_GPSPosition.fields{2}.units = '';
WR225E.SBG_GPSPosition.fields{2}.start_bit = 56;
WR225E.SBG_GPSPosition.fields{2}.bit_length = 32;
WR225E.SBG_GPSPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_GPSPosition.fields{2}.data_type = 'SIGNED';
WR225E.SBG_GPSPosition.fields{2}.scale = 1.000000e-07;
WR225E.SBG_GPSPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Gyro'
WR225E.SBG_Gyro = struct;
WR225E.SBG_Gyro.name = 'SBG_Gyro';
WR225E.SBG_Gyro.description = 'SBG_Gyro';
WR225E.SBG_Gyro.protocol = 'WR225E';
WR225E.SBG_Gyro.id = hex2dec('00000122');
WR225E.SBG_Gyro.idext = 'STANDARD';
WR225E.SBG_Gyro.idinherit = 0;
WR225E.SBG_Gyro.payload_size = 7;
WR225E.SBG_Gyro.payload_value = [];
WR225E.SBG_Gyro.payload_mask = [];
WR225E.SBG_Gyro.interval = 50;

WR225E.SBG_Gyro.fields{1}.name = 'SBG_YawRateZ';
WR225E.SBG_Gyro.fields{1}.units = 'deg/s';
WR225E.SBG_Gyro.fields{1}.start_bit = 24;
WR225E.SBG_Gyro.fields{1}.bit_length = 16;
WR225E.SBG_Gyro.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Gyro.fields{1}.data_type = 'SIGNED';
WR225E.SBG_Gyro.fields{1}.maximum = 1.877411e+03;
WR225E.SBG_Gyro.fields{1}.minimum = -1.877469e+03;
WR225E.SBG_Gyro.fields{1}.scale = 1.000000e-03;
WR225E.SBG_Gyro.fields{1}.offset = 0.000000e+00;

WR225E.SBG_Gyro.fields{2}.name = 'SBG_YawRateY';
WR225E.SBG_Gyro.fields{2}.units = 'deg/s';
WR225E.SBG_Gyro.fields{2}.start_bit = 40;
WR225E.SBG_Gyro.fields{2}.bit_length = 16;
WR225E.SBG_Gyro.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Gyro.fields{2}.data_type = 'SIGNED';
WR225E.SBG_Gyro.fields{2}.maximum = 1.877411e+03;
WR225E.SBG_Gyro.fields{2}.minimum = -1.877469e+03;
WR225E.SBG_Gyro.fields{2}.scale = 1.000000e-03;
WR225E.SBG_Gyro.fields{2}.offset = 0.000000e+00;

WR225E.SBG_Gyro.fields{3}.name = 'SBG_YawRateX';
WR225E.SBG_Gyro.fields{3}.units = 'deg/s';
WR225E.SBG_Gyro.fields{3}.start_bit = 56;
WR225E.SBG_Gyro.fields{3}.bit_length = 16;
WR225E.SBG_Gyro.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Gyro.fields{3}.data_type = 'SIGNED';
WR225E.SBG_Gyro.fields{3}.maximum = 1.877411e+03;
WR225E.SBG_Gyro.fields{3}.minimum = -1.877469e+03;
WR225E.SBG_Gyro.fields{3}.scale = 1.000000e-03;
WR225E.SBG_Gyro.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Heading'
WR225E.SBG_Heading = struct;
WR225E.SBG_Heading.name = 'SBG_Heading';
WR225E.SBG_Heading.description = 'SBG_Heading';
WR225E.SBG_Heading.protocol = 'WR225E';
WR225E.SBG_Heading.id = hex2dec('00000173');
WR225E.SBG_Heading.idext = 'STANDARD';
WR225E.SBG_Heading.idinherit = 0;
WR225E.SBG_Heading.payload_size = 3;
WR225E.SBG_Heading.payload_value = [];
WR225E.SBG_Heading.payload_mask = [];
WR225E.SBG_Heading.interval = 50;

WR225E.SBG_Heading.fields{1}.name = 'SBG_Heading';
WR225E.SBG_Heading.fields{1}.units = 'deg';
WR225E.SBG_Heading.fields{1}.start_bit = 56;
WR225E.SBG_Heading.fields{1}.bit_length = 16;
WR225E.SBG_Heading.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_Heading.fields{1}.data_type = 'UNSIGNED';
WR225E.SBG_Heading.fields{1}.maximum = 6.550000e+02;
WR225E.SBG_Heading.fields{1}.minimum = 0.000000e+00;
WR225E.SBG_Heading.fields{1}.scale = 1.000000e-02;
WR225E.SBG_Heading.fields{1}.offset = 0.000000e+00;

%===================================================================
case 'SBG_RollYawPitch'
WR225E.SBG_RollYawPitch = struct;
WR225E.SBG_RollYawPitch.name = 'SBG_RollYawPitch';
WR225E.SBG_RollYawPitch.description = 'SBG_RollYawPitch';
WR225E.SBG_RollYawPitch.protocol = 'WR225E';
WR225E.SBG_RollYawPitch.id = hex2dec('00000132');
WR225E.SBG_RollYawPitch.idext = 'STANDARD';
WR225E.SBG_RollYawPitch.idinherit = 0;
WR225E.SBG_RollYawPitch.payload_size = 7;
WR225E.SBG_RollYawPitch.payload_value = [];
WR225E.SBG_RollYawPitch.payload_mask = [];
WR225E.SBG_RollYawPitch.interval = 50;

WR225E.SBG_RollYawPitch.fields{1}.name = 'SBG_Yaw';
WR225E.SBG_RollYawPitch.fields{1}.units = 'rad';
WR225E.SBG_RollYawPitch.fields{1}.start_bit = 24;
WR225E.SBG_RollYawPitch.fields{1}.bit_length = 16;
WR225E.SBG_RollYawPitch.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_RollYawPitch.fields{1}.data_type = 'SIGNED';
WR225E.SBG_RollYawPitch.fields{1}.maximum = 1.877411e+03;
WR225E.SBG_RollYawPitch.fields{1}.minimum = -1.877469e+03;
WR225E.SBG_RollYawPitch.fields{1}.scale = 1.000000e-04;
WR225E.SBG_RollYawPitch.fields{1}.offset = 0.000000e+00;

WR225E.SBG_RollYawPitch.fields{2}.name = 'SBG_Roll';
WR225E.SBG_RollYawPitch.fields{2}.units = 'rad';
WR225E.SBG_RollYawPitch.fields{2}.start_bit = 56;
WR225E.SBG_RollYawPitch.fields{2}.bit_length = 16;
WR225E.SBG_RollYawPitch.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_RollYawPitch.fields{2}.data_type = 'SIGNED';
WR225E.SBG_RollYawPitch.fields{2}.maximum = 1.877411e+03;
WR225E.SBG_RollYawPitch.fields{2}.minimum = -1.877469e+03;
WR225E.SBG_RollYawPitch.fields{2}.scale = 1.000000e-04;
WR225E.SBG_RollYawPitch.fields{2}.offset = 0.000000e+00;

WR225E.SBG_RollYawPitch.fields{3}.name = 'SBG_Pitch';
WR225E.SBG_RollYawPitch.fields{3}.units = 'rad';
WR225E.SBG_RollYawPitch.fields{3}.start_bit = 40;
WR225E.SBG_RollYawPitch.fields{3}.bit_length = 16;
WR225E.SBG_RollYawPitch.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_RollYawPitch.fields{3}.data_type = 'SIGNED';
WR225E.SBG_RollYawPitch.fields{3}.maximum = 1.877411e+03;
WR225E.SBG_RollYawPitch.fields{3}.minimum = -1.877469e+03;
WR225E.SBG_RollYawPitch.fields{3}.scale = 1.000000e-04;
WR225E.SBG_RollYawPitch.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_VelocityInfo'
WR225E.SBG_VelocityInfo = struct;
WR225E.SBG_VelocityInfo.name = 'SBG_VelocityInfo';
WR225E.SBG_VelocityInfo.description = 'SBG_VelocityInfo';
WR225E.SBG_VelocityInfo.protocol = 'WR225E';
WR225E.SBG_VelocityInfo.id = hex2dec('00000172');
WR225E.SBG_VelocityInfo.idext = 'STANDARD';
WR225E.SBG_VelocityInfo.idinherit = 0;
WR225E.SBG_VelocityInfo.payload_size = 8;
WR225E.SBG_VelocityInfo.payload_value = [];
WR225E.SBG_VelocityInfo.payload_mask = [];
WR225E.SBG_VelocityInfo.interval = 50;

WR225E.SBG_VelocityInfo.fields{1}.name = 'SBG_VelocityType';
WR225E.SBG_VelocityInfo.fields{1}.units = '';
WR225E.SBG_VelocityInfo.fields{1}.start_bit = 30;
WR225E.SBG_VelocityInfo.fields{1}.bit_length = 6;
WR225E.SBG_VelocityInfo.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_VelocityInfo.fields{1}.data_type = 'UNSIGNED';
WR225E.SBG_VelocityInfo.fields{1}.maximum = 6.300000e+01;
WR225E.SBG_VelocityInfo.fields{1}.minimum = 0.000000e+00;
WR225E.SBG_VelocityInfo.fields{1}.scale = 1.000000e+00;
WR225E.SBG_VelocityInfo.fields{1}.offset = 0.000000e+00;

WR225E.SBG_VelocityInfo.fields{2}.name = 'SBG_VelocityStatus';
WR225E.SBG_VelocityInfo.fields{2}.units = '';
WR225E.SBG_VelocityInfo.fields{2}.start_bit = 24;
WR225E.SBG_VelocityInfo.fields{2}.bit_length = 6;
WR225E.SBG_VelocityInfo.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SBG_VelocityInfo.fields{2}.data_type = 'UNSIGNED';
WR225E.SBG_VelocityInfo.fields{2}.maximum = 6.300000e+01;
WR225E.SBG_VelocityInfo.fields{2}.minimum = 0.000000e+00;
WR225E.SBG_VelocityInfo.fields{2}.scale = 1.000000e+00;
WR225E.SBG_VelocityInfo.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Current2'
WR225E.SPDU_Current2 = struct;
WR225E.SPDU_Current2.name = 'SPDU_Current2';
WR225E.SPDU_Current2.description = 'SPDU_Current2';
WR225E.SPDU_Current2.protocol = 'WR225E';
WR225E.SPDU_Current2.id = hex2dec('00000131');
WR225E.SPDU_Current2.idext = 'STANDARD';
WR225E.SPDU_Current2.idinherit = 0;
WR225E.SPDU_Current2.payload_size = 8;
WR225E.SPDU_Current2.payload_value = [];
WR225E.SPDU_Current2.payload_mask = [];
WR225E.SPDU_Current2.interval = 50;

WR225E.SPDU_Current2.fields{1}.name = 'SPDU_Current_eFuse6';
WR225E.SPDU_Current2.fields{1}.units = '';
WR225E.SPDU_Current2.fields{1}.start_bit = 8;
WR225E.SPDU_Current2.fields{1}.bit_length = 16;
WR225E.SPDU_Current2.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Current2.fields{1}.data_type = 'SIGNED';
WR225E.SPDU_Current2.fields{1}.scale = 1.000000e-03;
WR225E.SPDU_Current2.fields{1}.offset = 0.000000e+00;

WR225E.SPDU_Current2.fields{2}.name = 'SPDU_Current_eFuse5';
WR225E.SPDU_Current2.fields{2}.units = '';
WR225E.SPDU_Current2.fields{2}.start_bit = 24;
WR225E.SPDU_Current2.fields{2}.bit_length = 16;
WR225E.SPDU_Current2.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Current2.fields{2}.data_type = 'SIGNED';
WR225E.SPDU_Current2.fields{2}.scale = 1.000000e-03;
WR225E.SPDU_Current2.fields{2}.offset = 0.000000e+00;

WR225E.SPDU_Current2.fields{3}.name = 'SPDU_Current_eFuse4';
WR225E.SPDU_Current2.fields{3}.units = '';
WR225E.SPDU_Current2.fields{3}.start_bit = 40;
WR225E.SPDU_Current2.fields{3}.bit_length = 16;
WR225E.SPDU_Current2.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Current2.fields{3}.data_type = 'SIGNED';
WR225E.SPDU_Current2.fields{3}.scale = 1.000000e-03;
WR225E.SPDU_Current2.fields{3}.offset = 0.000000e+00;

WR225E.SPDU_Current2.fields{4}.name = 'SPDU_Current_eFuse3';
WR225E.SPDU_Current2.fields{4}.units = '';
WR225E.SPDU_Current2.fields{4}.start_bit = 56;
WR225E.SPDU_Current2.fields{4}.bit_length = 16;
WR225E.SPDU_Current2.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Current2.fields{4}.data_type = 'SIGNED';
WR225E.SPDU_Current2.fields{4}.scale = 1.000000e-03;
WR225E.SPDU_Current2.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Faults'
WR225E.SPDU_Faults = struct;
WR225E.SPDU_Faults.name = 'SPDU_Faults';
WR225E.SPDU_Faults.description = 'SPDU_Faults';
WR225E.SPDU_Faults.protocol = 'WR225E';
WR225E.SPDU_Faults.id = hex2dec('00000033');
WR225E.SPDU_Faults.idext = 'STANDARD';
WR225E.SPDU_Faults.idinherit = 0;
WR225E.SPDU_Faults.payload_size = 1;
WR225E.SPDU_Faults.payload_value = [];
WR225E.SPDU_Faults.payload_mask = [];
WR225E.SPDU_Faults.interval = 50;

WR225E.SPDU_Faults.fields{1}.name = 'SPDU_Fault_eFuse6';
WR225E.SPDU_Faults.fields{1}.units = '';
WR225E.SPDU_Faults.fields{1}.start_bit = 63;
WR225E.SPDU_Faults.fields{1}.bit_length = 1;
WR225E.SPDU_Faults.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{1}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{1}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{1}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{2}.name = 'SPDU_Fault_eFuse5';
WR225E.SPDU_Faults.fields{2}.units = '';
WR225E.SPDU_Faults.fields{2}.start_bit = 62;
WR225E.SPDU_Faults.fields{2}.bit_length = 1;
WR225E.SPDU_Faults.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{2}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{2}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{2}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{3}.name = 'SPDU_Fault_eFuse4';
WR225E.SPDU_Faults.fields{3}.units = '';
WR225E.SPDU_Faults.fields{3}.start_bit = 61;
WR225E.SPDU_Faults.fields{3}.bit_length = 1;
WR225E.SPDU_Faults.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{3}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{3}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{3}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{4}.name = 'SPDU_Fault_eFuse3';
WR225E.SPDU_Faults.fields{4}.units = '';
WR225E.SPDU_Faults.fields{4}.start_bit = 60;
WR225E.SPDU_Faults.fields{4}.bit_length = 1;
WR225E.SPDU_Faults.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{4}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{4}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{4}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{5}.name = 'SPDU_Fault_eFuse2';
WR225E.SPDU_Faults.fields{5}.units = '';
WR225E.SPDU_Faults.fields{5}.start_bit = 59;
WR225E.SPDU_Faults.fields{5}.bit_length = 1;
WR225E.SPDU_Faults.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{5}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{5}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{5}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{6}.name = 'SPDU_Fault_eFuse1';
WR225E.SPDU_Faults.fields{6}.units = '';
WR225E.SPDU_Faults.fields{6}.start_bit = 58;
WR225E.SPDU_Faults.fields{6}.bit_length = 1;
WR225E.SPDU_Faults.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{6}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{6}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{6}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{7}.name = 'SPDU_Fault_AH2';
WR225E.SPDU_Faults.fields{7}.units = '';
WR225E.SPDU_Faults.fields{7}.start_bit = 57;
WR225E.SPDU_Faults.fields{7}.bit_length = 1;
WR225E.SPDU_Faults.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{7}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{7}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{7}.offset = 0.000000e+00;

WR225E.SPDU_Faults.fields{8}.name = 'SPDU_Fault_AH1';
WR225E.SPDU_Faults.fields{8}.units = '';
WR225E.SPDU_Faults.fields{8}.start_bit = 56;
WR225E.SPDU_Faults.fields{8}.bit_length = 1;
WR225E.SPDU_Faults.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR225E.SPDU_Faults.fields{8}.data_type = 'SIGNED';
WR225E.SPDU_Faults.fields{8}.scale = 1.000000e+00;
WR225E.SPDU_Faults.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'SW_Inputs'
WR225E.SW_Inputs = struct;
WR225E.SW_Inputs.name = 'SW_Inputs';
WR225E.SW_Inputs.description = 'SW_Inputs';
WR225E.SW_Inputs.protocol = 'WR225E';
WR225E.SW_Inputs.id = hex2dec('000000D5');
WR225E.SW_Inputs.idext = 'STANDARD';
WR225E.SW_Inputs.idinherit = 0;
WR225E.SW_Inputs.payload_size = 8;
WR225E.SW_Inputs.payload_value = [];
WR225E.SW_Inputs.payload_mask = [];
WR225E.SW_Inputs.interval = 50;

WR225E.SW_Inputs.fields{1}.name = 'SW_AIN_PD1_Analog1';
WR225E.SW_Inputs.fields{1}.units = '';
WR225E.SW_Inputs.fields{1}.start_bit = 32;
WR225E.SW_Inputs.fields{1}.bit_length = 16;
WR225E.SW_Inputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{1}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{1}.maximum = 6.553500e+04;
WR225E.SW_Inputs.fields{1}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{1}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{1}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{2}.name = 'SW_AIN_PD0_Analog0';
WR225E.SW_Inputs.fields{2}.units = '';
WR225E.SW_Inputs.fields{2}.start_bit = 16;
WR225E.SW_Inputs.fields{2}.bit_length = 16;
WR225E.SW_Inputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{2}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{2}.maximum = 6.553500e+04;
WR225E.SW_Inputs.fields{2}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{2}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{2}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{3}.name = 'SW_IN_PD2_Button14';
WR225E.SW_Inputs.fields{3}.units = '';
WR225E.SW_Inputs.fields{3}.start_bit = 40;
WR225E.SW_Inputs.fields{3}.bit_length = 1;
WR225E.SW_Inputs.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{3}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{3}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{3}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{3}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{3}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{4}.name = 'SW_IN_PD4_Button13';
WR225E.SW_Inputs.fields{4}.units = '';
WR225E.SW_Inputs.fields{4}.start_bit = 41;
WR225E.SW_Inputs.fields{4}.bit_length = 1;
WR225E.SW_Inputs.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{4}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{4}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{4}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{4}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{4}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{5}.name = 'SW_IN_PD6_Button12';
WR225E.SW_Inputs.fields{5}.units = '';
WR225E.SW_Inputs.fields{5}.start_bit = 42;
WR225E.SW_Inputs.fields{5}.bit_length = 1;
WR225E.SW_Inputs.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{5}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{5}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{5}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{5}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{5}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{6}.name = 'SW_IN_PB7_Button11';
WR225E.SW_Inputs.fields{6}.units = '';
WR225E.SW_Inputs.fields{6}.start_bit = 43;
WR225E.SW_Inputs.fields{6}.bit_length = 1;
WR225E.SW_Inputs.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{6}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{6}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{6}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{6}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{6}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{7}.name = 'SW_IN_PB6_Button10';
WR225E.SW_Inputs.fields{7}.units = '';
WR225E.SW_Inputs.fields{7}.start_bit = 44;
WR225E.SW_Inputs.fields{7}.bit_length = 1;
WR225E.SW_Inputs.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{7}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{7}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{7}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{7}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{7}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{8}.name = 'SW_IN_PB5_Button9';
WR225E.SW_Inputs.fields{8}.units = '';
WR225E.SW_Inputs.fields{8}.start_bit = 45;
WR225E.SW_Inputs.fields{8}.bit_length = 1;
WR225E.SW_Inputs.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{8}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{8}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{8}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{8}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{8}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{9}.name = 'SW_IN_PE2_Button1';
WR225E.SW_Inputs.fields{9}.units = '';
WR225E.SW_Inputs.fields{9}.start_bit = 48;
WR225E.SW_Inputs.fields{9}.bit_length = 1;
WR225E.SW_Inputs.fields{9}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{9}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{9}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{9}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{9}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{9}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{10}.name = 'SW_IN_PE5_Button2';
WR225E.SW_Inputs.fields{10}.units = '';
WR225E.SW_Inputs.fields{10}.start_bit = 49;
WR225E.SW_Inputs.fields{10}.bit_length = 1;
WR225E.SW_Inputs.fields{10}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{10}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{10}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{10}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{10}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{10}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{11}.name = 'SW_IN_PE4_Button3';
WR225E.SW_Inputs.fields{11}.units = '';
WR225E.SW_Inputs.fields{11}.start_bit = 50;
WR225E.SW_Inputs.fields{11}.bit_length = 1;
WR225E.SW_Inputs.fields{11}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{11}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{11}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{11}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{11}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{11}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{12}.name = 'SW_IN_PE1_Mode_Dial_Reserved';
WR225E.SW_Inputs.fields{12}.units = '';
WR225E.SW_Inputs.fields{12}.start_bit = 59;
WR225E.SW_Inputs.fields{12}.bit_length = 1;
WR225E.SW_Inputs.fields{12}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{12}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{12}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{12}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{12}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{12}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{13}.name = 'SW_IN_PD5_Button5';
WR225E.SW_Inputs.fields{13}.units = '';
WR225E.SW_Inputs.fields{13}.start_bit = 52;
WR225E.SW_Inputs.fields{13}.bit_length = 1;
WR225E.SW_Inputs.fields{13}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{13}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{13}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{13}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{13}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{13}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{14}.name = 'SW_IN_PA0_Button6';
WR225E.SW_Inputs.fields{14}.units = '';
WR225E.SW_Inputs.fields{14}.start_bit = 53;
WR225E.SW_Inputs.fields{14}.bit_length = 1;
WR225E.SW_Inputs.fields{14}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{14}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{14}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{14}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{14}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{14}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{15}.name = 'SW_IN_PA1_Button7';
WR225E.SW_Inputs.fields{15}.units = '';
WR225E.SW_Inputs.fields{15}.start_bit = 54;
WR225E.SW_Inputs.fields{15}.bit_length = 1;
WR225E.SW_Inputs.fields{15}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{15}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{15}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{15}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{15}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{15}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{16}.name = 'SW_IN_PB3_Button8';
WR225E.SW_Inputs.fields{16}.units = '';
WR225E.SW_Inputs.fields{16}.start_bit = 55;
WR225E.SW_Inputs.fields{16}.bit_length = 1;
WR225E.SW_Inputs.fields{16}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{16}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{16}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{16}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{16}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{16}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{17}.name = 'SW_IN_PA6_TC_Min';
WR225E.SW_Inputs.fields{17}.units = '';
WR225E.SW_Inputs.fields{17}.start_bit = 56;
WR225E.SW_Inputs.fields{17}.bit_length = 1;
WR225E.SW_Inputs.fields{17}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{17}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{17}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{17}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{17}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{17}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{18}.name = 'SW_IN_PA7_TC_Mid';
WR225E.SW_Inputs.fields{18}.units = '';
WR225E.SW_Inputs.fields{18}.start_bit = 57;
WR225E.SW_Inputs.fields{18}.bit_length = 1;
WR225E.SW_Inputs.fields{18}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{18}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{18}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{18}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{18}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{18}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{19}.name = 'SW_IN_PB2_TC_Max';
WR225E.SW_Inputs.fields{19}.units = '';
WR225E.SW_Inputs.fields{19}.start_bit = 58;
WR225E.SW_Inputs.fields{19}.bit_length = 1;
WR225E.SW_Inputs.fields{19}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{19}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{19}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{19}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{19}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{19}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{20}.name = 'SW_IN_PB1_Button4';
WR225E.SW_Inputs.fields{20}.units = '';
WR225E.SW_Inputs.fields{20}.start_bit = 51;
WR225E.SW_Inputs.fields{20}.bit_length = 1;
WR225E.SW_Inputs.fields{20}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{20}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{20}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{20}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{20}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{20}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{21}.name = 'SW_IN_PB0_Autocross';
WR225E.SW_Inputs.fields{21}.units = '';
WR225E.SW_Inputs.fields{21}.start_bit = 60;
WR225E.SW_Inputs.fields{21}.bit_length = 1;
WR225E.SW_Inputs.fields{21}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{21}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{21}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{21}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{21}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{21}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{22}.name = 'SW_IN_PF1_Endurance';
WR225E.SW_Inputs.fields{22}.units = '';
WR225E.SW_Inputs.fields{22}.start_bit = 61;
WR225E.SW_Inputs.fields{22}.bit_length = 1;
WR225E.SW_Inputs.fields{22}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{22}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{22}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{22}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{22}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{22}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{23}.name = 'SW_IN_PE3_Skidpad';
WR225E.SW_Inputs.fields{23}.units = '';
WR225E.SW_Inputs.fields{23}.start_bit = 62;
WR225E.SW_Inputs.fields{23}.bit_length = 1;
WR225E.SW_Inputs.fields{23}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{23}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{23}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{23}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{23}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{23}.offset = 0.000000e+00;

WR225E.SW_Inputs.fields{24}.name = 'SW_IN_PA3_Accel';
WR225E.SW_Inputs.fields{24}.units = '';
WR225E.SW_Inputs.fields{24}.start_bit = 63;
WR225E.SW_Inputs.fields{24}.bit_length = 1;
WR225E.SW_Inputs.fields{24}.byte_order = 'LITTLE_ENDIAN';
WR225E.SW_Inputs.fields{24}.data_type = 'UNSIGNED';
WR225E.SW_Inputs.fields{24}.maximum = 1.000000e+00;
WR225E.SW_Inputs.fields{24}.minimum = 0.000000e+00;
WR225E.SW_Inputs.fields{24}.scale = 1.000000e+00;
WR225E.SW_Inputs.fields{24}.offset = 0.000000e+00;

%===================================================================
case 'Shock_Pot_Values'
WR225E.Shock_Pot_Values = struct;
WR225E.Shock_Pot_Values.name = 'Shock_Pot_Values';
WR225E.Shock_Pot_Values.description = 'Shock_Pot_Values';
WR225E.Shock_Pot_Values.protocol = 'WR225E';
WR225E.Shock_Pot_Values.id = hex2dec('000000E6');
WR225E.Shock_Pot_Values.idext = 'STANDARD';
WR225E.Shock_Pot_Values.idinherit = 0;
WR225E.Shock_Pot_Values.payload_size = 8;
WR225E.Shock_Pot_Values.payload_value = [];
WR225E.Shock_Pot_Values.payload_mask = [];
WR225E.Shock_Pot_Values.interval = 50;

WR225E.Shock_Pot_Values.fields{1}.name = 'RR_Shock';
WR225E.Shock_Pot_Values.fields{1}.units = 'mm';
WR225E.Shock_Pot_Values.fields{1}.start_bit = 8;
WR225E.Shock_Pot_Values.fields{1}.bit_length = 16;
WR225E.Shock_Pot_Values.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Shock_Pot_Values.fields{1}.data_type = 'SIGNED';
WR225E.Shock_Pot_Values.fields{1}.maximum = 1.000000e+02;
WR225E.Shock_Pot_Values.fields{1}.minimum = 0.000000e+00;
WR225E.Shock_Pot_Values.fields{1}.scale = 1.000000e-01;
WR225E.Shock_Pot_Values.fields{1}.offset = 0.000000e+00;

WR225E.Shock_Pot_Values.fields{2}.name = 'RL_Shock';
WR225E.Shock_Pot_Values.fields{2}.units = 'mm';
WR225E.Shock_Pot_Values.fields{2}.start_bit = 24;
WR225E.Shock_Pot_Values.fields{2}.bit_length = 16;
WR225E.Shock_Pot_Values.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Shock_Pot_Values.fields{2}.data_type = 'SIGNED';
WR225E.Shock_Pot_Values.fields{2}.maximum = 1.000000e+02;
WR225E.Shock_Pot_Values.fields{2}.minimum = 0.000000e+00;
WR225E.Shock_Pot_Values.fields{2}.scale = 1.000000e-01;
WR225E.Shock_Pot_Values.fields{2}.offset = 0.000000e+00;

WR225E.Shock_Pot_Values.fields{3}.name = 'FL_Shock';
WR225E.Shock_Pot_Values.fields{3}.units = 'mm';
WR225E.Shock_Pot_Values.fields{3}.start_bit = 40;
WR225E.Shock_Pot_Values.fields{3}.bit_length = 16;
WR225E.Shock_Pot_Values.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Shock_Pot_Values.fields{3}.data_type = 'SIGNED';
WR225E.Shock_Pot_Values.fields{3}.maximum = 1.000000e+02;
WR225E.Shock_Pot_Values.fields{3}.minimum = 0.000000e+00;
WR225E.Shock_Pot_Values.fields{3}.scale = 1.000000e-01;
WR225E.Shock_Pot_Values.fields{3}.offset = 0.000000e+00;

WR225E.Shock_Pot_Values.fields{4}.name = 'FR_Shock';
WR225E.Shock_Pot_Values.fields{4}.units = 'mm';
WR225E.Shock_Pot_Values.fields{4}.start_bit = 56;
WR225E.Shock_Pot_Values.fields{4}.bit_length = 16;
WR225E.Shock_Pot_Values.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Shock_Pot_Values.fields{4}.data_type = 'SIGNED';
WR225E.Shock_Pot_Values.fields{4}.maximum = 1.000000e+02;
WR225E.Shock_Pot_Values.fields{4}.minimum = 0.000000e+00;
WR225E.Shock_Pot_Values.fields{4}.scale = 1.000000e-01;
WR225E.Shock_Pot_Values.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Torque_Requests'
WR225E.Torque_Requests = struct;
WR225E.Torque_Requests.name = 'Torque_Requests';
WR225E.Torque_Requests.description = 'Torque requests going to each inverter';
WR225E.Torque_Requests.protocol = 'WR225E';
WR225E.Torque_Requests.id = hex2dec('00000005');
WR225E.Torque_Requests.idext = 'STANDARD';
WR225E.Torque_Requests.idinherit = 0;
WR225E.Torque_Requests.payload_size = 8;
WR225E.Torque_Requests.payload_value = [];
WR225E.Torque_Requests.payload_mask = [];
WR225E.Torque_Requests.interval = 50;

WR225E.Torque_Requests.fields{1}.name = 'Torque_Request_Rear_Right';
WR225E.Torque_Requests.fields{1}.units = 'Nm';
WR225E.Torque_Requests.fields{1}.start_bit = 56;
WR225E.Torque_Requests.fields{1}.bit_length = 10;
WR225E.Torque_Requests.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Torque_Requests.fields{1}.data_type = 'SIGNED';
WR225E.Torque_Requests.fields{1}.maximum = 2.700000e+01;
WR225E.Torque_Requests.fields{1}.minimum = -2.700000e+01;
WR225E.Torque_Requests.fields{1}.scale = 1.000000e-01;
WR225E.Torque_Requests.fields{1}.offset = 0.000000e+00;

WR225E.Torque_Requests.fields{2}.name = 'Torque_Request_Rear_Left';
WR225E.Torque_Requests.fields{2}.units = 'Nm';
WR225E.Torque_Requests.fields{2}.start_bit = 50;
WR225E.Torque_Requests.fields{2}.bit_length = 10;
WR225E.Torque_Requests.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Torque_Requests.fields{2}.data_type = 'SIGNED';
WR225E.Torque_Requests.fields{2}.maximum = 2.700000e+01;
WR225E.Torque_Requests.fields{2}.minimum = -2.700000e+01;
WR225E.Torque_Requests.fields{2}.scale = 1.000000e-01;
WR225E.Torque_Requests.fields{2}.offset = 0.000000e+00;

WR225E.Torque_Requests.fields{3}.name = 'Torque_Request_Front_Right';
WR225E.Torque_Requests.fields{3}.units = 'Nm';
WR225E.Torque_Requests.fields{3}.start_bit = 44;
WR225E.Torque_Requests.fields{3}.bit_length = 10;
WR225E.Torque_Requests.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Torque_Requests.fields{3}.data_type = 'SIGNED';
WR225E.Torque_Requests.fields{3}.maximum = 2.700000e+01;
WR225E.Torque_Requests.fields{3}.minimum = -2.700000e+01;
WR225E.Torque_Requests.fields{3}.scale = 1.000000e-01;
WR225E.Torque_Requests.fields{3}.offset = 0.000000e+00;

WR225E.Torque_Requests.fields{4}.name = 'Torque_Request_Front_Left';
WR225E.Torque_Requests.fields{4}.units = 'Nm';
WR225E.Torque_Requests.fields{4}.start_bit = 38;
WR225E.Torque_Requests.fields{4}.bit_length = 10;
WR225E.Torque_Requests.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Torque_Requests.fields{4}.data_type = 'SIGNED';
WR225E.Torque_Requests.fields{4}.maximum = 2.700000e+01;
WR225E.Torque_Requests.fields{4}.minimum = -2.700000e+01;
WR225E.Torque_Requests.fields{4}.scale = 1.000000e-01;
WR225E.Torque_Requests.fields{4}.offset = 0.000000e+00;

WR225E.Torque_Requests.fields{5}.name = 'Requested_Torque_Driver';
WR225E.Torque_Requests.fields{5}.units = '%';
WR225E.Torque_Requests.fields{5}.start_bit = 16;
WR225E.Torque_Requests.fields{5}.bit_length = 11;
WR225E.Torque_Requests.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.Torque_Requests.fields{5}.data_type = 'SIGNED';
WR225E.Torque_Requests.fields{5}.maximum = 1.000000e+02;
WR225E.Torque_Requests.fields{5}.minimum = -1.000000e+02;
WR225E.Torque_Requests.fields{5}.scale = 1.000000e-01;
WR225E.Torque_Requests.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Traction_Control'
WR225E.Traction_Control = struct;
WR225E.Traction_Control.name = 'Traction_Control';
WR225E.Traction_Control.description = 'All data that is related to traction control';
WR225E.Traction_Control.protocol = 'WR225E';
WR225E.Traction_Control.id = hex2dec('00000004');
WR225E.Traction_Control.idext = 'STANDARD';
WR225E.Traction_Control.idinherit = 0;
WR225E.Traction_Control.payload_size = 8;
WR225E.Traction_Control.payload_value = [];
WR225E.Traction_Control.payload_mask = [];
WR225E.Traction_Control.interval = 50;

WR225E.Traction_Control.fields{1}.name = 'TC_Torque_Limit_Rear_Right';
WR225E.Traction_Control.fields{1}.units = '%';
WR225E.Traction_Control.fields{1}.start_bit = 56;
WR225E.Traction_Control.fields{1}.bit_length = 10;
WR225E.Traction_Control.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Traction_Control.fields{1}.data_type = 'UNSIGNED';
WR225E.Traction_Control.fields{1}.maximum = 1.000000e+02;
WR225E.Traction_Control.fields{1}.minimum = 0.000000e+00;
WR225E.Traction_Control.fields{1}.scale = 1.000000e-01;
WR225E.Traction_Control.fields{1}.offset = 0.000000e+00;

WR225E.Traction_Control.fields{2}.name = 'TC_Torque_Limit_Rear_Left';
WR225E.Traction_Control.fields{2}.units = '%';
WR225E.Traction_Control.fields{2}.start_bit = 50;
WR225E.Traction_Control.fields{2}.bit_length = 10;
WR225E.Traction_Control.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Traction_Control.fields{2}.data_type = 'UNSIGNED';
WR225E.Traction_Control.fields{2}.maximum = 1.000000e+02;
WR225E.Traction_Control.fields{2}.minimum = 0.000000e+00;
WR225E.Traction_Control.fields{2}.scale = 1.000000e-01;
WR225E.Traction_Control.fields{2}.offset = 0.000000e+00;

WR225E.Traction_Control.fields{3}.name = 'TC_Torque_Limit_Front_Right';
WR225E.Traction_Control.fields{3}.units = '%';
WR225E.Traction_Control.fields{3}.start_bit = 44;
WR225E.Traction_Control.fields{3}.bit_length = 10;
WR225E.Traction_Control.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Traction_Control.fields{3}.data_type = 'UNSIGNED';
WR225E.Traction_Control.fields{3}.maximum = 1.000000e+02;
WR225E.Traction_Control.fields{3}.minimum = 0.000000e+00;
WR225E.Traction_Control.fields{3}.scale = 1.000000e-01;
WR225E.Traction_Control.fields{3}.offset = 0.000000e+00;

WR225E.Traction_Control.fields{4}.name = 'TC_Torque_Limit_Front_Left';
WR225E.Traction_Control.fields{4}.units = '%';
WR225E.Traction_Control.fields{4}.start_bit = 38;
WR225E.Traction_Control.fields{4}.bit_length = 10;
WR225E.Traction_Control.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Traction_Control.fields{4}.data_type = 'UNSIGNED';
WR225E.Traction_Control.fields{4}.maximum = 1.000000e+02;
WR225E.Traction_Control.fields{4}.minimum = 0.000000e+00;
WR225E.Traction_Control.fields{4}.scale = 1.000000e-01;
WR225E.Traction_Control.fields{4}.offset = 0.000000e+00;

WR225E.Traction_Control.fields{5}.name = 'TC_Effect_Percent';
WR225E.Traction_Control.fields{5}.units = '%';
WR225E.Traction_Control.fields{5}.start_bit = 16;
WR225E.Traction_Control.fields{5}.bit_length = 7;
WR225E.Traction_Control.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR225E.Traction_Control.fields{5}.data_type = 'UNSIGNED';
WR225E.Traction_Control.fields{5}.maximum = 1.000000e+02;
WR225E.Traction_Control.fields{5}.minimum = 0.000000e+00;
WR225E.Traction_Control.fields{5}.scale = 1.000000e+00;
WR225E.Traction_Control.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Vehicle_Dynamics'
WR225E.Vehicle_Dynamics = struct;
WR225E.Vehicle_Dynamics.name = 'Vehicle_Dynamics';
WR225E.Vehicle_Dynamics.description = 'information on the vehicles state';
WR225E.Vehicle_Dynamics.protocol = 'WR225E';
WR225E.Vehicle_Dynamics.id = hex2dec('00000009');
WR225E.Vehicle_Dynamics.idext = 'STANDARD';
WR225E.Vehicle_Dynamics.idinherit = 0;
WR225E.Vehicle_Dynamics.payload_size = 8;
WR225E.Vehicle_Dynamics.payload_value = [];
WR225E.Vehicle_Dynamics.payload_mask = [];
WR225E.Vehicle_Dynamics.interval = 50;

WR225E.Vehicle_Dynamics.fields{1}.name = 'GPS_Speed_Calculated';
WR225E.Vehicle_Dynamics.fields{1}.units = 'mph';
WR225E.Vehicle_Dynamics.fields{1}.start_bit = 56;
WR225E.Vehicle_Dynamics.fields{1}.bit_length = 10;
WR225E.Vehicle_Dynamics.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Vehicle_Dynamics.fields{1}.data_type = 'UNSIGNED';
WR225E.Vehicle_Dynamics.fields{1}.maximum = 7.000000e+01;
WR225E.Vehicle_Dynamics.fields{1}.minimum = 0.000000e+00;
WR225E.Vehicle_Dynamics.fields{1}.scale = 1.000000e-01;
WR225E.Vehicle_Dynamics.fields{1}.offset = 0.000000e+00;

WR225E.Vehicle_Dynamics.fields{2}.name = 'Wheel_Speed_Front_Left';
WR225E.Vehicle_Dynamics.fields{2}.units = 'mph';
WR225E.Vehicle_Dynamics.fields{2}.start_bit = 50;
WR225E.Vehicle_Dynamics.fields{2}.bit_length = 10;
WR225E.Vehicle_Dynamics.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Vehicle_Dynamics.fields{2}.data_type = 'UNSIGNED';
WR225E.Vehicle_Dynamics.fields{2}.maximum = 7.000000e+01;
WR225E.Vehicle_Dynamics.fields{2}.minimum = 0.000000e+00;
WR225E.Vehicle_Dynamics.fields{2}.scale = 1.000000e-01;
WR225E.Vehicle_Dynamics.fields{2}.offset = 0.000000e+00;

WR225E.Vehicle_Dynamics.fields{3}.name = 'Wheel_Speed_Front_Right';
WR225E.Vehicle_Dynamics.fields{3}.units = 'mph';
WR225E.Vehicle_Dynamics.fields{3}.start_bit = 44;
WR225E.Vehicle_Dynamics.fields{3}.bit_length = 10;
WR225E.Vehicle_Dynamics.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Vehicle_Dynamics.fields{3}.data_type = 'UNSIGNED';
WR225E.Vehicle_Dynamics.fields{3}.maximum = 7.000000e+01;
WR225E.Vehicle_Dynamics.fields{3}.minimum = 0.000000e+00;
WR225E.Vehicle_Dynamics.fields{3}.scale = 1.000000e-01;
WR225E.Vehicle_Dynamics.fields{3}.offset = 0.000000e+00;

WR225E.Vehicle_Dynamics.fields{4}.name = 'Filtered_Speed';
WR225E.Vehicle_Dynamics.fields{4}.units = 'mph';
WR225E.Vehicle_Dynamics.fields{4}.start_bit = 38;
WR225E.Vehicle_Dynamics.fields{4}.bit_length = 10;
WR225E.Vehicle_Dynamics.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Vehicle_Dynamics.fields{4}.data_type = 'UNSIGNED';
WR225E.Vehicle_Dynamics.fields{4}.maximum = 7.000000e+01;
WR225E.Vehicle_Dynamics.fields{4}.minimum = 0.000000e+00;
WR225E.Vehicle_Dynamics.fields{4}.scale = 1.000000e-01;
WR225E.Vehicle_Dynamics.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Wheel_Slip'
WR225E.Wheel_Slip = struct;
WR225E.Wheel_Slip.name = 'Wheel_Slip';
WR225E.Wheel_Slip.description = 'The wheel slip of each wheel';
WR225E.Wheel_Slip.protocol = 'WR225E';
WR225E.Wheel_Slip.id = hex2dec('00000002');
WR225E.Wheel_Slip.idext = 'STANDARD';
WR225E.Wheel_Slip.idinherit = 0;
WR225E.Wheel_Slip.payload_size = 8;
WR225E.Wheel_Slip.payload_value = [];
WR225E.Wheel_Slip.payload_mask = [];
WR225E.Wheel_Slip.interval = 50;

WR225E.Wheel_Slip.fields{1}.name = 'Wheel_Slip_Rear_Right';
WR225E.Wheel_Slip.fields{1}.units = '%';
WR225E.Wheel_Slip.fields{1}.start_bit = 56;
WR225E.Wheel_Slip.fields{1}.bit_length = 13;
WR225E.Wheel_Slip.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR225E.Wheel_Slip.fields{1}.data_type = 'SIGNED';
WR225E.Wheel_Slip.fields{1}.maximum = 3.000000e+02;
WR225E.Wheel_Slip.fields{1}.minimum = -3.000000e+02;
WR225E.Wheel_Slip.fields{1}.scale = 1.000000e-01;
WR225E.Wheel_Slip.fields{1}.offset = 0.000000e+00;

WR225E.Wheel_Slip.fields{2}.name = 'Wheel_Slip_Rear_Left';
WR225E.Wheel_Slip.fields{2}.units = '%';
WR225E.Wheel_Slip.fields{2}.start_bit = 53;
WR225E.Wheel_Slip.fields{2}.bit_length = 13;
WR225E.Wheel_Slip.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR225E.Wheel_Slip.fields{2}.data_type = 'SIGNED';
WR225E.Wheel_Slip.fields{2}.maximum = 3.000000e+02;
WR225E.Wheel_Slip.fields{2}.minimum = -3.000000e+02;
WR225E.Wheel_Slip.fields{2}.scale = 1.000000e-01;
WR225E.Wheel_Slip.fields{2}.offset = 0.000000e+00;

WR225E.Wheel_Slip.fields{3}.name = 'Wheel_Slip_Front_Right';
WR225E.Wheel_Slip.fields{3}.units = '%';
WR225E.Wheel_Slip.fields{3}.start_bit = 34;
WR225E.Wheel_Slip.fields{3}.bit_length = 13;
WR225E.Wheel_Slip.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR225E.Wheel_Slip.fields{3}.data_type = 'SIGNED';
WR225E.Wheel_Slip.fields{3}.maximum = 3.000000e+02;
WR225E.Wheel_Slip.fields{3}.minimum = -3.000000e+02;
WR225E.Wheel_Slip.fields{3}.scale = 1.000000e-01;
WR225E.Wheel_Slip.fields{3}.offset = 0.000000e+00;

WR225E.Wheel_Slip.fields{4}.name = 'Wheel_Slip_Front_Left';
WR225E.Wheel_Slip.fields{4}.units = '%';
WR225E.Wheel_Slip.fields{4}.start_bit = 31;
WR225E.Wheel_Slip.fields{4}.bit_length = 13;
WR225E.Wheel_Slip.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR225E.Wheel_Slip.fields{4}.data_type = 'SIGNED';
WR225E.Wheel_Slip.fields{4}.maximum = 3.000000e+02;
WR225E.Wheel_Slip.fields{4}.minimum = -3.000000e+02;
WR225E.Wheel_Slip.fields{4}.scale = 1.000000e-01;
WR225E.Wheel_Slip.fields{4}.offset = 0.000000e+00;

end

try
	msg = WR225E.(msgname);
end