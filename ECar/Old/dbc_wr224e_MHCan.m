function msg = dbc_wr224e_MHCan(msgname)
%msg = dbc_wr224e_MHCan(msgname);

msg = struct;

switch msgname

%===================================================================
case 'AUX_AIN'
wr224e.AUX_AIN = struct;
wr224e.AUX_AIN.name = 'AUX_AIN';
wr224e.AUX_AIN.description = 'AUX_AIN';
wr224e.AUX_AIN.protocol = 'wr224e';
wr224e.AUX_AIN.id = hex2dec('00000160');
wr224e.AUX_AIN.idext = 'STANDARD';
wr224e.AUX_AIN.idinherit = 0;
wr224e.AUX_AIN.payload_size = 8;
wr224e.AUX_AIN.payload_value = [];
wr224e.AUX_AIN.payload_mask = [];
wr224e.AUX_AIN.interval = 50;

wr224e.AUX_AIN.fields{1}.name = 'AUX_BPD_FAULT_V';
wr224e.AUX_AIN.fields{1}.units = 'V';
wr224e.AUX_AIN.fields{1}.start_bit = 0;
wr224e.AUX_AIN.fields{1}.bit_length = 8;
wr224e.AUX_AIN.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{1}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{1}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{1}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{2}.name = 'AUX_AIR_COIL_V';
wr224e.AUX_AIN.fields{2}.units = 'V';
wr224e.AUX_AIN.fields{2}.start_bit = 8;
wr224e.AUX_AIN.fields{2}.bit_length = 8;
wr224e.AUX_AIN.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{2}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{2}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{2}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{3}.name = 'AUX_IMD_FAULT_V';
wr224e.AUX_AIN.fields{3}.units = 'V';
wr224e.AUX_AIN.fields{3}.start_bit = 16;
wr224e.AUX_AIN.fields{3}.bit_length = 8;
wr224e.AUX_AIN.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{3}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{3}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{3}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{4}.name = 'AUX_CURRENT_THRESH';
wr224e.AUX_AIN.fields{4}.units = 'V';
wr224e.AUX_AIN.fields{4}.start_bit = 24;
wr224e.AUX_AIN.fields{4}.bit_length = 8;
wr224e.AUX_AIN.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{4}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{4}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{4}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{5}.name = 'AUX_BRK_THRESH';
wr224e.AUX_AIN.fields{5}.units = 'V';
wr224e.AUX_AIN.fields{5}.start_bit = 32;
wr224e.AUX_AIN.fields{5}.bit_length = 8;
wr224e.AUX_AIN.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{5}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{5}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{5}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{6}.name = 'AUX_BRK_PRES1_V';
wr224e.AUX_AIN.fields{6}.units = 'V';
wr224e.AUX_AIN.fields{6}.start_bit = 40;
wr224e.AUX_AIN.fields{6}.bit_length = 8;
wr224e.AUX_AIN.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{6}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{6}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{6}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{7}.name = 'AUX_SDC_SUPPLY_V';
wr224e.AUX_AIN.fields{7}.units = 'V';
wr224e.AUX_AIN.fields{7}.start_bit = 48;
wr224e.AUX_AIN.fields{7}.bit_length = 8;
wr224e.AUX_AIN.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{7}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{7}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{7}.offset = 0.000000e+00;

wr224e.AUX_AIN.fields{8}.name = 'AUX_GLV_V';
wr224e.AUX_AIN.fields{8}.units = 'V';
wr224e.AUX_AIN.fields{8}.start_bit = 56;
wr224e.AUX_AIN.fields{8}.bit_length = 8;
wr224e.AUX_AIN.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_AIN.fields{8}.data_type = 'UNSIGNED';
wr224e.AUX_AIN.fields{8}.scale = 1.000000e-01;
wr224e.AUX_AIN.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'AUX_Faults'
wr224e.AUX_Faults = struct;
wr224e.AUX_Faults.name = 'AUX_Faults';
wr224e.AUX_Faults.description = 'AUX_Faults';
wr224e.AUX_Faults.protocol = 'wr224e';
wr224e.AUX_Faults.id = hex2dec('00000060');
wr224e.AUX_Faults.idext = 'STANDARD';
wr224e.AUX_Faults.idinherit = 0;
wr224e.AUX_Faults.payload_size = 2;
wr224e.AUX_Faults.payload_value = [];
wr224e.AUX_Faults.payload_mask = [];
wr224e.AUX_Faults.interval = 50;

wr224e.AUX_Faults.fields{1}.name = 'AUX_AIRS_CLOSED';
wr224e.AUX_Faults.fields{1}.units = '';
wr224e.AUX_Faults.fields{1}.start_bit = 56;
wr224e.AUX_Faults.fields{1}.bit_length = 1;
wr224e.AUX_Faults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{1}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{1}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{1}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{2}.name = 'AUX_IMD_NORMAL';
wr224e.AUX_Faults.fields{2}.units = '';
wr224e.AUX_Faults.fields{2}.start_bit = 57;
wr224e.AUX_Faults.fields{2}.bit_length = 1;
wr224e.AUX_Faults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{2}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{2}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{2}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{3}.name = 'AUX_IMD_UNDERVOLTAGE';
wr224e.AUX_Faults.fields{3}.units = '';
wr224e.AUX_Faults.fields{3}.start_bit = 58;
wr224e.AUX_Faults.fields{3}.bit_length = 1;
wr224e.AUX_Faults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{3}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{3}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{3}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{4}.name = 'AUX_IMD_SST';
wr224e.AUX_Faults.fields{4}.units = '';
wr224e.AUX_Faults.fields{4}.start_bit = 59;
wr224e.AUX_Faults.fields{4}.bit_length = 1;
wr224e.AUX_Faults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{4}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{4}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{4}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{5}.name = 'AUX_IMD_DEVICE_ERROR';
wr224e.AUX_Faults.fields{5}.units = '';
wr224e.AUX_Faults.fields{5}.start_bit = 60;
wr224e.AUX_Faults.fields{5}.bit_length = 1;
wr224e.AUX_Faults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{5}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{5}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{5}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{6}.name = 'AUX_IMD_KI31_FAULT';
wr224e.AUX_Faults.fields{6}.units = '';
wr224e.AUX_Faults.fields{6}.start_bit = 61;
wr224e.AUX_Faults.fields{6}.bit_length = 1;
wr224e.AUX_Faults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{6}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{6}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{6}.offset = 0.000000e+00;

wr224e.AUX_Faults.fields{7}.name = 'AUX_IMD_SHORT_CIRCUIT';
wr224e.AUX_Faults.fields{7}.units = '';
wr224e.AUX_Faults.fields{7}.start_bit = 62;
wr224e.AUX_Faults.fields{7}.bit_length = 1;
wr224e.AUX_Faults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Faults.fields{7}.data_type = 'UNSIGNED';
wr224e.AUX_Faults.fields{7}.scale = 1.000000e+00;
wr224e.AUX_Faults.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'AUX_Flags'
wr224e.AUX_Flags = struct;
wr224e.AUX_Flags.name = 'AUX_Flags';
wr224e.AUX_Flags.description = 'AUX_Flags';
wr224e.AUX_Flags.protocol = 'wr224e';
wr224e.AUX_Flags.id = hex2dec('00000030');
wr224e.AUX_Flags.idext = 'STANDARD';
wr224e.AUX_Flags.idinherit = 0;
wr224e.AUX_Flags.payload_size = 2;
wr224e.AUX_Flags.payload_value = [];
wr224e.AUX_Flags.payload_mask = [];
wr224e.AUX_Flags.interval = 50;

wr224e.AUX_Flags.fields{1}.name = 'AUX_IMD_FAULT';
wr224e.AUX_Flags.fields{1}.units = 'Flag';
wr224e.AUX_Flags.fields{1}.start_bit = 59;
wr224e.AUX_Flags.fields{1}.bit_length = 1;
wr224e.AUX_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{1}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{1}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{1}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{2}.name = 'AUX_SDC_RETURN';
wr224e.AUX_Flags.fields{2}.units = '';
wr224e.AUX_Flags.fields{2}.start_bit = 60;
wr224e.AUX_Flags.fields{2}.bit_length = 1;
wr224e.AUX_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{2}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{2}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{2}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{3}.name = 'AUX_SDC_SUPPLY';
wr224e.AUX_Flags.fields{3}.units = 'Flag';
wr224e.AUX_Flags.fields{3}.start_bit = 61;
wr224e.AUX_Flags.fields{3}.bit_length = 1;
wr224e.AUX_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{3}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{3}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{3}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{4}.name = 'AUX_MUX_DCDC_VALID';
wr224e.AUX_Flags.fields{4}.units = '';
wr224e.AUX_Flags.fields{4}.start_bit = 56;
wr224e.AUX_Flags.fields{4}.bit_length = 1;
wr224e.AUX_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{4}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{4}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{4}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{5}.name = 'AUX_MUX_BATT_VALID';
wr224e.AUX_Flags.fields{5}.units = '';
wr224e.AUX_Flags.fields{5}.start_bit = 57;
wr224e.AUX_Flags.fields{5}.bit_length = 1;
wr224e.AUX_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{5}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{5}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{5}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{6}.name = 'AUX_BPD_FAULT';
wr224e.AUX_Flags.fields{6}.units = 'Flag';
wr224e.AUX_Flags.fields{6}.start_bit = 58;
wr224e.AUX_Flags.fields{6}.bit_length = 1;
wr224e.AUX_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{6}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{6}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{6}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{7}.name = 'AUX_ECU_FAULT';
wr224e.AUX_Flags.fields{7}.units = '';
wr224e.AUX_Flags.fields{7}.start_bit = 62;
wr224e.AUX_Flags.fields{7}.bit_length = 1;
wr224e.AUX_Flags.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{7}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{7}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{7}.offset = 0.000000e+00;

wr224e.AUX_Flags.fields{8}.name = 'AUX_AIR_COIL';
wr224e.AUX_Flags.fields{8}.units = 'Flag';
wr224e.AUX_Flags.fields{8}.start_bit = 63;
wr224e.AUX_Flags.fields{8}.bit_length = 1;
wr224e.AUX_Flags.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.AUX_Flags.fields{8}.data_type = 'UNSIGNED';
wr224e.AUX_Flags.fields{8}.scale = 1.000000e+00;
wr224e.AUX_Flags.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'BMS_CellClosedVoltageStats'
wr224e.BMS_CellClosedVoltageStats = struct;
wr224e.BMS_CellClosedVoltageStats.name = 'BMS_CellClosedVoltageStats';
wr224e.BMS_CellClosedVoltageStats.description = 'BMS_CellClosedVoltageStats';
wr224e.BMS_CellClosedVoltageStats.protocol = 'wr224e';
wr224e.BMS_CellClosedVoltageStats.id = hex2dec('00000140');
wr224e.BMS_CellClosedVoltageStats.idext = 'STANDARD';
wr224e.BMS_CellClosedVoltageStats.idinherit = 0;
wr224e.BMS_CellClosedVoltageStats.payload_size = 8;
wr224e.BMS_CellClosedVoltageStats.payload_value = [];
wr224e.BMS_CellClosedVoltageStats.payload_mask = [];
wr224e.BMS_CellClosedVoltageStats.interval = 50;

wr224e.BMS_CellClosedVoltageStats.fields{1}.name = 'BMS_CellVoltageLowID';
wr224e.BMS_CellClosedVoltageStats.fields{1}.units = 'ID';
wr224e.BMS_CellClosedVoltageStats.fields{1}.start_bit = 0;
wr224e.BMS_CellClosedVoltageStats.fields{1}.bit_length = 8;
wr224e.BMS_CellClosedVoltageStats.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellClosedVoltageStats.fields{1}.data_type = 'UNSIGNED';
wr224e.BMS_CellClosedVoltageStats.fields{1}.scale = 1.000000e+00;
wr224e.BMS_CellClosedVoltageStats.fields{1}.offset = 0.000000e+00;

wr224e.BMS_CellClosedVoltageStats.fields{2}.name = 'BMS_CellVoltageLow';
wr224e.BMS_CellClosedVoltageStats.fields{2}.units = 'V';
wr224e.BMS_CellClosedVoltageStats.fields{2}.start_bit = 16;
wr224e.BMS_CellClosedVoltageStats.fields{2}.bit_length = 16;
wr224e.BMS_CellClosedVoltageStats.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellClosedVoltageStats.fields{2}.data_type = 'UNSIGNED';
wr224e.BMS_CellClosedVoltageStats.fields{2}.scale = 1.000000e-04;
wr224e.BMS_CellClosedVoltageStats.fields{2}.offset = 0.000000e+00;

wr224e.BMS_CellClosedVoltageStats.fields{3}.name = 'BMS_CellVoltageHighID';
wr224e.BMS_CellClosedVoltageStats.fields{3}.units = 'ID';
wr224e.BMS_CellClosedVoltageStats.fields{3}.start_bit = 24;
wr224e.BMS_CellClosedVoltageStats.fields{3}.bit_length = 8;
wr224e.BMS_CellClosedVoltageStats.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellClosedVoltageStats.fields{3}.data_type = 'UNSIGNED';
wr224e.BMS_CellClosedVoltageStats.fields{3}.scale = 1.000000e+00;
wr224e.BMS_CellClosedVoltageStats.fields{3}.offset = 0.000000e+00;

wr224e.BMS_CellClosedVoltageStats.fields{4}.name = 'BMS_CellVoltageHigh';
wr224e.BMS_CellClosedVoltageStats.fields{4}.units = 'V';
wr224e.BMS_CellClosedVoltageStats.fields{4}.start_bit = 40;
wr224e.BMS_CellClosedVoltageStats.fields{4}.bit_length = 16;
wr224e.BMS_CellClosedVoltageStats.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellClosedVoltageStats.fields{4}.data_type = 'UNSIGNED';
wr224e.BMS_CellClosedVoltageStats.fields{4}.scale = 1.000000e-04;
wr224e.BMS_CellClosedVoltageStats.fields{4}.offset = 0.000000e+00;

wr224e.BMS_CellClosedVoltageStats.fields{5}.name = 'BMS_CellVoltageAvg';
wr224e.BMS_CellClosedVoltageStats.fields{5}.units = 'V';
wr224e.BMS_CellClosedVoltageStats.fields{5}.start_bit = 56;
wr224e.BMS_CellClosedVoltageStats.fields{5}.bit_length = 16;
wr224e.BMS_CellClosedVoltageStats.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellClosedVoltageStats.fields{5}.data_type = 'UNSIGNED';
wr224e.BMS_CellClosedVoltageStats.fields{5}.scale = 1.000000e-04;
wr224e.BMS_CellClosedVoltageStats.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'BMS_CellTemperatureStats'
wr224e.BMS_CellTemperatureStats = struct;
wr224e.BMS_CellTemperatureStats.name = 'BMS_CellTemperatureStats';
wr224e.BMS_CellTemperatureStats.description = 'BMS_CellTemperatureStats';
wr224e.BMS_CellTemperatureStats.protocol = 'wr224e';
wr224e.BMS_CellTemperatureStats.id = hex2dec('00000141');
wr224e.BMS_CellTemperatureStats.idext = 'STANDARD';
wr224e.BMS_CellTemperatureStats.idinherit = 0;
wr224e.BMS_CellTemperatureStats.payload_size = 8;
wr224e.BMS_CellTemperatureStats.payload_value = [];
wr224e.BMS_CellTemperatureStats.payload_mask = [];
wr224e.BMS_CellTemperatureStats.interval = 50;

wr224e.BMS_CellTemperatureStats.fields{1}.name = 'BMS_CellTemperatureLowID';
wr224e.BMS_CellTemperatureStats.fields{1}.units = 'ID';
wr224e.BMS_CellTemperatureStats.fields{1}.start_bit = 0;
wr224e.BMS_CellTemperatureStats.fields{1}.bit_length = 8;
wr224e.BMS_CellTemperatureStats.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellTemperatureStats.fields{1}.data_type = 'UNSIGNED';
wr224e.BMS_CellTemperatureStats.fields{1}.scale = 1.000000e+00;
wr224e.BMS_CellTemperatureStats.fields{1}.offset = 0.000000e+00;

wr224e.BMS_CellTemperatureStats.fields{2}.name = 'BMS_CellTemperatureLow';
wr224e.BMS_CellTemperatureStats.fields{2}.units = 'de';
wr224e.BMS_CellTemperatureStats.fields{2}.start_bit = 16;
wr224e.BMS_CellTemperatureStats.fields{2}.bit_length = 16;
wr224e.BMS_CellTemperatureStats.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellTemperatureStats.fields{2}.data_type = 'SIGNED';
wr224e.BMS_CellTemperatureStats.fields{2}.scale = 1.000000e+00;
wr224e.BMS_CellTemperatureStats.fields{2}.offset = 0.000000e+00;

wr224e.BMS_CellTemperatureStats.fields{3}.name = 'BMS_CellTemperatureHighID';
wr224e.BMS_CellTemperatureStats.fields{3}.units = 'ID';
wr224e.BMS_CellTemperatureStats.fields{3}.start_bit = 24;
wr224e.BMS_CellTemperatureStats.fields{3}.bit_length = 8;
wr224e.BMS_CellTemperatureStats.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellTemperatureStats.fields{3}.data_type = 'UNSIGNED';
wr224e.BMS_CellTemperatureStats.fields{3}.scale = 1.000000e+00;
wr224e.BMS_CellTemperatureStats.fields{3}.offset = 0.000000e+00;

wr224e.BMS_CellTemperatureStats.fields{4}.name = 'BMS_CellTemperatureHigh';
wr224e.BMS_CellTemperatureStats.fields{4}.units = 'de';
wr224e.BMS_CellTemperatureStats.fields{4}.start_bit = 40;
wr224e.BMS_CellTemperatureStats.fields{4}.bit_length = 16;
wr224e.BMS_CellTemperatureStats.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellTemperatureStats.fields{4}.data_type = 'SIGNED';
wr224e.BMS_CellTemperatureStats.fields{4}.scale = 1.000000e+00;
wr224e.BMS_CellTemperatureStats.fields{4}.offset = 0.000000e+00;

wr224e.BMS_CellTemperatureStats.fields{5}.name = 'BMS_CellTemperatureAvg';
wr224e.BMS_CellTemperatureStats.fields{5}.units = 'de';
wr224e.BMS_CellTemperatureStats.fields{5}.start_bit = 56;
wr224e.BMS_CellTemperatureStats.fields{5}.bit_length = 16;
wr224e.BMS_CellTemperatureStats.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.BMS_CellTemperatureStats.fields{5}.data_type = 'SIGNED';
wr224e.BMS_CellTemperatureStats.fields{5}.scale = 1.000000e+00;
wr224e.BMS_CellTemperatureStats.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Ax'
wr224e.Bosch_Ax = struct;
wr224e.Bosch_Ax.name = 'Bosch_Ax';
wr224e.Bosch_Ax.description = 'Bosch_Ax';
wr224e.Bosch_Ax.protocol = 'wr224e';
wr224e.Bosch_Ax.id = hex2dec('00000178');
wr224e.Bosch_Ax.idext = 'STANDARD';
wr224e.Bosch_Ax.idinherit = 0;
wr224e.Bosch_Ax.payload_size = 8;
wr224e.Bosch_Ax.payload_value = [];
wr224e.Bosch_Ax.payload_mask = [];
wr224e.Bosch_Ax.interval = 50;

wr224e.Bosch_Ax.fields{1}.name = 'RolRate_Degs';
wr224e.Bosch_Ax.fields{1}.units = '';
wr224e.Bosch_Ax.fields{1}.start_bit = 56;
wr224e.Bosch_Ax.fields{1}.bit_length = 16;
wr224e.Bosch_Ax.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Ax.fields{1}.data_type = 'UNSIGNED';
wr224e.Bosch_Ax.fields{1}.maximum = 1.638350e+02;
wr224e.Bosch_Ax.fields{1}.minimum = -1.638400e+02;
wr224e.Bosch_Ax.fields{1}.scale = 5.000000e-03;
wr224e.Bosch_Ax.fields{1}.offset = -1.638400e+02;

wr224e.Bosch_Ax.fields{2}.name = 'Bosch_ax_g';
wr224e.Bosch_Ax.fields{2}.units = 'g';
wr224e.Bosch_Ax.fields{2}.start_bit = 24;
wr224e.Bosch_Ax.fields{2}.bit_length = 16;
wr224e.Bosch_Ax.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Ax.fields{2}.data_type = 'UNSIGNED';
wr224e.Bosch_Ax.fields{2}.maximum = 4.176500e+00;
wr224e.Bosch_Ax.fields{2}.minimum = -4.176800e+00;
wr224e.Bosch_Ax.fields{2}.scale = 1.274650e-04;
wr224e.Bosch_Ax.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Ay'
wr224e.Bosch_Ay = struct;
wr224e.Bosch_Ay.name = 'Bosch_Ay';
wr224e.Bosch_Ay.description = 'Bosch_Ay';
wr224e.Bosch_Ay.protocol = 'wr224e';
wr224e.Bosch_Ay.id = hex2dec('00000174');
wr224e.Bosch_Ay.idext = 'STANDARD';
wr224e.Bosch_Ay.idinherit = 0;
wr224e.Bosch_Ay.payload_size = 8;
wr224e.Bosch_Ay.payload_value = [];
wr224e.Bosch_Ay.payload_mask = [];
wr224e.Bosch_Ay.interval = 50;

wr224e.Bosch_Ay.fields{1}.name = 'YawRate_Deg';
wr224e.Bosch_Ay.fields{1}.units = '';
wr224e.Bosch_Ay.fields{1}.start_bit = 56;
wr224e.Bosch_Ay.fields{1}.bit_length = 16;
wr224e.Bosch_Ay.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Ay.fields{1}.data_type = 'UNSIGNED';
wr224e.Bosch_Ay.fields{1}.maximum = 1.638300e+02;
wr224e.Bosch_Ay.fields{1}.minimum = -1.638400e+02;
wr224e.Bosch_Ay.fields{1}.scale = 5.000000e-03;
wr224e.Bosch_Ay.fields{1}.offset = -1.638400e+02;

wr224e.Bosch_Ay.fields{2}.name = 'Bosch_ay_g';
wr224e.Bosch_Ay.fields{2}.units = 'g';
wr224e.Bosch_Ay.fields{2}.start_bit = 24;
wr224e.Bosch_Ay.fields{2}.bit_length = 16;
wr224e.Bosch_Ay.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Ay.fields{2}.data_type = 'UNSIGNED';
wr224e.Bosch_Ay.fields{2}.maximum = 4.176500e+00;
wr224e.Bosch_Ay.fields{2}.minimum = -4.176800e+00;
wr224e.Bosch_Ay.fields{2}.scale = 1.274650e-04;
wr224e.Bosch_Ay.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Az'
wr224e.Bosch_Az = struct;
wr224e.Bosch_Az.name = 'Bosch_Az';
wr224e.Bosch_Az.description = 'Bosch_Az';
wr224e.Bosch_Az.protocol = 'wr224e';
wr224e.Bosch_Az.id = hex2dec('0000017C');
wr224e.Bosch_Az.idext = 'STANDARD';
wr224e.Bosch_Az.idinherit = 0;
wr224e.Bosch_Az.payload_size = 8;
wr224e.Bosch_Az.payload_value = [];
wr224e.Bosch_Az.payload_mask = [];
wr224e.Bosch_Az.interval = 50;

wr224e.Bosch_Az.fields{1}.name = 'AZ_STAT';
wr224e.Bosch_Az.fields{1}.units = '';
wr224e.Bosch_Az.fields{1}.start_bit = 12;
wr224e.Bosch_Az.fields{1}.bit_length = 4;
wr224e.Bosch_Az.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Az.fields{1}.data_type = 'UNSIGNED';
wr224e.Bosch_Az.fields{1}.scale = 1.000000e+00;
wr224e.Bosch_Az.fields{1}.offset = 0.000000e+00;

wr224e.Bosch_Az.fields{2}.name = 'Bosch_az_g';
wr224e.Bosch_Az.fields{2}.units = 'g';
wr224e.Bosch_Az.fields{2}.start_bit = 24;
wr224e.Bosch_Az.fields{2}.bit_length = 16;
wr224e.Bosch_Az.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Az.fields{2}.data_type = 'UNSIGNED';
wr224e.Bosch_Az.fields{2}.maximum = 4.176500e+00;
wr224e.Bosch_Az.fields{2}.minimum = -4.176800e+00;
wr224e.Bosch_Az.fields{2}.scale = 1.274650e-04;
wr224e.Bosch_Az.fields{2}.offset = -4.176800e+00;

%===================================================================
case 'Bosch_Steering_Angle'
wr224e.Bosch_Steering_Angle = struct;
wr224e.Bosch_Steering_Angle.name = 'Bosch_Steering_Angle';
wr224e.Bosch_Steering_Angle.description = 'Bosch_Steering_Angle';
wr224e.Bosch_Steering_Angle.protocol = 'wr224e';
wr224e.Bosch_Steering_Angle.id = hex2dec('000002B0');
wr224e.Bosch_Steering_Angle.idext = 'STANDARD';
wr224e.Bosch_Steering_Angle.idinherit = 0;
wr224e.Bosch_Steering_Angle.payload_size = 5;
wr224e.Bosch_Steering_Angle.payload_value = [];
wr224e.Bosch_Steering_Angle.payload_mask = [];
wr224e.Bosch_Steering_Angle.interval = 50;

wr224e.Bosch_Steering_Angle.fields{1}.name = 'Trim';
wr224e.Bosch_Steering_Angle.fields{1}.units = '';
wr224e.Bosch_Steering_Angle.fields{1}.start_bit = 34;
wr224e.Bosch_Steering_Angle.fields{1}.bit_length = 1;
wr224e.Bosch_Steering_Angle.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle.fields{1}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle.fields{1}.maximum = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{1}.minimum = 0.000000e+00;
wr224e.Bosch_Steering_Angle.fields{1}.scale = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{1}.offset = 0.000000e+00;

wr224e.Bosch_Steering_Angle.fields{2}.name = 'Cal';
wr224e.Bosch_Steering_Angle.fields{2}.units = '';
wr224e.Bosch_Steering_Angle.fields{2}.start_bit = 33;
wr224e.Bosch_Steering_Angle.fields{2}.bit_length = 1;
wr224e.Bosch_Steering_Angle.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle.fields{2}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle.fields{2}.maximum = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{2}.minimum = 0.000000e+00;
wr224e.Bosch_Steering_Angle.fields{2}.scale = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{2}.offset = 0.000000e+00;

wr224e.Bosch_Steering_Angle.fields{3}.name = 'Ok';
wr224e.Bosch_Steering_Angle.fields{3}.units = '';
wr224e.Bosch_Steering_Angle.fields{3}.start_bit = 32;
wr224e.Bosch_Steering_Angle.fields{3}.bit_length = 1;
wr224e.Bosch_Steering_Angle.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle.fields{3}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle.fields{3}.maximum = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{3}.minimum = 0.000000e+00;
wr224e.Bosch_Steering_Angle.fields{3}.scale = 1.000000e+00;
wr224e.Bosch_Steering_Angle.fields{3}.offset = 0.000000e+00;

wr224e.Bosch_Steering_Angle.fields{4}.name = 'SAS_Speed';
wr224e.Bosch_Steering_Angle.fields{4}.units = 'deg/s';
wr224e.Bosch_Steering_Angle.fields{4}.start_bit = 40;
wr224e.Bosch_Steering_Angle.fields{4}.bit_length = 8;
wr224e.Bosch_Steering_Angle.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle.fields{4}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle.fields{4}.maximum = 1.016000e+03;
wr224e.Bosch_Steering_Angle.fields{4}.minimum = 0.000000e+00;
wr224e.Bosch_Steering_Angle.fields{4}.scale = 4.000000e+00;
wr224e.Bosch_Steering_Angle.fields{4}.offset = 0.000000e+00;

wr224e.Bosch_Steering_Angle.fields{5}.name = 'SAS_Angle';
wr224e.Bosch_Steering_Angle.fields{5}.units = 'deg';
wr224e.Bosch_Steering_Angle.fields{5}.start_bit = 56;
wr224e.Bosch_Steering_Angle.fields{5}.bit_length = 16;
wr224e.Bosch_Steering_Angle.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle.fields{5}.data_type = 'SIGNED';
wr224e.Bosch_Steering_Angle.fields{5}.maximum = 7.800000e+02;
wr224e.Bosch_Steering_Angle.fields{5}.minimum = -7.800000e+02;
wr224e.Bosch_Steering_Angle.fields{5}.scale = 1.000000e-01;
wr224e.Bosch_Steering_Angle.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Config'
wr224e.Bosch_Steering_Angle_Config = struct;
wr224e.Bosch_Steering_Angle_Config.name = 'Bosch_Steering_Angle_Config';
wr224e.Bosch_Steering_Angle_Config.description = 'Bosch_Steering_Angle_Config';
wr224e.Bosch_Steering_Angle_Config.protocol = 'wr224e';
wr224e.Bosch_Steering_Angle_Config.id = hex2dec('000007C0');
wr224e.Bosch_Steering_Angle_Config.idext = 'STANDARD';
wr224e.Bosch_Steering_Angle_Config.idinherit = 0;
wr224e.Bosch_Steering_Angle_Config.payload_size = 2;
wr224e.Bosch_Steering_Angle_Config.payload_value = [];
wr224e.Bosch_Steering_Angle_Config.payload_mask = [];
wr224e.Bosch_Steering_Angle_Config.interval = 50;

wr224e.Bosch_Steering_Angle_Config.fields{1}.name = 'Reset';
wr224e.Bosch_Steering_Angle_Config.fields{1}.units = '';
wr224e.Bosch_Steering_Angle_Config.fields{1}.start_bit = 60;
wr224e.Bosch_Steering_Angle_Config.fields{1}.bit_length = 12;
wr224e.Bosch_Steering_Angle_Config.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle_Config.fields{1}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle_Config.fields{1}.scale = 1.000000e+00;
wr224e.Bosch_Steering_Angle_Config.fields{1}.offset = 0.000000e+00;

wr224e.Bosch_Steering_Angle_Config.fields{2}.name = 'Cal_Zero';
wr224e.Bosch_Steering_Angle_Config.fields{2}.units = '';
wr224e.Bosch_Steering_Angle_Config.fields{2}.start_bit = 56;
wr224e.Bosch_Steering_Angle_Config.fields{2}.bit_length = 4;
wr224e.Bosch_Steering_Angle_Config.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Bosch_Steering_Angle_Config.fields{2}.data_type = 'UNSIGNED';
wr224e.Bosch_Steering_Angle_Config.fields{2}.scale = 1.000000e+00;
wr224e.Bosch_Steering_Angle_Config.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'Brake_Rotor_Sensor_Temp'
wr224e.Brake_Rotor_Sensor_Temp = struct;
wr224e.Brake_Rotor_Sensor_Temp.name = 'Brake_Rotor_Sensor_Temp';
wr224e.Brake_Rotor_Sensor_Temp.description = 'Brake_Rotor_Sensor_Temp';
wr224e.Brake_Rotor_Sensor_Temp.protocol = 'wr224e';
wr224e.Brake_Rotor_Sensor_Temp.id = hex2dec('000004C8');
wr224e.Brake_Rotor_Sensor_Temp.idext = 'STANDARD';
wr224e.Brake_Rotor_Sensor_Temp.idinherit = 0;
wr224e.Brake_Rotor_Sensor_Temp.payload_size = 2;
wr224e.Brake_Rotor_Sensor_Temp.payload_value = [];
wr224e.Brake_Rotor_Sensor_Temp.payload_mask = [];
wr224e.Brake_Rotor_Sensor_Temp.interval = 50;

wr224e.Brake_Rotor_Sensor_Temp.fields{1}.name = 'Brake_Sensor_Temp';
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.units = 'C';
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.start_bit = 48;
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.bit_length = 16;
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Sensor_Temp.fields{1}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp1'
wr224e.Brake_Rotor_Temp1 = struct;
wr224e.Brake_Rotor_Temp1.name = 'Brake_Rotor_Temp1';
wr224e.Brake_Rotor_Temp1.description = 'Brake_Rotor_Temp1';
wr224e.Brake_Rotor_Temp1.protocol = 'wr224e';
wr224e.Brake_Rotor_Temp1.id = hex2dec('000004C4');
wr224e.Brake_Rotor_Temp1.idext = 'STANDARD';
wr224e.Brake_Rotor_Temp1.idinherit = 0;
wr224e.Brake_Rotor_Temp1.payload_size = 8;
wr224e.Brake_Rotor_Temp1.payload_value = [];
wr224e.Brake_Rotor_Temp1.payload_mask = [];
wr224e.Brake_Rotor_Temp1.interval = 50;

wr224e.Brake_Rotor_Temp1.fields{1}.name = 'Rotor_Temp_1';
wr224e.Brake_Rotor_Temp1.fields{1}.units = 'C';
wr224e.Brake_Rotor_Temp1.fields{1}.start_bit = 48;
wr224e.Brake_Rotor_Temp1.fields{1}.bit_length = 16;
wr224e.Brake_Rotor_Temp1.fields{1}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp1.fields{1}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp1.fields{1}.maximum = 6.452000e+03;
wr224e.Brake_Rotor_Temp1.fields{1}.minimum = 0.000000e+00;
wr224e.Brake_Rotor_Temp1.fields{1}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp1.fields{1}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp1.fields{2}.name = 'Rotor_Temp_2';
wr224e.Brake_Rotor_Temp1.fields{2}.units = 'C';
wr224e.Brake_Rotor_Temp1.fields{2}.start_bit = 32;
wr224e.Brake_Rotor_Temp1.fields{2}.bit_length = 16;
wr224e.Brake_Rotor_Temp1.fields{2}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp1.fields{2}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp1.fields{2}.maximum = 6.452000e+03;
wr224e.Brake_Rotor_Temp1.fields{2}.minimum = 0.000000e+00;
wr224e.Brake_Rotor_Temp1.fields{2}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp1.fields{2}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp1.fields{3}.name = 'Rotor_Temp_3';
wr224e.Brake_Rotor_Temp1.fields{3}.units = 'C';
wr224e.Brake_Rotor_Temp1.fields{3}.start_bit = 16;
wr224e.Brake_Rotor_Temp1.fields{3}.bit_length = 16;
wr224e.Brake_Rotor_Temp1.fields{3}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp1.fields{3}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp1.fields{3}.maximum = 6.452000e+03;
wr224e.Brake_Rotor_Temp1.fields{3}.minimum = 0.000000e+00;
wr224e.Brake_Rotor_Temp1.fields{3}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp1.fields{3}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp1.fields{4}.name = 'Rotor_Temp_4';
wr224e.Brake_Rotor_Temp1.fields{4}.units = 'C';
wr224e.Brake_Rotor_Temp1.fields{4}.start_bit = 0;
wr224e.Brake_Rotor_Temp1.fields{4}.bit_length = 16;
wr224e.Brake_Rotor_Temp1.fields{4}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp1.fields{4}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp1.fields{4}.maximum = 6.452000e+03;
wr224e.Brake_Rotor_Temp1.fields{4}.minimum = 0.000000e+00;
wr224e.Brake_Rotor_Temp1.fields{4}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp1.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp2'
wr224e.Brake_Rotor_Temp2 = struct;
wr224e.Brake_Rotor_Temp2.name = 'Brake_Rotor_Temp2';
wr224e.Brake_Rotor_Temp2.description = 'Brake_Rotor_Temp2';
wr224e.Brake_Rotor_Temp2.protocol = 'wr224e';
wr224e.Brake_Rotor_Temp2.id = hex2dec('000004C5');
wr224e.Brake_Rotor_Temp2.idext = 'STANDARD';
wr224e.Brake_Rotor_Temp2.idinherit = 0;
wr224e.Brake_Rotor_Temp2.payload_size = 8;
wr224e.Brake_Rotor_Temp2.payload_value = [];
wr224e.Brake_Rotor_Temp2.payload_mask = [];
wr224e.Brake_Rotor_Temp2.interval = 50;

wr224e.Brake_Rotor_Temp2.fields{1}.name = 'Rotor_Temp_5';
wr224e.Brake_Rotor_Temp2.fields{1}.units = 'C';
wr224e.Brake_Rotor_Temp2.fields{1}.start_bit = 48;
wr224e.Brake_Rotor_Temp2.fields{1}.bit_length = 16;
wr224e.Brake_Rotor_Temp2.fields{1}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp2.fields{1}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp2.fields{1}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp2.fields{1}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp2.fields{2}.name = 'Rotor_Temp_6';
wr224e.Brake_Rotor_Temp2.fields{2}.units = 'C';
wr224e.Brake_Rotor_Temp2.fields{2}.start_bit = 32;
wr224e.Brake_Rotor_Temp2.fields{2}.bit_length = 16;
wr224e.Brake_Rotor_Temp2.fields{2}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp2.fields{2}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp2.fields{2}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp2.fields{2}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp2.fields{3}.name = 'Rotor_Temp_7';
wr224e.Brake_Rotor_Temp2.fields{3}.units = 'C';
wr224e.Brake_Rotor_Temp2.fields{3}.start_bit = 16;
wr224e.Brake_Rotor_Temp2.fields{3}.bit_length = 16;
wr224e.Brake_Rotor_Temp2.fields{3}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp2.fields{3}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp2.fields{3}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp2.fields{3}.offset = -1.000000e+02;

wr224e.Brake_Rotor_Temp2.fields{4}.name = 'Rotor_Temp_8';
wr224e.Brake_Rotor_Temp2.fields{4}.units = 'C';
wr224e.Brake_Rotor_Temp2.fields{4}.start_bit = 0;
wr224e.Brake_Rotor_Temp2.fields{4}.bit_length = 16;
wr224e.Brake_Rotor_Temp2.fields{4}.byte_order = 'BIG_ENDIAN';
wr224e.Brake_Rotor_Temp2.fields{4}.data_type = 'UNSIGNED';
wr224e.Brake_Rotor_Temp2.fields{4}.scale = 1.000000e-01;
wr224e.Brake_Rotor_Temp2.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Coolant_Temps'
wr224e.Coolant_Temps = struct;
wr224e.Coolant_Temps.name = 'Coolant_Temps';
wr224e.Coolant_Temps.description = 'Coolant_Temps';
wr224e.Coolant_Temps.protocol = 'wr224e';
wr224e.Coolant_Temps.id = hex2dec('00000336');
wr224e.Coolant_Temps.idext = 'STANDARD';
wr224e.Coolant_Temps.idinherit = 0;
wr224e.Coolant_Temps.payload_size = 8;
wr224e.Coolant_Temps.payload_value = [];
wr224e.Coolant_Temps.payload_mask = [];
wr224e.Coolant_Temps.interval = 50;

wr224e.Coolant_Temps.fields{1}.name = 'Coolant_Temp_L';
wr224e.Coolant_Temps.fields{1}.units = '';
wr224e.Coolant_Temps.fields{1}.start_bit = 56;
wr224e.Coolant_Temps.fields{1}.bit_length = 16;
wr224e.Coolant_Temps.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Coolant_Temps.fields{1}.data_type = 'UNSIGNED';
wr224e.Coolant_Temps.fields{1}.scale = 1.000000e-01;
wr224e.Coolant_Temps.fields{1}.offset = 0.000000e+00;

wr224e.Coolant_Temps.fields{2}.name = 'Coolant_Temp_R';
wr224e.Coolant_Temps.fields{2}.units = '';
wr224e.Coolant_Temps.fields{2}.start_bit = 40;
wr224e.Coolant_Temps.fields{2}.bit_length = 16;
wr224e.Coolant_Temps.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Coolant_Temps.fields{2}.data_type = 'UNSIGNED';
wr224e.Coolant_Temps.fields{2}.scale = 1.000000e-01;
wr224e.Coolant_Temps.fields{2}.offset = 0.000000e+00;

wr224e.Coolant_Temps.fields{3}.name = 'Temp_1';
wr224e.Coolant_Temps.fields{3}.units = '';
wr224e.Coolant_Temps.fields{3}.start_bit = 24;
wr224e.Coolant_Temps.fields{3}.bit_length = 16;
wr224e.Coolant_Temps.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.Coolant_Temps.fields{3}.data_type = 'UNSIGNED';
wr224e.Coolant_Temps.fields{3}.scale = 1.000000e-01;
wr224e.Coolant_Temps.fields{3}.offset = 0.000000e+00;

wr224e.Coolant_Temps.fields{4}.name = 'Temp_2';
wr224e.Coolant_Temps.fields{4}.units = '';
wr224e.Coolant_Temps.fields{4}.start_bit = 8;
wr224e.Coolant_Temps.fields{4}.bit_length = 16;
wr224e.Coolant_Temps.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.Coolant_Temps.fields{4}.data_type = 'UNSIGNED';
wr224e.Coolant_Temps.fields{4}.scale = 1.000000e-01;
wr224e.Coolant_Temps.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Controls'
wr224e.IFL_Controls = struct;
wr224e.IFL_Controls.name = 'IFL_Controls';
wr224e.IFL_Controls.description = 'IFL_Controls';
wr224e.IFL_Controls.protocol = 'wr224e';
wr224e.IFL_Controls.id = hex2dec('000000C4');
wr224e.IFL_Controls.idext = 'STANDARD';
wr224e.IFL_Controls.idinherit = 0;
wr224e.IFL_Controls.payload_size = 8;
wr224e.IFL_Controls.payload_value = [];
wr224e.IFL_Controls.payload_mask = [];
wr224e.IFL_Controls.interval = 50;

wr224e.IFL_Controls.fields{1}.name = 'IFL_T_IGBT';
wr224e.IFL_Controls.fields{1}.units = 'C';
wr224e.IFL_Controls.fields{1}.start_bit = 0;
wr224e.IFL_Controls.fields{1}.bit_length = 8;
wr224e.IFL_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Controls.fields{1}.data_type = 'UNSIGNED';
wr224e.IFL_Controls.fields{1}.maximum = 2.160000e+02;
wr224e.IFL_Controls.fields{1}.minimum = 0.000000e+00;
wr224e.IFL_Controls.fields{1}.scale = 1.000000e+00;
wr224e.IFL_Controls.fields{1}.offset = -4.000000e+01;

wr224e.IFL_Controls.fields{2}.name = 'IFL_T_winding';
wr224e.IFL_Controls.fields{2}.units = 'C';
wr224e.IFL_Controls.fields{2}.start_bit = 8;
wr224e.IFL_Controls.fields{2}.bit_length = 8;
wr224e.IFL_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Controls.fields{2}.data_type = 'UNSIGNED';
wr224e.IFL_Controls.fields{2}.maximum = 2.160000e+02;
wr224e.IFL_Controls.fields{2}.minimum = 0.000000e+00;
wr224e.IFL_Controls.fields{2}.scale = 1.000000e+00;
wr224e.IFL_Controls.fields{2}.offset = -4.000000e+01;

wr224e.IFL_Controls.fields{3}.name = 'IFL_I_s_amplitude';
wr224e.IFL_Controls.fields{3}.units = 'A';
wr224e.IFL_Controls.fields{3}.start_bit = 24;
wr224e.IFL_Controls.fields{3}.bit_length = 16;
wr224e.IFL_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Controls.fields{3}.data_type = 'SIGNED';
wr224e.IFL_Controls.fields{3}.maximum = 6.550000e+02;
wr224e.IFL_Controls.fields{3}.minimum = -6.550000e+02;
wr224e.IFL_Controls.fields{3}.scale = 2.000000e-02;
wr224e.IFL_Controls.fields{3}.offset = 0.000000e+00;

wr224e.IFL_Controls.fields{4}.name = 'IFL_P_ac';
wr224e.IFL_Controls.fields{4}.units = 'W';
wr224e.IFL_Controls.fields{4}.start_bit = 40;
wr224e.IFL_Controls.fields{4}.bit_length = 16;
wr224e.IFL_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Controls.fields{4}.data_type = 'SIGNED';
wr224e.IFL_Controls.fields{4}.maximum = 6.400000e+04;
wr224e.IFL_Controls.fields{4}.minimum = -6.400000e+04;
wr224e.IFL_Controls.fields{4}.scale = 2.000000e+00;
wr224e.IFL_Controls.fields{4}.offset = 0.000000e+00;

wr224e.IFL_Controls.fields{5}.name = 'IFL_MotorSpeed';
wr224e.IFL_Controls.fields{5}.units = 'RPM';
wr224e.IFL_Controls.fields{5}.start_bit = 56;
wr224e.IFL_Controls.fields{5}.bit_length = 16;
wr224e.IFL_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Controls.fields{5}.data_type = 'SIGNED';
wr224e.IFL_Controls.fields{5}.maximum = 3.270000e+04;
wr224e.IFL_Controls.fields{5}.minimum = -3.270000e+04;
wr224e.IFL_Controls.fields{5}.scale = 1.000000e+00;
wr224e.IFL_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Debug'
wr224e.IFL_Debug = struct;
wr224e.IFL_Debug.name = 'IFL_Debug';
wr224e.IFL_Debug.description = 'IFL_Debug';
wr224e.IFL_Debug.protocol = 'wr224e';
wr224e.IFL_Debug.id = hex2dec('000001D9');
wr224e.IFL_Debug.idext = 'STANDARD';
wr224e.IFL_Debug.idinherit = 0;
wr224e.IFL_Debug.payload_size = 8;
wr224e.IFL_Debug.payload_value = [];
wr224e.IFL_Debug.payload_mask = [];
wr224e.IFL_Debug.interval = 50;

wr224e.IFL_Debug.fields{1}.name = 'Debug_Value4';
wr224e.IFL_Debug.fields{1}.units = '';
wr224e.IFL_Debug.fields{1}.start_bit = 8;
wr224e.IFL_Debug.fields{1}.bit_length = 16;
wr224e.IFL_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Debug.fields{1}.data_type = 'SIGNED';
wr224e.IFL_Debug.fields{1}.scale = 1.000000e-02;
wr224e.IFL_Debug.fields{1}.offset = 0.000000e+00;

wr224e.IFL_Debug.fields{2}.name = 'Debug_Value3';
wr224e.IFL_Debug.fields{2}.units = '';
wr224e.IFL_Debug.fields{2}.start_bit = 24;
wr224e.IFL_Debug.fields{2}.bit_length = 16;
wr224e.IFL_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Debug.fields{2}.data_type = 'SIGNED';
wr224e.IFL_Debug.fields{2}.scale = 1.000000e-02;
wr224e.IFL_Debug.fields{2}.offset = 0.000000e+00;

wr224e.IFL_Debug.fields{3}.name = 'Debug_Value2';
wr224e.IFL_Debug.fields{3}.units = '';
wr224e.IFL_Debug.fields{3}.start_bit = 40;
wr224e.IFL_Debug.fields{3}.bit_length = 16;
wr224e.IFL_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Debug.fields{3}.data_type = 'SIGNED';
wr224e.IFL_Debug.fields{3}.scale = 1.000000e-02;
wr224e.IFL_Debug.fields{3}.offset = 0.000000e+00;

wr224e.IFL_Debug.fields{4}.name = 'Debug_Value1';
wr224e.IFL_Debug.fields{4}.units = '';
wr224e.IFL_Debug.fields{4}.start_bit = 56;
wr224e.IFL_Debug.fields{4}.bit_length = 16;
wr224e.IFL_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Debug.fields{4}.data_type = 'SIGNED';
wr224e.IFL_Debug.fields{4}.scale = 1.000000e-02;
wr224e.IFL_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_FaultCode'
wr224e.IFL_FaultCode = struct;
wr224e.IFL_FaultCode.name = 'IFL_FaultCode';
wr224e.IFL_FaultCode.description = 'IFL_FaultCode';
wr224e.IFL_FaultCode.protocol = 'wr224e';
wr224e.IFL_FaultCode.id = hex2dec('000000D1');
wr224e.IFL_FaultCode.idext = 'STANDARD';
wr224e.IFL_FaultCode.idinherit = 0;
wr224e.IFL_FaultCode.payload_size = 5;
wr224e.IFL_FaultCode.payload_value = [];
wr224e.IFL_FaultCode.payload_mask = [];
wr224e.IFL_FaultCode.interval = 50;

wr224e.IFL_FaultCode.fields{1}.name = 'IFL_SDCControl';
wr224e.IFL_FaultCode.fields{1}.units = '';
wr224e.IFL_FaultCode.fields{1}.start_bit = 24;
wr224e.IFL_FaultCode.fields{1}.bit_length = 8;
wr224e.IFL_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_FaultCode.fields{1}.data_type = 'UNSIGNED';
wr224e.IFL_FaultCode.fields{1}.scale = 1.000000e+00;
wr224e.IFL_FaultCode.fields{1}.offset = 0.000000e+00;

wr224e.IFL_FaultCode.fields{2}.name = 'IFL_DiagnosticInfo';
wr224e.IFL_FaultCode.fields{2}.units = '';
wr224e.IFL_FaultCode.fields{2}.start_bit = 40;
wr224e.IFL_FaultCode.fields{2}.bit_length = 16;
wr224e.IFL_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_FaultCode.fields{2}.data_type = 'UNSIGNED';
wr224e.IFL_FaultCode.fields{2}.maximum = 6.553600e+04;
wr224e.IFL_FaultCode.fields{2}.minimum = 0.000000e+00;
wr224e.IFL_FaultCode.fields{2}.scale = 1.000000e+00;
wr224e.IFL_FaultCode.fields{2}.offset = 0.000000e+00;

wr224e.IFL_FaultCode.fields{3}.name = 'IFL_FaultCode';
wr224e.IFL_FaultCode.fields{3}.units = '';
wr224e.IFL_FaultCode.fields{3}.start_bit = 56;
wr224e.IFL_FaultCode.fields{3}.bit_length = 16;
wr224e.IFL_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_FaultCode.fields{3}.data_type = 'UNSIGNED';
wr224e.IFL_FaultCode.fields{3}.maximum = 6.553600e+04;
wr224e.IFL_FaultCode.fields{3}.minimum = 0.000000e+00;
wr224e.IFL_FaultCode.fields{3}.scale = 1.000000e+00;
wr224e.IFL_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Measured_Vdq'
wr224e.IFL_Measured_Vdq = struct;
wr224e.IFL_Measured_Vdq.name = 'IFL_Measured_Vdq';
wr224e.IFL_Measured_Vdq.description = 'IFL_Measured_Vdq';
wr224e.IFL_Measured_Vdq.protocol = 'wr224e';
wr224e.IFL_Measured_Vdq.id = hex2dec('000000DB');
wr224e.IFL_Measured_Vdq.idext = 'STANDARD';
wr224e.IFL_Measured_Vdq.idinherit = 0;
wr224e.IFL_Measured_Vdq.payload_size = 8;
wr224e.IFL_Measured_Vdq.payload_value = [];
wr224e.IFL_Measured_Vdq.payload_mask = [];
wr224e.IFL_Measured_Vdq.interval = 50;

wr224e.IFL_Measured_Vdq.fields{1}.name = 'IFL_elecAngle';
wr224e.IFL_Measured_Vdq.fields{1}.units = 'Degrees';
wr224e.IFL_Measured_Vdq.fields{1}.start_bit = 8;
wr224e.IFL_Measured_Vdq.fields{1}.bit_length = 16;
wr224e.IFL_Measured_Vdq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Measured_Vdq.fields{1}.data_type = 'SIGNED';
wr224e.IFL_Measured_Vdq.fields{1}.scale = 1.000000e+00;
wr224e.IFL_Measured_Vdq.fields{1}.offset = 0.000000e+00;

wr224e.IFL_Measured_Vdq.fields{2}.name = 'IFL_mechAngle';
wr224e.IFL_Measured_Vdq.fields{2}.units = 'Degrees';
wr224e.IFL_Measured_Vdq.fields{2}.start_bit = 24;
wr224e.IFL_Measured_Vdq.fields{2}.bit_length = 16;
wr224e.IFL_Measured_Vdq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Measured_Vdq.fields{2}.data_type = 'UNSIGNED';
wr224e.IFL_Measured_Vdq.fields{2}.scale = 1.000000e+00;
wr224e.IFL_Measured_Vdq.fields{2}.offset = 0.000000e+00;

wr224e.IFL_Measured_Vdq.fields{3}.name = 'IFL_meas_Vq';
wr224e.IFL_Measured_Vdq.fields{3}.units = 'V';
wr224e.IFL_Measured_Vdq.fields{3}.start_bit = 40;
wr224e.IFL_Measured_Vdq.fields{3}.bit_length = 16;
wr224e.IFL_Measured_Vdq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Measured_Vdq.fields{3}.data_type = 'SIGNED';
wr224e.IFL_Measured_Vdq.fields{3}.scale = 2.000000e-02;
wr224e.IFL_Measured_Vdq.fields{3}.offset = 0.000000e+00;

wr224e.IFL_Measured_Vdq.fields{4}.name = 'IFL_meas_Vd';
wr224e.IFL_Measured_Vdq.fields{4}.units = 'V';
wr224e.IFL_Measured_Vdq.fields{4}.start_bit = 56;
wr224e.IFL_Measured_Vdq.fields{4}.bit_length = 16;
wr224e.IFL_Measured_Vdq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Measured_Vdq.fields{4}.data_type = 'SIGNED';
wr224e.IFL_Measured_Vdq.fields{4}.scale = 1.000000e+00;
wr224e.IFL_Measured_Vdq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_MotorParams'
wr224e.IFL_MotorParams = struct;
wr224e.IFL_MotorParams.name = 'IFL_MotorParams';
wr224e.IFL_MotorParams.description = 'IFL_MotorParams';
wr224e.IFL_MotorParams.protocol = 'wr224e';
wr224e.IFL_MotorParams.id = hex2dec('000000C5');
wr224e.IFL_MotorParams.idext = 'STANDARD';
wr224e.IFL_MotorParams.idinherit = 0;
wr224e.IFL_MotorParams.payload_size = 8;
wr224e.IFL_MotorParams.payload_value = [];
wr224e.IFL_MotorParams.payload_mask = [];
wr224e.IFL_MotorParams.interval = 50;

wr224e.IFL_MotorParams.fields{1}.name = 'IFL_Vdc';
wr224e.IFL_MotorParams.fields{1}.units = 'V';
wr224e.IFL_MotorParams.fields{1}.start_bit = 8;
wr224e.IFL_MotorParams.fields{1}.bit_length = 16;
wr224e.IFL_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{1}.data_type = 'SIGNED';
wr224e.IFL_MotorParams.fields{1}.maximum = 3.270000e+02;
wr224e.IFL_MotorParams.fields{1}.minimum = -3.270000e+02;
wr224e.IFL_MotorParams.fields{1}.scale = 1.000000e-02;
wr224e.IFL_MotorParams.fields{1}.offset = 0.000000e+00;

wr224e.IFL_MotorParams.fields{2}.name = 'IFL_Torque_est';
wr224e.IFL_MotorParams.fields{2}.units = 'Nm';
wr224e.IFL_MotorParams.fields{2}.start_bit = 24;
wr224e.IFL_MotorParams.fields{2}.bit_length = 16;
wr224e.IFL_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{2}.data_type = 'SIGNED';
wr224e.IFL_MotorParams.fields{2}.scale = 2.000000e-03;
wr224e.IFL_MotorParams.fields{2}.offset = 0.000000e+00;

wr224e.IFL_MotorParams.fields{3}.name = 'IFL_R_winding';
wr224e.IFL_MotorParams.fields{3}.units = 'mOhm';
wr224e.IFL_MotorParams.fields{3}.start_bit = 32;
wr224e.IFL_MotorParams.fields{3}.bit_length = 8;
wr224e.IFL_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{3}.data_type = 'UNSIGNED';
wr224e.IFL_MotorParams.fields{3}.maximum = 5.120000e+01;
wr224e.IFL_MotorParams.fields{3}.minimum = 0.000000e+00;
wr224e.IFL_MotorParams.fields{3}.scale = 2.000000e-01;
wr224e.IFL_MotorParams.fields{3}.offset = 0.000000e+00;

wr224e.IFL_MotorParams.fields{4}.name = 'IFL_K_t';
wr224e.IFL_MotorParams.fields{4}.units = 'Nm/A';
wr224e.IFL_MotorParams.fields{4}.start_bit = 40;
wr224e.IFL_MotorParams.fields{4}.bit_length = 8;
wr224e.IFL_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{4}.data_type = 'UNSIGNED';
wr224e.IFL_MotorParams.fields{4}.maximum = 5.120000e-01;
wr224e.IFL_MotorParams.fields{4}.minimum = 0.000000e+00;
wr224e.IFL_MotorParams.fields{4}.scale = 2.000000e-03;
wr224e.IFL_MotorParams.fields{4}.offset = 0.000000e+00;

wr224e.IFL_MotorParams.fields{5}.name = 'IFL_L_q';
wr224e.IFL_MotorParams.fields{5}.units = 'uH';
wr224e.IFL_MotorParams.fields{5}.start_bit = 48;
wr224e.IFL_MotorParams.fields{5}.bit_length = 8;
wr224e.IFL_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{5}.data_type = 'UNSIGNED';
wr224e.IFL_MotorParams.fields{5}.maximum = 1.280000e+02;
wr224e.IFL_MotorParams.fields{5}.minimum = 0.000000e+00;
wr224e.IFL_MotorParams.fields{5}.scale = 5.000000e-01;
wr224e.IFL_MotorParams.fields{5}.offset = 0.000000e+00;

wr224e.IFL_MotorParams.fields{6}.name = 'IFL_L_d';
wr224e.IFL_MotorParams.fields{6}.units = 'uH';
wr224e.IFL_MotorParams.fields{6}.start_bit = 56;
wr224e.IFL_MotorParams.fields{6}.bit_length = 8;
wr224e.IFL_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_MotorParams.fields{6}.data_type = 'UNSIGNED';
wr224e.IFL_MotorParams.fields{6}.maximum = 1.280000e+02;
wr224e.IFL_MotorParams.fields{6}.minimum = 0.000000e+00;
wr224e.IFL_MotorParams.fields{6}.scale = 5.000000e-01;
wr224e.IFL_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Observer'
wr224e.IFL_Observer = struct;
wr224e.IFL_Observer.name = 'IFL_Observer';
wr224e.IFL_Observer.description = 'IFL_Observer';
wr224e.IFL_Observer.protocol = 'wr224e';
wr224e.IFL_Observer.id = hex2dec('000001D1');
wr224e.IFL_Observer.idext = 'STANDARD';
wr224e.IFL_Observer.idinherit = 0;
wr224e.IFL_Observer.payload_size = 8;
wr224e.IFL_Observer.payload_value = [];
wr224e.IFL_Observer.payload_mask = [];
wr224e.IFL_Observer.interval = 50;

wr224e.IFL_Observer.fields{1}.name = 'IFL_Iq_ref';
wr224e.IFL_Observer.fields{1}.units = 'A';
wr224e.IFL_Observer.fields{1}.start_bit = 40;
wr224e.IFL_Observer.fields{1}.bit_length = 16;
wr224e.IFL_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Observer.fields{1}.data_type = 'SIGNED';
wr224e.IFL_Observer.fields{1}.scale = 2.000000e-02;
wr224e.IFL_Observer.fields{1}.offset = 0.000000e+00;

wr224e.IFL_Observer.fields{2}.name = 'IFL_Id_ref';
wr224e.IFL_Observer.fields{2}.units = 'A';
wr224e.IFL_Observer.fields{2}.start_bit = 56;
wr224e.IFL_Observer.fields{2}.bit_length = 16;
wr224e.IFL_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Observer.fields{2}.data_type = 'SIGNED';
wr224e.IFL_Observer.fields{2}.scale = 2.000000e-02;
wr224e.IFL_Observer.fields{2}.offset = 0.000000e+00;

wr224e.IFL_Observer.fields{3}.name = 'IFL_flux_q';
wr224e.IFL_Observer.fields{3}.units = 'VpHz';
wr224e.IFL_Observer.fields{3}.start_bit = 8;
wr224e.IFL_Observer.fields{3}.bit_length = 16;
wr224e.IFL_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Observer.fields{3}.data_type = 'SIGNED';
wr224e.IFL_Observer.fields{3}.scale = 3.333333e-04;
wr224e.IFL_Observer.fields{3}.offset = 0.000000e+00;

wr224e.IFL_Observer.fields{4}.name = 'IFL_flux_d';
wr224e.IFL_Observer.fields{4}.units = 'VpHz';
wr224e.IFL_Observer.fields{4}.start_bit = 24;
wr224e.IFL_Observer.fields{4}.bit_length = 16;
wr224e.IFL_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Observer.fields{4}.data_type = 'SIGNED';
wr224e.IFL_Observer.fields{4}.scale = 3.333333e-04;
wr224e.IFL_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFL_Offsets'
wr224e.IFL_Offsets = struct;
wr224e.IFL_Offsets.name = 'IFL_Offsets';
wr224e.IFL_Offsets.description = 'IFL_Offsets';
wr224e.IFL_Offsets.protocol = 'wr224e';
wr224e.IFL_Offsets.id = hex2dec('000000C7');
wr224e.IFL_Offsets.idext = 'STANDARD';
wr224e.IFL_Offsets.idinherit = 0;
wr224e.IFL_Offsets.payload_size = 6;
wr224e.IFL_Offsets.payload_value = [];
wr224e.IFL_Offsets.payload_mask = [];
wr224e.IFL_Offsets.interval = 50;

wr224e.IFL_Offsets.fields{1}.name = 'IFL_V_c_offset';
wr224e.IFL_Offsets.fields{1}.units = 'ADC';
wr224e.IFL_Offsets.fields{1}.start_bit = 16;
wr224e.IFL_Offsets.fields{1}.bit_length = 8;
wr224e.IFL_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{1}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{1}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{1}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{1}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{1}.offset = 0.000000e+00;

wr224e.IFL_Offsets.fields{2}.name = 'IFL_V_b_offset';
wr224e.IFL_Offsets.fields{2}.units = 'ADC';
wr224e.IFL_Offsets.fields{2}.start_bit = 24;
wr224e.IFL_Offsets.fields{2}.bit_length = 8;
wr224e.IFL_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{2}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{2}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{2}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{2}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{2}.offset = 0.000000e+00;

wr224e.IFL_Offsets.fields{3}.name = 'IFL_V_a_offset';
wr224e.IFL_Offsets.fields{3}.units = 'ADC';
wr224e.IFL_Offsets.fields{3}.start_bit = 32;
wr224e.IFL_Offsets.fields{3}.bit_length = 8;
wr224e.IFL_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{3}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{3}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{3}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{3}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{3}.offset = 0.000000e+00;

wr224e.IFL_Offsets.fields{4}.name = 'IFL_I_c_offset';
wr224e.IFL_Offsets.fields{4}.units = 'ADC';
wr224e.IFL_Offsets.fields{4}.start_bit = 40;
wr224e.IFL_Offsets.fields{4}.bit_length = 8;
wr224e.IFL_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{4}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{4}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{4}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{4}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{4}.offset = 0.000000e+00;

wr224e.IFL_Offsets.fields{5}.name = 'IFL_I_b_offset';
wr224e.IFL_Offsets.fields{5}.units = 'ADC';
wr224e.IFL_Offsets.fields{5}.start_bit = 48;
wr224e.IFL_Offsets.fields{5}.bit_length = 8;
wr224e.IFL_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{5}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{5}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{5}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{5}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{5}.offset = 0.000000e+00;

wr224e.IFL_Offsets.fields{6}.name = 'IFL_I_a_offset';
wr224e.IFL_Offsets.fields{6}.units = 'ADC';
wr224e.IFL_Offsets.fields{6}.start_bit = 56;
wr224e.IFL_Offsets.fields{6}.bit_length = 8;
wr224e.IFL_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_Offsets.fields{6}.data_type = 'SIGNED';
wr224e.IFL_Offsets.fields{6}.maximum = 1.280000e+02;
wr224e.IFL_Offsets.fields{6}.minimum = -1.270000e+02;
wr224e.IFL_Offsets.fields{6}.scale = 1.000000e+00;
wr224e.IFL_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFL_dq'
wr224e.IFL_dq = struct;
wr224e.IFL_dq.name = 'IFL_dq';
wr224e.IFL_dq.description = 'IFL_dq';
wr224e.IFL_dq.protocol = 'wr224e';
wr224e.IFL_dq.id = hex2dec('000000C6');
wr224e.IFL_dq.idext = 'STANDARD';
wr224e.IFL_dq.idinherit = 0;
wr224e.IFL_dq.payload_size = 8;
wr224e.IFL_dq.payload_value = [];
wr224e.IFL_dq.payload_mask = [];
wr224e.IFL_dq.interval = 50;

wr224e.IFL_dq.fields{1}.name = 'IFL_I_q';
wr224e.IFL_dq.fields{1}.units = 'A';
wr224e.IFL_dq.fields{1}.start_bit = 8;
wr224e.IFL_dq.fields{1}.bit_length = 16;
wr224e.IFL_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_dq.fields{1}.data_type = 'SIGNED';
wr224e.IFL_dq.fields{1}.maximum = 6.540000e+02;
wr224e.IFL_dq.fields{1}.minimum = -6.540000e+02;
wr224e.IFL_dq.fields{1}.scale = 2.000000e-02;
wr224e.IFL_dq.fields{1}.offset = 0.000000e+00;

wr224e.IFL_dq.fields{2}.name = 'IFL_I_d';
wr224e.IFL_dq.fields{2}.units = 'A';
wr224e.IFL_dq.fields{2}.start_bit = 24;
wr224e.IFL_dq.fields{2}.bit_length = 16;
wr224e.IFL_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_dq.fields{2}.data_type = 'SIGNED';
wr224e.IFL_dq.fields{2}.maximum = 6.540000e+02;
wr224e.IFL_dq.fields{2}.minimum = -6.540000e+02;
wr224e.IFL_dq.fields{2}.scale = 2.000000e-02;
wr224e.IFL_dq.fields{2}.offset = 0.000000e+00;

wr224e.IFL_dq.fields{3}.name = 'IFL_V_q';
wr224e.IFL_dq.fields{3}.units = 'V';
wr224e.IFL_dq.fields{3}.start_bit = 40;
wr224e.IFL_dq.fields{3}.bit_length = 16;
wr224e.IFL_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_dq.fields{3}.data_type = 'SIGNED';
wr224e.IFL_dq.fields{3}.maximum = 6.540000e+02;
wr224e.IFL_dq.fields{3}.minimum = -6.540000e+02;
wr224e.IFL_dq.fields{3}.scale = 2.000000e-02;
wr224e.IFL_dq.fields{3}.offset = 0.000000e+00;

wr224e.IFL_dq.fields{4}.name = 'IFL_V_d';
wr224e.IFL_dq.fields{4}.units = 'V';
wr224e.IFL_dq.fields{4}.start_bit = 56;
wr224e.IFL_dq.fields{4}.bit_length = 16;
wr224e.IFL_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFL_dq.fields{4}.data_type = 'SIGNED';
wr224e.IFL_dq.fields{4}.maximum = 6.540000e+02;
wr224e.IFL_dq.fields{4}.minimum = -6.540000e+02;
wr224e.IFL_dq.fields{4}.scale = 2.000000e-02;
wr224e.IFL_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFLrx_Bootloader'
wr224e.IFLrx_Bootloader = struct;
wr224e.IFLrx_Bootloader.name = 'IFLrx_Bootloader';
wr224e.IFLrx_Bootloader.description = 'IFLrx_Bootloader';
wr224e.IFLrx_Bootloader.protocol = 'wr224e';
wr224e.IFLrx_Bootloader.id = hex2dec('000000B2');
wr224e.IFLrx_Bootloader.idext = 'STANDARD';
wr224e.IFLrx_Bootloader.idinherit = 0;
wr224e.IFLrx_Bootloader.payload_size = 8;
wr224e.IFLrx_Bootloader.payload_value = [];
wr224e.IFLrx_Bootloader.payload_mask = [];
wr224e.IFLrx_Bootloader.interval = 50;

%===================================================================
case 'IFR_Controls'
wr224e.IFR_Controls = struct;
wr224e.IFR_Controls.name = 'IFR_Controls';
wr224e.IFR_Controls.description = 'IFR_Controls';
wr224e.IFR_Controls.protocol = 'wr224e';
wr224e.IFR_Controls.id = hex2dec('000000C0');
wr224e.IFR_Controls.idext = 'STANDARD';
wr224e.IFR_Controls.idinherit = 0;
wr224e.IFR_Controls.payload_size = 8;
wr224e.IFR_Controls.payload_value = [];
wr224e.IFR_Controls.payload_mask = [];
wr224e.IFR_Controls.interval = 50;

wr224e.IFR_Controls.fields{1}.name = 'IFR_T_IGBT';
wr224e.IFR_Controls.fields{1}.units = 'C';
wr224e.IFR_Controls.fields{1}.start_bit = 0;
wr224e.IFR_Controls.fields{1}.bit_length = 8;
wr224e.IFR_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Controls.fields{1}.data_type = 'UNSIGNED';
wr224e.IFR_Controls.fields{1}.maximum = 2.160000e+02;
wr224e.IFR_Controls.fields{1}.minimum = 0.000000e+00;
wr224e.IFR_Controls.fields{1}.scale = 1.000000e+00;
wr224e.IFR_Controls.fields{1}.offset = -4.000000e+01;

wr224e.IFR_Controls.fields{2}.name = 'IFR_T_winding';
wr224e.IFR_Controls.fields{2}.units = 'C';
wr224e.IFR_Controls.fields{2}.start_bit = 8;
wr224e.IFR_Controls.fields{2}.bit_length = 8;
wr224e.IFR_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Controls.fields{2}.data_type = 'UNSIGNED';
wr224e.IFR_Controls.fields{2}.maximum = 2.160000e+02;
wr224e.IFR_Controls.fields{2}.minimum = 0.000000e+00;
wr224e.IFR_Controls.fields{2}.scale = 1.000000e+00;
wr224e.IFR_Controls.fields{2}.offset = -4.000000e+01;

wr224e.IFR_Controls.fields{3}.name = 'IFR_I_s_amplitude';
wr224e.IFR_Controls.fields{3}.units = 'A';
wr224e.IFR_Controls.fields{3}.start_bit = 24;
wr224e.IFR_Controls.fields{3}.bit_length = 16;
wr224e.IFR_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Controls.fields{3}.data_type = 'SIGNED';
wr224e.IFR_Controls.fields{3}.maximum = 6.550000e+02;
wr224e.IFR_Controls.fields{3}.minimum = -6.550000e+02;
wr224e.IFR_Controls.fields{3}.scale = 2.000000e-02;
wr224e.IFR_Controls.fields{3}.offset = 0.000000e+00;

wr224e.IFR_Controls.fields{4}.name = 'IFR_P_ac';
wr224e.IFR_Controls.fields{4}.units = 'W';
wr224e.IFR_Controls.fields{4}.start_bit = 40;
wr224e.IFR_Controls.fields{4}.bit_length = 16;
wr224e.IFR_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Controls.fields{4}.data_type = 'SIGNED';
wr224e.IFR_Controls.fields{4}.maximum = 6.400000e+04;
wr224e.IFR_Controls.fields{4}.minimum = -6.400000e+04;
wr224e.IFR_Controls.fields{4}.scale = 2.000000e+00;
wr224e.IFR_Controls.fields{4}.offset = 0.000000e+00;

wr224e.IFR_Controls.fields{5}.name = 'IFR_MotorSpeed';
wr224e.IFR_Controls.fields{5}.units = 'RPM';
wr224e.IFR_Controls.fields{5}.start_bit = 56;
wr224e.IFR_Controls.fields{5}.bit_length = 16;
wr224e.IFR_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Controls.fields{5}.data_type = 'SIGNED';
wr224e.IFR_Controls.fields{5}.maximum = 3.270000e+04;
wr224e.IFR_Controls.fields{5}.minimum = -3.270000e+04;
wr224e.IFR_Controls.fields{5}.scale = 1.000000e+00;
wr224e.IFR_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Debug'
wr224e.IFR_Debug = struct;
wr224e.IFR_Debug.name = 'IFR_Debug';
wr224e.IFR_Debug.description = 'IFR_Debug';
wr224e.IFR_Debug.protocol = 'wr224e';
wr224e.IFR_Debug.id = hex2dec('000001D8');
wr224e.IFR_Debug.idext = 'STANDARD';
wr224e.IFR_Debug.idinherit = 0;
wr224e.IFR_Debug.payload_size = 8;
wr224e.IFR_Debug.payload_value = [];
wr224e.IFR_Debug.payload_mask = [];
wr224e.IFR_Debug.interval = 50;

wr224e.IFR_Debug.fields{1}.name = 'Debug_Value4';
wr224e.IFR_Debug.fields{1}.units = '';
wr224e.IFR_Debug.fields{1}.start_bit = 8;
wr224e.IFR_Debug.fields{1}.bit_length = 16;
wr224e.IFR_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Debug.fields{1}.data_type = 'SIGNED';
wr224e.IFR_Debug.fields{1}.scale = 1.000000e-02;
wr224e.IFR_Debug.fields{1}.offset = 0.000000e+00;

wr224e.IFR_Debug.fields{2}.name = 'Debug_Value3';
wr224e.IFR_Debug.fields{2}.units = '';
wr224e.IFR_Debug.fields{2}.start_bit = 24;
wr224e.IFR_Debug.fields{2}.bit_length = 16;
wr224e.IFR_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Debug.fields{2}.data_type = 'SIGNED';
wr224e.IFR_Debug.fields{2}.scale = 1.000000e-02;
wr224e.IFR_Debug.fields{2}.offset = 0.000000e+00;

wr224e.IFR_Debug.fields{3}.name = 'Debug_Value2';
wr224e.IFR_Debug.fields{3}.units = '';
wr224e.IFR_Debug.fields{3}.start_bit = 40;
wr224e.IFR_Debug.fields{3}.bit_length = 16;
wr224e.IFR_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Debug.fields{3}.data_type = 'SIGNED';
wr224e.IFR_Debug.fields{3}.scale = 1.000000e-02;
wr224e.IFR_Debug.fields{3}.offset = 0.000000e+00;

wr224e.IFR_Debug.fields{4}.name = 'Debug_Value1';
wr224e.IFR_Debug.fields{4}.units = '';
wr224e.IFR_Debug.fields{4}.start_bit = 56;
wr224e.IFR_Debug.fields{4}.bit_length = 16;
wr224e.IFR_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Debug.fields{4}.data_type = 'SIGNED';
wr224e.IFR_Debug.fields{4}.scale = 1.000000e-02;
wr224e.IFR_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_FaultCode'
wr224e.IFR_FaultCode = struct;
wr224e.IFR_FaultCode.name = 'IFR_FaultCode';
wr224e.IFR_FaultCode.description = 'IFR_FaultCode';
wr224e.IFR_FaultCode.protocol = 'wr224e';
wr224e.IFR_FaultCode.id = hex2dec('000000D0');
wr224e.IFR_FaultCode.idext = 'STANDARD';
wr224e.IFR_FaultCode.idinherit = 0;
wr224e.IFR_FaultCode.payload_size = 5;
wr224e.IFR_FaultCode.payload_value = [];
wr224e.IFR_FaultCode.payload_mask = [];
wr224e.IFR_FaultCode.interval = 50;

wr224e.IFR_FaultCode.fields{1}.name = 'IFR_SDCControl';
wr224e.IFR_FaultCode.fields{1}.units = '';
wr224e.IFR_FaultCode.fields{1}.start_bit = 24;
wr224e.IFR_FaultCode.fields{1}.bit_length = 8;
wr224e.IFR_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_FaultCode.fields{1}.data_type = 'UNSIGNED';
wr224e.IFR_FaultCode.fields{1}.scale = 1.000000e+00;
wr224e.IFR_FaultCode.fields{1}.offset = 0.000000e+00;

wr224e.IFR_FaultCode.fields{2}.name = 'IFR_DiagnosticInfo';
wr224e.IFR_FaultCode.fields{2}.units = '';
wr224e.IFR_FaultCode.fields{2}.start_bit = 40;
wr224e.IFR_FaultCode.fields{2}.bit_length = 16;
wr224e.IFR_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_FaultCode.fields{2}.data_type = 'UNSIGNED';
wr224e.IFR_FaultCode.fields{2}.maximum = 6.553600e+04;
wr224e.IFR_FaultCode.fields{2}.minimum = 0.000000e+00;
wr224e.IFR_FaultCode.fields{2}.scale = 1.000000e+00;
wr224e.IFR_FaultCode.fields{2}.offset = 0.000000e+00;

wr224e.IFR_FaultCode.fields{3}.name = 'IFR_FaultCode';
wr224e.IFR_FaultCode.fields{3}.units = '';
wr224e.IFR_FaultCode.fields{3}.start_bit = 56;
wr224e.IFR_FaultCode.fields{3}.bit_length = 16;
wr224e.IFR_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_FaultCode.fields{3}.data_type = 'UNSIGNED';
wr224e.IFR_FaultCode.fields{3}.maximum = 6.553600e+04;
wr224e.IFR_FaultCode.fields{3}.minimum = 0.000000e+00;
wr224e.IFR_FaultCode.fields{3}.scale = 1.000000e+00;
wr224e.IFR_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Measured_Vdq'
wr224e.IFR_Measured_Vdq = struct;
wr224e.IFR_Measured_Vdq.name = 'IFR_Measured_Vdq';
wr224e.IFR_Measured_Vdq.description = 'IFR_Measured_Vdq';
wr224e.IFR_Measured_Vdq.protocol = 'wr224e';
wr224e.IFR_Measured_Vdq.id = hex2dec('000000DA');
wr224e.IFR_Measured_Vdq.idext = 'STANDARD';
wr224e.IFR_Measured_Vdq.idinherit = 0;
wr224e.IFR_Measured_Vdq.payload_size = 8;
wr224e.IFR_Measured_Vdq.payload_value = [];
wr224e.IFR_Measured_Vdq.payload_mask = [];
wr224e.IFR_Measured_Vdq.interval = 50;

wr224e.IFR_Measured_Vdq.fields{1}.name = 'IFR_elecAngle';
wr224e.IFR_Measured_Vdq.fields{1}.units = 'Degrees';
wr224e.IFR_Measured_Vdq.fields{1}.start_bit = 8;
wr224e.IFR_Measured_Vdq.fields{1}.bit_length = 16;
wr224e.IFR_Measured_Vdq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Measured_Vdq.fields{1}.data_type = 'SIGNED';
wr224e.IFR_Measured_Vdq.fields{1}.scale = 1.000000e+00;
wr224e.IFR_Measured_Vdq.fields{1}.offset = 0.000000e+00;

wr224e.IFR_Measured_Vdq.fields{2}.name = 'IFR_mechAngle';
wr224e.IFR_Measured_Vdq.fields{2}.units = 'Degrees';
wr224e.IFR_Measured_Vdq.fields{2}.start_bit = 24;
wr224e.IFR_Measured_Vdq.fields{2}.bit_length = 16;
wr224e.IFR_Measured_Vdq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Measured_Vdq.fields{2}.data_type = 'UNSIGNED';
wr224e.IFR_Measured_Vdq.fields{2}.scale = 1.000000e+00;
wr224e.IFR_Measured_Vdq.fields{2}.offset = 0.000000e+00;

wr224e.IFR_Measured_Vdq.fields{3}.name = 'IFR_meas_Vd';
wr224e.IFR_Measured_Vdq.fields{3}.units = 'V';
wr224e.IFR_Measured_Vdq.fields{3}.start_bit = 56;
wr224e.IFR_Measured_Vdq.fields{3}.bit_length = 16;
wr224e.IFR_Measured_Vdq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Measured_Vdq.fields{3}.data_type = 'SIGNED';
wr224e.IFR_Measured_Vdq.fields{3}.scale = 1.000000e+00;
wr224e.IFR_Measured_Vdq.fields{3}.offset = 0.000000e+00;

wr224e.IFR_Measured_Vdq.fields{4}.name = 'IFR_meas_Vq';
wr224e.IFR_Measured_Vdq.fields{4}.units = 'V';
wr224e.IFR_Measured_Vdq.fields{4}.start_bit = 40;
wr224e.IFR_Measured_Vdq.fields{4}.bit_length = 16;
wr224e.IFR_Measured_Vdq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Measured_Vdq.fields{4}.data_type = 'SIGNED';
wr224e.IFR_Measured_Vdq.fields{4}.scale = 2.000000e-02;
wr224e.IFR_Measured_Vdq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_MotorParams'
wr224e.IFR_MotorParams = struct;
wr224e.IFR_MotorParams.name = 'IFR_MotorParams';
wr224e.IFR_MotorParams.description = 'IFR_MotorParams';
wr224e.IFR_MotorParams.protocol = 'wr224e';
wr224e.IFR_MotorParams.id = hex2dec('000000C1');
wr224e.IFR_MotorParams.idext = 'STANDARD';
wr224e.IFR_MotorParams.idinherit = 0;
wr224e.IFR_MotorParams.payload_size = 8;
wr224e.IFR_MotorParams.payload_value = [];
wr224e.IFR_MotorParams.payload_mask = [];
wr224e.IFR_MotorParams.interval = 50;

wr224e.IFR_MotorParams.fields{1}.name = 'IFR_Vdc';
wr224e.IFR_MotorParams.fields{1}.units = 'V';
wr224e.IFR_MotorParams.fields{1}.start_bit = 8;
wr224e.IFR_MotorParams.fields{1}.bit_length = 16;
wr224e.IFR_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{1}.data_type = 'SIGNED';
wr224e.IFR_MotorParams.fields{1}.maximum = 3.270000e+02;
wr224e.IFR_MotorParams.fields{1}.minimum = -3.270000e+02;
wr224e.IFR_MotorParams.fields{1}.scale = 1.000000e-02;
wr224e.IFR_MotorParams.fields{1}.offset = 0.000000e+00;

wr224e.IFR_MotorParams.fields{2}.name = 'IFR_Torque_est';
wr224e.IFR_MotorParams.fields{2}.units = 'Nm';
wr224e.IFR_MotorParams.fields{2}.start_bit = 24;
wr224e.IFR_MotorParams.fields{2}.bit_length = 16;
wr224e.IFR_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{2}.data_type = 'SIGNED';
wr224e.IFR_MotorParams.fields{2}.scale = 2.000000e-03;
wr224e.IFR_MotorParams.fields{2}.offset = 0.000000e+00;

wr224e.IFR_MotorParams.fields{3}.name = 'IFR_R_winding';
wr224e.IFR_MotorParams.fields{3}.units = 'mOhm';
wr224e.IFR_MotorParams.fields{3}.start_bit = 32;
wr224e.IFR_MotorParams.fields{3}.bit_length = 8;
wr224e.IFR_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{3}.data_type = 'UNSIGNED';
wr224e.IFR_MotorParams.fields{3}.maximum = 5.120000e+01;
wr224e.IFR_MotorParams.fields{3}.minimum = 0.000000e+00;
wr224e.IFR_MotorParams.fields{3}.scale = 2.000000e-01;
wr224e.IFR_MotorParams.fields{3}.offset = 0.000000e+00;

wr224e.IFR_MotorParams.fields{4}.name = 'IFR_K_t';
wr224e.IFR_MotorParams.fields{4}.units = 'Nm/A';
wr224e.IFR_MotorParams.fields{4}.start_bit = 40;
wr224e.IFR_MotorParams.fields{4}.bit_length = 8;
wr224e.IFR_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{4}.data_type = 'UNSIGNED';
wr224e.IFR_MotorParams.fields{4}.maximum = 5.120000e-01;
wr224e.IFR_MotorParams.fields{4}.minimum = 0.000000e+00;
wr224e.IFR_MotorParams.fields{4}.scale = 2.000000e-03;
wr224e.IFR_MotorParams.fields{4}.offset = 0.000000e+00;

wr224e.IFR_MotorParams.fields{5}.name = 'IFR_L_q';
wr224e.IFR_MotorParams.fields{5}.units = 'uH';
wr224e.IFR_MotorParams.fields{5}.start_bit = 48;
wr224e.IFR_MotorParams.fields{5}.bit_length = 8;
wr224e.IFR_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{5}.data_type = 'UNSIGNED';
wr224e.IFR_MotorParams.fields{5}.maximum = 1.280000e+02;
wr224e.IFR_MotorParams.fields{5}.minimum = 0.000000e+00;
wr224e.IFR_MotorParams.fields{5}.scale = 5.000000e-01;
wr224e.IFR_MotorParams.fields{5}.offset = 0.000000e+00;

wr224e.IFR_MotorParams.fields{6}.name = 'IFR_L_d';
wr224e.IFR_MotorParams.fields{6}.units = 'uH';
wr224e.IFR_MotorParams.fields{6}.start_bit = 56;
wr224e.IFR_MotorParams.fields{6}.bit_length = 8;
wr224e.IFR_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_MotorParams.fields{6}.data_type = 'UNSIGNED';
wr224e.IFR_MotorParams.fields{6}.maximum = 1.280000e+02;
wr224e.IFR_MotorParams.fields{6}.minimum = 0.000000e+00;
wr224e.IFR_MotorParams.fields{6}.scale = 5.000000e-01;
wr224e.IFR_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Observer'
wr224e.IFR_Observer = struct;
wr224e.IFR_Observer.name = 'IFR_Observer';
wr224e.IFR_Observer.description = 'IFR_Observer';
wr224e.IFR_Observer.protocol = 'wr224e';
wr224e.IFR_Observer.id = hex2dec('000001D0');
wr224e.IFR_Observer.idext = 'STANDARD';
wr224e.IFR_Observer.idinherit = 0;
wr224e.IFR_Observer.payload_size = 8;
wr224e.IFR_Observer.payload_value = [];
wr224e.IFR_Observer.payload_mask = [];
wr224e.IFR_Observer.interval = 50;

wr224e.IFR_Observer.fields{1}.name = 'IFR_Iq_ref';
wr224e.IFR_Observer.fields{1}.units = 'A';
wr224e.IFR_Observer.fields{1}.start_bit = 40;
wr224e.IFR_Observer.fields{1}.bit_length = 16;
wr224e.IFR_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Observer.fields{1}.data_type = 'SIGNED';
wr224e.IFR_Observer.fields{1}.scale = 2.000000e-02;
wr224e.IFR_Observer.fields{1}.offset = 0.000000e+00;

wr224e.IFR_Observer.fields{2}.name = 'IFR_Id_ref';
wr224e.IFR_Observer.fields{2}.units = 'A';
wr224e.IFR_Observer.fields{2}.start_bit = 56;
wr224e.IFR_Observer.fields{2}.bit_length = 16;
wr224e.IFR_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Observer.fields{2}.data_type = 'SIGNED';
wr224e.IFR_Observer.fields{2}.scale = 2.000000e-02;
wr224e.IFR_Observer.fields{2}.offset = 0.000000e+00;

wr224e.IFR_Observer.fields{3}.name = 'IFR_flux_q';
wr224e.IFR_Observer.fields{3}.units = 'VpHz';
wr224e.IFR_Observer.fields{3}.start_bit = 8;
wr224e.IFR_Observer.fields{3}.bit_length = 16;
wr224e.IFR_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Observer.fields{3}.data_type = 'SIGNED';
wr224e.IFR_Observer.fields{3}.scale = 3.333333e-04;
wr224e.IFR_Observer.fields{3}.offset = 0.000000e+00;

wr224e.IFR_Observer.fields{4}.name = 'IFR_flux_d';
wr224e.IFR_Observer.fields{4}.units = 'VpHz';
wr224e.IFR_Observer.fields{4}.start_bit = 24;
wr224e.IFR_Observer.fields{4}.bit_length = 16;
wr224e.IFR_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Observer.fields{4}.data_type = 'SIGNED';
wr224e.IFR_Observer.fields{4}.scale = 3.333333e-04;
wr224e.IFR_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFR_Offsets'
wr224e.IFR_Offsets = struct;
wr224e.IFR_Offsets.name = 'IFR_Offsets';
wr224e.IFR_Offsets.description = 'IFR_Offsets';
wr224e.IFR_Offsets.protocol = 'wr224e';
wr224e.IFR_Offsets.id = hex2dec('000000C3');
wr224e.IFR_Offsets.idext = 'STANDARD';
wr224e.IFR_Offsets.idinherit = 0;
wr224e.IFR_Offsets.payload_size = 6;
wr224e.IFR_Offsets.payload_value = [];
wr224e.IFR_Offsets.payload_mask = [];
wr224e.IFR_Offsets.interval = 50;

wr224e.IFR_Offsets.fields{1}.name = 'IFR_V_c_offset';
wr224e.IFR_Offsets.fields{1}.units = 'ADC';
wr224e.IFR_Offsets.fields{1}.start_bit = 16;
wr224e.IFR_Offsets.fields{1}.bit_length = 8;
wr224e.IFR_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{1}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{1}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{1}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{1}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{1}.offset = 0.000000e+00;

wr224e.IFR_Offsets.fields{2}.name = 'IFR_V_b_offset';
wr224e.IFR_Offsets.fields{2}.units = 'ADC';
wr224e.IFR_Offsets.fields{2}.start_bit = 24;
wr224e.IFR_Offsets.fields{2}.bit_length = 8;
wr224e.IFR_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{2}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{2}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{2}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{2}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{2}.offset = 0.000000e+00;

wr224e.IFR_Offsets.fields{3}.name = 'IFR_V_a_offset';
wr224e.IFR_Offsets.fields{3}.units = 'ADC';
wr224e.IFR_Offsets.fields{3}.start_bit = 32;
wr224e.IFR_Offsets.fields{3}.bit_length = 8;
wr224e.IFR_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{3}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{3}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{3}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{3}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{3}.offset = 0.000000e+00;

wr224e.IFR_Offsets.fields{4}.name = 'IFR_I_c_offset';
wr224e.IFR_Offsets.fields{4}.units = 'ADC';
wr224e.IFR_Offsets.fields{4}.start_bit = 40;
wr224e.IFR_Offsets.fields{4}.bit_length = 8;
wr224e.IFR_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{4}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{4}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{4}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{4}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{4}.offset = 0.000000e+00;

wr224e.IFR_Offsets.fields{5}.name = 'IFR_I_b_offset';
wr224e.IFR_Offsets.fields{5}.units = 'ADC';
wr224e.IFR_Offsets.fields{5}.start_bit = 48;
wr224e.IFR_Offsets.fields{5}.bit_length = 8;
wr224e.IFR_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{5}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{5}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{5}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{5}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{5}.offset = 0.000000e+00;

wr224e.IFR_Offsets.fields{6}.name = 'IFR_I_a_offset';
wr224e.IFR_Offsets.fields{6}.units = 'ADC';
wr224e.IFR_Offsets.fields{6}.start_bit = 56;
wr224e.IFR_Offsets.fields{6}.bit_length = 8;
wr224e.IFR_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_Offsets.fields{6}.data_type = 'SIGNED';
wr224e.IFR_Offsets.fields{6}.maximum = 1.280000e+02;
wr224e.IFR_Offsets.fields{6}.minimum = -1.270000e+02;
wr224e.IFR_Offsets.fields{6}.scale = 1.000000e+00;
wr224e.IFR_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IFR_dq'
wr224e.IFR_dq = struct;
wr224e.IFR_dq.name = 'IFR_dq';
wr224e.IFR_dq.description = 'IFR_dq';
wr224e.IFR_dq.protocol = 'wr224e';
wr224e.IFR_dq.id = hex2dec('000000C2');
wr224e.IFR_dq.idext = 'STANDARD';
wr224e.IFR_dq.idinherit = 0;
wr224e.IFR_dq.payload_size = 8;
wr224e.IFR_dq.payload_value = [];
wr224e.IFR_dq.payload_mask = [];
wr224e.IFR_dq.interval = 50;

wr224e.IFR_dq.fields{1}.name = 'IFR_I_q';
wr224e.IFR_dq.fields{1}.units = 'A';
wr224e.IFR_dq.fields{1}.start_bit = 8;
wr224e.IFR_dq.fields{1}.bit_length = 16;
wr224e.IFR_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_dq.fields{1}.data_type = 'SIGNED';
wr224e.IFR_dq.fields{1}.maximum = 6.540000e+02;
wr224e.IFR_dq.fields{1}.minimum = -6.540000e+02;
wr224e.IFR_dq.fields{1}.scale = 2.000000e-02;
wr224e.IFR_dq.fields{1}.offset = 0.000000e+00;

wr224e.IFR_dq.fields{2}.name = 'IFR_I_d';
wr224e.IFR_dq.fields{2}.units = 'A';
wr224e.IFR_dq.fields{2}.start_bit = 24;
wr224e.IFR_dq.fields{2}.bit_length = 16;
wr224e.IFR_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_dq.fields{2}.data_type = 'SIGNED';
wr224e.IFR_dq.fields{2}.maximum = 6.540000e+02;
wr224e.IFR_dq.fields{2}.minimum = -6.540000e+02;
wr224e.IFR_dq.fields{2}.scale = 2.000000e-02;
wr224e.IFR_dq.fields{2}.offset = 0.000000e+00;

wr224e.IFR_dq.fields{3}.name = 'IFR_V_q';
wr224e.IFR_dq.fields{3}.units = 'V';
wr224e.IFR_dq.fields{3}.start_bit = 40;
wr224e.IFR_dq.fields{3}.bit_length = 16;
wr224e.IFR_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_dq.fields{3}.data_type = 'SIGNED';
wr224e.IFR_dq.fields{3}.maximum = 6.540000e+02;
wr224e.IFR_dq.fields{3}.minimum = -6.540000e+02;
wr224e.IFR_dq.fields{3}.scale = 2.000000e-02;
wr224e.IFR_dq.fields{3}.offset = 0.000000e+00;

wr224e.IFR_dq.fields{4}.name = 'IFR_V_d';
wr224e.IFR_dq.fields{4}.units = 'V';
wr224e.IFR_dq.fields{4}.start_bit = 56;
wr224e.IFR_dq.fields{4}.bit_length = 16;
wr224e.IFR_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IFR_dq.fields{4}.data_type = 'SIGNED';
wr224e.IFR_dq.fields{4}.maximum = 6.540000e+02;
wr224e.IFR_dq.fields{4}.minimum = -6.540000e+02;
wr224e.IFR_dq.fields{4}.scale = 2.000000e-02;
wr224e.IFR_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IFRrx_Bootloader'
wr224e.IFRrx_Bootloader = struct;
wr224e.IFRrx_Bootloader.name = 'IFRrx_Bootloader';
wr224e.IFRrx_Bootloader.description = 'IFRrx_Bootloader';
wr224e.IFRrx_Bootloader.protocol = 'wr224e';
wr224e.IFRrx_Bootloader.id = hex2dec('000000B1');
wr224e.IFRrx_Bootloader.idext = 'STANDARD';
wr224e.IFRrx_Bootloader.idinherit = 0;
wr224e.IFRrx_Bootloader.payload_size = 8;
wr224e.IFRrx_Bootloader.payload_value = [];
wr224e.IFRrx_Bootloader.payload_mask = [];
wr224e.IFRrx_Bootloader.interval = 50;

%===================================================================
case 'INVrx_Bootloader'
wr224e.INVrx_Bootloader = struct;
wr224e.INVrx_Bootloader.name = 'INVrx_Bootloader';
wr224e.INVrx_Bootloader.description = 'INVrx_Bootloader';
wr224e.INVrx_Bootloader.protocol = 'wr224e';
wr224e.INVrx_Bootloader.id = hex2dec('000000B0');
wr224e.INVrx_Bootloader.idext = 'STANDARD';
wr224e.INVrx_Bootloader.idinherit = 0;
wr224e.INVrx_Bootloader.payload_size = 8;
wr224e.INVrx_Bootloader.payload_value = [];
wr224e.INVrx_Bootloader.payload_mask = [];
wr224e.INVrx_Bootloader.interval = 50;

%===================================================================
case 'INVrx_Config'
wr224e.INVrx_Config = struct;
wr224e.INVrx_Config.name = 'INVrx_Config';
wr224e.INVrx_Config.description = 'INVrx_Config';
wr224e.INVrx_Config.protocol = 'wr224e';
wr224e.INVrx_Config.id = hex2dec('000000E3');
wr224e.INVrx_Config.idext = 'STANDARD';
wr224e.INVrx_Config.idinherit = 0;
wr224e.INVrx_Config.payload_size = 8;
wr224e.INVrx_Config.payload_value = [];
wr224e.INVrx_Config.payload_mask = [];
wr224e.INVrx_Config.interval = 50;

wr224e.INVrx_Config.fields{1}.name = 'INVrx_Command';
wr224e.INVrx_Config.fields{1}.units = '';
wr224e.INVrx_Config.fields{1}.start_bit = 40;
wr224e.INVrx_Config.fields{1}.bit_length = 16;
wr224e.INVrx_Config.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_Config.fields{1}.data_type = 'UNSIGNED';
wr224e.INVrx_Config.fields{1}.scale = 1.000000e+00;
wr224e.INVrx_Config.fields{1}.offset = 0.000000e+00;

wr224e.INVrx_Config.fields{2}.name = 'INVrx_CommandValue';
wr224e.INVrx_Config.fields{2}.units = '';
wr224e.INVrx_Config.fields{2}.start_bit = 24;
wr224e.INVrx_Config.fields{2}.bit_length = 32;
wr224e.INVrx_Config.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_Config.fields{2}.data_type = 'UNSIGNED';
wr224e.INVrx_Config.fields{2}.scale = 1.000000e+00;
wr224e.INVrx_Config.fields{2}.offset = 0.000000e+00;

wr224e.INVrx_Config.fields{3}.name = 'INVrx_CommandTarget';
wr224e.INVrx_Config.fields{3}.units = '';
wr224e.INVrx_Config.fields{3}.start_bit = 56;
wr224e.INVrx_Config.fields{3}.bit_length = 16;
wr224e.INVrx_Config.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_Config.fields{3}.data_type = 'UNSIGNED';
wr224e.INVrx_Config.fields{3}.scale = 1.000000e+00;
wr224e.INVrx_Config.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'INVrx_CurrentRequest'
wr224e.INVrx_CurrentRequest = struct;
wr224e.INVrx_CurrentRequest.name = 'INVrx_CurrentRequest';
wr224e.INVrx_CurrentRequest.description = 'INVrx_CurrentRequest';
wr224e.INVrx_CurrentRequest.protocol = 'wr224e';
wr224e.INVrx_CurrentRequest.id = hex2dec('000000E4');
wr224e.INVrx_CurrentRequest.idext = 'STANDARD';
wr224e.INVrx_CurrentRequest.idinherit = 0;
wr224e.INVrx_CurrentRequest.payload_size = 6;
wr224e.INVrx_CurrentRequest.payload_value = [];
wr224e.INVrx_CurrentRequest.payload_mask = [];
wr224e.INVrx_CurrentRequest.interval = 50;

wr224e.INVrx_CurrentRequest.fields{1}.name = 'INVrx_current_controller';
wr224e.INVrx_CurrentRequest.fields{1}.units = '';
wr224e.INVrx_CurrentRequest.fields{1}.start_bit = 56;
wr224e.INVrx_CurrentRequest.fields{1}.bit_length = 16;
wr224e.INVrx_CurrentRequest.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_CurrentRequest.fields{1}.data_type = 'SIGNED';
wr224e.INVrx_CurrentRequest.fields{1}.scale = 1.000000e+00;
wr224e.INVrx_CurrentRequest.fields{1}.offset = 0.000000e+00;

wr224e.INVrx_CurrentRequest.fields{2}.name = 'INVrx_Iq_command';
wr224e.INVrx_CurrentRequest.fields{2}.units = 'A';
wr224e.INVrx_CurrentRequest.fields{2}.start_bit = 24;
wr224e.INVrx_CurrentRequest.fields{2}.bit_length = 16;
wr224e.INVrx_CurrentRequest.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_CurrentRequest.fields{2}.data_type = 'SIGNED';
wr224e.INVrx_CurrentRequest.fields{2}.scale = 2.000000e-02;
wr224e.INVrx_CurrentRequest.fields{2}.offset = 0.000000e+00;

wr224e.INVrx_CurrentRequest.fields{3}.name = 'INVrx_Id_request';
wr224e.INVrx_CurrentRequest.fields{3}.units = 'A';
wr224e.INVrx_CurrentRequest.fields{3}.start_bit = 40;
wr224e.INVrx_CurrentRequest.fields{3}.bit_length = 16;
wr224e.INVrx_CurrentRequest.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_CurrentRequest.fields{3}.data_type = 'SIGNED';
wr224e.INVrx_CurrentRequest.fields{3}.scale = 2.000000e-02;
wr224e.INVrx_CurrentRequest.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'INVrx_TorqueRequest'
wr224e.INVrx_TorqueRequest = struct;
wr224e.INVrx_TorqueRequest.name = 'INVrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.description = 'INVrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.protocol = 'wr224e';
wr224e.INVrx_TorqueRequest.id = hex2dec('000000E2');
wr224e.INVrx_TorqueRequest.idext = 'STANDARD';
wr224e.INVrx_TorqueRequest.idinherit = 0;
wr224e.INVrx_TorqueRequest.payload_size = 8;
wr224e.INVrx_TorqueRequest.payload_value = [];
wr224e.INVrx_TorqueRequest.payload_mask = [];
wr224e.INVrx_TorqueRequest.interval = 50;

wr224e.INVrx_TorqueRequest.fields{1}.name = 'IRRrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.fields{1}.units = 'Nm';
wr224e.INVrx_TorqueRequest.fields{1}.start_bit = 8;
wr224e.INVrx_TorqueRequest.fields{1}.bit_length = 16;
wr224e.INVrx_TorqueRequest.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_TorqueRequest.fields{1}.data_type = 'SIGNED';
wr224e.INVrx_TorqueRequest.fields{1}.maximum = 6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{1}.minimum = -6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{1}.scale = 2.000000e-03;
wr224e.INVrx_TorqueRequest.fields{1}.offset = 0.000000e+00;

wr224e.INVrx_TorqueRequest.fields{2}.name = 'IRLrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.fields{2}.units = 'Nm';
wr224e.INVrx_TorqueRequest.fields{2}.start_bit = 24;
wr224e.INVrx_TorqueRequest.fields{2}.bit_length = 16;
wr224e.INVrx_TorqueRequest.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_TorqueRequest.fields{2}.data_type = 'SIGNED';
wr224e.INVrx_TorqueRequest.fields{2}.maximum = 6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{2}.minimum = -6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{2}.scale = 2.000000e-03;
wr224e.INVrx_TorqueRequest.fields{2}.offset = 0.000000e+00;

wr224e.INVrx_TorqueRequest.fields{3}.name = 'IFLrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.fields{3}.units = 'Nm';
wr224e.INVrx_TorqueRequest.fields{3}.start_bit = 40;
wr224e.INVrx_TorqueRequest.fields{3}.bit_length = 16;
wr224e.INVrx_TorqueRequest.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_TorqueRequest.fields{3}.data_type = 'SIGNED';
wr224e.INVrx_TorqueRequest.fields{3}.maximum = 6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{3}.minimum = -6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{3}.scale = 2.000000e-03;
wr224e.INVrx_TorqueRequest.fields{3}.offset = 0.000000e+00;

wr224e.INVrx_TorqueRequest.fields{4}.name = 'IFRrx_TorqueRequest';
wr224e.INVrx_TorqueRequest.fields{4}.units = 'Nm';
wr224e.INVrx_TorqueRequest.fields{4}.start_bit = 56;
wr224e.INVrx_TorqueRequest.fields{4}.bit_length = 16;
wr224e.INVrx_TorqueRequest.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.INVrx_TorqueRequest.fields{4}.data_type = 'SIGNED';
wr224e.INVrx_TorqueRequest.fields{4}.maximum = 6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{4}.minimum = -6.550000e+01;
wr224e.INVrx_TorqueRequest.fields{4}.scale = 2.000000e-03;
wr224e.INVrx_TorqueRequest.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Controls'
wr224e.IRL_Controls = struct;
wr224e.IRL_Controls.name = 'IRL_Controls';
wr224e.IRL_Controls.description = 'IRL_Controls';
wr224e.IRL_Controls.protocol = 'wr224e';
wr224e.IRL_Controls.id = hex2dec('000000C8');
wr224e.IRL_Controls.idext = 'STANDARD';
wr224e.IRL_Controls.idinherit = 0;
wr224e.IRL_Controls.payload_size = 8;
wr224e.IRL_Controls.payload_value = [];
wr224e.IRL_Controls.payload_mask = [];
wr224e.IRL_Controls.interval = 50;

wr224e.IRL_Controls.fields{1}.name = 'IRL_T_IGBT';
wr224e.IRL_Controls.fields{1}.units = 'C';
wr224e.IRL_Controls.fields{1}.start_bit = 0;
wr224e.IRL_Controls.fields{1}.bit_length = 8;
wr224e.IRL_Controls.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Controls.fields{1}.data_type = 'UNSIGNED';
wr224e.IRL_Controls.fields{1}.maximum = 2.160000e+02;
wr224e.IRL_Controls.fields{1}.minimum = 0.000000e+00;
wr224e.IRL_Controls.fields{1}.scale = 1.000000e+00;
wr224e.IRL_Controls.fields{1}.offset = -4.000000e+01;

wr224e.IRL_Controls.fields{2}.name = 'IRL_T_winding';
wr224e.IRL_Controls.fields{2}.units = 'C';
wr224e.IRL_Controls.fields{2}.start_bit = 8;
wr224e.IRL_Controls.fields{2}.bit_length = 8;
wr224e.IRL_Controls.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Controls.fields{2}.data_type = 'UNSIGNED';
wr224e.IRL_Controls.fields{2}.maximum = 2.160000e+02;
wr224e.IRL_Controls.fields{2}.minimum = 0.000000e+00;
wr224e.IRL_Controls.fields{2}.scale = 1.000000e+00;
wr224e.IRL_Controls.fields{2}.offset = -4.000000e+01;

wr224e.IRL_Controls.fields{3}.name = 'IRL_I_s_amplitude';
wr224e.IRL_Controls.fields{3}.units = 'A';
wr224e.IRL_Controls.fields{3}.start_bit = 24;
wr224e.IRL_Controls.fields{3}.bit_length = 16;
wr224e.IRL_Controls.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Controls.fields{3}.data_type = 'SIGNED';
wr224e.IRL_Controls.fields{3}.maximum = 6.550000e+02;
wr224e.IRL_Controls.fields{3}.minimum = -6.550000e+02;
wr224e.IRL_Controls.fields{3}.scale = 2.000000e-02;
wr224e.IRL_Controls.fields{3}.offset = 0.000000e+00;

wr224e.IRL_Controls.fields{4}.name = 'IRL_P_ac';
wr224e.IRL_Controls.fields{4}.units = 'W';
wr224e.IRL_Controls.fields{4}.start_bit = 40;
wr224e.IRL_Controls.fields{4}.bit_length = 16;
wr224e.IRL_Controls.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Controls.fields{4}.data_type = 'SIGNED';
wr224e.IRL_Controls.fields{4}.maximum = 6.400000e+04;
wr224e.IRL_Controls.fields{4}.minimum = -6.400000e+04;
wr224e.IRL_Controls.fields{4}.scale = 2.000000e+00;
wr224e.IRL_Controls.fields{4}.offset = 0.000000e+00;

wr224e.IRL_Controls.fields{5}.name = 'IRL_MotorSpeed';
wr224e.IRL_Controls.fields{5}.units = 'RPM';
wr224e.IRL_Controls.fields{5}.start_bit = 56;
wr224e.IRL_Controls.fields{5}.bit_length = 16;
wr224e.IRL_Controls.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Controls.fields{5}.data_type = 'SIGNED';
wr224e.IRL_Controls.fields{5}.maximum = 3.270000e+04;
wr224e.IRL_Controls.fields{5}.minimum = -3.270000e+04;
wr224e.IRL_Controls.fields{5}.scale = 1.000000e+00;
wr224e.IRL_Controls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Debug'
wr224e.IRL_Debug = struct;
wr224e.IRL_Debug.name = 'IRL_Debug';
wr224e.IRL_Debug.description = 'IRL_Debug';
wr224e.IRL_Debug.protocol = 'wr224e';
wr224e.IRL_Debug.id = hex2dec('000001DA');
wr224e.IRL_Debug.idext = 'STANDARD';
wr224e.IRL_Debug.idinherit = 0;
wr224e.IRL_Debug.payload_size = 8;
wr224e.IRL_Debug.payload_value = [];
wr224e.IRL_Debug.payload_mask = [];
wr224e.IRL_Debug.interval = 50;

wr224e.IRL_Debug.fields{1}.name = 'Debug_Value4';
wr224e.IRL_Debug.fields{1}.units = '';
wr224e.IRL_Debug.fields{1}.start_bit = 8;
wr224e.IRL_Debug.fields{1}.bit_length = 16;
wr224e.IRL_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Debug.fields{1}.data_type = 'SIGNED';
wr224e.IRL_Debug.fields{1}.scale = 1.000000e-02;
wr224e.IRL_Debug.fields{1}.offset = 0.000000e+00;

wr224e.IRL_Debug.fields{2}.name = 'Debug_Value3';
wr224e.IRL_Debug.fields{2}.units = '';
wr224e.IRL_Debug.fields{2}.start_bit = 24;
wr224e.IRL_Debug.fields{2}.bit_length = 16;
wr224e.IRL_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Debug.fields{2}.data_type = 'SIGNED';
wr224e.IRL_Debug.fields{2}.scale = 1.000000e-02;
wr224e.IRL_Debug.fields{2}.offset = 0.000000e+00;

wr224e.IRL_Debug.fields{3}.name = 'Debug_Value2';
wr224e.IRL_Debug.fields{3}.units = '';
wr224e.IRL_Debug.fields{3}.start_bit = 40;
wr224e.IRL_Debug.fields{3}.bit_length = 16;
wr224e.IRL_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Debug.fields{3}.data_type = 'SIGNED';
wr224e.IRL_Debug.fields{3}.scale = 1.000000e-02;
wr224e.IRL_Debug.fields{3}.offset = 0.000000e+00;

wr224e.IRL_Debug.fields{4}.name = 'Debug_Value1';
wr224e.IRL_Debug.fields{4}.units = '';
wr224e.IRL_Debug.fields{4}.start_bit = 56;
wr224e.IRL_Debug.fields{4}.bit_length = 16;
wr224e.IRL_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Debug.fields{4}.data_type = 'SIGNED';
wr224e.IRL_Debug.fields{4}.scale = 1.000000e-02;
wr224e.IRL_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_FaultCode'
wr224e.IRL_FaultCode = struct;
wr224e.IRL_FaultCode.name = 'IRL_FaultCode';
wr224e.IRL_FaultCode.description = 'IRL_FaultCode';
wr224e.IRL_FaultCode.protocol = 'wr224e';
wr224e.IRL_FaultCode.id = hex2dec('000000D2');
wr224e.IRL_FaultCode.idext = 'STANDARD';
wr224e.IRL_FaultCode.idinherit = 0;
wr224e.IRL_FaultCode.payload_size = 5;
wr224e.IRL_FaultCode.payload_value = [];
wr224e.IRL_FaultCode.payload_mask = [];
wr224e.IRL_FaultCode.interval = 50;

wr224e.IRL_FaultCode.fields{1}.name = 'IRL_SDCControl';
wr224e.IRL_FaultCode.fields{1}.units = '';
wr224e.IRL_FaultCode.fields{1}.start_bit = 24;
wr224e.IRL_FaultCode.fields{1}.bit_length = 8;
wr224e.IRL_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_FaultCode.fields{1}.data_type = 'UNSIGNED';
wr224e.IRL_FaultCode.fields{1}.scale = 1.000000e+00;
wr224e.IRL_FaultCode.fields{1}.offset = 0.000000e+00;

wr224e.IRL_FaultCode.fields{2}.name = 'IRL_DiagnosticInfo';
wr224e.IRL_FaultCode.fields{2}.units = '';
wr224e.IRL_FaultCode.fields{2}.start_bit = 40;
wr224e.IRL_FaultCode.fields{2}.bit_length = 16;
wr224e.IRL_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_FaultCode.fields{2}.data_type = 'UNSIGNED';
wr224e.IRL_FaultCode.fields{2}.maximum = 6.553600e+04;
wr224e.IRL_FaultCode.fields{2}.minimum = 0.000000e+00;
wr224e.IRL_FaultCode.fields{2}.scale = 1.000000e+00;
wr224e.IRL_FaultCode.fields{2}.offset = 0.000000e+00;

wr224e.IRL_FaultCode.fields{3}.name = 'IRL_FaultCode';
wr224e.IRL_FaultCode.fields{3}.units = '';
wr224e.IRL_FaultCode.fields{3}.start_bit = 56;
wr224e.IRL_FaultCode.fields{3}.bit_length = 16;
wr224e.IRL_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_FaultCode.fields{3}.data_type = 'UNSIGNED';
wr224e.IRL_FaultCode.fields{3}.maximum = 6.553600e+04;
wr224e.IRL_FaultCode.fields{3}.minimum = 0.000000e+00;
wr224e.IRL_FaultCode.fields{3}.scale = 1.000000e+00;
wr224e.IRL_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Flags'
wr224e.IRL_Flags = struct;
wr224e.IRL_Flags.name = 'IRL_Flags';
wr224e.IRL_Flags.description = 'IRL_Flags';
wr224e.IRL_Flags.protocol = 'wr224e';
wr224e.IRL_Flags.id = hex2dec('000001D6');
wr224e.IRL_Flags.idext = 'STANDARD';
wr224e.IRL_Flags.idinherit = 0;
wr224e.IRL_Flags.payload_size = 1;
wr224e.IRL_Flags.payload_value = [];
wr224e.IRL_Flags.payload_mask = [];
wr224e.IRL_Flags.interval = 50;

wr224e.IRL_Flags.fields{1}.name = 'IRL_flag_run';
wr224e.IRL_Flags.fields{1}.units = 'Flag';
wr224e.IRL_Flags.fields{1}.start_bit = 56;
wr224e.IRL_Flags.fields{1}.bit_length = 1;
wr224e.IRL_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{1}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{1}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{1}.offset = 0.000000e+00;

wr224e.IRL_Flags.fields{2}.name = 'IRL_hw_run';
wr224e.IRL_Flags.fields{2}.units = '';
wr224e.IRL_Flags.fields{2}.start_bit = 57;
wr224e.IRL_Flags.fields{2}.bit_length = 1;
wr224e.IRL_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{2}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{2}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{2}.offset = 0.000000e+00;

wr224e.IRL_Flags.fields{3}.name = 'IRL_ecu_run';
wr224e.IRL_Flags.fields{3}.units = 'Flag';
wr224e.IRL_Flags.fields{3}.start_bit = 58;
wr224e.IRL_Flags.fields{3}.bit_length = 1;
wr224e.IRL_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{3}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{3}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{3}.offset = 0.000000e+00;

wr224e.IRL_Flags.fields{4}.name = 'IRL_gate_deadzon';
wr224e.IRL_Flags.fields{4}.units = '';
wr224e.IRL_Flags.fields{4}.start_bit = 59;
wr224e.IRL_Flags.fields{4}.bit_length = 1;
wr224e.IRL_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{4}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{4}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{4}.offset = 0.000000e+00;

wr224e.IRL_Flags.fields{5}.name = 'IRL_ecu_shutdown';
wr224e.IRL_Flags.fields{5}.units = '';
wr224e.IRL_Flags.fields{5}.start_bit = 60;
wr224e.IRL_Flags.fields{5}.bit_length = 1;
wr224e.IRL_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{5}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{5}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{5}.offset = 0.000000e+00;

wr224e.IRL_Flags.fields{6}.name = 'IRL_Offset_cal';
wr224e.IRL_Flags.fields{6}.units = 'Flag';
wr224e.IRL_Flags.fields{6}.start_bit = 61;
wr224e.IRL_Flags.fields{6}.bit_length = 1;
wr224e.IRL_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Flags.fields{6}.data_type = 'UNSIGNED';
wr224e.IRL_Flags.fields{6}.scale = 1.000000e+00;
wr224e.IRL_Flags.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_MotorParams'
wr224e.IRL_MotorParams = struct;
wr224e.IRL_MotorParams.name = 'IRL_MotorParams';
wr224e.IRL_MotorParams.description = 'IRL_MotorParams';
wr224e.IRL_MotorParams.protocol = 'wr224e';
wr224e.IRL_MotorParams.id = hex2dec('000000C9');
wr224e.IRL_MotorParams.idext = 'STANDARD';
wr224e.IRL_MotorParams.idinherit = 0;
wr224e.IRL_MotorParams.payload_size = 8;
wr224e.IRL_MotorParams.payload_value = [];
wr224e.IRL_MotorParams.payload_mask = [];
wr224e.IRL_MotorParams.interval = 50;

wr224e.IRL_MotorParams.fields{1}.name = 'IRL_Vdc';
wr224e.IRL_MotorParams.fields{1}.units = 'V';
wr224e.IRL_MotorParams.fields{1}.start_bit = 8;
wr224e.IRL_MotorParams.fields{1}.bit_length = 16;
wr224e.IRL_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{1}.data_type = 'SIGNED';
wr224e.IRL_MotorParams.fields{1}.maximum = 3.270000e+02;
wr224e.IRL_MotorParams.fields{1}.minimum = -3.270000e+02;
wr224e.IRL_MotorParams.fields{1}.scale = 1.000000e-02;
wr224e.IRL_MotorParams.fields{1}.offset = 0.000000e+00;

wr224e.IRL_MotorParams.fields{2}.name = 'IRL_Torque_est';
wr224e.IRL_MotorParams.fields{2}.units = 'Nm';
wr224e.IRL_MotorParams.fields{2}.start_bit = 24;
wr224e.IRL_MotorParams.fields{2}.bit_length = 16;
wr224e.IRL_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{2}.data_type = 'SIGNED';
wr224e.IRL_MotorParams.fields{2}.scale = 2.000000e-03;
wr224e.IRL_MotorParams.fields{2}.offset = 0.000000e+00;

wr224e.IRL_MotorParams.fields{3}.name = 'IRL_R_winding';
wr224e.IRL_MotorParams.fields{3}.units = 'mOhm';
wr224e.IRL_MotorParams.fields{3}.start_bit = 32;
wr224e.IRL_MotorParams.fields{3}.bit_length = 8;
wr224e.IRL_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{3}.data_type = 'UNSIGNED';
wr224e.IRL_MotorParams.fields{3}.maximum = 5.120000e+01;
wr224e.IRL_MotorParams.fields{3}.minimum = 0.000000e+00;
wr224e.IRL_MotorParams.fields{3}.scale = 2.000000e-01;
wr224e.IRL_MotorParams.fields{3}.offset = 0.000000e+00;

wr224e.IRL_MotorParams.fields{4}.name = 'IRL_K_t';
wr224e.IRL_MotorParams.fields{4}.units = 'Nm/A';
wr224e.IRL_MotorParams.fields{4}.start_bit = 40;
wr224e.IRL_MotorParams.fields{4}.bit_length = 8;
wr224e.IRL_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{4}.data_type = 'UNSIGNED';
wr224e.IRL_MotorParams.fields{4}.maximum = 5.120000e-01;
wr224e.IRL_MotorParams.fields{4}.minimum = 0.000000e+00;
wr224e.IRL_MotorParams.fields{4}.scale = 2.000000e-03;
wr224e.IRL_MotorParams.fields{4}.offset = 0.000000e+00;

wr224e.IRL_MotorParams.fields{5}.name = 'IRL_L_q';
wr224e.IRL_MotorParams.fields{5}.units = 'uH';
wr224e.IRL_MotorParams.fields{5}.start_bit = 48;
wr224e.IRL_MotorParams.fields{5}.bit_length = 8;
wr224e.IRL_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{5}.data_type = 'UNSIGNED';
wr224e.IRL_MotorParams.fields{5}.maximum = 1.280000e+02;
wr224e.IRL_MotorParams.fields{5}.minimum = 0.000000e+00;
wr224e.IRL_MotorParams.fields{5}.scale = 5.000000e-01;
wr224e.IRL_MotorParams.fields{5}.offset = 0.000000e+00;

wr224e.IRL_MotorParams.fields{6}.name = 'IRL_L_d';
wr224e.IRL_MotorParams.fields{6}.units = 'uH';
wr224e.IRL_MotorParams.fields{6}.start_bit = 56;
wr224e.IRL_MotorParams.fields{6}.bit_length = 8;
wr224e.IRL_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_MotorParams.fields{6}.data_type = 'UNSIGNED';
wr224e.IRL_MotorParams.fields{6}.maximum = 1.280000e+02;
wr224e.IRL_MotorParams.fields{6}.minimum = 0.000000e+00;
wr224e.IRL_MotorParams.fields{6}.scale = 5.000000e-01;
wr224e.IRL_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Observer'
wr224e.IRL_Observer = struct;
wr224e.IRL_Observer.name = 'IRL_Observer';
wr224e.IRL_Observer.description = 'IRL_Observer';
wr224e.IRL_Observer.protocol = 'wr224e';
wr224e.IRL_Observer.id = hex2dec('000001D2');
wr224e.IRL_Observer.idext = 'STANDARD';
wr224e.IRL_Observer.idinherit = 0;
wr224e.IRL_Observer.payload_size = 8;
wr224e.IRL_Observer.payload_value = [];
wr224e.IRL_Observer.payload_mask = [];
wr224e.IRL_Observer.interval = 50;

wr224e.IRL_Observer.fields{1}.name = 'IRL_Iq_ref';
wr224e.IRL_Observer.fields{1}.units = 'A';
wr224e.IRL_Observer.fields{1}.start_bit = 40;
wr224e.IRL_Observer.fields{1}.bit_length = 16;
wr224e.IRL_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Observer.fields{1}.data_type = 'SIGNED';
wr224e.IRL_Observer.fields{1}.scale = 2.000000e-02;
wr224e.IRL_Observer.fields{1}.offset = 0.000000e+00;

wr224e.IRL_Observer.fields{2}.name = 'IRL_Id_ref';
wr224e.IRL_Observer.fields{2}.units = 'A';
wr224e.IRL_Observer.fields{2}.start_bit = 56;
wr224e.IRL_Observer.fields{2}.bit_length = 16;
wr224e.IRL_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Observer.fields{2}.data_type = 'SIGNED';
wr224e.IRL_Observer.fields{2}.scale = 2.000000e-02;
wr224e.IRL_Observer.fields{2}.offset = 0.000000e+00;

wr224e.IRL_Observer.fields{3}.name = 'IRL_SAS_Speed';
wr224e.IRL_Observer.fields{3}.units = 'deg/s';
wr224e.IRL_Observer.fields{3}.start_bit = 8;
wr224e.IRL_Observer.fields{3}.bit_length = 16;
wr224e.IRL_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Observer.fields{3}.data_type = 'SIGNED';
wr224e.IRL_Observer.fields{3}.scale = 1.000000e-02;
wr224e.IRL_Observer.fields{3}.offset = 0.000000e+00;

wr224e.IRL_Observer.fields{4}.name = 'IRL_SAS_Angle';
wr224e.IRL_Observer.fields{4}.units = 'deg';
wr224e.IRL_Observer.fields{4}.start_bit = 24;
wr224e.IRL_Observer.fields{4}.bit_length = 16;
wr224e.IRL_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Observer.fields{4}.data_type = 'SIGNED';
wr224e.IRL_Observer.fields{4}.scale = 1.000000e-02;
wr224e.IRL_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_Offsets'
wr224e.IRL_Offsets = struct;
wr224e.IRL_Offsets.name = 'IRL_Offsets';
wr224e.IRL_Offsets.description = 'IRL_Offsets';
wr224e.IRL_Offsets.protocol = 'wr224e';
wr224e.IRL_Offsets.id = hex2dec('000000CB');
wr224e.IRL_Offsets.idext = 'STANDARD';
wr224e.IRL_Offsets.idinherit = 0;
wr224e.IRL_Offsets.payload_size = 6;
wr224e.IRL_Offsets.payload_value = [];
wr224e.IRL_Offsets.payload_mask = [];
wr224e.IRL_Offsets.interval = 50;

wr224e.IRL_Offsets.fields{1}.name = 'IRL_V_c_offset';
wr224e.IRL_Offsets.fields{1}.units = 'ADC';
wr224e.IRL_Offsets.fields{1}.start_bit = 16;
wr224e.IRL_Offsets.fields{1}.bit_length = 8;
wr224e.IRL_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{1}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{1}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{1}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{1}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{1}.offset = 0.000000e+00;

wr224e.IRL_Offsets.fields{2}.name = 'IRL_V_b_offset';
wr224e.IRL_Offsets.fields{2}.units = 'ADC';
wr224e.IRL_Offsets.fields{2}.start_bit = 24;
wr224e.IRL_Offsets.fields{2}.bit_length = 8;
wr224e.IRL_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{2}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{2}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{2}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{2}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{2}.offset = 0.000000e+00;

wr224e.IRL_Offsets.fields{3}.name = 'IRL_V_a_offset';
wr224e.IRL_Offsets.fields{3}.units = 'ADC';
wr224e.IRL_Offsets.fields{3}.start_bit = 32;
wr224e.IRL_Offsets.fields{3}.bit_length = 8;
wr224e.IRL_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{3}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{3}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{3}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{3}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{3}.offset = 0.000000e+00;

wr224e.IRL_Offsets.fields{4}.name = 'IRL_I_c_offset';
wr224e.IRL_Offsets.fields{4}.units = 'ADC';
wr224e.IRL_Offsets.fields{4}.start_bit = 40;
wr224e.IRL_Offsets.fields{4}.bit_length = 8;
wr224e.IRL_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{4}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{4}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{4}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{4}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{4}.offset = 0.000000e+00;

wr224e.IRL_Offsets.fields{5}.name = 'IRL_I_b_offset';
wr224e.IRL_Offsets.fields{5}.units = 'ADC';
wr224e.IRL_Offsets.fields{5}.start_bit = 48;
wr224e.IRL_Offsets.fields{5}.bit_length = 8;
wr224e.IRL_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{5}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{5}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{5}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{5}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{5}.offset = 0.000000e+00;

wr224e.IRL_Offsets.fields{6}.name = 'IRL_I_a_offset';
wr224e.IRL_Offsets.fields{6}.units = 'ADC';
wr224e.IRL_Offsets.fields{6}.start_bit = 56;
wr224e.IRL_Offsets.fields{6}.bit_length = 8;
wr224e.IRL_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_Offsets.fields{6}.data_type = 'SIGNED';
wr224e.IRL_Offsets.fields{6}.maximum = 1.280000e+02;
wr224e.IRL_Offsets.fields{6}.minimum = -1.270000e+02;
wr224e.IRL_Offsets.fields{6}.scale = 1.000000e+00;
wr224e.IRL_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRL_SensorData'
wr224e.IRL_SensorData = struct;
wr224e.IRL_SensorData.name = 'IRL_SensorData';
wr224e.IRL_SensorData.description = 'IRL_SensorData';
wr224e.IRL_SensorData.protocol = 'wr224e';
wr224e.IRL_SensorData.id = hex2dec('000000DC');
wr224e.IRL_SensorData.idext = 'STANDARD';
wr224e.IRL_SensorData.idinherit = 0;
wr224e.IRL_SensorData.payload_size = 8;
wr224e.IRL_SensorData.payload_value = [];
wr224e.IRL_SensorData.payload_mask = [];
wr224e.IRL_SensorData.interval = 50;

wr224e.IRL_SensorData.fields{1}.name = 'IRL_Ib';
wr224e.IRL_SensorData.fields{1}.units = 'A';
wr224e.IRL_SensorData.fields{1}.start_bit = 8;
wr224e.IRL_SensorData.fields{1}.bit_length = 16;
wr224e.IRL_SensorData.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_SensorData.fields{1}.data_type = 'SIGNED';
wr224e.IRL_SensorData.fields{1}.scale = 1.000000e-02;
wr224e.IRL_SensorData.fields{1}.offset = 0.000000e+00;

wr224e.IRL_SensorData.fields{2}.name = 'IRL_Ia';
wr224e.IRL_SensorData.fields{2}.units = 'A';
wr224e.IRL_SensorData.fields{2}.start_bit = 24;
wr224e.IRL_SensorData.fields{2}.bit_length = 16;
wr224e.IRL_SensorData.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_SensorData.fields{2}.data_type = 'SIGNED';
wr224e.IRL_SensorData.fields{2}.scale = 1.000000e-02;
wr224e.IRL_SensorData.fields{2}.offset = 0.000000e+00;

wr224e.IRL_SensorData.fields{3}.name = 'IRL_MechSpeed';
wr224e.IRL_SensorData.fields{3}.units = 'RPM';
wr224e.IRL_SensorData.fields{3}.start_bit = 40;
wr224e.IRL_SensorData.fields{3}.bit_length = 16;
wr224e.IRL_SensorData.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_SensorData.fields{3}.data_type = 'SIGNED';
wr224e.IRL_SensorData.fields{3}.scale = 1.000000e+00;
wr224e.IRL_SensorData.fields{3}.offset = 0.000000e+00;

wr224e.IRL_SensorData.fields{4}.name = 'IRL_MechAccel';
wr224e.IRL_SensorData.fields{4}.units = 'm/s^2';
wr224e.IRL_SensorData.fields{4}.start_bit = 56;
wr224e.IRL_SensorData.fields{4}.bit_length = 16;
wr224e.IRL_SensorData.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_SensorData.fields{4}.data_type = 'SIGNED';
wr224e.IRL_SensorData.fields{4}.scale = 2.000000e-03;
wr224e.IRL_SensorData.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRL_dq'
wr224e.IRL_dq = struct;
wr224e.IRL_dq.name = 'IRL_dq';
wr224e.IRL_dq.description = 'IRL_dq';
wr224e.IRL_dq.protocol = 'wr224e';
wr224e.IRL_dq.id = hex2dec('000000CA');
wr224e.IRL_dq.idext = 'STANDARD';
wr224e.IRL_dq.idinherit = 0;
wr224e.IRL_dq.payload_size = 8;
wr224e.IRL_dq.payload_value = [];
wr224e.IRL_dq.payload_mask = [];
wr224e.IRL_dq.interval = 50;

wr224e.IRL_dq.fields{1}.name = 'IRL_I_q';
wr224e.IRL_dq.fields{1}.units = 'A';
wr224e.IRL_dq.fields{1}.start_bit = 8;
wr224e.IRL_dq.fields{1}.bit_length = 16;
wr224e.IRL_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_dq.fields{1}.data_type = 'SIGNED';
wr224e.IRL_dq.fields{1}.maximum = 6.540000e+02;
wr224e.IRL_dq.fields{1}.minimum = -6.540000e+02;
wr224e.IRL_dq.fields{1}.scale = 2.000000e-02;
wr224e.IRL_dq.fields{1}.offset = 0.000000e+00;

wr224e.IRL_dq.fields{2}.name = 'IRL_I_d';
wr224e.IRL_dq.fields{2}.units = 'A';
wr224e.IRL_dq.fields{2}.start_bit = 24;
wr224e.IRL_dq.fields{2}.bit_length = 16;
wr224e.IRL_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_dq.fields{2}.data_type = 'SIGNED';
wr224e.IRL_dq.fields{2}.maximum = 6.540000e+02;
wr224e.IRL_dq.fields{2}.minimum = -6.540000e+02;
wr224e.IRL_dq.fields{2}.scale = 2.000000e-02;
wr224e.IRL_dq.fields{2}.offset = 0.000000e+00;

wr224e.IRL_dq.fields{3}.name = 'IRL_V_q';
wr224e.IRL_dq.fields{3}.units = 'V';
wr224e.IRL_dq.fields{3}.start_bit = 40;
wr224e.IRL_dq.fields{3}.bit_length = 16;
wr224e.IRL_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_dq.fields{3}.data_type = 'SIGNED';
wr224e.IRL_dq.fields{3}.maximum = 6.540000e+02;
wr224e.IRL_dq.fields{3}.minimum = -6.540000e+02;
wr224e.IRL_dq.fields{3}.scale = 2.000000e-02;
wr224e.IRL_dq.fields{3}.offset = 0.000000e+00;

wr224e.IRL_dq.fields{4}.name = 'IRL_V_d';
wr224e.IRL_dq.fields{4}.units = 'V';
wr224e.IRL_dq.fields{4}.start_bit = 56;
wr224e.IRL_dq.fields{4}.bit_length = 16;
wr224e.IRL_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRL_dq.fields{4}.data_type = 'SIGNED';
wr224e.IRL_dq.fields{4}.maximum = 6.540000e+02;
wr224e.IRL_dq.fields{4}.minimum = -6.540000e+02;
wr224e.IRL_dq.fields{4}.scale = 2.000000e-02;
wr224e.IRL_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRLrx_Bootloader'
wr224e.IRLrx_Bootloader = struct;
wr224e.IRLrx_Bootloader.name = 'IRLrx_Bootloader';
wr224e.IRLrx_Bootloader.description = 'IRLrx_Bootloader';
wr224e.IRLrx_Bootloader.protocol = 'wr224e';
wr224e.IRLrx_Bootloader.id = hex2dec('000000B3');
wr224e.IRLrx_Bootloader.idext = 'STANDARD';
wr224e.IRLrx_Bootloader.idinherit = 0;
wr224e.IRLrx_Bootloader.payload_size = 8;
wr224e.IRLrx_Bootloader.payload_value = [];
wr224e.IRLrx_Bootloader.payload_mask = [];
wr224e.IRLrx_Bootloader.interval = 50;

%===================================================================
case 'IRR_Debug'
wr224e.IRR_Debug = struct;
wr224e.IRR_Debug.name = 'IRR_Debug';
wr224e.IRR_Debug.description = 'IRR_Debug';
wr224e.IRR_Debug.protocol = 'wr224e';
wr224e.IRR_Debug.id = hex2dec('000001DB');
wr224e.IRR_Debug.idext = 'STANDARD';
wr224e.IRR_Debug.idinherit = 0;
wr224e.IRR_Debug.payload_size = 8;
wr224e.IRR_Debug.payload_value = [];
wr224e.IRR_Debug.payload_mask = [];
wr224e.IRR_Debug.interval = 50;

wr224e.IRR_Debug.fields{1}.name = 'Debug_Value4';
wr224e.IRR_Debug.fields{1}.units = '';
wr224e.IRR_Debug.fields{1}.start_bit = 8;
wr224e.IRR_Debug.fields{1}.bit_length = 16;
wr224e.IRR_Debug.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Debug.fields{1}.data_type = 'SIGNED';
wr224e.IRR_Debug.fields{1}.scale = 1.000000e-02;
wr224e.IRR_Debug.fields{1}.offset = 0.000000e+00;

wr224e.IRR_Debug.fields{2}.name = 'Debug_Value3';
wr224e.IRR_Debug.fields{2}.units = '';
wr224e.IRR_Debug.fields{2}.start_bit = 24;
wr224e.IRR_Debug.fields{2}.bit_length = 16;
wr224e.IRR_Debug.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Debug.fields{2}.data_type = 'SIGNED';
wr224e.IRR_Debug.fields{2}.scale = 1.000000e-02;
wr224e.IRR_Debug.fields{2}.offset = 0.000000e+00;

wr224e.IRR_Debug.fields{3}.name = 'Debug_Value2';
wr224e.IRR_Debug.fields{3}.units = '';
wr224e.IRR_Debug.fields{3}.start_bit = 40;
wr224e.IRR_Debug.fields{3}.bit_length = 16;
wr224e.IRR_Debug.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Debug.fields{3}.data_type = 'SIGNED';
wr224e.IRR_Debug.fields{3}.scale = 1.000000e-02;
wr224e.IRR_Debug.fields{3}.offset = 0.000000e+00;

wr224e.IRR_Debug.fields{4}.name = 'Debug_Value1';
wr224e.IRR_Debug.fields{4}.units = '';
wr224e.IRR_Debug.fields{4}.start_bit = 56;
wr224e.IRR_Debug.fields{4}.bit_length = 16;
wr224e.IRR_Debug.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Debug.fields{4}.data_type = 'SIGNED';
wr224e.IRR_Debug.fields{4}.scale = 1.000000e-02;
wr224e.IRR_Debug.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_FaultCode'
wr224e.IRR_FaultCode = struct;
wr224e.IRR_FaultCode.name = 'IRR_FaultCode';
wr224e.IRR_FaultCode.description = 'IRR_FaultCode';
wr224e.IRR_FaultCode.protocol = 'wr224e';
wr224e.IRR_FaultCode.id = hex2dec('000000D3');
wr224e.IRR_FaultCode.idext = 'STANDARD';
wr224e.IRR_FaultCode.idinherit = 0;
wr224e.IRR_FaultCode.payload_size = 5;
wr224e.IRR_FaultCode.payload_value = [];
wr224e.IRR_FaultCode.payload_mask = [];
wr224e.IRR_FaultCode.interval = 50;

wr224e.IRR_FaultCode.fields{1}.name = 'IRR_SDCControl';
wr224e.IRR_FaultCode.fields{1}.units = '';
wr224e.IRR_FaultCode.fields{1}.start_bit = 24;
wr224e.IRR_FaultCode.fields{1}.bit_length = 8;
wr224e.IRR_FaultCode.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_FaultCode.fields{1}.data_type = 'UNSIGNED';
wr224e.IRR_FaultCode.fields{1}.scale = 1.000000e+00;
wr224e.IRR_FaultCode.fields{1}.offset = 0.000000e+00;

wr224e.IRR_FaultCode.fields{2}.name = 'IRR_DiagnosticInfo';
wr224e.IRR_FaultCode.fields{2}.units = '';
wr224e.IRR_FaultCode.fields{2}.start_bit = 40;
wr224e.IRR_FaultCode.fields{2}.bit_length = 16;
wr224e.IRR_FaultCode.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_FaultCode.fields{2}.data_type = 'UNSIGNED';
wr224e.IRR_FaultCode.fields{2}.maximum = 6.553600e+04;
wr224e.IRR_FaultCode.fields{2}.minimum = 0.000000e+00;
wr224e.IRR_FaultCode.fields{2}.scale = 1.000000e+00;
wr224e.IRR_FaultCode.fields{2}.offset = 0.000000e+00;

wr224e.IRR_FaultCode.fields{3}.name = 'IRR_FaultCode';
wr224e.IRR_FaultCode.fields{3}.units = '';
wr224e.IRR_FaultCode.fields{3}.start_bit = 56;
wr224e.IRR_FaultCode.fields{3}.bit_length = 16;
wr224e.IRR_FaultCode.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_FaultCode.fields{3}.data_type = 'UNSIGNED';
wr224e.IRR_FaultCode.fields{3}.maximum = 6.553600e+04;
wr224e.IRR_FaultCode.fields{3}.minimum = 0.000000e+00;
wr224e.IRR_FaultCode.fields{3}.scale = 1.000000e+00;
wr224e.IRR_FaultCode.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Flags'
wr224e.IRR_Flags = struct;
wr224e.IRR_Flags.name = 'IRR_Flags';
wr224e.IRR_Flags.description = 'IRR_Flags';
wr224e.IRR_Flags.protocol = 'wr224e';
wr224e.IRR_Flags.id = hex2dec('000001D7');
wr224e.IRR_Flags.idext = 'STANDARD';
wr224e.IRR_Flags.idinherit = 0;
wr224e.IRR_Flags.payload_size = 1;
wr224e.IRR_Flags.payload_value = [];
wr224e.IRR_Flags.payload_mask = [];
wr224e.IRR_Flags.interval = 50;

wr224e.IRR_Flags.fields{1}.name = 'IRR_flag_run';
wr224e.IRR_Flags.fields{1}.units = 'Flag';
wr224e.IRR_Flags.fields{1}.start_bit = 56;
wr224e.IRR_Flags.fields{1}.bit_length = 1;
wr224e.IRR_Flags.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{1}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{1}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{1}.offset = 0.000000e+00;

wr224e.IRR_Flags.fields{2}.name = 'IRR_hw_run';
wr224e.IRR_Flags.fields{2}.units = '';
wr224e.IRR_Flags.fields{2}.start_bit = 57;
wr224e.IRR_Flags.fields{2}.bit_length = 1;
wr224e.IRR_Flags.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{2}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{2}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{2}.offset = 0.000000e+00;

wr224e.IRR_Flags.fields{3}.name = 'IRR_ecu_run';
wr224e.IRR_Flags.fields{3}.units = 'Flag';
wr224e.IRR_Flags.fields{3}.start_bit = 58;
wr224e.IRR_Flags.fields{3}.bit_length = 1;
wr224e.IRR_Flags.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{3}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{3}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{3}.offset = 0.000000e+00;

wr224e.IRR_Flags.fields{4}.name = 'IRR_gate_deadzon';
wr224e.IRR_Flags.fields{4}.units = '';
wr224e.IRR_Flags.fields{4}.start_bit = 59;
wr224e.IRR_Flags.fields{4}.bit_length = 1;
wr224e.IRR_Flags.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{4}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{4}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{4}.offset = 0.000000e+00;

wr224e.IRR_Flags.fields{5}.name = 'IRR_ecu_shutdown';
wr224e.IRR_Flags.fields{5}.units = '';
wr224e.IRR_Flags.fields{5}.start_bit = 60;
wr224e.IRR_Flags.fields{5}.bit_length = 1;
wr224e.IRR_Flags.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{5}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{5}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{5}.offset = 0.000000e+00;

wr224e.IRR_Flags.fields{6}.name = 'IRR_Offset_cal';
wr224e.IRR_Flags.fields{6}.units = 'Flag';
wr224e.IRR_Flags.fields{6}.start_bit = 61;
wr224e.IRR_Flags.fields{6}.bit_length = 1;
wr224e.IRR_Flags.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Flags.fields{6}.data_type = 'UNSIGNED';
wr224e.IRR_Flags.fields{6}.scale = 1.000000e+00;
wr224e.IRR_Flags.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_MotorParams'
wr224e.IRR_MotorParams = struct;
wr224e.IRR_MotorParams.name = 'IRR_MotorParams';
wr224e.IRR_MotorParams.description = 'IRR_MotorParams';
wr224e.IRR_MotorParams.protocol = 'wr224e';
wr224e.IRR_MotorParams.id = hex2dec('000000CD');
wr224e.IRR_MotorParams.idext = 'STANDARD';
wr224e.IRR_MotorParams.idinherit = 0;
wr224e.IRR_MotorParams.payload_size = 8;
wr224e.IRR_MotorParams.payload_value = [];
wr224e.IRR_MotorParams.payload_mask = [];
wr224e.IRR_MotorParams.interval = 50;

wr224e.IRR_MotorParams.fields{1}.name = 'IRR_Vdc';
wr224e.IRR_MotorParams.fields{1}.units = 'V';
wr224e.IRR_MotorParams.fields{1}.start_bit = 8;
wr224e.IRR_MotorParams.fields{1}.bit_length = 16;
wr224e.IRR_MotorParams.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{1}.data_type = 'SIGNED';
wr224e.IRR_MotorParams.fields{1}.maximum = 3.270000e+02;
wr224e.IRR_MotorParams.fields{1}.minimum = -3.270000e+02;
wr224e.IRR_MotorParams.fields{1}.scale = 1.000000e-02;
wr224e.IRR_MotorParams.fields{1}.offset = 0.000000e+00;

wr224e.IRR_MotorParams.fields{2}.name = 'IRR_Torque_est';
wr224e.IRR_MotorParams.fields{2}.units = 'Nm';
wr224e.IRR_MotorParams.fields{2}.start_bit = 24;
wr224e.IRR_MotorParams.fields{2}.bit_length = 16;
wr224e.IRR_MotorParams.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{2}.data_type = 'SIGNED';
wr224e.IRR_MotorParams.fields{2}.scale = 2.000000e-03;
wr224e.IRR_MotorParams.fields{2}.offset = 0.000000e+00;

wr224e.IRR_MotorParams.fields{3}.name = 'IRR_R_winding';
wr224e.IRR_MotorParams.fields{3}.units = 'mOhm';
wr224e.IRR_MotorParams.fields{3}.start_bit = 32;
wr224e.IRR_MotorParams.fields{3}.bit_length = 8;
wr224e.IRR_MotorParams.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{3}.data_type = 'UNSIGNED';
wr224e.IRR_MotorParams.fields{3}.maximum = 5.120000e+01;
wr224e.IRR_MotorParams.fields{3}.minimum = 0.000000e+00;
wr224e.IRR_MotorParams.fields{3}.scale = 2.000000e-01;
wr224e.IRR_MotorParams.fields{3}.offset = 0.000000e+00;

wr224e.IRR_MotorParams.fields{4}.name = 'IRR_K_t';
wr224e.IRR_MotorParams.fields{4}.units = 'Nm/A';
wr224e.IRR_MotorParams.fields{4}.start_bit = 40;
wr224e.IRR_MotorParams.fields{4}.bit_length = 8;
wr224e.IRR_MotorParams.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{4}.data_type = 'UNSIGNED';
wr224e.IRR_MotorParams.fields{4}.maximum = 5.120000e-01;
wr224e.IRR_MotorParams.fields{4}.minimum = 0.000000e+00;
wr224e.IRR_MotorParams.fields{4}.scale = 2.000000e-03;
wr224e.IRR_MotorParams.fields{4}.offset = 0.000000e+00;

wr224e.IRR_MotorParams.fields{5}.name = 'IRR_L_q';
wr224e.IRR_MotorParams.fields{5}.units = 'uH';
wr224e.IRR_MotorParams.fields{5}.start_bit = 48;
wr224e.IRR_MotorParams.fields{5}.bit_length = 8;
wr224e.IRR_MotorParams.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{5}.data_type = 'UNSIGNED';
wr224e.IRR_MotorParams.fields{5}.maximum = 1.280000e+02;
wr224e.IRR_MotorParams.fields{5}.minimum = 0.000000e+00;
wr224e.IRR_MotorParams.fields{5}.scale = 5.000000e-01;
wr224e.IRR_MotorParams.fields{5}.offset = 0.000000e+00;

wr224e.IRR_MotorParams.fields{6}.name = 'IRR_L_d';
wr224e.IRR_MotorParams.fields{6}.units = 'uH';
wr224e.IRR_MotorParams.fields{6}.start_bit = 56;
wr224e.IRR_MotorParams.fields{6}.bit_length = 8;
wr224e.IRR_MotorParams.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_MotorParams.fields{6}.data_type = 'UNSIGNED';
wr224e.IRR_MotorParams.fields{6}.maximum = 1.280000e+02;
wr224e.IRR_MotorParams.fields{6}.minimum = 0.000000e+00;
wr224e.IRR_MotorParams.fields{6}.scale = 5.000000e-01;
wr224e.IRR_MotorParams.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Observer'
wr224e.IRR_Observer = struct;
wr224e.IRR_Observer.name = 'IRR_Observer';
wr224e.IRR_Observer.description = 'IRR_Observer';
wr224e.IRR_Observer.protocol = 'wr224e';
wr224e.IRR_Observer.id = hex2dec('000001D3');
wr224e.IRR_Observer.idext = 'STANDARD';
wr224e.IRR_Observer.idinherit = 0;
wr224e.IRR_Observer.payload_size = 8;
wr224e.IRR_Observer.payload_value = [];
wr224e.IRR_Observer.payload_mask = [];
wr224e.IRR_Observer.interval = 50;

wr224e.IRR_Observer.fields{1}.name = 'IRR_Iq_ref';
wr224e.IRR_Observer.fields{1}.units = 'A';
wr224e.IRR_Observer.fields{1}.start_bit = 40;
wr224e.IRR_Observer.fields{1}.bit_length = 16;
wr224e.IRR_Observer.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Observer.fields{1}.data_type = 'SIGNED';
wr224e.IRR_Observer.fields{1}.scale = 2.000000e-02;
wr224e.IRR_Observer.fields{1}.offset = 0.000000e+00;

wr224e.IRR_Observer.fields{2}.name = 'IRR_Id_ref';
wr224e.IRR_Observer.fields{2}.units = 'A';
wr224e.IRR_Observer.fields{2}.start_bit = 56;
wr224e.IRR_Observer.fields{2}.bit_length = 16;
wr224e.IRR_Observer.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Observer.fields{2}.data_type = 'SIGNED';
wr224e.IRR_Observer.fields{2}.scale = 2.000000e-02;
wr224e.IRR_Observer.fields{2}.offset = 0.000000e+00;

wr224e.IRR_Observer.fields{3}.name = 'IRR_SAS_Speed';
wr224e.IRR_Observer.fields{3}.units = 'deg/s';
wr224e.IRR_Observer.fields{3}.start_bit = 8;
wr224e.IRR_Observer.fields{3}.bit_length = 16;
wr224e.IRR_Observer.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Observer.fields{3}.data_type = 'SIGNED';
wr224e.IRR_Observer.fields{3}.scale = 1.000000e-02;
wr224e.IRR_Observer.fields{3}.offset = 0.000000e+00;

wr224e.IRR_Observer.fields{4}.name = 'IRR_SAS_Angle';
wr224e.IRR_Observer.fields{4}.units = 'deg';
wr224e.IRR_Observer.fields{4}.start_bit = 24;
wr224e.IRR_Observer.fields{4}.bit_length = 16;
wr224e.IRR_Observer.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Observer.fields{4}.data_type = 'SIGNED';
wr224e.IRR_Observer.fields{4}.scale = 1.000000e-02;
wr224e.IRR_Observer.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_Offsets'
wr224e.IRR_Offsets = struct;
wr224e.IRR_Offsets.name = 'IRR_Offsets';
wr224e.IRR_Offsets.description = 'IRR_Offsets';
wr224e.IRR_Offsets.protocol = 'wr224e';
wr224e.IRR_Offsets.id = hex2dec('000000CF');
wr224e.IRR_Offsets.idext = 'STANDARD';
wr224e.IRR_Offsets.idinherit = 0;
wr224e.IRR_Offsets.payload_size = 6;
wr224e.IRR_Offsets.payload_value = [];
wr224e.IRR_Offsets.payload_mask = [];
wr224e.IRR_Offsets.interval = 50;

wr224e.IRR_Offsets.fields{1}.name = 'IRR_V_c_offset';
wr224e.IRR_Offsets.fields{1}.units = 'ADC';
wr224e.IRR_Offsets.fields{1}.start_bit = 16;
wr224e.IRR_Offsets.fields{1}.bit_length = 8;
wr224e.IRR_Offsets.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{1}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{1}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{1}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{1}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{1}.offset = 0.000000e+00;

wr224e.IRR_Offsets.fields{2}.name = 'IRR_V_b_offset';
wr224e.IRR_Offsets.fields{2}.units = 'ADC';
wr224e.IRR_Offsets.fields{2}.start_bit = 24;
wr224e.IRR_Offsets.fields{2}.bit_length = 8;
wr224e.IRR_Offsets.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{2}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{2}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{2}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{2}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{2}.offset = 0.000000e+00;

wr224e.IRR_Offsets.fields{3}.name = 'IRR_V_a_offset';
wr224e.IRR_Offsets.fields{3}.units = 'ADC';
wr224e.IRR_Offsets.fields{3}.start_bit = 32;
wr224e.IRR_Offsets.fields{3}.bit_length = 8;
wr224e.IRR_Offsets.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{3}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{3}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{3}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{3}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{3}.offset = 0.000000e+00;

wr224e.IRR_Offsets.fields{4}.name = 'IRR_I_c_offset';
wr224e.IRR_Offsets.fields{4}.units = 'ADC';
wr224e.IRR_Offsets.fields{4}.start_bit = 40;
wr224e.IRR_Offsets.fields{4}.bit_length = 8;
wr224e.IRR_Offsets.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{4}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{4}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{4}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{4}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{4}.offset = 0.000000e+00;

wr224e.IRR_Offsets.fields{5}.name = 'IRR_I_b_offset';
wr224e.IRR_Offsets.fields{5}.units = 'ADC';
wr224e.IRR_Offsets.fields{5}.start_bit = 48;
wr224e.IRR_Offsets.fields{5}.bit_length = 8;
wr224e.IRR_Offsets.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{5}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{5}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{5}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{5}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{5}.offset = 0.000000e+00;

wr224e.IRR_Offsets.fields{6}.name = 'IRR_I_a_offset';
wr224e.IRR_Offsets.fields{6}.units = 'ADC';
wr224e.IRR_Offsets.fields{6}.start_bit = 56;
wr224e.IRR_Offsets.fields{6}.bit_length = 8;
wr224e.IRR_Offsets.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_Offsets.fields{6}.data_type = 'SIGNED';
wr224e.IRR_Offsets.fields{6}.maximum = 1.280000e+02;
wr224e.IRR_Offsets.fields{6}.minimum = -1.270000e+02;
wr224e.IRR_Offsets.fields{6}.scale = 1.000000e+00;
wr224e.IRR_Offsets.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'IRR_PCMControls'
wr224e.IRR_PCMControls = struct;
wr224e.IRR_PCMControls.name = 'IRR_PCMControls';
wr224e.IRR_PCMControls.description = 'IRR_PCMControls';
wr224e.IRR_PCMControls.protocol = 'wr224e';
wr224e.IRR_PCMControls.id = hex2dec('000000CC');
wr224e.IRR_PCMControls.idext = 'STANDARD';
wr224e.IRR_PCMControls.idinherit = 0;
wr224e.IRR_PCMControls.payload_size = 8;
wr224e.IRR_PCMControls.payload_value = [];
wr224e.IRR_PCMControls.payload_mask = [];
wr224e.IRR_PCMControls.interval = 50;

wr224e.IRR_PCMControls.fields{1}.name = 'IRR_T_IGBT';
wr224e.IRR_PCMControls.fields{1}.units = 'C';
wr224e.IRR_PCMControls.fields{1}.start_bit = 0;
wr224e.IRR_PCMControls.fields{1}.bit_length = 8;
wr224e.IRR_PCMControls.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_PCMControls.fields{1}.data_type = 'UNSIGNED';
wr224e.IRR_PCMControls.fields{1}.maximum = 2.160000e+02;
wr224e.IRR_PCMControls.fields{1}.minimum = 0.000000e+00;
wr224e.IRR_PCMControls.fields{1}.scale = 1.000000e+00;
wr224e.IRR_PCMControls.fields{1}.offset = -4.000000e+01;

wr224e.IRR_PCMControls.fields{2}.name = 'IRR_T_winding';
wr224e.IRR_PCMControls.fields{2}.units = 'C';
wr224e.IRR_PCMControls.fields{2}.start_bit = 8;
wr224e.IRR_PCMControls.fields{2}.bit_length = 8;
wr224e.IRR_PCMControls.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_PCMControls.fields{2}.data_type = 'UNSIGNED';
wr224e.IRR_PCMControls.fields{2}.maximum = 2.160000e+02;
wr224e.IRR_PCMControls.fields{2}.minimum = 0.000000e+00;
wr224e.IRR_PCMControls.fields{2}.scale = 1.000000e+00;
wr224e.IRR_PCMControls.fields{2}.offset = -4.000000e+01;

wr224e.IRR_PCMControls.fields{3}.name = 'IRR_I_s_amplitude';
wr224e.IRR_PCMControls.fields{3}.units = 'A';
wr224e.IRR_PCMControls.fields{3}.start_bit = 24;
wr224e.IRR_PCMControls.fields{3}.bit_length = 16;
wr224e.IRR_PCMControls.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_PCMControls.fields{3}.data_type = 'SIGNED';
wr224e.IRR_PCMControls.fields{3}.maximum = 6.550000e+02;
wr224e.IRR_PCMControls.fields{3}.minimum = -6.550000e+02;
wr224e.IRR_PCMControls.fields{3}.scale = 2.000000e-02;
wr224e.IRR_PCMControls.fields{3}.offset = 0.000000e+00;

wr224e.IRR_PCMControls.fields{4}.name = 'IRR_P_ac';
wr224e.IRR_PCMControls.fields{4}.units = 'W';
wr224e.IRR_PCMControls.fields{4}.start_bit = 40;
wr224e.IRR_PCMControls.fields{4}.bit_length = 16;
wr224e.IRR_PCMControls.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_PCMControls.fields{4}.data_type = 'SIGNED';
wr224e.IRR_PCMControls.fields{4}.maximum = 6.400000e+04;
wr224e.IRR_PCMControls.fields{4}.minimum = -6.400000e+04;
wr224e.IRR_PCMControls.fields{4}.scale = 2.000000e+00;
wr224e.IRR_PCMControls.fields{4}.offset = 0.000000e+00;

wr224e.IRR_PCMControls.fields{5}.name = 'IRR_MotorSpeed';
wr224e.IRR_PCMControls.fields{5}.units = 'RPM';
wr224e.IRR_PCMControls.fields{5}.start_bit = 56;
wr224e.IRR_PCMControls.fields{5}.bit_length = 16;
wr224e.IRR_PCMControls.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_PCMControls.fields{5}.data_type = 'SIGNED';
wr224e.IRR_PCMControls.fields{5}.maximum = 3.270000e+04;
wr224e.IRR_PCMControls.fields{5}.minimum = -3.270000e+04;
wr224e.IRR_PCMControls.fields{5}.scale = 1.000000e+00;
wr224e.IRR_PCMControls.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'IRR_SensorData'
wr224e.IRR_SensorData = struct;
wr224e.IRR_SensorData.name = 'IRR_SensorData';
wr224e.IRR_SensorData.description = 'IRR_SensorData';
wr224e.IRR_SensorData.protocol = 'wr224e';
wr224e.IRR_SensorData.id = hex2dec('000000DD');
wr224e.IRR_SensorData.idext = 'STANDARD';
wr224e.IRR_SensorData.idinherit = 0;
wr224e.IRR_SensorData.payload_size = 8;
wr224e.IRR_SensorData.payload_value = [];
wr224e.IRR_SensorData.payload_mask = [];
wr224e.IRR_SensorData.interval = 50;

wr224e.IRR_SensorData.fields{1}.name = 'IRR_Ib';
wr224e.IRR_SensorData.fields{1}.units = 'A';
wr224e.IRR_SensorData.fields{1}.start_bit = 8;
wr224e.IRR_SensorData.fields{1}.bit_length = 16;
wr224e.IRR_SensorData.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_SensorData.fields{1}.data_type = 'SIGNED';
wr224e.IRR_SensorData.fields{1}.scale = 1.000000e-02;
wr224e.IRR_SensorData.fields{1}.offset = 0.000000e+00;

wr224e.IRR_SensorData.fields{2}.name = 'IRR_Ia';
wr224e.IRR_SensorData.fields{2}.units = 'A';
wr224e.IRR_SensorData.fields{2}.start_bit = 24;
wr224e.IRR_SensorData.fields{2}.bit_length = 16;
wr224e.IRR_SensorData.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_SensorData.fields{2}.data_type = 'SIGNED';
wr224e.IRR_SensorData.fields{2}.scale = 1.000000e-02;
wr224e.IRR_SensorData.fields{2}.offset = 0.000000e+00;

wr224e.IRR_SensorData.fields{3}.name = 'IRR_MechSpeed';
wr224e.IRR_SensorData.fields{3}.units = 'RPM';
wr224e.IRR_SensorData.fields{3}.start_bit = 40;
wr224e.IRR_SensorData.fields{3}.bit_length = 16;
wr224e.IRR_SensorData.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_SensorData.fields{3}.data_type = 'SIGNED';
wr224e.IRR_SensorData.fields{3}.scale = 1.000000e+00;
wr224e.IRR_SensorData.fields{3}.offset = 0.000000e+00;

wr224e.IRR_SensorData.fields{4}.name = 'IRR_MechAccel';
wr224e.IRR_SensorData.fields{4}.units = 'm/s^2';
wr224e.IRR_SensorData.fields{4}.start_bit = 56;
wr224e.IRR_SensorData.fields{4}.bit_length = 16;
wr224e.IRR_SensorData.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_SensorData.fields{4}.data_type = 'SIGNED';
wr224e.IRR_SensorData.fields{4}.scale = 2.000000e-03;
wr224e.IRR_SensorData.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRR_dq'
wr224e.IRR_dq = struct;
wr224e.IRR_dq.name = 'IRR_dq';
wr224e.IRR_dq.description = 'IRR_dq';
wr224e.IRR_dq.protocol = 'wr224e';
wr224e.IRR_dq.id = hex2dec('000000CE');
wr224e.IRR_dq.idext = 'STANDARD';
wr224e.IRR_dq.idinherit = 0;
wr224e.IRR_dq.payload_size = 8;
wr224e.IRR_dq.payload_value = [];
wr224e.IRR_dq.payload_mask = [];
wr224e.IRR_dq.interval = 50;

wr224e.IRR_dq.fields{1}.name = 'IRR_I_q';
wr224e.IRR_dq.fields{1}.units = 'A';
wr224e.IRR_dq.fields{1}.start_bit = 8;
wr224e.IRR_dq.fields{1}.bit_length = 16;
wr224e.IRR_dq.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_dq.fields{1}.data_type = 'SIGNED';
wr224e.IRR_dq.fields{1}.maximum = 6.540000e+02;
wr224e.IRR_dq.fields{1}.minimum = -6.540000e+02;
wr224e.IRR_dq.fields{1}.scale = 2.000000e-02;
wr224e.IRR_dq.fields{1}.offset = 0.000000e+00;

wr224e.IRR_dq.fields{2}.name = 'IRR_I_d';
wr224e.IRR_dq.fields{2}.units = 'A';
wr224e.IRR_dq.fields{2}.start_bit = 24;
wr224e.IRR_dq.fields{2}.bit_length = 16;
wr224e.IRR_dq.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_dq.fields{2}.data_type = 'SIGNED';
wr224e.IRR_dq.fields{2}.maximum = 6.540000e+02;
wr224e.IRR_dq.fields{2}.minimum = -6.540000e+02;
wr224e.IRR_dq.fields{2}.scale = 2.000000e-02;
wr224e.IRR_dq.fields{2}.offset = 0.000000e+00;

wr224e.IRR_dq.fields{3}.name = 'IRR_V_q';
wr224e.IRR_dq.fields{3}.units = 'V';
wr224e.IRR_dq.fields{3}.start_bit = 40;
wr224e.IRR_dq.fields{3}.bit_length = 16;
wr224e.IRR_dq.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_dq.fields{3}.data_type = 'SIGNED';
wr224e.IRR_dq.fields{3}.maximum = 6.540000e+02;
wr224e.IRR_dq.fields{3}.minimum = -6.540000e+02;
wr224e.IRR_dq.fields{3}.scale = 2.000000e-02;
wr224e.IRR_dq.fields{3}.offset = 0.000000e+00;

wr224e.IRR_dq.fields{4}.name = 'IRR_V_d';
wr224e.IRR_dq.fields{4}.units = 'V';
wr224e.IRR_dq.fields{4}.start_bit = 56;
wr224e.IRR_dq.fields{4}.bit_length = 16;
wr224e.IRR_dq.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.IRR_dq.fields{4}.data_type = 'SIGNED';
wr224e.IRR_dq.fields{4}.maximum = 6.540000e+02;
wr224e.IRR_dq.fields{4}.minimum = -6.540000e+02;
wr224e.IRR_dq.fields{4}.scale = 2.000000e-02;
wr224e.IRR_dq.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'IRRrx_Bootloader'
wr224e.IRRrx_Bootloader = struct;
wr224e.IRRrx_Bootloader.name = 'IRRrx_Bootloader';
wr224e.IRRrx_Bootloader.description = 'IRRrx_Bootloader';
wr224e.IRRrx_Bootloader.protocol = 'wr224e';
wr224e.IRRrx_Bootloader.id = hex2dec('000000B4');
wr224e.IRRrx_Bootloader.idext = 'STANDARD';
wr224e.IRRrx_Bootloader.idinherit = 0;
wr224e.IRRrx_Bootloader.payload_size = 8;
wr224e.IRRrx_Bootloader.payload_value = [];
wr224e.IRRrx_Bootloader.payload_mask = [];
wr224e.IRRrx_Bootloader.interval = 50;

%===================================================================
case 'PCM_DAQ13'
wr224e.PCM_DAQ13 = struct;
wr224e.PCM_DAQ13.name = 'PCM_DAQ13';
wr224e.PCM_DAQ13.description = 'PCM_DAQ13';
wr224e.PCM_DAQ13.protocol = 'wr224e';
wr224e.PCM_DAQ13.id = hex2dec('00000313');
wr224e.PCM_DAQ13.idext = 'STANDARD';
wr224e.PCM_DAQ13.idinherit = 0;
wr224e.PCM_DAQ13.payload_size = 8;
wr224e.PCM_DAQ13.payload_value = [];
wr224e.PCM_DAQ13.payload_mask = [];
wr224e.PCM_DAQ13.interval = 50;

wr224e.PCM_DAQ13.fields{1}.name = 'DAQ13_WhlTrqReduction_Percent';
wr224e.PCM_DAQ13.fields{1}.units = '%';
wr224e.PCM_DAQ13.fields{1}.start_bit = 0;
wr224e.PCM_DAQ13.fields{1}.bit_length = 8;
wr224e.PCM_DAQ13.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ13.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{2}.name = 'DAQ13_WhlTrqReduction_Nm';
wr224e.PCM_DAQ13.fields{2}.units = 'Nm';
wr224e.PCM_DAQ13.fields{2}.start_bit = 8;
wr224e.PCM_DAQ13.fields{2}.bit_length = 8;
wr224e.PCM_DAQ13.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{2}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{3}.name = 'DAQ13_WhlTrqMaxRearAxle';
wr224e.PCM_DAQ13.fields{3}.units = 'Nm';
wr224e.PCM_DAQ13.fields{3}.start_bit = 16;
wr224e.PCM_DAQ13.fields{3}.bit_length = 8;
wr224e.PCM_DAQ13.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{3}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{4}.name = 'DAQ13_WhlTrqMaxFrontAxle';
wr224e.PCM_DAQ13.fields{4}.units = 'Nm';
wr224e.PCM_DAQ13.fields{4}.start_bit = 24;
wr224e.PCM_DAQ13.fields{4}.bit_length = 8;
wr224e.PCM_DAQ13.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{4}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{5}.name = 'DAQ13_PedalReqRaw';
wr224e.PCM_DAQ13.fields{5}.units = 'Nm';
wr224e.PCM_DAQ13.fields{5}.start_bit = 48;
wr224e.PCM_DAQ13.fields{5}.bit_length = 8;
wr224e.PCM_DAQ13.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{5}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{6}.name = 'DAQ13_PedalReq_Final';
wr224e.PCM_DAQ13.fields{6}.units = 'Nm';
wr224e.PCM_DAQ13.fields{6}.start_bit = 32;
wr224e.PCM_DAQ13.fields{6}.bit_length = 8;
wr224e.PCM_DAQ13.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{6}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{7}.name = 'DAQ13_PedalReq';
wr224e.PCM_DAQ13.fields{7}.units = 'Nm';
wr224e.PCM_DAQ13.fields{7}.start_bit = 40;
wr224e.PCM_DAQ13.fields{7}.bit_length = 8;
wr224e.PCM_DAQ13.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ13.fields{7}.scale = 2.000000e+00;
wr224e.PCM_DAQ13.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ13.fields{8}.name = 'DAQ13_DeltaTorqueReq';
wr224e.PCM_DAQ13.fields{8}.units = 'Nm';
wr224e.PCM_DAQ13.fields{8}.start_bit = 56;
wr224e.PCM_DAQ13.fields{8}.bit_length = 8;
wr224e.PCM_DAQ13.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ13.fields{8}.data_type = 'SIGNED';
wr224e.PCM_DAQ13.fields{8}.scale = 4.000000e+00;
wr224e.PCM_DAQ13.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ14'
wr224e.PCM_DAQ14 = struct;
wr224e.PCM_DAQ14.name = 'PCM_DAQ14';
wr224e.PCM_DAQ14.description = 'PCM_DAQ14';
wr224e.PCM_DAQ14.protocol = 'wr224e';
wr224e.PCM_DAQ14.id = hex2dec('00000314');
wr224e.PCM_DAQ14.idext = 'STANDARD';
wr224e.PCM_DAQ14.idinherit = 0;
wr224e.PCM_DAQ14.payload_size = 7;
wr224e.PCM_DAQ14.payload_value = [];
wr224e.PCM_DAQ14.payload_mask = [];
wr224e.PCM_DAQ14.interval = 50;

wr224e.PCM_DAQ14.fields{1}.name = 'DAQ14_YawRateReq';
wr224e.PCM_DAQ14.fields{1}.units = 'deg/s';
wr224e.PCM_DAQ14.fields{1}.start_bit = 56;
wr224e.PCM_DAQ14.fields{1}.bit_length = 8;
wr224e.PCM_DAQ14.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ14.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{2}.name = 'DAQ14_YawMomentReq_StrButton';
wr224e.PCM_DAQ14.fields{2}.units = 'Nm';
wr224e.PCM_DAQ14.fields{2}.start_bit = 48;
wr224e.PCM_DAQ14.fields{2}.bit_length = 8;
wr224e.PCM_DAQ14.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{2}.scale = 1.000000e+01;
wr224e.PCM_DAQ14.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{3}.name = 'DAQ14_YawMomentReq';
wr224e.PCM_DAQ14.fields{3}.units = 'Nm';
wr224e.PCM_DAQ14.fields{3}.start_bit = 8;
wr224e.PCM_DAQ14.fields{3}.bit_length = 8;
wr224e.PCM_DAQ14.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{3}.scale = 1.000000e+01;
wr224e.PCM_DAQ14.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{4}.name = 'DAQ14_YawMomentPID_Pterm';
wr224e.PCM_DAQ14.fields{4}.units = 'Nm';
wr224e.PCM_DAQ14.fields{4}.start_bit = 32;
wr224e.PCM_DAQ14.fields{4}.bit_length = 8;
wr224e.PCM_DAQ14.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{4}.scale = 1.000000e+01;
wr224e.PCM_DAQ14.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{5}.name = 'DAQ14_YawMomentPID_Out';
wr224e.PCM_DAQ14.fields{5}.units = 'Nm';
wr224e.PCM_DAQ14.fields{5}.start_bit = 40;
wr224e.PCM_DAQ14.fields{5}.bit_length = 8;
wr224e.PCM_DAQ14.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{5}.scale = 1.000000e+01;
wr224e.PCM_DAQ14.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{6}.name = 'DAQ14_YawMomentPID_Iterm';
wr224e.PCM_DAQ14.fields{6}.units = 'Nm';
wr224e.PCM_DAQ14.fields{6}.start_bit = 24;
wr224e.PCM_DAQ14.fields{6}.bit_length = 8;
wr224e.PCM_DAQ14.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{6}.scale = 1.000000e+01;
wr224e.PCM_DAQ14.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ14.fields{7}.name = 'DAQ14_YawMomentPID_Error';
wr224e.PCM_DAQ14.fields{7}.units = 'Nm';
wr224e.PCM_DAQ14.fields{7}.start_bit = 16;
wr224e.PCM_DAQ14.fields{7}.bit_length = 8;
wr224e.PCM_DAQ14.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ14.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ14.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ14.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ15'
wr224e.PCM_DAQ15 = struct;
wr224e.PCM_DAQ15.name = 'PCM_DAQ15';
wr224e.PCM_DAQ15.description = 'PCM_DAQ15';
wr224e.PCM_DAQ15.protocol = 'wr224e';
wr224e.PCM_DAQ15.id = hex2dec('00000315');
wr224e.PCM_DAQ15.idext = 'STANDARD';
wr224e.PCM_DAQ15.idinherit = 0;
wr224e.PCM_DAQ15.payload_size = 6;
wr224e.PCM_DAQ15.payload_value = [];
wr224e.PCM_DAQ15.payload_mask = [];
wr224e.PCM_DAQ15.interval = 50;

wr224e.PCM_DAQ15.fields{1}.name = 'DAQ15_WhlTrqLimFR_Traction';
wr224e.PCM_DAQ15.fields{1}.units = 'Nm';
wr224e.PCM_DAQ15.fields{1}.start_bit = 24;
wr224e.PCM_DAQ15.fields{1}.bit_length = 8;
wr224e.PCM_DAQ15.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ15.fields{2}.name = 'DAQ15_WhlTrqLimFR_Thermal';
wr224e.PCM_DAQ15.fields{2}.units = 'Nm';
wr224e.PCM_DAQ15.fields{2}.start_bit = 32;
wr224e.PCM_DAQ15.fields{2}.bit_length = 8;
wr224e.PCM_DAQ15.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ15.fields{3}.name = 'DAQ15_WhlTrqLimFR_TractionBrk';
wr224e.PCM_DAQ15.fields{3}.units = 'Nm';
wr224e.PCM_DAQ15.fields{3}.start_bit = 16;
wr224e.PCM_DAQ15.fields{3}.bit_length = 8;
wr224e.PCM_DAQ15.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ15.fields{4}.name = 'DAQ15_WhlTrqLimFL_Traction';
wr224e.PCM_DAQ15.fields{4}.units = 'Nm';
wr224e.PCM_DAQ15.fields{4}.start_bit = 48;
wr224e.PCM_DAQ15.fields{4}.bit_length = 8;
wr224e.PCM_DAQ15.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ15.fields{5}.name = 'DAQ15_WhlTrqLimFL_Thermal';
wr224e.PCM_DAQ15.fields{5}.units = 'Nm';
wr224e.PCM_DAQ15.fields{5}.start_bit = 56;
wr224e.PCM_DAQ15.fields{5}.bit_length = 8;
wr224e.PCM_DAQ15.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ15.fields{6}.name = 'DAQ15_WhlTrqLimFL_TractionBrk';
wr224e.PCM_DAQ15.fields{6}.units = 'Nm';
wr224e.PCM_DAQ15.fields{6}.start_bit = 40;
wr224e.PCM_DAQ15.fields{6}.bit_length = 8;
wr224e.PCM_DAQ15.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ15.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ15.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ15.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ16'
wr224e.PCM_DAQ16 = struct;
wr224e.PCM_DAQ16.name = 'PCM_DAQ16';
wr224e.PCM_DAQ16.description = 'PCM_DAQ16';
wr224e.PCM_DAQ16.protocol = 'wr224e';
wr224e.PCM_DAQ16.id = hex2dec('00000316');
wr224e.PCM_DAQ16.idext = 'STANDARD';
wr224e.PCM_DAQ16.idinherit = 0;
wr224e.PCM_DAQ16.payload_size = 4;
wr224e.PCM_DAQ16.payload_value = [];
wr224e.PCM_DAQ16.payload_mask = [];
wr224e.PCM_DAQ16.interval = 50;

wr224e.PCM_DAQ16.fields{1}.name = 'DAQ16_FinalRegPwrLim';
wr224e.PCM_DAQ16.fields{1}.units = 'kW';
wr224e.PCM_DAQ16.fields{1}.start_bit = 48;
wr224e.PCM_DAQ16.fields{1}.bit_length = 8;
wr224e.PCM_DAQ16.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ16.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ16.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ16.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ16.fields{2}.name = 'DAQ16_VehicleSpdLim';
wr224e.PCM_DAQ16.fields{2}.units = 'mph';
wr224e.PCM_DAQ16.fields{2}.start_bit = 56;
wr224e.PCM_DAQ16.fields{2}.bit_length = 8;
wr224e.PCM_DAQ16.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ16.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ16.fields{2}.scale = 2.500000e-01;
wr224e.PCM_DAQ16.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ16.fields{3}.name = 'DAQ16_LVTransition';
wr224e.PCM_DAQ16.fields{3}.units = 'boolean';
wr224e.PCM_DAQ16.fields{3}.start_bit = 39;
wr224e.PCM_DAQ16.fields{3}.bit_length = 1;
wr224e.PCM_DAQ16.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ16.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ16.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ16.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ16.fields{4}.name = 'DAQ16_LSO_Enable';
wr224e.PCM_DAQ16.fields{4}.units = 'boolean';
wr224e.PCM_DAQ16.fields{4}.start_bit = 38;
wr224e.PCM_DAQ16.fields{4}.bit_length = 1;
wr224e.PCM_DAQ16.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ16.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ16.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ16.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ16.fields{5}.name = 'DAQ16_FinalPwrLim';
wr224e.PCM_DAQ16.fields{5}.units = 'kW';
wr224e.PCM_DAQ16.fields{5}.start_bit = 40;
wr224e.PCM_DAQ16.fields{5}.bit_length = 8;
wr224e.PCM_DAQ16.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ16.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ16.fields{5}.scale = 5.000000e-01;
wr224e.PCM_DAQ16.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ17'
wr224e.PCM_DAQ17 = struct;
wr224e.PCM_DAQ17.name = 'PCM_DAQ17';
wr224e.PCM_DAQ17.description = 'PCM_DAQ17';
wr224e.PCM_DAQ17.protocol = 'wr224e';
wr224e.PCM_DAQ17.id = hex2dec('00000317');
wr224e.PCM_DAQ17.idext = 'STANDARD';
wr224e.PCM_DAQ17.idinherit = 0;
wr224e.PCM_DAQ17.payload_size = 8;
wr224e.PCM_DAQ17.payload_value = [];
wr224e.PCM_DAQ17.payload_mask = [];
wr224e.PCM_DAQ17.interval = 50;

wr224e.PCM_DAQ17.fields{1}.name = 'DAQ17_TractionLimRR';
wr224e.PCM_DAQ17.fields{1}.units = 'Nm';
wr224e.PCM_DAQ17.fields{1}.start_bit = 0;
wr224e.PCM_DAQ17.fields{1}.bit_length = 8;
wr224e.PCM_DAQ17.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{1}.scale = 2.000000e+00;
wr224e.PCM_DAQ17.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{2}.name = 'DAQ17_TractionLimRL';
wr224e.PCM_DAQ17.fields{2}.units = 'Nm';
wr224e.PCM_DAQ17.fields{2}.start_bit = 8;
wr224e.PCM_DAQ17.fields{2}.bit_length = 8;
wr224e.PCM_DAQ17.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{2}.scale = 2.000000e+00;
wr224e.PCM_DAQ17.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{3}.name = 'DAQ17_TractionLimFR';
wr224e.PCM_DAQ17.fields{3}.units = 'Nm';
wr224e.PCM_DAQ17.fields{3}.start_bit = 16;
wr224e.PCM_DAQ17.fields{3}.bit_length = 8;
wr224e.PCM_DAQ17.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{3}.scale = 2.000000e+00;
wr224e.PCM_DAQ17.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{4}.name = 'DAQ17_TractionLimFL';
wr224e.PCM_DAQ17.fields{4}.units = 'Nm';
wr224e.PCM_DAQ17.fields{4}.start_bit = 24;
wr224e.PCM_DAQ17.fields{4}.bit_length = 8;
wr224e.PCM_DAQ17.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{4}.scale = 2.000000e+00;
wr224e.PCM_DAQ17.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{5}.name = 'DAQ17_TireUtilizationRR';
wr224e.PCM_DAQ17.fields{5}.units = 'Nm';
wr224e.PCM_DAQ17.fields{5}.start_bit = 32;
wr224e.PCM_DAQ17.fields{5}.bit_length = 8;
wr224e.PCM_DAQ17.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ17.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{6}.name = 'DAQ17_TireUtilizationRL';
wr224e.PCM_DAQ17.fields{6}.units = 'Nm';
wr224e.PCM_DAQ17.fields{6}.start_bit = 40;
wr224e.PCM_DAQ17.fields{6}.bit_length = 8;
wr224e.PCM_DAQ17.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ17.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{7}.name = 'DAQ17_TireUtilizationFR';
wr224e.PCM_DAQ17.fields{7}.units = 'Nm';
wr224e.PCM_DAQ17.fields{7}.start_bit = 48;
wr224e.PCM_DAQ17.fields{7}.bit_length = 8;
wr224e.PCM_DAQ17.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ17.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ17.fields{8}.name = 'DAQ17_TireUtilizationFL';
wr224e.PCM_DAQ17.fields{8}.units = 'Nm';
wr224e.PCM_DAQ17.fields{8}.start_bit = 56;
wr224e.PCM_DAQ17.fields{8}.bit_length = 8;
wr224e.PCM_DAQ17.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ17.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ17.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ17.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ18'
wr224e.PCM_DAQ18 = struct;
wr224e.PCM_DAQ18.name = 'PCM_DAQ18';
wr224e.PCM_DAQ18.description = 'PCM_DAQ18';
wr224e.PCM_DAQ18.protocol = 'wr224e';
wr224e.PCM_DAQ18.id = hex2dec('00000318');
wr224e.PCM_DAQ18.idext = 'STANDARD';
wr224e.PCM_DAQ18.idinherit = 0;
wr224e.PCM_DAQ18.payload_size = 8;
wr224e.PCM_DAQ18.payload_value = [];
wr224e.PCM_DAQ18.payload_mask = [];
wr224e.PCM_DAQ18.interval = 50;

wr224e.PCM_DAQ18.fields{1}.name = 'DAQ18_LongUtilizationRR';
wr224e.PCM_DAQ18.fields{1}.units = '%';
wr224e.PCM_DAQ18.fields{1}.start_bit = 0;
wr224e.PCM_DAQ18.fields{1}.bit_length = 8;
wr224e.PCM_DAQ18.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{2}.name = 'DAQ18_LongUtilizationRL';
wr224e.PCM_DAQ18.fields{2}.units = '%';
wr224e.PCM_DAQ18.fields{2}.start_bit = 8;
wr224e.PCM_DAQ18.fields{2}.bit_length = 8;
wr224e.PCM_DAQ18.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{3}.name = 'DAQ18_LongUtilizationFR';
wr224e.PCM_DAQ18.fields{3}.units = '%';
wr224e.PCM_DAQ18.fields{3}.start_bit = 16;
wr224e.PCM_DAQ18.fields{3}.bit_length = 8;
wr224e.PCM_DAQ18.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{4}.name = 'DAQ18_LongUtilizationFL';
wr224e.PCM_DAQ18.fields{4}.units = '%';
wr224e.PCM_DAQ18.fields{4}.start_bit = 24;
wr224e.PCM_DAQ18.fields{4}.bit_length = 8;
wr224e.PCM_DAQ18.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{5}.name = 'DAQ18_LatUtilizationRR';
wr224e.PCM_DAQ18.fields{5}.units = '%';
wr224e.PCM_DAQ18.fields{5}.start_bit = 32;
wr224e.PCM_DAQ18.fields{5}.bit_length = 8;
wr224e.PCM_DAQ18.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{6}.name = 'DAQ18_LatUtilizationRL';
wr224e.PCM_DAQ18.fields{6}.units = '%';
wr224e.PCM_DAQ18.fields{6}.start_bit = 40;
wr224e.PCM_DAQ18.fields{6}.bit_length = 8;
wr224e.PCM_DAQ18.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{7}.name = 'DAQ18_LatUtilizationFR';
wr224e.PCM_DAQ18.fields{7}.units = '%';
wr224e.PCM_DAQ18.fields{7}.start_bit = 48;
wr224e.PCM_DAQ18.fields{7}.bit_length = 8;
wr224e.PCM_DAQ18.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ18.fields{8}.name = 'DAQ18_LatUtilizationFL';
wr224e.PCM_DAQ18.fields{8}.units = '%';
wr224e.PCM_DAQ18.fields{8}.start_bit = 56;
wr224e.PCM_DAQ18.fields{8}.bit_length = 8;
wr224e.PCM_DAQ18.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ18.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ18.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ18.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ19'
wr224e.PCM_DAQ19 = struct;
wr224e.PCM_DAQ19.name = 'PCM_DAQ19';
wr224e.PCM_DAQ19.description = 'PCM_DAQ19';
wr224e.PCM_DAQ19.protocol = 'wr224e';
wr224e.PCM_DAQ19.id = hex2dec('00000319');
wr224e.PCM_DAQ19.idext = 'STANDARD';
wr224e.PCM_DAQ19.idinherit = 0;
wr224e.PCM_DAQ19.payload_size = 8;
wr224e.PCM_DAQ19.payload_value = [];
wr224e.PCM_DAQ19.payload_mask = [];
wr224e.PCM_DAQ19.interval = 50;

wr224e.PCM_DAQ19.fields{1}.name = 'DAQ19_LatforceRR';
wr224e.PCM_DAQ19.fields{1}.units = 'N';
wr224e.PCM_DAQ19.fields{1}.start_bit = 8;
wr224e.PCM_DAQ19.fields{1}.bit_length = 16;
wr224e.PCM_DAQ19.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ19.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ19.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ19.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ19.fields{2}.name = 'DAQ19_LatforceRL';
wr224e.PCM_DAQ19.fields{2}.units = 'N';
wr224e.PCM_DAQ19.fields{2}.start_bit = 24;
wr224e.PCM_DAQ19.fields{2}.bit_length = 16;
wr224e.PCM_DAQ19.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ19.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ19.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ19.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ19.fields{3}.name = 'DAQ19_LatforceFR';
wr224e.PCM_DAQ19.fields{3}.units = 'N';
wr224e.PCM_DAQ19.fields{3}.start_bit = 40;
wr224e.PCM_DAQ19.fields{3}.bit_length = 16;
wr224e.PCM_DAQ19.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ19.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ19.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ19.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ19.fields{4}.name = 'DAQ19_LatforceFL';
wr224e.PCM_DAQ19.fields{4}.units = 'N';
wr224e.PCM_DAQ19.fields{4}.start_bit = 56;
wr224e.PCM_DAQ19.fields{4}.bit_length = 16;
wr224e.PCM_DAQ19.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ19.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ19.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ19.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ2'
wr224e.PCM_DAQ2 = struct;
wr224e.PCM_DAQ2.name = 'PCM_DAQ2';
wr224e.PCM_DAQ2.description = 'PCM_DAQ2';
wr224e.PCM_DAQ2.protocol = 'wr224e';
wr224e.PCM_DAQ2.id = hex2dec('00000302');
wr224e.PCM_DAQ2.idext = 'STANDARD';
wr224e.PCM_DAQ2.idinherit = 0;
wr224e.PCM_DAQ2.payload_size = 6;
wr224e.PCM_DAQ2.payload_value = [];
wr224e.PCM_DAQ2.payload_mask = [];
wr224e.PCM_DAQ2.interval = 50;

wr224e.PCM_DAQ2.fields{1}.name = 'DAQ2_RTD_Sound_PostCal';
wr224e.PCM_DAQ2.fields{1}.units = 'boolean';
wr224e.PCM_DAQ2.fields{1}.start_bit = 23;
wr224e.PCM_DAQ2.fields{1}.bit_length = 1;
wr224e.PCM_DAQ2.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ2.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{2}.name = 'DAQ2_PumpsL_DutyCycle';
wr224e.PCM_DAQ2.fields{2}.units = 'PWM';
wr224e.PCM_DAQ2.fields{2}.start_bit = 48;
wr224e.PCM_DAQ2.fields{2}.bit_length = 8;
wr224e.PCM_DAQ2.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{2}.scale = 5.000000e-01;
wr224e.PCM_DAQ2.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{3}.name = 'DAQ2_Pump_Current_L';
wr224e.PCM_DAQ2.fields{3}.units = 'A';
wr224e.PCM_DAQ2.fields{3}.start_bit = 40;
wr224e.PCM_DAQ2.fields{3}.bit_length = 8;
wr224e.PCM_DAQ2.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{3}.scale = 1.000000e-01;
wr224e.PCM_DAQ2.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{4}.name = 'DAQ2_Fan_Current_F';
wr224e.PCM_DAQ2.fields{4}.units = 'A';
wr224e.PCM_DAQ2.fields{4}.start_bit = 56;
wr224e.PCM_DAQ2.fields{4}.bit_length = 8;
wr224e.PCM_DAQ2.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{4}.scale = 1.000000e-01;
wr224e.PCM_DAQ2.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{5}.name = 'DAQ2_Enable_Motors_PostCal';
wr224e.PCM_DAQ2.fields{5}.units = 'boolean';
wr224e.PCM_DAQ2.fields{5}.start_bit = 22;
wr224e.PCM_DAQ2.fields{5}.bit_length = 1;
wr224e.PCM_DAQ2.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ2.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{6}.name = 'DAQ2_PCM_Close_SDC_PostCal';
wr224e.PCM_DAQ2.fields{6}.units = 'boolean';
wr224e.PCM_DAQ2.fields{6}.start_bit = 21;
wr224e.PCM_DAQ2.fields{6}.bit_length = 1;
wr224e.PCM_DAQ2.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ2.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{7}.name = 'DAQ2_Cooling_FansL_DutyCycle';
wr224e.PCM_DAQ2.fields{7}.units = 'PWM';
wr224e.PCM_DAQ2.fields{7}.start_bit = 24;
wr224e.PCM_DAQ2.fields{7}.bit_length = 8;
wr224e.PCM_DAQ2.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{7}.scale = 5.000000e-01;
wr224e.PCM_DAQ2.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ2.fields{8}.name = 'DAQ2_Cooling_FansR_DutyCycle';
wr224e.PCM_DAQ2.fields{8}.units = 'PWM';
wr224e.PCM_DAQ2.fields{8}.start_bit = 32;
wr224e.PCM_DAQ2.fields{8}.bit_length = 8;
wr224e.PCM_DAQ2.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ2.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ2.fields{8}.scale = 5.000000e-01;
wr224e.PCM_DAQ2.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ20'
wr224e.PCM_DAQ20 = struct;
wr224e.PCM_DAQ20.name = 'PCM_DAQ20';
wr224e.PCM_DAQ20.description = 'PCM_DAQ20';
wr224e.PCM_DAQ20.protocol = 'wr224e';
wr224e.PCM_DAQ20.id = hex2dec('00000320');
wr224e.PCM_DAQ20.idext = 'STANDARD';
wr224e.PCM_DAQ20.idinherit = 0;
wr224e.PCM_DAQ20.payload_size = 6;
wr224e.PCM_DAQ20.payload_value = [];
wr224e.PCM_DAQ20.payload_mask = [];
wr224e.PCM_DAQ20.interval = 50;

wr224e.PCM_DAQ20.fields{1}.name = 'DAQ20_Tyre_FCDiam_Total_R';
wr224e.PCM_DAQ20.fields{1}.units = '';
wr224e.PCM_DAQ20.fields{1}.start_bit = 24;
wr224e.PCM_DAQ20.fields{1}.bit_length = 8;
wr224e.PCM_DAQ20.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ20.fields{1}.scale = 1.000000e-02;
wr224e.PCM_DAQ20.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ20.fields{2}.name = 'DAQ20_Tyre_FCDiam_Total_F';
wr224e.PCM_DAQ20.fields{2}.units = '';
wr224e.PCM_DAQ20.fields{2}.start_bit = 32;
wr224e.PCM_DAQ20.fields{2}.bit_length = 8;
wr224e.PCM_DAQ20.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ20.fields{2}.scale = 1.000000e-02;
wr224e.PCM_DAQ20.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ20.fields{3}.name = 'DAQ20_LongLoadXfer';
wr224e.PCM_DAQ20.fields{3}.units = 'N';
wr224e.PCM_DAQ20.fields{3}.start_bit = 56;
wr224e.PCM_DAQ20.fields{3}.bit_length = 8;
wr224e.PCM_DAQ20.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ20.fields{3}.scale = 1.000000e+01;
wr224e.PCM_DAQ20.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ20.fields{4}.name = 'DAQ20_LatLoadXferR';
wr224e.PCM_DAQ20.fields{4}.units = 'N';
wr224e.PCM_DAQ20.fields{4}.start_bit = 40;
wr224e.PCM_DAQ20.fields{4}.bit_length = 8;
wr224e.PCM_DAQ20.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ20.fields{4}.scale = 1.000000e+01;
wr224e.PCM_DAQ20.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ20.fields{5}.name = 'DAQ20_LatLoadXferF';
wr224e.PCM_DAQ20.fields{5}.units = 'N';
wr224e.PCM_DAQ20.fields{5}.start_bit = 48;
wr224e.PCM_DAQ20.fields{5}.bit_length = 8;
wr224e.PCM_DAQ20.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ20.fields{5}.scale = 1.000000e+01;
wr224e.PCM_DAQ20.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ20.fields{6}.name = 'DAQ20_ActualTorque';
wr224e.PCM_DAQ20.fields{6}.units = 'Nm';
wr224e.PCM_DAQ20.fields{6}.start_bit = 16;
wr224e.PCM_DAQ20.fields{6}.bit_length = 8;
wr224e.PCM_DAQ20.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ20.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ20.fields{6}.scale = 1.000000e+01;
wr224e.PCM_DAQ20.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ22'
wr224e.PCM_DAQ22 = struct;
wr224e.PCM_DAQ22.name = 'PCM_DAQ22';
wr224e.PCM_DAQ22.description = 'PCM_DAQ22';
wr224e.PCM_DAQ22.protocol = 'wr224e';
wr224e.PCM_DAQ22.id = hex2dec('00000322');
wr224e.PCM_DAQ22.idext = 'STANDARD';
wr224e.PCM_DAQ22.idinherit = 0;
wr224e.PCM_DAQ22.payload_size = 6;
wr224e.PCM_DAQ22.payload_value = [];
wr224e.PCM_DAQ22.payload_mask = [];
wr224e.PCM_DAQ22.interval = 50;

wr224e.PCM_DAQ22.fields{1}.name = 'DAQ22_Distance_Powercycle';
wr224e.PCM_DAQ22.fields{1}.units = 'mile';
wr224e.PCM_DAQ22.fields{1}.start_bit = 56;
wr224e.PCM_DAQ22.fields{1}.bit_length = 8;
wr224e.PCM_DAQ22.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ22.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ22.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ22.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ22.fields{2}.name = 'DAQ22_Distance_CalibrationReset';
wr224e.PCM_DAQ22.fields{2}.units = 'mile';
wr224e.PCM_DAQ22.fields{2}.start_bit = 48;
wr224e.PCM_DAQ22.fields{2}.bit_length = 8;
wr224e.PCM_DAQ22.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ22.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ22.fields{2}.scale = 5.000000e-01;
wr224e.PCM_DAQ22.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ22.fields{3}.name = 'DAQ22_CornerRadiusPostFilt';
wr224e.PCM_DAQ22.fields{3}.units = 'm';
wr224e.PCM_DAQ22.fields{3}.start_bit = 24;
wr224e.PCM_DAQ22.fields{3}.bit_length = 16;
wr224e.PCM_DAQ22.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ22.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ22.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ22.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ22.fields{4}.name = 'DAQ22_AccelYPostFilt';
wr224e.PCM_DAQ22.fields{4}.units = 'm/s2';
wr224e.PCM_DAQ22.fields{4}.start_bit = 32;
wr224e.PCM_DAQ22.fields{4}.bit_length = 8;
wr224e.PCM_DAQ22.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ22.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ22.fields{4}.scale = 2.000000e-02;
wr224e.PCM_DAQ22.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ22.fields{5}.name = 'DAQ22_AccelXPostFilt';
wr224e.PCM_DAQ22.fields{5}.units = 'm/s2';
wr224e.PCM_DAQ22.fields{5}.start_bit = 40;
wr224e.PCM_DAQ22.fields{5}.bit_length = 8;
wr224e.PCM_DAQ22.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ22.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ22.fields{5}.scale = 2.000000e-02;
wr224e.PCM_DAQ22.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ25'
wr224e.PCM_DAQ25 = struct;
wr224e.PCM_DAQ25.name = 'PCM_DAQ25';
wr224e.PCM_DAQ25.description = 'PCM_DAQ25';
wr224e.PCM_DAQ25.protocol = 'wr224e';
wr224e.PCM_DAQ25.id = hex2dec('00000325');
wr224e.PCM_DAQ25.idext = 'STANDARD';
wr224e.PCM_DAQ25.idinherit = 0;
wr224e.PCM_DAQ25.payload_size = 4;
wr224e.PCM_DAQ25.payload_value = [];
wr224e.PCM_DAQ25.payload_mask = [];
wr224e.PCM_DAQ25.interval = 50;

wr224e.PCM_DAQ25.fields{1}.name = 'DAQ25_OptimalSplit';
wr224e.PCM_DAQ25.fields{1}.units = '%';
wr224e.PCM_DAQ25.fields{1}.start_bit = 56;
wr224e.PCM_DAQ25.fields{1}.bit_length = 8;
wr224e.PCM_DAQ25.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ25.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ25.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ25.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ25.fields{2}.name = 'DAQ25_APPS_Arb';
wr224e.PCM_DAQ25.fields{2}.units = '%';
wr224e.PCM_DAQ25.fields{2}.start_bit = 48;
wr224e.PCM_DAQ25.fields{2}.bit_length = 8;
wr224e.PCM_DAQ25.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ25.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ25.fields{2}.scale = 5.000000e-01;
wr224e.PCM_DAQ25.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ25.fields{3}.name = 'DAQ25_AccumPowerFinal';
wr224e.PCM_DAQ25.fields{3}.units = 'kW';
wr224e.PCM_DAQ25.fields{3}.start_bit = 32;
wr224e.PCM_DAQ25.fields{3}.bit_length = 8;
wr224e.PCM_DAQ25.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ25.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ25.fields{3}.scale = 5.000000e-01;
wr224e.PCM_DAQ25.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ25.fields{4}.name = 'DAQ25_AccumCurrentFinal';
wr224e.PCM_DAQ25.fields{4}.units = 'A';
wr224e.PCM_DAQ25.fields{4}.start_bit = 40;
wr224e.PCM_DAQ25.fields{4}.bit_length = 8;
wr224e.PCM_DAQ25.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ25.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ25.fields{4}.scale = 5.000000e+00;
wr224e.PCM_DAQ25.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ26'
wr224e.PCM_DAQ26 = struct;
wr224e.PCM_DAQ26.name = 'PCM_DAQ26';
wr224e.PCM_DAQ26.description = 'PCM_DAQ26';
wr224e.PCM_DAQ26.protocol = 'wr224e';
wr224e.PCM_DAQ26.id = hex2dec('00000326');
wr224e.PCM_DAQ26.idext = 'STANDARD';
wr224e.PCM_DAQ26.idinherit = 0;
wr224e.PCM_DAQ26.payload_size = 5;
wr224e.PCM_DAQ26.payload_value = [];
wr224e.PCM_DAQ26.payload_mask = [];
wr224e.PCM_DAQ26.interval = 50;

wr224e.PCM_DAQ26.fields{1}.name = 'DAQ26_RR_AC_Current_Filt';
wr224e.PCM_DAQ26.fields{1}.units = 'A';
wr224e.PCM_DAQ26.fields{1}.start_bit = 32;
wr224e.PCM_DAQ26.fields{1}.bit_length = 8;
wr224e.PCM_DAQ26.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ26.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ26.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ26.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ26.fields{2}.name = 'DAQ26_RR_Motor_Speed';
wr224e.PCM_DAQ26.fields{2}.units = 'rpm';
wr224e.PCM_DAQ26.fields{2}.start_bit = 24;
wr224e.PCM_DAQ26.fields{2}.bit_length = 8;
wr224e.PCM_DAQ26.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ26.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ26.fields{2}.scale = 5.000000e+01;
wr224e.PCM_DAQ26.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ26.fields{3}.name = 'DAQ26_RL_AC_Current_Filt';
wr224e.PCM_DAQ26.fields{3}.units = 'A';
wr224e.PCM_DAQ26.fields{3}.start_bit = 48;
wr224e.PCM_DAQ26.fields{3}.bit_length = 8;
wr224e.PCM_DAQ26.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ26.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ26.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ26.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ26.fields{4}.name = 'DAQ26_RL_Motor_Speed_Filt';
wr224e.PCM_DAQ26.fields{4}.units = 'rpm';
wr224e.PCM_DAQ26.fields{4}.start_bit = 40;
wr224e.PCM_DAQ26.fields{4}.bit_length = 8;
wr224e.PCM_DAQ26.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ26.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ26.fields{4}.scale = 5.000000e+01;
wr224e.PCM_DAQ26.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ26.fields{5}.name = 'DAQ26_FR_Motor_Speed_Filt';
wr224e.PCM_DAQ26.fields{5}.units = 'rpm';
wr224e.PCM_DAQ26.fields{5}.start_bit = 56;
wr224e.PCM_DAQ26.fields{5}.bit_length = 8;
wr224e.PCM_DAQ26.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ26.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ26.fields{5}.scale = 5.000000e+01;
wr224e.PCM_DAQ26.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ3'
wr224e.PCM_DAQ3 = struct;
wr224e.PCM_DAQ3.name = 'PCM_DAQ3';
wr224e.PCM_DAQ3.description = 'PCM_DAQ3';
wr224e.PCM_DAQ3.protocol = 'wr224e';
wr224e.PCM_DAQ3.id = hex2dec('00000303');
wr224e.PCM_DAQ3.idext = 'STANDARD';
wr224e.PCM_DAQ3.idinherit = 0;
wr224e.PCM_DAQ3.payload_size = 8;
wr224e.PCM_DAQ3.payload_value = [];
wr224e.PCM_DAQ3.payload_mask = [];
wr224e.PCM_DAQ3.interval = 50;

wr224e.PCM_DAQ3.fields{1}.name = 'DAQ3_FinalMtrTorqueRR';
wr224e.PCM_DAQ3.fields{1}.units = 'Nm';
wr224e.PCM_DAQ3.fields{1}.start_bit = 8;
wr224e.PCM_DAQ3.fields{1}.bit_length = 16;
wr224e.PCM_DAQ3.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ3.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ3.fields{1}.maximum = 6.550000e+01;
wr224e.PCM_DAQ3.fields{1}.minimum = -6.550000e+01;
wr224e.PCM_DAQ3.fields{1}.scale = 2.000000e-03;
wr224e.PCM_DAQ3.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ3.fields{2}.name = 'DAQ3_FinalMtrTorqueRL';
wr224e.PCM_DAQ3.fields{2}.units = 'Nm';
wr224e.PCM_DAQ3.fields{2}.start_bit = 24;
wr224e.PCM_DAQ3.fields{2}.bit_length = 16;
wr224e.PCM_DAQ3.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ3.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ3.fields{2}.maximum = 6.550000e+01;
wr224e.PCM_DAQ3.fields{2}.minimum = -6.550000e+01;
wr224e.PCM_DAQ3.fields{2}.scale = 2.000000e-03;
wr224e.PCM_DAQ3.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ3.fields{3}.name = 'DAQ3_FinalMtrTorqueFL';
wr224e.PCM_DAQ3.fields{3}.units = 'Nm';
wr224e.PCM_DAQ3.fields{3}.start_bit = 40;
wr224e.PCM_DAQ3.fields{3}.bit_length = 16;
wr224e.PCM_DAQ3.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ3.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ3.fields{3}.maximum = 6.550000e+01;
wr224e.PCM_DAQ3.fields{3}.minimum = -6.550000e+01;
wr224e.PCM_DAQ3.fields{3}.scale = 2.000000e-03;
wr224e.PCM_DAQ3.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ3.fields{4}.name = 'DAQ3_FinalMtrTorqueFR';
wr224e.PCM_DAQ3.fields{4}.units = 'Nm';
wr224e.PCM_DAQ3.fields{4}.start_bit = 56;
wr224e.PCM_DAQ3.fields{4}.bit_length = 16;
wr224e.PCM_DAQ3.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ3.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ3.fields{4}.maximum = 6.550000e+01;
wr224e.PCM_DAQ3.fields{4}.minimum = -6.550000e+01;
wr224e.PCM_DAQ3.fields{4}.scale = 2.000000e-03;
wr224e.PCM_DAQ3.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ31'
wr224e.PCM_DAQ31 = struct;
wr224e.PCM_DAQ31.name = 'PCM_DAQ31';
wr224e.PCM_DAQ31.description = 'PCM_DAQ31';
wr224e.PCM_DAQ31.protocol = 'wr224e';
wr224e.PCM_DAQ31.id = hex2dec('00000331');
wr224e.PCM_DAQ31.idext = 'STANDARD';
wr224e.PCM_DAQ31.idinherit = 0;
wr224e.PCM_DAQ31.payload_size = 7;
wr224e.PCM_DAQ31.payload_value = [];
wr224e.PCM_DAQ31.payload_mask = [];
wr224e.PCM_DAQ31.interval = 50;

wr224e.PCM_DAQ31.fields{1}.name = 'DAQ31_LV_Current';
wr224e.PCM_DAQ31.fields{1}.units = 'A';
wr224e.PCM_DAQ31.fields{1}.start_bit = 24;
wr224e.PCM_DAQ31.fields{1}.bit_length = 8;
wr224e.PCM_DAQ31.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ31.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ31.fields{1}.scale = 1.000000e-01;
wr224e.PCM_DAQ31.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ31.fields{2}.name = 'DAQ31_Brake_Pressure_Rear';
wr224e.PCM_DAQ31.fields{2}.units = '16';
wr224e.PCM_DAQ31.fields{2}.start_bit = 40;
wr224e.PCM_DAQ31.fields{2}.bit_length = 16;
wr224e.PCM_DAQ31.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ31.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ31.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ31.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ31.fields{3}.name = 'DAQ31_Brake_Pressure_Front';
wr224e.PCM_DAQ31.fields{3}.units = '16';
wr224e.PCM_DAQ31.fields{3}.start_bit = 56;
wr224e.PCM_DAQ31.fields{3}.bit_length = 16;
wr224e.PCM_DAQ31.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ31.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ31.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ31.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ31.fields{4}.name = 'DAQ31_AntiRollR';
wr224e.PCM_DAQ31.fields{4}.units = '%';
wr224e.PCM_DAQ31.fields{4}.start_bit = 8;
wr224e.PCM_DAQ31.fields{4}.bit_length = 8;
wr224e.PCM_DAQ31.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ31.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ31.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ31.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ31.fields{5}.name = 'DAQ31_AntiRollF';
wr224e.PCM_DAQ31.fields{5}.units = '%';
wr224e.PCM_DAQ31.fields{5}.start_bit = 16;
wr224e.PCM_DAQ31.fields{5}.bit_length = 8;
wr224e.PCM_DAQ31.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ31.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ31.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ31.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ32'
wr224e.PCM_DAQ32 = struct;
wr224e.PCM_DAQ32.name = 'PCM_DAQ32';
wr224e.PCM_DAQ32.description = 'PCM_DAQ32';
wr224e.PCM_DAQ32.protocol = 'wr224e';
wr224e.PCM_DAQ32.id = hex2dec('00000332');
wr224e.PCM_DAQ32.idext = 'STANDARD';
wr224e.PCM_DAQ32.idinherit = 0;
wr224e.PCM_DAQ32.payload_size = 3;
wr224e.PCM_DAQ32.payload_value = [];
wr224e.PCM_DAQ32.payload_mask = [];
wr224e.PCM_DAQ32.interval = 50;

wr224e.PCM_DAQ32.fields{1}.name = 'DAQ32_Aux_MCU_Status';
wr224e.PCM_DAQ32.fields{1}.units = 'boolean';
wr224e.PCM_DAQ32.fields{1}.start_bit = 53;
wr224e.PCM_DAQ32.fields{1}.bit_length = 1;
wr224e.PCM_DAQ32.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{2}.name = 'DAQ32_Streer_Whl_Buttons_Status';
wr224e.PCM_DAQ32.fields{2}.units = 'boolean';
wr224e.PCM_DAQ32.fields{2}.start_bit = 61;
wr224e.PCM_DAQ32.fields{2}.bit_length = 1;
wr224e.PCM_DAQ32.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{3}.name = 'DAQ32_Str_Ang_SDO_Resp_Status';
wr224e.PCM_DAQ32.fields{3}.units = 'boolean';
wr224e.PCM_DAQ32.fields{3}.start_bit = 45;
wr224e.PCM_DAQ32.fields{3}.bit_length = 1;
wr224e.PCM_DAQ32.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{4}.name = 'DAQ32_SDC_Closed_PostCal';
wr224e.PCM_DAQ32.fields{4}.units = 'boolean';
wr224e.PCM_DAQ32.fields{4}.start_bit = 62;
wr224e.PCM_DAQ32.fields{4}.bit_length = 1;
wr224e.PCM_DAQ32.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{5}.name = 'DAQ32_RTD_State';
wr224e.PCM_DAQ32.fields{5}.units = 'boolean';
wr224e.PCM_DAQ32.fields{5}.start_bit = 43;
wr224e.PCM_DAQ32.fields{5}.bit_length = 1;
wr224e.PCM_DAQ32.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{6}.name = 'DAQ32_RTD_PostCal';
wr224e.PCM_DAQ32.fields{6}.units = 'boolean';
wr224e.PCM_DAQ32.fields{6}.start_bit = 63;
wr224e.PCM_DAQ32.fields{6}.bit_length = 1;
wr224e.PCM_DAQ32.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{7}.name = 'DAQ32_RR_Controls_Status';
wr224e.PCM_DAQ32.fields{7}.units = 'boolean';
wr224e.PCM_DAQ32.fields{7}.start_bit = 46;
wr224e.PCM_DAQ32.fields{7}.bit_length = 1;
wr224e.PCM_DAQ32.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{8}.name = 'DAQ32_RL_Controls_Status';
wr224e.PCM_DAQ32.fields{8}.units = 'boolean';
wr224e.PCM_DAQ32.fields{8}.start_bit = 47;
wr224e.PCM_DAQ32.fields{8}.bit_length = 1;
wr224e.PCM_DAQ32.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{8}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{9}.name = 'DAQ32_FR_Controls_Status';
wr224e.PCM_DAQ32.fields{9}.units = 'boolean';
wr224e.PCM_DAQ32.fields{9}.start_bit = 48;
wr224e.PCM_DAQ32.fields{9}.bit_length = 1;
wr224e.PCM_DAQ32.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{9}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{9}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{9}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{10}.name = 'DAQ32_FL_Controls_Status';
wr224e.PCM_DAQ32.fields{10}.units = 'boolean';
wr224e.PCM_DAQ32.fields{10}.start_bit = 49;
wr224e.PCM_DAQ32.fields{10}.bit_length = 1;
wr224e.PCM_DAQ32.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{10}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{10}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{11}.name = 'DAQ32_CAN_Gyro2_Status';
wr224e.PCM_DAQ32.fields{11}.units = 'boolean';
wr224e.PCM_DAQ32.fields{11}.start_bit = 59;
wr224e.PCM_DAQ32.fields{11}.bit_length = 1;
wr224e.PCM_DAQ32.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{11}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{11}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{12}.name = 'DAQ32_CAN_Gyro1_Status';
wr224e.PCM_DAQ32.fields{12}.units = 'boolean';
wr224e.PCM_DAQ32.fields{12}.start_bit = 60;
wr224e.PCM_DAQ32.fields{12}.bit_length = 1;
wr224e.PCM_DAQ32.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{12}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{12}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{12}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{13}.name = 'DAQ32_CAN_Ellipse5_Status';
wr224e.PCM_DAQ32.fields{13}.units = 'boolean';
wr224e.PCM_DAQ32.fields{13}.start_bit = 54;
wr224e.PCM_DAQ32.fields{13}.bit_length = 1;
wr224e.PCM_DAQ32.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{13}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{13}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{13}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{14}.name = 'DAQ32_CAN_Ellipse4_Status';
wr224e.PCM_DAQ32.fields{14}.units = 'boolean';
wr224e.PCM_DAQ32.fields{14}.start_bit = 55;
wr224e.PCM_DAQ32.fields{14}.bit_length = 1;
wr224e.PCM_DAQ32.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{14}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{14}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{14}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{15}.name = 'DAQ32_CAN_Ellipse3_Status';
wr224e.PCM_DAQ32.fields{15}.units = 'boolean';
wr224e.PCM_DAQ32.fields{15}.start_bit = 56;
wr224e.PCM_DAQ32.fields{15}.bit_length = 1;
wr224e.PCM_DAQ32.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{15}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{15}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{15}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{16}.name = 'DAQ32_CAN_Ellipse2_Status';
wr224e.PCM_DAQ32.fields{16}.units = 'boolean';
wr224e.PCM_DAQ32.fields{16}.start_bit = 57;
wr224e.PCM_DAQ32.fields{16}.bit_length = 1;
wr224e.PCM_DAQ32.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{16}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{16}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{16}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{17}.name = 'DAQ32_CAN_Ellipse1_Status';
wr224e.PCM_DAQ32.fields{17}.units = 'boolean';
wr224e.PCM_DAQ32.fields{17}.start_bit = 58;
wr224e.PCM_DAQ32.fields{17}.bit_length = 1;
wr224e.PCM_DAQ32.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{17}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{17}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{17}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{18}.name = 'DAQ32_Brake_Flag';
wr224e.PCM_DAQ32.fields{18}.units = 'boolean';
wr224e.PCM_DAQ32.fields{18}.start_bit = 44;
wr224e.PCM_DAQ32.fields{18}.bit_length = 1;
wr224e.PCM_DAQ32.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{18}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{18}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{18}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{19}.name = 'DAQ32_BMS_Vitals_Status';
wr224e.PCM_DAQ32.fields{19}.units = 'boolean';
wr224e.PCM_DAQ32.fields{19}.start_bit = 51;
wr224e.PCM_DAQ32.fields{19}.bit_length = 1;
wr224e.PCM_DAQ32.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{19}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{19}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{19}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{20}.name = 'DAQ32_BMS_Pack_Stats_Status';
wr224e.PCM_DAQ32.fields{20}.units = 'boolean';
wr224e.PCM_DAQ32.fields{20}.start_bit = 50;
wr224e.PCM_DAQ32.fields{20}.bit_length = 1;
wr224e.PCM_DAQ32.fields{20}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{20}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{20}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{20}.offset = 0.000000e+00;

wr224e.PCM_DAQ32.fields{21}.name = 'DAQ32_Aux_MCU_Flags_Status';
wr224e.PCM_DAQ32.fields{21}.units = 'boolean';
wr224e.PCM_DAQ32.fields{21}.start_bit = 52;
wr224e.PCM_DAQ32.fields{21}.bit_length = 1;
wr224e.PCM_DAQ32.fields{21}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ32.fields{21}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ32.fields{21}.scale = 1.000000e+00;
wr224e.PCM_DAQ32.fields{21}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ33'
wr224e.PCM_DAQ33 = struct;
wr224e.PCM_DAQ33.name = 'PCM_DAQ33';
wr224e.PCM_DAQ33.description = 'PCM_DAQ33';
wr224e.PCM_DAQ33.protocol = 'wr224e';
wr224e.PCM_DAQ33.id = hex2dec('00000333');
wr224e.PCM_DAQ33.idext = 'STANDARD';
wr224e.PCM_DAQ33.idinherit = 0;
wr224e.PCM_DAQ33.payload_size = 6;
wr224e.PCM_DAQ33.payload_value = [];
wr224e.PCM_DAQ33.payload_mask = [];
wr224e.PCM_DAQ33.interval = 50;

wr224e.PCM_DAQ33.fields{1}.name = 'DAQ33_WhlTrqLim_RR_Traction';
wr224e.PCM_DAQ33.fields{1}.units = 'Nm';
wr224e.PCM_DAQ33.fields{1}.start_bit = 24;
wr224e.PCM_DAQ33.fields{1}.bit_length = 8;
wr224e.PCM_DAQ33.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{1}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ33.fields{2}.name = 'DAQ33_WhlTrqLim_RR_Thermal';
wr224e.PCM_DAQ33.fields{2}.units = 'Nm';
wr224e.PCM_DAQ33.fields{2}.start_bit = 32;
wr224e.PCM_DAQ33.fields{2}.bit_length = 8;
wr224e.PCM_DAQ33.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{2}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ33.fields{3}.name = 'DAQ33_WhlTrqLim_RR_TractionBrk';
wr224e.PCM_DAQ33.fields{3}.units = 'Nm';
wr224e.PCM_DAQ33.fields{3}.start_bit = 16;
wr224e.PCM_DAQ33.fields{3}.bit_length = 8;
wr224e.PCM_DAQ33.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{3}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ33.fields{4}.name = 'DAQ33_WhlTrqLim_RL_Traction';
wr224e.PCM_DAQ33.fields{4}.units = 'Nm';
wr224e.PCM_DAQ33.fields{4}.start_bit = 48;
wr224e.PCM_DAQ33.fields{4}.bit_length = 8;
wr224e.PCM_DAQ33.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{4}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ33.fields{5}.name = 'DAQ33_WhlTrqLim_RL_Thermal';
wr224e.PCM_DAQ33.fields{5}.units = 'Nm';
wr224e.PCM_DAQ33.fields{5}.start_bit = 56;
wr224e.PCM_DAQ33.fields{5}.bit_length = 8;
wr224e.PCM_DAQ33.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{5}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ33.fields{6}.name = 'DAQ33_WhlTrqLim_RL_TractionBrk';
wr224e.PCM_DAQ33.fields{6}.units = 'Nm';
wr224e.PCM_DAQ33.fields{6}.start_bit = 40;
wr224e.PCM_DAQ33.fields{6}.bit_length = 8;
wr224e.PCM_DAQ33.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ33.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ33.fields{6}.scale = 2.000000e+00;
wr224e.PCM_DAQ33.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ34'
wr224e.PCM_DAQ34 = struct;
wr224e.PCM_DAQ34.name = 'PCM_DAQ34';
wr224e.PCM_DAQ34.description = 'PCM_DAQ34';
wr224e.PCM_DAQ34.protocol = 'wr224e';
wr224e.PCM_DAQ34.id = hex2dec('00000334');
wr224e.PCM_DAQ34.idext = 'STANDARD';
wr224e.PCM_DAQ34.idinherit = 0;
wr224e.PCM_DAQ34.payload_size = 4;
wr224e.PCM_DAQ34.payload_value = [];
wr224e.PCM_DAQ34.payload_mask = [];
wr224e.PCM_DAQ34.interval = 50;

wr224e.PCM_DAQ34.fields{1}.name = 'DAQ34_NormalForceRR';
wr224e.PCM_DAQ34.fields{1}.units = 'N';
wr224e.PCM_DAQ34.fields{1}.start_bit = 32;
wr224e.PCM_DAQ34.fields{1}.bit_length = 8;
wr224e.PCM_DAQ34.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ34.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ34.fields{1}.scale = 1.000000e+01;
wr224e.PCM_DAQ34.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ34.fields{2}.name = 'DAQ34_NormalForceRL';
wr224e.PCM_DAQ34.fields{2}.units = 'N';
wr224e.PCM_DAQ34.fields{2}.start_bit = 40;
wr224e.PCM_DAQ34.fields{2}.bit_length = 8;
wr224e.PCM_DAQ34.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ34.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ34.fields{2}.scale = 1.000000e+01;
wr224e.PCM_DAQ34.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ34.fields{3}.name = 'DAQ34_NormalForceFR';
wr224e.PCM_DAQ34.fields{3}.units = 'N';
wr224e.PCM_DAQ34.fields{3}.start_bit = 48;
wr224e.PCM_DAQ34.fields{3}.bit_length = 8;
wr224e.PCM_DAQ34.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ34.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ34.fields{3}.scale = 1.000000e+01;
wr224e.PCM_DAQ34.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ34.fields{4}.name = 'DAQ34_NormalForceFL';
wr224e.PCM_DAQ34.fields{4}.units = 'N';
wr224e.PCM_DAQ34.fields{4}.start_bit = 56;
wr224e.PCM_DAQ34.fields{4}.bit_length = 8;
wr224e.PCM_DAQ34.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ34.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ34.fields{4}.scale = 1.000000e+01;
wr224e.PCM_DAQ34.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ35'
wr224e.PCM_DAQ35 = struct;
wr224e.PCM_DAQ35.name = 'PCM_DAQ35';
wr224e.PCM_DAQ35.description = 'PCM_DAQ35';
wr224e.PCM_DAQ35.protocol = 'wr224e';
wr224e.PCM_DAQ35.id = hex2dec('00000335');
wr224e.PCM_DAQ35.idext = 'STANDARD';
wr224e.PCM_DAQ35.idinherit = 0;
wr224e.PCM_DAQ35.payload_size = 8;
wr224e.PCM_DAQ35.payload_value = [];
wr224e.PCM_DAQ35.payload_mask = [];
wr224e.PCM_DAQ35.interval = 50;

wr224e.PCM_DAQ35.fields{1}.name = 'DAQ35_MediumBrakeEvent';
wr224e.PCM_DAQ35.fields{1}.units = '';
wr224e.PCM_DAQ35.fields{1}.start_bit = 8;
wr224e.PCM_DAQ35.fields{1}.bit_length = 8;
wr224e.PCM_DAQ35.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{1}.scale = 2.000000e+00;
wr224e.PCM_DAQ35.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{2}.name = 'DAQ35_HardBrakeEvent';
wr224e.PCM_DAQ35.fields{2}.units = '';
wr224e.PCM_DAQ35.fields{2}.start_bit = 0;
wr224e.PCM_DAQ35.fields{2}.bit_length = 8;
wr224e.PCM_DAQ35.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{2}.scale = 2.000000e+00;
wr224e.PCM_DAQ35.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{3}.name = 'DAQ35_EasyBrakeEvent';
wr224e.PCM_DAQ35.fields{3}.units = '';
wr224e.PCM_DAQ35.fields{3}.start_bit = 16;
wr224e.PCM_DAQ35.fields{3}.bit_length = 8;
wr224e.PCM_DAQ35.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{3}.scale = 2.000000e+00;
wr224e.PCM_DAQ35.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{4}.name = 'DAQ35_BrakeEnergyRR';
wr224e.PCM_DAQ35.fields{4}.units = 'J';
wr224e.PCM_DAQ35.fields{4}.start_bit = 32;
wr224e.PCM_DAQ35.fields{4}.bit_length = 8;
wr224e.PCM_DAQ35.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{4}.scale = 5.000000e+05;
wr224e.PCM_DAQ35.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{5}.name = 'DAQ35_BrakeEnergyRL';
wr224e.PCM_DAQ35.fields{5}.units = 'J';
wr224e.PCM_DAQ35.fields{5}.start_bit = 40;
wr224e.PCM_DAQ35.fields{5}.bit_length = 8;
wr224e.PCM_DAQ35.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{5}.scale = 5.000000e+05;
wr224e.PCM_DAQ35.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{6}.name = 'DAQ35_BrakeEnergyFR';
wr224e.PCM_DAQ35.fields{6}.units = 'J';
wr224e.PCM_DAQ35.fields{6}.start_bit = 48;
wr224e.PCM_DAQ35.fields{6}.bit_length = 8;
wr224e.PCM_DAQ35.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{6}.scale = 5.000000e+05;
wr224e.PCM_DAQ35.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{7}.name = 'DAQ35_BrakeEnergyFL';
wr224e.PCM_DAQ35.fields{7}.units = 'J';
wr224e.PCM_DAQ35.fields{7}.start_bit = 56;
wr224e.PCM_DAQ35.fields{7}.bit_length = 8;
wr224e.PCM_DAQ35.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{7}.scale = 5.000000e+05;
wr224e.PCM_DAQ35.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ35.fields{8}.name = 'DAQ35_BrakeEnergyF_Single';
wr224e.PCM_DAQ35.fields{8}.units = 'J';
wr224e.PCM_DAQ35.fields{8}.start_bit = 24;
wr224e.PCM_DAQ35.fields{8}.bit_length = 8;
wr224e.PCM_DAQ35.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ35.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ35.fields{8}.scale = 5.000000e+05;
wr224e.PCM_DAQ35.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ4'
wr224e.PCM_DAQ4 = struct;
wr224e.PCM_DAQ4.name = 'PCM_DAQ4';
wr224e.PCM_DAQ4.description = 'PCM_DAQ4';
wr224e.PCM_DAQ4.protocol = 'wr224e';
wr224e.PCM_DAQ4.id = hex2dec('00000304');
wr224e.PCM_DAQ4.idext = 'STANDARD';
wr224e.PCM_DAQ4.idinherit = 0;
wr224e.PCM_DAQ4.payload_size = 8;
wr224e.PCM_DAQ4.payload_value = [];
wr224e.PCM_DAQ4.payload_mask = [];
wr224e.PCM_DAQ4.interval = 50;

wr224e.PCM_DAQ4.fields{1}.name = 'DAQ4_ModifiedWhlTorqueRR';
wr224e.PCM_DAQ4.fields{1}.units = 'Nm';
wr224e.PCM_DAQ4.fields{1}.start_bit = 8;
wr224e.PCM_DAQ4.fields{1}.bit_length = 16;
wr224e.PCM_DAQ4.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ4.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ4.fields{1}.maximum = 6.553500e+02;
wr224e.PCM_DAQ4.fields{1}.minimum = 0.000000e+00;
wr224e.PCM_DAQ4.fields{1}.scale = 1.000000e-02;
wr224e.PCM_DAQ4.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ4.fields{2}.name = 'DAQ4_ModifiedWhlTorqueRL';
wr224e.PCM_DAQ4.fields{2}.units = 'Nm';
wr224e.PCM_DAQ4.fields{2}.start_bit = 24;
wr224e.PCM_DAQ4.fields{2}.bit_length = 16;
wr224e.PCM_DAQ4.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ4.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ4.fields{2}.maximum = 6.553500e+02;
wr224e.PCM_DAQ4.fields{2}.minimum = 0.000000e+00;
wr224e.PCM_DAQ4.fields{2}.scale = 1.000000e-02;
wr224e.PCM_DAQ4.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ4.fields{3}.name = 'DAQ4_ModifiedWhlTorqueFR';
wr224e.PCM_DAQ4.fields{3}.units = 'Nm';
wr224e.PCM_DAQ4.fields{3}.start_bit = 40;
wr224e.PCM_DAQ4.fields{3}.bit_length = 16;
wr224e.PCM_DAQ4.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ4.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ4.fields{3}.maximum = 6.553500e+02;
wr224e.PCM_DAQ4.fields{3}.minimum = 0.000000e+00;
wr224e.PCM_DAQ4.fields{3}.scale = 1.000000e-02;
wr224e.PCM_DAQ4.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ4.fields{4}.name = 'DAQ4_ModifiedWhlTorqueFL';
wr224e.PCM_DAQ4.fields{4}.units = 'Nm';
wr224e.PCM_DAQ4.fields{4}.start_bit = 56;
wr224e.PCM_DAQ4.fields{4}.bit_length = 16;
wr224e.PCM_DAQ4.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ4.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ4.fields{4}.maximum = 6.553500e+02;
wr224e.PCM_DAQ4.fields{4}.minimum = 0.000000e+00;
wr224e.PCM_DAQ4.fields{4}.scale = 1.000000e-02;
wr224e.PCM_DAQ4.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ5'
wr224e.PCM_DAQ5 = struct;
wr224e.PCM_DAQ5.name = 'PCM_DAQ5';
wr224e.PCM_DAQ5.description = 'PCM_DAQ5';
wr224e.PCM_DAQ5.protocol = 'wr224e';
wr224e.PCM_DAQ5.id = hex2dec('00000305');
wr224e.PCM_DAQ5.idext = 'STANDARD';
wr224e.PCM_DAQ5.idinherit = 0;
wr224e.PCM_DAQ5.payload_size = 8;
wr224e.PCM_DAQ5.payload_value = [];
wr224e.PCM_DAQ5.payload_mask = [];
wr224e.PCM_DAQ5.interval = 50;

wr224e.PCM_DAQ5.fields{1}.name = 'DAQ5_VehSpdLimitFeedbackActive';
wr224e.PCM_DAQ5.fields{1}.units = 'boolean';
wr224e.PCM_DAQ5.fields{1}.start_bit = 15;
wr224e.PCM_DAQ5.fields{1}.bit_length = 1;
wr224e.PCM_DAQ5.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ5.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{2}.name = 'DAQ5_PowerLimitFeedbackActive';
wr224e.PCM_DAQ5.fields{2}.units = 'boolean';
wr224e.PCM_DAQ5.fields{2}.start_bit = 14;
wr224e.PCM_DAQ5.fields{2}.bit_length = 1;
wr224e.PCM_DAQ5.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ5.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{3}.name = 'DAQ5_LimitPercentR_VehSpd';
wr224e.PCM_DAQ5.fields{3}.units = '%';
wr224e.PCM_DAQ5.fields{3}.start_bit = 48;
wr224e.PCM_DAQ5.fields{3}.bit_length = 8;
wr224e.PCM_DAQ5.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{3}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{4}.name = 'DAQ5_LimitPercentR_PowerLim';
wr224e.PCM_DAQ5.fields{4}.units = '%';
wr224e.PCM_DAQ5.fields{4}.start_bit = 16;
wr224e.PCM_DAQ5.fields{4}.bit_length = 8;
wr224e.PCM_DAQ5.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{4}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{5}.name = 'DAQ5_LimitPercentF_VehSpd';
wr224e.PCM_DAQ5.fields{5}.units = '%';
wr224e.PCM_DAQ5.fields{5}.start_bit = 56;
wr224e.PCM_DAQ5.fields{5}.bit_length = 8;
wr224e.PCM_DAQ5.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{5}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{6}.name = 'DAQ5_LimitPercentF_PowerLim';
wr224e.PCM_DAQ5.fields{6}.units = '%';
wr224e.PCM_DAQ5.fields{6}.start_bit = 24;
wr224e.PCM_DAQ5.fields{6}.bit_length = 8;
wr224e.PCM_DAQ5.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{6}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{7}.name = 'DAQ5_AccumPower_PreFilt';
wr224e.PCM_DAQ5.fields{7}.units = 'W';
wr224e.PCM_DAQ5.fields{7}.start_bit = 40;
wr224e.PCM_DAQ5.fields{7}.bit_length = 8;
wr224e.PCM_DAQ5.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{7}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ5.fields{8}.name = 'DAQ5_AccumPower_Filt';
wr224e.PCM_DAQ5.fields{8}.units = 'W';
wr224e.PCM_DAQ5.fields{8}.start_bit = 32;
wr224e.PCM_DAQ5.fields{8}.bit_length = 8;
wr224e.PCM_DAQ5.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ5.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ5.fields{8}.scale = 5.000000e-01;
wr224e.PCM_DAQ5.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ7'
wr224e.PCM_DAQ7 = struct;
wr224e.PCM_DAQ7.name = 'PCM_DAQ7';
wr224e.PCM_DAQ7.description = 'PCM_DAQ7';
wr224e.PCM_DAQ7.protocol = 'wr224e';
wr224e.PCM_DAQ7.id = hex2dec('00000307');
wr224e.PCM_DAQ7.idext = 'STANDARD';
wr224e.PCM_DAQ7.idinherit = 0;
wr224e.PCM_DAQ7.payload_size = 8;
wr224e.PCM_DAQ7.payload_value = [];
wr224e.PCM_DAQ7.payload_mask = [];
wr224e.PCM_DAQ7.interval = 50;

wr224e.PCM_DAQ7.fields{1}.name = 'DAQ7_WhlTorqueRR_PowerLimited';
wr224e.PCM_DAQ7.fields{1}.units = 'Nm';
wr224e.PCM_DAQ7.fields{1}.start_bit = 32;
wr224e.PCM_DAQ7.fields{1}.bit_length = 8;
wr224e.PCM_DAQ7.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ7.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{2}.name = 'DAQ7_WhlTorqueRL_PowerLimited';
wr224e.PCM_DAQ7.fields{2}.units = 'Nm';
wr224e.PCM_DAQ7.fields{2}.start_bit = 40;
wr224e.PCM_DAQ7.fields{2}.bit_length = 8;
wr224e.PCM_DAQ7.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ7.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{3}.name = 'DAQ7_WhlTorqueFR_PowerLimited';
wr224e.PCM_DAQ7.fields{3}.units = 'Nm';
wr224e.PCM_DAQ7.fields{3}.start_bit = 48;
wr224e.PCM_DAQ7.fields{3}.bit_length = 8;
wr224e.PCM_DAQ7.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ7.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{4}.name = 'DAQ7_WhlTorqueFL_PowerLimited';
wr224e.PCM_DAQ7.fields{4}.units = 'Nm';
wr224e.PCM_DAQ7.fields{4}.start_bit = 56;
wr224e.PCM_DAQ7.fields{4}.bit_length = 8;
wr224e.PCM_DAQ7.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ7.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{5}.name = 'DAQ7_DeltaWhlTorqueRR_PowerLim';
wr224e.PCM_DAQ7.fields{5}.units = 'Nm';
wr224e.PCM_DAQ7.fields{5}.start_bit = 0;
wr224e.PCM_DAQ7.fields{5}.bit_length = 8;
wr224e.PCM_DAQ7.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ7.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{6}.name = 'DAQ7_DeltaWhlTorqueRL_PowerLim';
wr224e.PCM_DAQ7.fields{6}.units = 'Nm';
wr224e.PCM_DAQ7.fields{6}.start_bit = 8;
wr224e.PCM_DAQ7.fields{6}.bit_length = 8;
wr224e.PCM_DAQ7.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ7.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{7}.name = 'DAQ7_DeltaWhlTorqueFR_PowerLim';
wr224e.PCM_DAQ7.fields{7}.units = 'Nm';
wr224e.PCM_DAQ7.fields{7}.start_bit = 16;
wr224e.PCM_DAQ7.fields{7}.bit_length = 8;
wr224e.PCM_DAQ7.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ7.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ7.fields{8}.name = 'DAQ7_DeltaWhlTorqueFL_PowerLim';
wr224e.PCM_DAQ7.fields{8}.units = 'Nm';
wr224e.PCM_DAQ7.fields{8}.start_bit = 24;
wr224e.PCM_DAQ7.fields{8}.bit_length = 8;
wr224e.PCM_DAQ7.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ7.fields{8}.data_type = 'SIGNED';
wr224e.PCM_DAQ7.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ7.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ8'
wr224e.PCM_DAQ8 = struct;
wr224e.PCM_DAQ8.name = 'PCM_DAQ8';
wr224e.PCM_DAQ8.description = 'PCM_DAQ8';
wr224e.PCM_DAQ8.protocol = 'wr224e';
wr224e.PCM_DAQ8.id = hex2dec('00000308');
wr224e.PCM_DAQ8.idext = 'STANDARD';
wr224e.PCM_DAQ8.idinherit = 0;
wr224e.PCM_DAQ8.payload_size = 7;
wr224e.PCM_DAQ8.payload_value = [];
wr224e.PCM_DAQ8.payload_mask = [];
wr224e.PCM_DAQ8.interval = 50;

wr224e.PCM_DAQ8.fields{1}.name = 'DAQ8_PowerLimited_Request';
wr224e.PCM_DAQ8.fields{1}.units = 'boolean';
wr224e.PCM_DAQ8.fields{1}.start_bit = 23;
wr224e.PCM_DAQ8.fields{1}.bit_length = 1;
wr224e.PCM_DAQ8.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ8.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ8.fields{2}.name = 'DAQ8_MotorPowerTotal_Estimated';
wr224e.PCM_DAQ8.fields{2}.units = 'kW';
wr224e.PCM_DAQ8.fields{2}.start_bit = 24;
wr224e.PCM_DAQ8.fields{2}.bit_length = 8;
wr224e.PCM_DAQ8.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{2}.scale = 5.000000e-01;
wr224e.PCM_DAQ8.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ8.fields{3}.name = 'DAQ8_Motor_Power_RR';
wr224e.PCM_DAQ8.fields{3}.units = 'kW';
wr224e.PCM_DAQ8.fields{3}.start_bit = 32;
wr224e.PCM_DAQ8.fields{3}.bit_length = 8;
wr224e.PCM_DAQ8.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{3}.scale = 1.000000e-01;
wr224e.PCM_DAQ8.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ8.fields{4}.name = 'DAQ8_Motor_Power_RL';
wr224e.PCM_DAQ8.fields{4}.units = 'kW';
wr224e.PCM_DAQ8.fields{4}.start_bit = 40;
wr224e.PCM_DAQ8.fields{4}.bit_length = 8;
wr224e.PCM_DAQ8.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{4}.scale = 1.000000e-01;
wr224e.PCM_DAQ8.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ8.fields{5}.name = 'DAQ8_Motor_Power_FR';
wr224e.PCM_DAQ8.fields{5}.units = 'kW';
wr224e.PCM_DAQ8.fields{5}.start_bit = 48;
wr224e.PCM_DAQ8.fields{5}.bit_length = 8;
wr224e.PCM_DAQ8.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{5}.scale = 1.000000e-01;
wr224e.PCM_DAQ8.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ8.fields{6}.name = 'DAQ8_Motor_Power_FL';
wr224e.PCM_DAQ8.fields{6}.units = 'kW';
wr224e.PCM_DAQ8.fields{6}.start_bit = 56;
wr224e.PCM_DAQ8.fields{6}.bit_length = 8;
wr224e.PCM_DAQ8.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ8.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ8.fields{6}.scale = 1.000000e-01;
wr224e.PCM_DAQ8.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ9'
wr224e.PCM_DAQ9 = struct;
wr224e.PCM_DAQ9.name = 'PCM_DAQ9';
wr224e.PCM_DAQ9.description = 'PCM_DAQ9';
wr224e.PCM_DAQ9.protocol = 'wr224e';
wr224e.PCM_DAQ9.id = hex2dec('00000309');
wr224e.PCM_DAQ9.idext = 'STANDARD';
wr224e.PCM_DAQ9.idinherit = 0;
wr224e.PCM_DAQ9.payload_size = 8;
wr224e.PCM_DAQ9.payload_value = [];
wr224e.PCM_DAQ9.payload_mask = [];
wr224e.PCM_DAQ9.interval = 50;

wr224e.PCM_DAQ9.fields{1}.name = 'DAQ9_FinalWhlTrqRR';
wr224e.PCM_DAQ9.fields{1}.units = 'Nm';
wr224e.PCM_DAQ9.fields{1}.start_bit = 0;
wr224e.PCM_DAQ9.fields{1}.bit_length = 8;
wr224e.PCM_DAQ9.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ9.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{2}.name = 'DAQ9_FinalWhlTrqRL';
wr224e.PCM_DAQ9.fields{2}.units = 'Nm';
wr224e.PCM_DAQ9.fields{2}.start_bit = 8;
wr224e.PCM_DAQ9.fields{2}.bit_length = 8;
wr224e.PCM_DAQ9.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ9.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{3}.name = 'DAQ9_FinalWhlTrqFR';
wr224e.PCM_DAQ9.fields{3}.units = 'Nm';
wr224e.PCM_DAQ9.fields{3}.start_bit = 16;
wr224e.PCM_DAQ9.fields{3}.bit_length = 8;
wr224e.PCM_DAQ9.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ9.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{4}.name = 'DAQ9_FinalWhlTrqFL';
wr224e.PCM_DAQ9.fields{4}.units = 'Nm';
wr224e.PCM_DAQ9.fields{4}.start_bit = 24;
wr224e.PCM_DAQ9.fields{4}.bit_length = 8;
wr224e.PCM_DAQ9.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ9.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{5}.name = 'DAQ9_DeltaWhlTrqRR';
wr224e.PCM_DAQ9.fields{5}.units = 'Nm';
wr224e.PCM_DAQ9.fields{5}.start_bit = 32;
wr224e.PCM_DAQ9.fields{5}.bit_length = 8;
wr224e.PCM_DAQ9.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ9.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{6}.name = 'DAQ9_DeltaWhlTrqRL';
wr224e.PCM_DAQ9.fields{6}.units = 'Nm';
wr224e.PCM_DAQ9.fields{6}.start_bit = 40;
wr224e.PCM_DAQ9.fields{6}.bit_length = 8;
wr224e.PCM_DAQ9.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ9.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{7}.name = 'DAQ9_DeltaWhlTrqFR';
wr224e.PCM_DAQ9.fields{7}.units = 'Nm';
wr224e.PCM_DAQ9.fields{7}.start_bit = 48;
wr224e.PCM_DAQ9.fields{7}.bit_length = 8;
wr224e.PCM_DAQ9.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ9.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ9.fields{8}.name = 'DAQ9_DeltaWhlTrqFL';
wr224e.PCM_DAQ9.fields{8}.units = 'Nm';
wr224e.PCM_DAQ9.fields{8}.start_bit = 56;
wr224e.PCM_DAQ9.fields{8}.bit_length = 8;
wr224e.PCM_DAQ9.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ9.fields{8}.data_type = 'SIGNED';
wr224e.PCM_DAQ9.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ9.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_BMSFilt'
wr224e.PCM_DAQ_BMSFilt = struct;
wr224e.PCM_DAQ_BMSFilt.name = 'PCM_DAQ_BMSFilt';
wr224e.PCM_DAQ_BMSFilt.description = 'PCM_DAQ_BMSFilt';
wr224e.PCM_DAQ_BMSFilt.protocol = 'wr224e';
wr224e.PCM_DAQ_BMSFilt.id = hex2dec('00000327');
wr224e.PCM_DAQ_BMSFilt.idext = 'STANDARD';
wr224e.PCM_DAQ_BMSFilt.idinherit = 0;
wr224e.PCM_DAQ_BMSFilt.payload_size = 7;
wr224e.PCM_DAQ_BMSFilt.payload_value = [];
wr224e.PCM_DAQ_BMSFilt.payload_mask = [];
wr224e.PCM_DAQ_BMSFilt.interval = 50;

wr224e.PCM_DAQ_BMSFilt.fields{1}.name = 'DAQ27_BMS_Pack_Open_Voltage_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{1}.units = 'V';
wr224e.PCM_DAQ_BMSFilt.fields{1}.start_bit = 8;
wr224e.PCM_DAQ_BMSFilt.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{1}.scale = 5.000000e-01;
wr224e.PCM_DAQ_BMSFilt.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{2}.name = 'DAQ27_BMS_High_Temp_Processed';
wr224e.PCM_DAQ_BMSFilt.fields{2}.units = 'C';
wr224e.PCM_DAQ_BMSFilt.fields{2}.start_bit = 16;
wr224e.PCM_DAQ_BMSFilt.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_BMSFilt.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{3}.name = 'DAQ27_FR_AC_Current_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{3}.units = 'A';
wr224e.PCM_DAQ_BMSFilt.fields{3}.start_bit = 24;
wr224e.PCM_DAQ_BMSFilt.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_BMSFilt.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{4}.name = 'DAQ27_FL_AC_Current_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{4}.units = 'A';
wr224e.PCM_DAQ_BMSFilt.fields{4}.start_bit = 40;
wr224e.PCM_DAQ_BMSFilt.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_BMSFilt.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{5}.name = 'DAQ27_FL_Motor_Speed_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{5}.units = 'RPM';
wr224e.PCM_DAQ_BMSFilt.fields{5}.start_bit = 32;
wr224e.PCM_DAQ_BMSFilt.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{5}.scale = 5.000000e+01;
wr224e.PCM_DAQ_BMSFilt.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{6}.name = 'DAQ27_BMS_Pack_Sum_Voltage_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{6}.units = 'V';
wr224e.PCM_DAQ_BMSFilt.fields{6}.start_bit = 48;
wr224e.PCM_DAQ_BMSFilt.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_BMSFilt.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_BMSFilt.fields{7}.name = 'DAQ27_BMS_Pack_Current_Filt';
wr224e.PCM_DAQ_BMSFilt.fields{7}.units = 'A';
wr224e.PCM_DAQ_BMSFilt.fields{7}.start_bit = 56;
wr224e.PCM_DAQ_BMSFilt.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_BMSFilt.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_BMSFilt.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_BMSFilt.fields{7}.scale = 5.000000e+00;
wr224e.PCM_DAQ_BMSFilt.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_DeltaTorqueLimits'
wr224e.PCM_DAQ_DeltaTorqueLimits = struct;
wr224e.PCM_DAQ_DeltaTorqueLimits.name = 'PCM_DAQ_DeltaTorqueLimits';
wr224e.PCM_DAQ_DeltaTorqueLimits.description = 'PCM_DAQ_DeltaTorqueLimits';
wr224e.PCM_DAQ_DeltaTorqueLimits.protocol = 'wr224e';
wr224e.PCM_DAQ_DeltaTorqueLimits.id = hex2dec('00000310');
wr224e.PCM_DAQ_DeltaTorqueLimits.idext = 'STANDARD';
wr224e.PCM_DAQ_DeltaTorqueLimits.idinherit = 0;
wr224e.PCM_DAQ_DeltaTorqueLimits.payload_size = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.payload_value = [];
wr224e.PCM_DAQ_DeltaTorqueLimits.payload_mask = [];
wr224e.PCM_DAQ_DeltaTorqueLimits.interval = 50;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.name = 'DAQ10_DeltaTrqLimits_UpperRR';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.start_bit = 32;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.name = 'DAQ10_DeltaTrqLimits_UpperRL';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.start_bit = 40;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.name = 'DAQ10_DeltaTrqLimits_UpperFR';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.start_bit = 48;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.name = 'DAQ10_DeltaTrqLimits_UpperFL';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.start_bit = 56;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.name = 'DAQ10_DeltaTrqLimits_LowerRR';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.start_bit = 0;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.name = 'DAQ10_DeltaTrqLimits_LowerRL';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.start_bit = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.name = 'DAQ10_DeltaTrqLimits_LowerFR';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.start_bit = 16;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.name = 'DAQ10_DeltaTrqLimits_LowerFL';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.units = 'Nm';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.start_bit = 24;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ_DeltaTorqueLimits.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_EllipseFilt'
wr224e.PCM_DAQ_EllipseFilt = struct;
wr224e.PCM_DAQ_EllipseFilt.name = 'PCM_DAQ_EllipseFilt';
wr224e.PCM_DAQ_EllipseFilt.description = 'PCM_DAQ_EllipseFilt';
wr224e.PCM_DAQ_EllipseFilt.protocol = 'wr224e';
wr224e.PCM_DAQ_EllipseFilt.id = hex2dec('00000328');
wr224e.PCM_DAQ_EllipseFilt.idext = 'STANDARD';
wr224e.PCM_DAQ_EllipseFilt.idinherit = 0;
wr224e.PCM_DAQ_EllipseFilt.payload_size = 7;
wr224e.PCM_DAQ_EllipseFilt.payload_value = [];
wr224e.PCM_DAQ_EllipseFilt.payload_mask = [];
wr224e.PCM_DAQ_EllipseFilt.interval = 50;

wr224e.PCM_DAQ_EllipseFilt.fields{1}.name = 'DAQ28_Ellipse_YawRateZ_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{1}.units = 'deg/s';
wr224e.PCM_DAQ_EllipseFilt.fields{1}.start_bit = 40;
wr224e.PCM_DAQ_EllipseFilt.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_EllipseFilt.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{2}.name = 'DAQ28_Ellipse_YawAngleZ_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{2}.units = 'deg';
wr224e.PCM_DAQ_EllipseFilt.fields{2}.start_bit = 32;
wr224e.PCM_DAQ_EllipseFilt.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_EllipseFilt.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{3}.name = 'DAQ28_Ellipse_VelZ_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{3}.units = 'mph';
wr224e.PCM_DAQ_EllipseFilt.fields{3}.start_bit = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_EllipseFilt.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{4}.name = 'DAQ28_Ellipse_VelY_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{4}.units = 'mph';
wr224e.PCM_DAQ_EllipseFilt.fields{4}.start_bit = 16;
wr224e.PCM_DAQ_EllipseFilt.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_EllipseFilt.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{5}.name = 'DAQ28_Ellipse_VelX_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{5}.units = 'mph';
wr224e.PCM_DAQ_EllipseFilt.fields{5}.start_bit = 24;
wr224e.PCM_DAQ_EllipseFilt.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_EllipseFilt.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{6}.name = 'DAQ28_Ellipse_AccY_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{6}.units = 'm/s2';
wr224e.PCM_DAQ_EllipseFilt.fields{6}.start_bit = 48;
wr224e.PCM_DAQ_EllipseFilt.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{6}.maximum = 2.540000e+01;
wr224e.PCM_DAQ_EllipseFilt.fields{6}.minimum = -2.560000e+01;
wr224e.PCM_DAQ_EllipseFilt.fields{6}.scale = 2.000000e-01;
wr224e.PCM_DAQ_EllipseFilt.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_EllipseFilt.fields{7}.name = 'DAQ28_Ellipse_AccX_filt';
wr224e.PCM_DAQ_EllipseFilt.fields{7}.units = 'm/s2';
wr224e.PCM_DAQ_EllipseFilt.fields{7}.start_bit = 56;
wr224e.PCM_DAQ_EllipseFilt.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_EllipseFilt.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_EllipseFilt.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ_EllipseFilt.fields{7}.maximum = 2.540000e+01;
wr224e.PCM_DAQ_EllipseFilt.fields{7}.minimum = -2.560000e+01;
wr224e.PCM_DAQ_EllipseFilt.fields{7}.scale = 2.000000e-01;
wr224e.PCM_DAQ_EllipseFilt.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_Faults'
wr224e.PCM_DAQ_Faults = struct;
wr224e.PCM_DAQ_Faults.name = 'PCM_DAQ_Faults';
wr224e.PCM_DAQ_Faults.description = 'PCM_DAQ_Faults';
wr224e.PCM_DAQ_Faults.protocol = 'wr224e';
wr224e.PCM_DAQ_Faults.id = hex2dec('00000301');
wr224e.PCM_DAQ_Faults.idext = 'STANDARD';
wr224e.PCM_DAQ_Faults.idinherit = 0;
wr224e.PCM_DAQ_Faults.payload_size = 8;
wr224e.PCM_DAQ_Faults.payload_value = [];
wr224e.PCM_DAQ_Faults.payload_mask = [];
wr224e.PCM_DAQ_Faults.interval = 50;

wr224e.PCM_DAQ_Faults.fields{1}.name = 'DAQ1_SDC_Low_cell_voltage_High';
wr224e.PCM_DAQ_Faults.fields{1}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{1}.start_bit = 15;
wr224e.PCM_DAQ_Faults.fields{1}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{2}.name = 'DAQ1_SDC_Low_cell_voltage_Low';
wr224e.PCM_DAQ_Faults.fields{2}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{2}.start_bit = 14;
wr224e.PCM_DAQ_Faults.fields{2}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{3}.name = 'DAQ1_RR_Is_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{3}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{3}.start_bit = 23;
wr224e.PCM_DAQ_Faults.fields{3}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{3}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{3}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{4}.name = 'DAQ1_FR_Vdcbus_Overvoltage';
wr224e.PCM_DAQ_Faults.fields{4}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{4}.start_bit = 22;
wr224e.PCM_DAQ_Faults.fields{4}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{4}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{4}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{5}.name = 'DAQ1_FL_Vdcbus_Overvoltage';
wr224e.PCM_DAQ_Faults.fields{5}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{5}.start_bit = 21;
wr224e.PCM_DAQ_Faults.fields{5}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{5}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{5}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{6}.name = 'DAQ1_RL_Vdcbus_Overvoltage';
wr224e.PCM_DAQ_Faults.fields{6}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{6}.start_bit = 20;
wr224e.PCM_DAQ_Faults.fields{6}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{6}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{6}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{7}.name = 'DAQ1_RR_Vdcbus_Overvoltage';
wr224e.PCM_DAQ_Faults.fields{7}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{7}.start_bit = 19;
wr224e.PCM_DAQ_Faults.fields{7}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{7}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{7}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{8}.name = 'DAQ1_DCDC_Voltage_Implausibility';
wr224e.PCM_DAQ_Faults.fields{8}.units = 'Flag';
wr224e.PCM_DAQ_Faults.fields{8}.start_bit = 18;
wr224e.PCM_DAQ_Faults.fields{8}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{8}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{8}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{8}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{9}.name = 'DAQ1_SDC_High_cell_voltage_High';
wr224e.PCM_DAQ_Faults.fields{9}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{9}.start_bit = 17;
wr224e.PCM_DAQ_Faults.fields{9}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{9}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{9}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{9}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{10}.name = 'DAQ1_SDC_High_cell_voltage_Low';
wr224e.PCM_DAQ_Faults.fields{10}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{10}.start_bit = 16;
wr224e.PCM_DAQ_Faults.fields{10}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{10}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{10}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{11}.name = 'DAQ1_RR_Phase_B_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{11}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{11}.start_bit = 31;
wr224e.PCM_DAQ_Faults.fields{11}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{11}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{11}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{11}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{11}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{12}.name = 'DAQ1_FR_Phase_A_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{12}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{12}.start_bit = 30;
wr224e.PCM_DAQ_Faults.fields{12}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{12}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{12}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{12}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{12}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{12}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{13}.name = 'DAQ1_FL_Phase_A_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{13}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{13}.start_bit = 29;
wr224e.PCM_DAQ_Faults.fields{13}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{13}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{13}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{13}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{13}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{13}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{14}.name = 'DAQ1_RL_Phase_A_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{14}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{14}.start_bit = 28;
wr224e.PCM_DAQ_Faults.fields{14}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{14}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{14}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{14}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{14}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{14}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{15}.name = 'DAQ1_RR_Phase_A_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{15}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{15}.start_bit = 27;
wr224e.PCM_DAQ_Faults.fields{15}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{15}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{15}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{15}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{15}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{15}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{16}.name = 'DAQ1_FR_Is_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{16}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{16}.start_bit = 26;
wr224e.PCM_DAQ_Faults.fields{16}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{16}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{16}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{16}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{16}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{16}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{17}.name = 'DAQ1_FL_Is_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{17}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{17}.start_bit = 25;
wr224e.PCM_DAQ_Faults.fields{17}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{17}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{17}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{17}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{17}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{17}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{18}.name = 'DAQ1_RL_Is_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{18}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{18}.start_bit = 24;
wr224e.PCM_DAQ_Faults.fields{18}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{18}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{18}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{18}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{18}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{18}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{19}.name = 'DAQ1_RR_RST_LOW_FAULT';
wr224e.PCM_DAQ_Faults.fields{19}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{19}.start_bit = 39;
wr224e.PCM_DAQ_Faults.fields{19}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{19}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{19}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{19}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{19}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{19}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{20}.name = 'DAQ1_FR_Phase_C_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{20}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{20}.start_bit = 38;
wr224e.PCM_DAQ_Faults.fields{20}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{20}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{20}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{20}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{20}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{20}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{20}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{21}.name = 'DAQ1_FL_Phase_C_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{21}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{21}.start_bit = 37;
wr224e.PCM_DAQ_Faults.fields{21}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{21}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{21}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{21}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{21}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{21}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{21}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{22}.name = 'DAQ1_RL_Phase_C_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{22}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{22}.start_bit = 36;
wr224e.PCM_DAQ_Faults.fields{22}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{22}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{22}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{22}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{22}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{22}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{22}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{23}.name = 'DAQ1_RR_Phase_C_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{23}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{23}.start_bit = 35;
wr224e.PCM_DAQ_Faults.fields{23}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{23}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{23}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{23}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{23}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{23}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{23}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{24}.name = 'DAQ1_FR_Phase_B_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{24}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{24}.start_bit = 34;
wr224e.PCM_DAQ_Faults.fields{24}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{24}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{24}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{24}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{24}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{24}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{24}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{25}.name = 'DAQ1_FL_Phase_B_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{25}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{25}.start_bit = 33;
wr224e.PCM_DAQ_Faults.fields{25}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{25}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{25}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{25}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{25}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{25}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{25}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{26}.name = 'DAQ1_RL_Phase_B_Overcurrent';
wr224e.PCM_DAQ_Faults.fields{26}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{26}.start_bit = 32;
wr224e.PCM_DAQ_Faults.fields{26}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{26}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{26}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{26}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{26}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{26}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{26}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{27}.name = 'DAQ1_RR_Motor_Overtemp';
wr224e.PCM_DAQ_Faults.fields{27}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{27}.start_bit = 47;
wr224e.PCM_DAQ_Faults.fields{27}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{27}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{27}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{27}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{27}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{27}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{27}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{28}.name = 'DAQ1_FL_IGBT_Overtemp';
wr224e.PCM_DAQ_Faults.fields{28}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{28}.start_bit = 45;
wr224e.PCM_DAQ_Faults.fields{28}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{28}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{28}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{28}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{28}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{28}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{28}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{29}.name = 'DAQ1_FR_IGBT_Overtemp';
wr224e.PCM_DAQ_Faults.fields{29}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{29}.start_bit = 46;
wr224e.PCM_DAQ_Faults.fields{29}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{29}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{29}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{29}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{29}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{29}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{29}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{30}.name = 'DAQ1_RL_IGBT_Overtemp';
wr224e.PCM_DAQ_Faults.fields{30}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{30}.start_bit = 44;
wr224e.PCM_DAQ_Faults.fields{30}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{30}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{30}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{30}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{30}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{30}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{30}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{31}.name = 'DAQ1_RR_IGBT_Overtemp';
wr224e.PCM_DAQ_Faults.fields{31}.units = 'FLAG';
wr224e.PCM_DAQ_Faults.fields{31}.start_bit = 43;
wr224e.PCM_DAQ_Faults.fields{31}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{31}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{31}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{31}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{31}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{31}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{31}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{32}.name = 'DAQ1_FR_RST_LOW_FAULT';
wr224e.PCM_DAQ_Faults.fields{32}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{32}.start_bit = 42;
wr224e.PCM_DAQ_Faults.fields{32}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{32}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{32}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{32}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{32}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{32}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{32}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{33}.name = 'DAQ1_FL_RST_LOW_FAULT';
wr224e.PCM_DAQ_Faults.fields{33}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{33}.start_bit = 41;
wr224e.PCM_DAQ_Faults.fields{33}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{33}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{33}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{33}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{33}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{33}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{33}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{34}.name = 'DAQ1_RL_RST_LOW_FAULT';
wr224e.PCM_DAQ_Faults.fields{34}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{34}.start_bit = 40;
wr224e.PCM_DAQ_Faults.fields{34}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{34}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{34}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{34}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{34}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{34}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{34}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{35}.name = 'DAQ1_BrakePressure_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{35}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{35}.start_bit = 55;
wr224e.PCM_DAQ_Faults.fields{35}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{35}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{35}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{35}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{35}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{36}.name = 'DAQ1_MotFR_TorqueControl_Fault';
wr224e.PCM_DAQ_Faults.fields{36}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{36}.start_bit = 54;
wr224e.PCM_DAQ_Faults.fields{36}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{36}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{36}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{36}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{36}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{37}.name = 'DAQ1_MotFL_TorqueControl_Fault';
wr224e.PCM_DAQ_Faults.fields{37}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{37}.start_bit = 53;
wr224e.PCM_DAQ_Faults.fields{37}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{37}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{37}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{37}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{37}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{38}.name = 'DAQ1_MotRL_TorqueControl_Fault';
wr224e.PCM_DAQ_Faults.fields{38}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{38}.start_bit = 52;
wr224e.PCM_DAQ_Faults.fields{38}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{38}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{38}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{38}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{38}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{39}.name = 'DAQ1_MotRR_TorqueControl_Fault';
wr224e.PCM_DAQ_Faults.fields{39}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{39}.start_bit = 51;
wr224e.PCM_DAQ_Faults.fields{39}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{39}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{39}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{39}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{39}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{40}.name = 'DAQ1_FR_Motor_Overtemp';
wr224e.PCM_DAQ_Faults.fields{40}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{40}.start_bit = 50;
wr224e.PCM_DAQ_Faults.fields{40}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{40}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{40}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{40}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{40}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{40}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{40}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{41}.name = 'DAQ1_FL_Motor_Overtemp';
wr224e.PCM_DAQ_Faults.fields{41}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{41}.start_bit = 49;
wr224e.PCM_DAQ_Faults.fields{41}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{41}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{41}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{41}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{41}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{41}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{41}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{42}.name = 'DAQ1_RL_Motor_Overtemp';
wr224e.PCM_DAQ_Faults.fields{42}.units = 'flag';
wr224e.PCM_DAQ_Faults.fields{42}.start_bit = 48;
wr224e.PCM_DAQ_Faults.fields{42}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{42}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{42}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{42}.maximum = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{42}.minimum = 0.000000e+00;
wr224e.PCM_DAQ_Faults.fields{42}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{42}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{43}.name = 'DAQ1_APPS_Sensor_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{43}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{43}.start_bit = 63;
wr224e.PCM_DAQ_Faults.fields{43}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{43}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{43}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{43}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{43}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{44}.name = 'DAQ1_APPS_Brake_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{44}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{44}.start_bit = 62;
wr224e.PCM_DAQ_Faults.fields{44}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{44}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{44}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{44}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{44}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{45}.name = 'DAQ1_SteerAng_Sensor_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{45}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{45}.start_bit = 61;
wr224e.PCM_DAQ_Faults.fields{45}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{45}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{45}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{45}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{45}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{46}.name = 'DAQ1_YawRate_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{46}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{46}.start_bit = 58;
wr224e.PCM_DAQ_Faults.fields{46}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{46}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{46}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{46}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{46}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{47}.name = 'DAQ1_YawAccel_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{47}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{47}.start_bit = 57;
wr224e.PCM_DAQ_Faults.fields{47}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{47}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{47}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{47}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{47}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{48}.name = 'DAQ1_VehSpd_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{48}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{48}.start_bit = 56;
wr224e.PCM_DAQ_Faults.fields{48}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{48}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{48}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{48}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{48}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{49}.name = 'DAQ1_AccelY_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{49}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{49}.start_bit = 59;
wr224e.PCM_DAQ_Faults.fields{49}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{49}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{49}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{49}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{49}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Faults.fields{50}.name = 'DAQ1_AccelX_Plaus_Fault';
wr224e.PCM_DAQ_Faults.fields{50}.units = 'boolean';
wr224e.PCM_DAQ_Faults.fields{50}.start_bit = 60;
wr224e.PCM_DAQ_Faults.fields{50}.bit_length = 1;
wr224e.PCM_DAQ_Faults.fields{50}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Faults.fields{50}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_Faults.fields{50}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Faults.fields{50}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_GyroFilt'
wr224e.PCM_DAQ_GyroFilt = struct;
wr224e.PCM_DAQ_GyroFilt.name = 'PCM_DAQ_GyroFilt';
wr224e.PCM_DAQ_GyroFilt.description = 'PCM_DAQ_GyroFilt';
wr224e.PCM_DAQ_GyroFilt.protocol = 'wr224e';
wr224e.PCM_DAQ_GyroFilt.id = hex2dec('00000329');
wr224e.PCM_DAQ_GyroFilt.idext = 'STANDARD';
wr224e.PCM_DAQ_GyroFilt.idinherit = 0;
wr224e.PCM_DAQ_GyroFilt.payload_size = 6;
wr224e.PCM_DAQ_GyroFilt.payload_value = [];
wr224e.PCM_DAQ_GyroFilt.payload_mask = [];
wr224e.PCM_DAQ_GyroFilt.interval = 50;

wr224e.PCM_DAQ_GyroFilt.fields{1}.name = 'DAQ29_Gyro_YawRate_filt';
wr224e.PCM_DAQ_GyroFilt.fields{1}.units = 'deg/s';
wr224e.PCM_DAQ_GyroFilt.fields{1}.start_bit = 32;
wr224e.PCM_DAQ_GyroFilt.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_GyroFilt.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_GyroFilt.fields{2}.name = 'DAQ29_Gyro_YawAccel_filt';
wr224e.PCM_DAQ_GyroFilt.fields{2}.units = 'deg/s2';
wr224e.PCM_DAQ_GyroFilt.fields{2}.start_bit = 16;
wr224e.PCM_DAQ_GyroFilt.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{2}.scale = 2.000000e+01;
wr224e.PCM_DAQ_GyroFilt.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_GyroFilt.fields{3}.name = 'DAQ29_Gyro_AccY_filt';
wr224e.PCM_DAQ_GyroFilt.fields{3}.units = 'm/s2';
wr224e.PCM_DAQ_GyroFilt.fields{3}.start_bit = 40;
wr224e.PCM_DAQ_GyroFilt.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{3}.maximum = 2.540000e+01;
wr224e.PCM_DAQ_GyroFilt.fields{3}.minimum = -2.560000e+01;
wr224e.PCM_DAQ_GyroFilt.fields{3}.scale = 2.000000e-01;
wr224e.PCM_DAQ_GyroFilt.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_GyroFilt.fields{4}.name = 'DAQ29_Gyro_AccX_filt';
wr224e.PCM_DAQ_GyroFilt.fields{4}.units = 'm/s2';
wr224e.PCM_DAQ_GyroFilt.fields{4}.start_bit = 24;
wr224e.PCM_DAQ_GyroFilt.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{4}.maximum = 2.540000e+01;
wr224e.PCM_DAQ_GyroFilt.fields{4}.minimum = -2.560000e+01;
wr224e.PCM_DAQ_GyroFilt.fields{4}.scale = 2.000000e-01;
wr224e.PCM_DAQ_GyroFilt.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_GyroFilt.fields{5}.name = 'DAQ29_PCMPower';
wr224e.PCM_DAQ_GyroFilt.fields{5}.units = 'V';
wr224e.PCM_DAQ_GyroFilt.fields{5}.start_bit = 56;
wr224e.PCM_DAQ_GyroFilt.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{5}.scale = 1.000000e-01;
wr224e.PCM_DAQ_GyroFilt.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_GyroFilt.fields{6}.name = 'DAQ29_DriverPower';
wr224e.PCM_DAQ_GyroFilt.fields{6}.units = 'V';
wr224e.PCM_DAQ_GyroFilt.fields{6}.start_bit = 48;
wr224e.PCM_DAQ_GyroFilt.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_GyroFilt.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_GyroFilt.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_GyroFilt.fields{6}.scale = 1.000000e-01;
wr224e.PCM_DAQ_GyroFilt.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_OptimizedTRQ'
wr224e.PCM_DAQ_OptimizedTRQ = struct;
wr224e.PCM_DAQ_OptimizedTRQ.name = 'PCM_DAQ_OptimizedTRQ';
wr224e.PCM_DAQ_OptimizedTRQ.description = 'PCM_DAQ_OptimizedTRQ';
wr224e.PCM_DAQ_OptimizedTRQ.protocol = 'wr224e';
wr224e.PCM_DAQ_OptimizedTRQ.id = hex2dec('00000311');
wr224e.PCM_DAQ_OptimizedTRQ.idext = 'STANDARD';
wr224e.PCM_DAQ_OptimizedTRQ.idinherit = 0;
wr224e.PCM_DAQ_OptimizedTRQ.payload_size = 8;
wr224e.PCM_DAQ_OptimizedTRQ.payload_value = [];
wr224e.PCM_DAQ_OptimizedTRQ.payload_mask = [];
wr224e.PCM_DAQ_OptimizedTRQ.interval = 50;

wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.name = 'DAQ11_OptimizedBased_Total';
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.start_bit = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.name = 'DAQ11_OptimizedBased_RearAxle';
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.start_bit = 16;
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.name = 'DAQ11_OptimizedBased_FrontAxle';
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.start_bit = 24;
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.name = 'DAQ11_OptimizedBased_F';
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.units = '%';
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.start_bit = 0;
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.scale = 5.000000e-01;
wr224e.PCM_DAQ_OptimizedTRQ.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.name = 'DAQ11_OptimalBased_Total';
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.start_bit = 40;
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.name = 'DAQ11_OptimalBased_RearAxle';
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.start_bit = 48;
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.name = 'DAQ11_OptimalBased_FrontAxle';
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.units = 'Nm';
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.start_bit = 56;
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.scale = 2.000000e+00;
wr224e.PCM_DAQ_OptimizedTRQ.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.name = 'DAQ11_OptimalBased_F';
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.units = '%';
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.start_bit = 32;
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.scale = 5.000000e-01;
wr224e.PCM_DAQ_OptimizedTRQ.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_Slip'
wr224e.PCM_DAQ_Slip = struct;
wr224e.PCM_DAQ_Slip.name = 'PCM_DAQ_Slip';
wr224e.PCM_DAQ_Slip.description = 'PCM_DAQ_Slip';
wr224e.PCM_DAQ_Slip.protocol = 'wr224e';
wr224e.PCM_DAQ_Slip.id = hex2dec('00000321');
wr224e.PCM_DAQ_Slip.idext = 'STANDARD';
wr224e.PCM_DAQ_Slip.idinherit = 0;
wr224e.PCM_DAQ_Slip.payload_size = 8;
wr224e.PCM_DAQ_Slip.payload_value = [];
wr224e.PCM_DAQ_Slip.payload_mask = [];
wr224e.PCM_DAQ_Slip.interval = 50;

wr224e.PCM_DAQ_Slip.fields{1}.name = 'DAQ21_TireAngleRR';
wr224e.PCM_DAQ_Slip.fields{1}.units = 'deg';
wr224e.PCM_DAQ_Slip.fields{1}.start_bit = 0;
wr224e.PCM_DAQ_Slip.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{1}.scale = 2.500000e-01;
wr224e.PCM_DAQ_Slip.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{2}.name = 'DAQ21_TireAngleRL';
wr224e.PCM_DAQ_Slip.fields{2}.units = 'deg';
wr224e.PCM_DAQ_Slip.fields{2}.start_bit = 8;
wr224e.PCM_DAQ_Slip.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{2}.scale = 2.500000e-01;
wr224e.PCM_DAQ_Slip.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{3}.name = 'DAQ21_TireAngleFR';
wr224e.PCM_DAQ_Slip.fields{3}.units = 'deg';
wr224e.PCM_DAQ_Slip.fields{3}.start_bit = 16;
wr224e.PCM_DAQ_Slip.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{3}.scale = 2.500000e-01;
wr224e.PCM_DAQ_Slip.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{4}.name = 'DAQ21_TireAngleFL';
wr224e.PCM_DAQ_Slip.fields{4}.units = 'deg';
wr224e.PCM_DAQ_Slip.fields{4}.start_bit = 24;
wr224e.PCM_DAQ_Slip.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{4}.scale = 2.500000e-01;
wr224e.PCM_DAQ_Slip.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{5}.name = 'DAQ21_SlipRR';
wr224e.PCM_DAQ_Slip.fields{5}.units = '%';
wr224e.PCM_DAQ_Slip.fields{5}.start_bit = 32;
wr224e.PCM_DAQ_Slip.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Slip.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{6}.name = 'DAQ21_SlipRL';
wr224e.PCM_DAQ_Slip.fields{6}.units = '%';
wr224e.PCM_DAQ_Slip.fields{6}.start_bit = 40;
wr224e.PCM_DAQ_Slip.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Slip.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{7}.name = 'DAQ21_SlipFR';
wr224e.PCM_DAQ_Slip.fields{7}.units = '%';
wr224e.PCM_DAQ_Slip.fields{7}.start_bit = 48;
wr224e.PCM_DAQ_Slip.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Slip.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_Slip.fields{8}.name = 'DAQ21_SlipFL';
wr224e.PCM_DAQ_Slip.fields{8}.units = '%';
wr224e.PCM_DAQ_Slip.fields{8}.start_bit = 56;
wr224e.PCM_DAQ_Slip.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_Slip.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_Slip.fields{8}.data_type = 'SIGNED';
wr224e.PCM_DAQ_Slip.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ_Slip.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_TCPID'
wr224e.PCM_DAQ_TCPID = struct;
wr224e.PCM_DAQ_TCPID.name = 'PCM_DAQ_TCPID';
wr224e.PCM_DAQ_TCPID.description = 'PCM_DAQ_TCPID';
wr224e.PCM_DAQ_TCPID.protocol = 'wr224e';
wr224e.PCM_DAQ_TCPID.id = hex2dec('00000306');
wr224e.PCM_DAQ_TCPID.idext = 'STANDARD';
wr224e.PCM_DAQ_TCPID.idinherit = 0;
wr224e.PCM_DAQ_TCPID.payload_size = 8;
wr224e.PCM_DAQ_TCPID.payload_value = [];
wr224e.PCM_DAQ_TCPID.payload_mask = [];
wr224e.PCM_DAQ_TCPID.interval = 50;

wr224e.PCM_DAQ_TCPID.fields{1}.name = 'DAQ6_TireMuRearAdapting';
wr224e.PCM_DAQ_TCPID.fields{1}.units = 'boolean';
wr224e.PCM_DAQ_TCPID.fields{1}.start_bit = 11;
wr224e.PCM_DAQ_TCPID.fields{1}.bit_length = 1;
wr224e.PCM_DAQ_TCPID.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{2}.name = 'DAQ6_TireMuFrontAdapting';
wr224e.PCM_DAQ_TCPID.fields{2}.units = 'boolean';
wr224e.PCM_DAQ_TCPID.fields{2}.start_bit = 12;
wr224e.PCM_DAQ_TCPID.fields{2}.bit_length = 1;
wr224e.PCM_DAQ_TCPID.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{3}.name = 'DAQ6_TCSlipFeedbackEnabled';
wr224e.PCM_DAQ_TCPID.fields{3}.units = 'boolean';
wr224e.PCM_DAQ_TCPID.fields{3}.start_bit = 15;
wr224e.PCM_DAQ_TCPID.fields{3}.bit_length = 1;
wr224e.PCM_DAQ_TCPID.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{4}.name = 'DAQ6_TCFeedbackRActive';
wr224e.PCM_DAQ_TCPID.fields{4}.units = 'boolean';
wr224e.PCM_DAQ_TCPID.fields{4}.start_bit = 13;
wr224e.PCM_DAQ_TCPID.fields{4}.bit_length = 1;
wr224e.PCM_DAQ_TCPID.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{5}.name = 'DAQ6_TCFeedbackFActive';
wr224e.PCM_DAQ_TCPID.fields{5}.units = 'boolean';
wr224e.PCM_DAQ_TCPID.fields{5}.start_bit = 14;
wr224e.PCM_DAQ_TCPID.fields{5}.bit_length = 1;
wr224e.PCM_DAQ_TCPID.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{6}.name = 'DAQ6_PIRear_Out';
wr224e.PCM_DAQ_TCPID.fields{6}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{6}.start_bit = 24;
wr224e.PCM_DAQ_TCPID.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{6}.scale = 5.000000e-01;
wr224e.PCM_DAQ_TCPID.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{7}.name = 'DAQ6_PIRear_Error';
wr224e.PCM_DAQ_TCPID.fields{7}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{7}.start_bit = 16;
wr224e.PCM_DAQ_TCPID.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ_TCPID.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{8}.name = 'DAQ6_PIFront_Out';
wr224e.PCM_DAQ_TCPID.fields{8}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{8}.start_bit = 40;
wr224e.PCM_DAQ_TCPID.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{8}.scale = 5.000000e-01;
wr224e.PCM_DAQ_TCPID.fields{8}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{9}.name = 'DAQ6_PIFront_Error';
wr224e.PCM_DAQ_TCPID.fields{9}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{9}.start_bit = 32;
wr224e.PCM_DAQ_TCPID.fields{9}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{9}.data_type = 'SIGNED';
wr224e.PCM_DAQ_TCPID.fields{9}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TCPID.fields{9}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{10}.name = 'DAQ6_LimitPercentR_TC';
wr224e.PCM_DAQ_TCPID.fields{10}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{10}.start_bit = 48;
wr224e.PCM_DAQ_TCPID.fields{10}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{10}.scale = 5.000000e-01;
wr224e.PCM_DAQ_TCPID.fields{10}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TCPID.fields{11}.name = 'DAQ6_LimitPercentF_TC';
wr224e.PCM_DAQ_TCPID.fields{11}.units = '%';
wr224e.PCM_DAQ_TCPID.fields{11}.start_bit = 56;
wr224e.PCM_DAQ_TCPID.fields{11}.bit_length = 8;
wr224e.PCM_DAQ_TCPID.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TCPID.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TCPID.fields{11}.scale = 5.000000e-01;
wr224e.PCM_DAQ_TCPID.fields{11}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_TRQ_Limits'
wr224e.PCM_DAQ_TRQ_Limits = struct;
wr224e.PCM_DAQ_TRQ_Limits.name = 'PCM_DAQ_TRQ_Limits';
wr224e.PCM_DAQ_TRQ_Limits.description = 'PCM_DAQ_TRQ_Limits';
wr224e.PCM_DAQ_TRQ_Limits.protocol = 'wr224e';
wr224e.PCM_DAQ_TRQ_Limits.id = hex2dec('00000312');
wr224e.PCM_DAQ_TRQ_Limits.idext = 'STANDARD';
wr224e.PCM_DAQ_TRQ_Limits.idinherit = 0;
wr224e.PCM_DAQ_TRQ_Limits.payload_size = 8;
wr224e.PCM_DAQ_TRQ_Limits.payload_value = [];
wr224e.PCM_DAQ_TRQ_Limits.payload_mask = [];
wr224e.PCM_DAQ_TRQ_Limits.interval = 50;

wr224e.PCM_DAQ_TRQ_Limits.fields{1}.name = 'DAQ12_TractionLimitedFL';
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.start_bit = 15;
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{2}.name = 'DAQ12_TractionLimitedFR';
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.start_bit = 14;
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{3}.name = 'DAQ12_TractionLimitedRL';
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.start_bit = 13;
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{4}.name = 'DAQ12_TractionLimitedRR';
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.start_bit = 12;
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{5}.name = 'DAQ12_Thermal_Limited_RL';
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.start_bit = 23;
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{6}.name = 'DAQ12_Traction_Limited_RL';
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.start_bit = 22;
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{7}.name = 'DAQ12_Motor_Disable_Limited_RL';
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.start_bit = 21;
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{8}.name = 'DAQ12_GlobalTrqCal_Limited_RL';
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.start_bit = 20;
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{8}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{9}.name = 'DAQ12_Thermal_Limited_RR';
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.start_bit = 19;
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{9}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{10}.name = 'DAQ12_Traction_Limited_RR';
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.start_bit = 18;
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{10}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{11}.name = 'DAQ12_Motor_Disable_Limited_RR';
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.start_bit = 17;
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{11}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{12}.name = 'DAQ12_GlobalTrqCal_Limited_RR';
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.start_bit = 16;
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{12}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{13}.name = 'DAQ12_Thermal_Limited_FL';
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.start_bit = 31;
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{13}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{14}.name = 'DAQ12_Traction_Limited_FL';
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.start_bit = 30;
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{14}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{15}.name = 'DAQ12_Motor_Disable_Limited_FL';
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.start_bit = 29;
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{15}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{16}.name = 'DAQ12_GlobalTrqCal_Limited_FL';
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.start_bit = 28;
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{16}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{17}.name = 'DAQ12_Thermal_Limited_FR';
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.start_bit = 27;
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{17}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{18}.name = 'DAQ12_Traction_Limited_FR';
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.start_bit = 26;
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{18}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{19}.name = 'DAQ12_Motor_Disable_Limited_FR';
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.start_bit = 25;
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{19}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{20}.name = 'DAQ12_GlobalTrqCal_Limited_FR';
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.units = 'boolean';
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.start_bit = 24;
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.bit_length = 1;
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{20}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{21}.name = 'DAQ12_WhlTrq_Limit_RR';
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.units = 'Nm';
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.start_bit = 32;
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.bit_length = 8;
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{21}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{22}.name = 'DAQ12_WhlTrq_Limit_RL';
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.units = 'Nm';
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.start_bit = 40;
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.bit_length = 8;
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{22}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{23}.name = 'DAQ12_WhlTrq_Limit_FR';
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.units = 'Nm';
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.start_bit = 48;
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.bit_length = 8;
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{23}.offset = 0.000000e+00;

wr224e.PCM_DAQ_TRQ_Limits.fields{24}.name = 'DAQ12_WhlTrq_Limit_FL';
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.units = 'Nm';
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.start_bit = 56;
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.bit_length = 8;
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.scale = 1.000000e+00;
wr224e.PCM_DAQ_TRQ_Limits.fields{24}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_VelSpeed'
wr224e.PCM_DAQ_VelSpeed = struct;
wr224e.PCM_DAQ_VelSpeed.name = 'PCM_DAQ_VelSpeed';
wr224e.PCM_DAQ_VelSpeed.description = 'PCM_DAQ_VelSpeed';
wr224e.PCM_DAQ_VelSpeed.protocol = 'wr224e';
wr224e.PCM_DAQ_VelSpeed.id = hex2dec('00000323');
wr224e.PCM_DAQ_VelSpeed.idext = 'STANDARD';
wr224e.PCM_DAQ_VelSpeed.idinherit = 0;
wr224e.PCM_DAQ_VelSpeed.payload_size = 8;
wr224e.PCM_DAQ_VelSpeed.payload_value = [];
wr224e.PCM_DAQ_VelSpeed.payload_mask = [];
wr224e.PCM_DAQ_VelSpeed.interval = 50;

wr224e.PCM_DAQ_VelSpeed.fields{1}.name = 'DAQ23_YawAccelFromEllipse';
wr224e.PCM_DAQ_VelSpeed.fields{1}.units = 'deg/s2';
wr224e.PCM_DAQ_VelSpeed.fields{1}.start_bit = 48;
wr224e.PCM_DAQ_VelSpeed.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{1}.scale = 2.000000e+01;
wr224e.PCM_DAQ_VelSpeed.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{2}.name = 'DAQ23_VelocitySource';
wr224e.PCM_DAQ_VelSpeed.fields{2}.units = 'boolean';
wr224e.PCM_DAQ_VelSpeed.fields{2}.start_bit = 7;
wr224e.PCM_DAQ_VelSpeed.fields{2}.bit_length = 1;
wr224e.PCM_DAQ_VelSpeed.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{2}.scale = 1.000000e+00;
wr224e.PCM_DAQ_VelSpeed.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{3}.name = 'DAQ23_VehSpdRR';
wr224e.PCM_DAQ_VelSpeed.fields{3}.units = 'mph';
wr224e.PCM_DAQ_VelSpeed.fields{3}.start_bit = 8;
wr224e.PCM_DAQ_VelSpeed.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{3}.scale = 2.500000e-01;
wr224e.PCM_DAQ_VelSpeed.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{4}.name = 'DAQ23_VehSpdRL';
wr224e.PCM_DAQ_VelSpeed.fields{4}.units = 'mph';
wr224e.PCM_DAQ_VelSpeed.fields{4}.start_bit = 16;
wr224e.PCM_DAQ_VelSpeed.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{4}.scale = 2.500000e-01;
wr224e.PCM_DAQ_VelSpeed.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{5}.name = 'DAQ23_VehSpdFR';
wr224e.PCM_DAQ_VelSpeed.fields{5}.units = 'mph';
wr224e.PCM_DAQ_VelSpeed.fields{5}.start_bit = 24;
wr224e.PCM_DAQ_VelSpeed.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{5}.scale = 2.500000e-01;
wr224e.PCM_DAQ_VelSpeed.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{6}.name = 'DAQ23_VehSpdFL';
wr224e.PCM_DAQ_VelSpeed.fields{6}.units = 'mph';
wr224e.PCM_DAQ_VelSpeed.fields{6}.start_bit = 32;
wr224e.PCM_DAQ_VelSpeed.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{6}.scale = 2.500000e-01;
wr224e.PCM_DAQ_VelSpeed.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{7}.name = 'DAQ23_VehicleYawFilt';
wr224e.PCM_DAQ_VelSpeed.fields{7}.units = 'deg/s';
wr224e.PCM_DAQ_VelSpeed.fields{7}.start_bit = 56;
wr224e.PCM_DAQ_VelSpeed.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{7}.scale = 1.000000e+00;
wr224e.PCM_DAQ_VelSpeed.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_VelSpeed.fields{8}.name = 'DAQ23_VehicleSpeed';
wr224e.PCM_DAQ_VelSpeed.fields{8}.units = 'mph';
wr224e.PCM_DAQ_VelSpeed.fields{8}.start_bit = 40;
wr224e.PCM_DAQ_VelSpeed.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_VelSpeed.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_VelSpeed.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_VelSpeed.fields{8}.scale = 2.500000e-01;
wr224e.PCM_DAQ_VelSpeed.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_WHLTRQ'
wr224e.PCM_DAQ_WHLTRQ = struct;
wr224e.PCM_DAQ_WHLTRQ.name = 'PCM_DAQ_WHLTRQ';
wr224e.PCM_DAQ_WHLTRQ.description = 'PCM_DAQ_WHLTRQ';
wr224e.PCM_DAQ_WHLTRQ.protocol = 'wr224e';
wr224e.PCM_DAQ_WHLTRQ.id = hex2dec('00000330');
wr224e.PCM_DAQ_WHLTRQ.idext = 'STANDARD';
wr224e.PCM_DAQ_WHLTRQ.idinherit = 0;
wr224e.PCM_DAQ_WHLTRQ.payload_size = 8;
wr224e.PCM_DAQ_WHLTRQ.payload_value = [];
wr224e.PCM_DAQ_WHLTRQ.payload_mask = [];
wr224e.PCM_DAQ_WHLTRQ.interval = 50;

wr224e.PCM_DAQ_WHLTRQ.fields{1}.name = 'DAQ30_WhlTorqueRR';
wr224e.PCM_DAQ_WHLTRQ.fields{1}.units = 'Nm';
wr224e.PCM_DAQ_WHLTRQ.fields{1}.start_bit = 8;
wr224e.PCM_DAQ_WHLTRQ.fields{1}.bit_length = 16;
wr224e.PCM_DAQ_WHLTRQ.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WHLTRQ.fields{1}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WHLTRQ.fields{1}.maximum = 6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{1}.minimum = -6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{1}.scale = 2.000000e-02;
wr224e.PCM_DAQ_WHLTRQ.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WHLTRQ.fields{2}.name = 'DAQ30_WhlTorqueRL';
wr224e.PCM_DAQ_WHLTRQ.fields{2}.units = 'Nm';
wr224e.PCM_DAQ_WHLTRQ.fields{2}.start_bit = 24;
wr224e.PCM_DAQ_WHLTRQ.fields{2}.bit_length = 16;
wr224e.PCM_DAQ_WHLTRQ.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WHLTRQ.fields{2}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WHLTRQ.fields{2}.maximum = 6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{2}.minimum = -6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{2}.scale = 2.000000e-02;
wr224e.PCM_DAQ_WHLTRQ.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WHLTRQ.fields{3}.name = 'DAQ30_WhlTorqueFR';
wr224e.PCM_DAQ_WHLTRQ.fields{3}.units = 'Nm';
wr224e.PCM_DAQ_WHLTRQ.fields{3}.start_bit = 56;
wr224e.PCM_DAQ_WHLTRQ.fields{3}.bit_length = 16;
wr224e.PCM_DAQ_WHLTRQ.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WHLTRQ.fields{3}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WHLTRQ.fields{3}.maximum = 6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{3}.minimum = -6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{3}.scale = 2.000000e-02;
wr224e.PCM_DAQ_WHLTRQ.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WHLTRQ.fields{4}.name = 'DAQ30_WhlTorqueFL';
wr224e.PCM_DAQ_WHLTRQ.fields{4}.units = 'Nm';
wr224e.PCM_DAQ_WHLTRQ.fields{4}.start_bit = 40;
wr224e.PCM_DAQ_WHLTRQ.fields{4}.bit_length = 16;
wr224e.PCM_DAQ_WHLTRQ.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WHLTRQ.fields{4}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WHLTRQ.fields{4}.maximum = 6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{4}.minimum = -6.553400e+02;
wr224e.PCM_DAQ_WHLTRQ.fields{4}.scale = 2.000000e-02;
wr224e.PCM_DAQ_WHLTRQ.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_DAQ_WheelSpeed'
wr224e.PCM_DAQ_WheelSpeed = struct;
wr224e.PCM_DAQ_WheelSpeed.name = 'PCM_DAQ_WheelSpeed';
wr224e.PCM_DAQ_WheelSpeed.description = 'PCM_DAQ_WheelSpeed';
wr224e.PCM_DAQ_WheelSpeed.protocol = 'wr224e';
wr224e.PCM_DAQ_WheelSpeed.id = hex2dec('00000324');
wr224e.PCM_DAQ_WheelSpeed.idext = 'STANDARD';
wr224e.PCM_DAQ_WheelSpeed.idinherit = 0;
wr224e.PCM_DAQ_WheelSpeed.payload_size = 8;
wr224e.PCM_DAQ_WheelSpeed.payload_value = [];
wr224e.PCM_DAQ_WheelSpeed.payload_mask = [];
wr224e.PCM_DAQ_WheelSpeed.interval = 50;

wr224e.PCM_DAQ_WheelSpeed.fields{1}.name = 'DAQ24_WhlSpdRR';
wr224e.PCM_DAQ_WheelSpeed.fields{1}.units = 'mph';
wr224e.PCM_DAQ_WheelSpeed.fields{1}.start_bit = 0;
wr224e.PCM_DAQ_WheelSpeed.fields{1}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{1}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{1}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{2}.name = 'DAQ24_WhlSpdRL';
wr224e.PCM_DAQ_WheelSpeed.fields{2}.units = 'mph';
wr224e.PCM_DAQ_WheelSpeed.fields{2}.start_bit = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{2}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{2}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{2}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{3}.name = 'DAQ24_WhlSpdFR';
wr224e.PCM_DAQ_WheelSpeed.fields{3}.units = 'mph';
wr224e.PCM_DAQ_WheelSpeed.fields{3}.start_bit = 16;
wr224e.PCM_DAQ_WheelSpeed.fields{3}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{3}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{3}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{4}.name = 'DAQ24_WhlSpdFL';
wr224e.PCM_DAQ_WheelSpeed.fields{4}.units = 'mph';
wr224e.PCM_DAQ_WheelSpeed.fields{4}.start_bit = 24;
wr224e.PCM_DAQ_WheelSpeed.fields{4}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{4}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{4}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{5}.name = 'DAQ24_Steering_Sensor_Pos2_filt';
wr224e.PCM_DAQ_WheelSpeed.fields{5}.units = 'deg';
wr224e.PCM_DAQ_WheelSpeed.fields{5}.start_bit = 48;
wr224e.PCM_DAQ_WheelSpeed.fields{5}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{5}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{5}.scale = 1.000000e+00;
wr224e.PCM_DAQ_WheelSpeed.fields{5}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{6}.name = 'DAQ24_Steering_Sensor_Pos1_filt';
wr224e.PCM_DAQ_WheelSpeed.fields{6}.units = 'deg';
wr224e.PCM_DAQ_WheelSpeed.fields{6}.start_bit = 56;
wr224e.PCM_DAQ_WheelSpeed.fields{6}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{6}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{6}.scale = 1.000000e+00;
wr224e.PCM_DAQ_WheelSpeed.fields{6}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{7}.name = 'DAQ24_FRWhlAng_Steering';
wr224e.PCM_DAQ_WheelSpeed.fields{7}.units = 'deg';
wr224e.PCM_DAQ_WheelSpeed.fields{7}.start_bit = 32;
wr224e.PCM_DAQ_WheelSpeed.fields{7}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{7}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{7}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{7}.offset = 0.000000e+00;

wr224e.PCM_DAQ_WheelSpeed.fields{8}.name = 'DAQ24_FLWhlAng_Steering';
wr224e.PCM_DAQ_WheelSpeed.fields{8}.units = 'deg';
wr224e.PCM_DAQ_WheelSpeed.fields{8}.start_bit = 40;
wr224e.PCM_DAQ_WheelSpeed.fields{8}.bit_length = 8;
wr224e.PCM_DAQ_WheelSpeed.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_DAQ_WheelSpeed.fields{8}.data_type = 'SIGNED';
wr224e.PCM_DAQ_WheelSpeed.fields{8}.scale = 2.500000e-01;
wr224e.PCM_DAQ_WheelSpeed.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_Driver_Inputs'
wr224e.PCM_Driver_Inputs = struct;
wr224e.PCM_Driver_Inputs.name = 'PCM_Driver_Inputs';
wr224e.PCM_Driver_Inputs.description = 'PCM_Driver_Inputs';
wr224e.PCM_Driver_Inputs.protocol = 'wr224e';
wr224e.PCM_Driver_Inputs.id = hex2dec('000000E1');
wr224e.PCM_Driver_Inputs.idext = 'STANDARD';
wr224e.PCM_Driver_Inputs.idinherit = 0;
wr224e.PCM_Driver_Inputs.payload_size = 4;
wr224e.PCM_Driver_Inputs.payload_value = [];
wr224e.PCM_Driver_Inputs.payload_mask = [];
wr224e.PCM_Driver_Inputs.interval = 50;

wr224e.PCM_Driver_Inputs.fields{1}.name = 'PCM_Brake';
wr224e.PCM_Driver_Inputs.fields{1}.units = '';
wr224e.PCM_Driver_Inputs.fields{1}.start_bit = 40;
wr224e.PCM_Driver_Inputs.fields{1}.bit_length = 16;
wr224e.PCM_Driver_Inputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_Driver_Inputs.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_Driver_Inputs.fields{1}.scale = 1.000000e+00;
wr224e.PCM_Driver_Inputs.fields{1}.offset = 0.000000e+00;

wr224e.PCM_Driver_Inputs.fields{2}.name = 'PCM_Throttle';
wr224e.PCM_Driver_Inputs.fields{2}.units = '';
wr224e.PCM_Driver_Inputs.fields{2}.start_bit = 56;
wr224e.PCM_Driver_Inputs.fields{2}.bit_length = 16;
wr224e.PCM_Driver_Inputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_Driver_Inputs.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_Driver_Inputs.fields{2}.scale = 1.000000e+00;
wr224e.PCM_Driver_Inputs.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'PCM_FAULT_MSG'
wr224e.PCM_FAULT_MSG = struct;
wr224e.PCM_FAULT_MSG.name = 'PCM_FAULT_MSG';
wr224e.PCM_FAULT_MSG.description = 'PCM_FAULT_MSG';
wr224e.PCM_FAULT_MSG.protocol = 'wr224e';
wr224e.PCM_FAULT_MSG.id = hex2dec('000000F4');
wr224e.PCM_FAULT_MSG.idext = 'STANDARD';
wr224e.PCM_FAULT_MSG.idinherit = 0;
wr224e.PCM_FAULT_MSG.payload_size = 1;
wr224e.PCM_FAULT_MSG.payload_value = [];
wr224e.PCM_FAULT_MSG.payload_mask = [];
wr224e.PCM_FAULT_MSG.interval = 50;

wr224e.PCM_FAULT_MSG.fields{1}.name = 'PCM_STR_WHL_INDICATOR';
wr224e.PCM_FAULT_MSG.fields{1}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{1}.start_bit = 56;
wr224e.PCM_FAULT_MSG.fields{1}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{1}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{1}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{1}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{1}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{2}.name = 'PCM_INVERTER_FAULT';
wr224e.PCM_FAULT_MSG.fields{2}.units = 'FLAG';
wr224e.PCM_FAULT_MSG.fields{2}.start_bit = 57;
wr224e.PCM_FAULT_MSG.fields{2}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{2}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{2}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{2}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{2}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{3}.name = 'PCM_BMS_OPENED_SDC';
wr224e.PCM_FAULT_MSG.fields{3}.units = 'flag';
wr224e.PCM_FAULT_MSG.fields{3}.start_bit = 58;
wr224e.PCM_FAULT_MSG.fields{3}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{3}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{3}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{3}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{3}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{4}.name = 'DAQ1_DCDC_Voltage_Implausibility';
wr224e.PCM_FAULT_MSG.fields{4}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{4}.start_bit = 59;
wr224e.PCM_FAULT_MSG.fields{4}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{4}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{4}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{4}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{4}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{5}.name = 'PCM_AUX_AIR_COIL';
wr224e.PCM_FAULT_MSG.fields{5}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{5}.start_bit = 60;
wr224e.PCM_FAULT_MSG.fields{5}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{5}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{5}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{6}.name = 'PCM_Steering_Sensor_Implausibili';
wr224e.PCM_FAULT_MSG.fields{6}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{6}.start_bit = 61;
wr224e.PCM_FAULT_MSG.fields{6}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{6}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{6}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{6}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{6}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{7}.name = 'PCM_BSE_Implausibility';
wr224e.PCM_FAULT_MSG.fields{7}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{7}.start_bit = 62;
wr224e.PCM_FAULT_MSG.fields{7}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{7}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{7}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{7}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{7}.offset = 0.000000e+00;

wr224e.PCM_FAULT_MSG.fields{8}.name = 'PCM_APPS_Implausibility';
wr224e.PCM_FAULT_MSG.fields{8}.units = 'Flag';
wr224e.PCM_FAULT_MSG.fields{8}.start_bit = 63;
wr224e.PCM_FAULT_MSG.fields{8}.bit_length = 1;
wr224e.PCM_FAULT_MSG.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_FAULT_MSG.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_FAULT_MSG.fields{8}.maximum = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{8}.minimum = 0.000000e+00;
wr224e.PCM_FAULT_MSG.fields{8}.scale = 1.000000e+00;
wr224e.PCM_FAULT_MSG.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_MotorCriticalFaults'
wr224e.PCM_MotorCriticalFaults = struct;
wr224e.PCM_MotorCriticalFaults.name = 'PCM_MotorCriticalFaults';
wr224e.PCM_MotorCriticalFaults.description = 'PCM_MotorCriticalFaults';
wr224e.PCM_MotorCriticalFaults.protocol = 'wr224e';
wr224e.PCM_MotorCriticalFaults.id = hex2dec('000000F1');
wr224e.PCM_MotorCriticalFaults.idext = 'STANDARD';
wr224e.PCM_MotorCriticalFaults.idinherit = 0;
wr224e.PCM_MotorCriticalFaults.payload_size = 3;
wr224e.PCM_MotorCriticalFaults.payload_value = [];
wr224e.PCM_MotorCriticalFaults.payload_mask = [];
wr224e.PCM_MotorCriticalFaults.interval = 50;

wr224e.PCM_MotorCriticalFaults.fields{1}.name = 'PCM_RR_Motor_CAN_Timeout';
wr224e.PCM_MotorCriticalFaults.fields{1}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{1}.start_bit = 40;
wr224e.PCM_MotorCriticalFaults.fields{1}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{1}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{1}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{1}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{1}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{2}.name = 'PCM_RL_Motor_CAN_Timeout';
wr224e.PCM_MotorCriticalFaults.fields{2}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{2}.start_bit = 41;
wr224e.PCM_MotorCriticalFaults.fields{2}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{2}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{2}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{2}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{2}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{3}.name = 'PCM_FL_Motor_CAN_Timeout';
wr224e.PCM_MotorCriticalFaults.fields{3}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{3}.start_bit = 42;
wr224e.PCM_MotorCriticalFaults.fields{3}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{3}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{3}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{3}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{3}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{4}.name = 'PCM_FR_Motor_CAN_Timeout';
wr224e.PCM_MotorCriticalFaults.fields{4}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{4}.start_bit = 43;
wr224e.PCM_MotorCriticalFaults.fields{4}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{4}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{4}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{4}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{4}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{5}.name = 'PCM_RR_IGBT_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{5}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{5}.start_bit = 48;
wr224e.PCM_MotorCriticalFaults.fields{5}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{5}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{5}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{5}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{5}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{6}.name = 'PCM_RL_IGBT_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{6}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{6}.start_bit = 49;
wr224e.PCM_MotorCriticalFaults.fields{6}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{6}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{6}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{6}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{6}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{7}.name = 'PCM_FL_IGBT_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{7}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{7}.start_bit = 50;
wr224e.PCM_MotorCriticalFaults.fields{7}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{7}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{7}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{7}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{7}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{8}.name = 'PCM_FR_IGBT_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{8}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{8}.start_bit = 51;
wr224e.PCM_MotorCriticalFaults.fields{8}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{8}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{8}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{8}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{8}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{9}.name = 'PCM_RR_Motor_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{9}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{9}.start_bit = 52;
wr224e.PCM_MotorCriticalFaults.fields{9}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{9}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{9}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{9}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{9}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{9}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{10}.name = 'PCM_RL_Motor_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{10}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{10}.start_bit = 53;
wr224e.PCM_MotorCriticalFaults.fields{10}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{10}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{10}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{10}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{10}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{11}.name = 'PCM_FL_Motor_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{11}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{11}.start_bit = 54;
wr224e.PCM_MotorCriticalFaults.fields{11}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{11}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{11}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{11}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{11}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{12}.name = 'PCM_FR_Motor_High_Temp';
wr224e.PCM_MotorCriticalFaults.fields{12}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{12}.start_bit = 55;
wr224e.PCM_MotorCriticalFaults.fields{12}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{12}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{12}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{12}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{12}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{12}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{13}.name = 'PCM_RR_IGBT_Low_Implausible_Temp';
wr224e.PCM_MotorCriticalFaults.fields{13}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{13}.start_bit = 56;
wr224e.PCM_MotorCriticalFaults.fields{13}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{13}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{13}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{13}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{13}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{13}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{14}.name = 'PCM_RL_IGBT_Low_Implausible_Temp';
wr224e.PCM_MotorCriticalFaults.fields{14}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{14}.start_bit = 57;
wr224e.PCM_MotorCriticalFaults.fields{14}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{14}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{14}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{14}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{14}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{14}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{15}.name = 'PCM_FL_IGBT_Low_Implausible_Temp';
wr224e.PCM_MotorCriticalFaults.fields{15}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{15}.start_bit = 58;
wr224e.PCM_MotorCriticalFaults.fields{15}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{15}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{15}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{15}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{15}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{15}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{16}.name = 'PCM_FR_IGBT_Low_Implausible_Temp';
wr224e.PCM_MotorCriticalFaults.fields{16}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{16}.start_bit = 59;
wr224e.PCM_MotorCriticalFaults.fields{16}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{16}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{16}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{16}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{16}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{16}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{17}.name = 'PCM_RR_Motor_Low_Implausible_Tem';
wr224e.PCM_MotorCriticalFaults.fields{17}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{17}.start_bit = 60;
wr224e.PCM_MotorCriticalFaults.fields{17}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{17}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{17}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{17}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{17}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{17}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{18}.name = 'PCM_RL_Motor_Low_Implausible_Tem';
wr224e.PCM_MotorCriticalFaults.fields{18}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{18}.start_bit = 61;
wr224e.PCM_MotorCriticalFaults.fields{18}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{18}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{18}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{18}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{18}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{18}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{19}.name = 'PCM_FR_Motor_Low_Implausible_Tem';
wr224e.PCM_MotorCriticalFaults.fields{19}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{19}.start_bit = 63;
wr224e.PCM_MotorCriticalFaults.fields{19}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{19}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{19}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{19}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{19}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{19}.offset = 0.000000e+00;

wr224e.PCM_MotorCriticalFaults.fields{20}.name = 'PCM_FL_Motor_Low_Implausible_Tem';
wr224e.PCM_MotorCriticalFaults.fields{20}.units = 'Flag';
wr224e.PCM_MotorCriticalFaults.fields{20}.start_bit = 62;
wr224e.PCM_MotorCriticalFaults.fields{20}.bit_length = 1;
wr224e.PCM_MotorCriticalFaults.fields{20}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_MotorCriticalFaults.fields{20}.data_type = 'UNSIGNED';
wr224e.PCM_MotorCriticalFaults.fields{20}.maximum = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{20}.minimum = 0.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{20}.scale = 1.000000e+00;
wr224e.PCM_MotorCriticalFaults.fields{20}.offset = 0.000000e+00;

%===================================================================
case 'PCM_OtherCriticalFaults'
wr224e.PCM_OtherCriticalFaults = struct;
wr224e.PCM_OtherCriticalFaults.name = 'PCM_OtherCriticalFaults';
wr224e.PCM_OtherCriticalFaults.description = 'PCM_OtherCriticalFaults';
wr224e.PCM_OtherCriticalFaults.protocol = 'wr224e';
wr224e.PCM_OtherCriticalFaults.id = hex2dec('000000F2');
wr224e.PCM_OtherCriticalFaults.idext = 'STANDARD';
wr224e.PCM_OtherCriticalFaults.idinherit = 0;
wr224e.PCM_OtherCriticalFaults.payload_size = 3;
wr224e.PCM_OtherCriticalFaults.payload_value = [];
wr224e.PCM_OtherCriticalFaults.payload_mask = [];
wr224e.PCM_OtherCriticalFaults.interval = 50;

wr224e.PCM_OtherCriticalFaults.fields{1}.name = 'PCM_BPDFault';
wr224e.PCM_OtherCriticalFaults.fields{1}.units = 'FLAG';
wr224e.PCM_OtherCriticalFaults.fields{1}.start_bit = 42;
wr224e.PCM_OtherCriticalFaults.fields{1}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{1}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{1}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{1}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{1}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{2}.name = 'PCM_IMDFault';
wr224e.PCM_OtherCriticalFaults.fields{2}.units = 'FLAG';
wr224e.PCM_OtherCriticalFaults.fields{2}.start_bit = 48;
wr224e.PCM_OtherCriticalFaults.fields{2}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{2}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{2}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{2}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{2}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{3}.name = 'PCM_BMSOpenedSDC';
wr224e.PCM_OtherCriticalFaults.fields{3}.units = 'flag';
wr224e.PCM_OtherCriticalFaults.fields{3}.start_bit = 53;
wr224e.PCM_OtherCriticalFaults.fields{3}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{3}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{3}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{3}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{3}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{4}.name = 'PCM_LVOvervoltage';
wr224e.PCM_OtherCriticalFaults.fields{4}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{4}.start_bit = 40;
wr224e.PCM_OtherCriticalFaults.fields{4}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{4}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{4}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{4}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{4}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{5}.name = 'PCM_LVUndervoltage';
wr224e.PCM_OtherCriticalFaults.fields{5}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{5}.start_bit = 41;
wr224e.PCM_OtherCriticalFaults.fields{5}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{5}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{5}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{5}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{5}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{6}.name = 'PCM_CellUndervoltageDetected';
wr224e.PCM_OtherCriticalFaults.fields{6}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{6}.start_bit = 49;
wr224e.PCM_OtherCriticalFaults.fields{6}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{6}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{6}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{6}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{6}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{7}.name = 'PCM_CellOvervoltageDetected';
wr224e.PCM_OtherCriticalFaults.fields{7}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{7}.start_bit = 50;
wr224e.PCM_OtherCriticalFaults.fields{7}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{7}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{7}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{7}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{7}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{8}.name = 'PCM_CellOvertemperatureLimit';
wr224e.PCM_OtherCriticalFaults.fields{8}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{8}.start_bit = 51;
wr224e.PCM_OtherCriticalFaults.fields{8}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{8}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{8}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{8}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{8}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{9}.name = 'PCM_BMSCANTimeout';
wr224e.PCM_OtherCriticalFaults.fields{9}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{9}.start_bit = 52;
wr224e.PCM_OtherCriticalFaults.fields{9}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{9}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{9}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{9}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{9}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{9}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{10}.name = 'PCM_PumpsImplausibleToInput';
wr224e.PCM_OtherCriticalFaults.fields{10}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{10}.start_bit = 54;
wr224e.PCM_OtherCriticalFaults.fields{10}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{10}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{10}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{10}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{10}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{10}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{11}.name = 'PCM_FansImplausibleToInput';
wr224e.PCM_OtherCriticalFaults.fields{11}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{11}.start_bit = 55;
wr224e.PCM_OtherCriticalFaults.fields{11}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{11}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{11}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{11}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{11}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{11}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{12}.name = 'PCM_YRS_3_Timeout';
wr224e.PCM_OtherCriticalFaults.fields{12}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{12}.start_bit = 56;
wr224e.PCM_OtherCriticalFaults.fields{12}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{12}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{12}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{12}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{12}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{12}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{13}.name = 'PCM_EllipseTimeout';
wr224e.PCM_OtherCriticalFaults.fields{13}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{13}.start_bit = 57;
wr224e.PCM_OtherCriticalFaults.fields{13}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{13}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{13}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{13}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{13}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{13}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{14}.name = 'PCM_SteeringSensorImplausibility';
wr224e.PCM_OtherCriticalFaults.fields{14}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{14}.start_bit = 58;
wr224e.PCM_OtherCriticalFaults.fields{14}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{14}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{14}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{14}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{14}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{14}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{15}.name = 'PCM_SteeringSensorTimeout';
wr224e.PCM_OtherCriticalFaults.fields{15}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{15}.start_bit = 59;
wr224e.PCM_OtherCriticalFaults.fields{15}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{15}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{15}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{15}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{15}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{15}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{16}.name = 'PCM_BSE_Implausibility';
wr224e.PCM_OtherCriticalFaults.fields{16}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{16}.start_bit = 60;
wr224e.PCM_OtherCriticalFaults.fields{16}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{16}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{16}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{16}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{16}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{16}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{17}.name = 'PCM_BSE_ShortOrDisconnect';
wr224e.PCM_OtherCriticalFaults.fields{17}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{17}.start_bit = 61;
wr224e.PCM_OtherCriticalFaults.fields{17}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{17}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{17}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{17}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{17}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{17}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{18}.name = 'PCM_APPS_Implausibility';
wr224e.PCM_OtherCriticalFaults.fields{18}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{18}.start_bit = 62;
wr224e.PCM_OtherCriticalFaults.fields{18}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{18}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{18}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{18}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{18}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{18}.offset = 0.000000e+00;

wr224e.PCM_OtherCriticalFaults.fields{19}.name = 'PCM_APPS_ShortOrDisconnect';
wr224e.PCM_OtherCriticalFaults.fields{19}.units = 'Flag';
wr224e.PCM_OtherCriticalFaults.fields{19}.start_bit = 63;
wr224e.PCM_OtherCriticalFaults.fields{19}.bit_length = 1;
wr224e.PCM_OtherCriticalFaults.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_OtherCriticalFaults.fields{19}.data_type = 'UNSIGNED';
wr224e.PCM_OtherCriticalFaults.fields{19}.maximum = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{19}.minimum = 0.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{19}.scale = 1.000000e+00;
wr224e.PCM_OtherCriticalFaults.fields{19}.offset = 0.000000e+00;

%===================================================================
case 'PCM_PerformanceFaults'
wr224e.PCM_PerformanceFaults = struct;
wr224e.PCM_PerformanceFaults.name = 'PCM_PerformanceFaults';
wr224e.PCM_PerformanceFaults.description = 'PCM_PerformanceFaults';
wr224e.PCM_PerformanceFaults.protocol = 'wr224e';
wr224e.PCM_PerformanceFaults.id = hex2dec('000000F3');
wr224e.PCM_PerformanceFaults.idext = 'STANDARD';
wr224e.PCM_PerformanceFaults.idinherit = 0;
wr224e.PCM_PerformanceFaults.payload_size = 1;
wr224e.PCM_PerformanceFaults.payload_value = [];
wr224e.PCM_PerformanceFaults.payload_mask = [];
wr224e.PCM_PerformanceFaults.interval = 50;

wr224e.PCM_PerformanceFaults.fields{1}.name = 'PCM_PF_HVactive';
wr224e.PCM_PerformanceFaults.fields{1}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{1}.start_bit = 63;
wr224e.PCM_PerformanceFaults.fields{1}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{1}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{1}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{1}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{2}.name = 'PCM_PF_TractionLimited';
wr224e.PCM_PerformanceFaults.fields{2}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{2}.start_bit = 57;
wr224e.PCM_PerformanceFaults.fields{2}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{2}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{2}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{2}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{3}.name = 'PCM_PF_SpinoutDetected';
wr224e.PCM_PerformanceFaults.fields{3}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{3}.start_bit = 60;
wr224e.PCM_PerformanceFaults.fields{3}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{3}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{3}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{3}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{4}.name = 'PCM_PF_SlipCntlActive';
wr224e.PCM_PerformanceFaults.fields{4}.units = '';
wr224e.PCM_PerformanceFaults.fields{4}.start_bit = 58;
wr224e.PCM_PerformanceFaults.fields{4}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{4}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{4}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{4}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{5}.name = 'PCM_PF_PowerLimiterActive';
wr224e.PCM_PerformanceFaults.fields{5}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{5}.start_bit = 56;
wr224e.PCM_PerformanceFaults.fields{5}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{5}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{5}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{5}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{6}.name = 'PCM_PF_SDC';
wr224e.PCM_PerformanceFaults.fields{6}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{6}.start_bit = 61;
wr224e.PCM_PerformanceFaults.fields{6}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{6}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{6}.maximum = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{6}.minimum = 0.000000e+00;
wr224e.PCM_PerformanceFaults.fields{6}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{6}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{7}.name = 'PCM_PF_RTD';
wr224e.PCM_PerformanceFaults.fields{7}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{7}.start_bit = 62;
wr224e.PCM_PerformanceFaults.fields{7}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{7}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{7}.maximum = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{7}.minimum = 0.000000e+00;
wr224e.PCM_PerformanceFaults.fields{7}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{7}.offset = 0.000000e+00;

wr224e.PCM_PerformanceFaults.fields{8}.name = 'PCM_PF_RegenActive';
wr224e.PCM_PerformanceFaults.fields{8}.units = 'flag';
wr224e.PCM_PerformanceFaults.fields{8}.start_bit = 59;
wr224e.PCM_PerformanceFaults.fields{8}.bit_length = 1;
wr224e.PCM_PerformanceFaults.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.PCM_PerformanceFaults.fields{8}.data_type = 'UNSIGNED';
wr224e.PCM_PerformanceFaults.fields{8}.scale = 1.000000e+00;
wr224e.PCM_PerformanceFaults.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Acceleration'
wr224e.SBG_Acceleration = struct;
wr224e.SBG_Acceleration.name = 'SBG_Acceleration';
wr224e.SBG_Acceleration.description = 'SBG_Acceleration';
wr224e.SBG_Acceleration.protocol = 'wr224e';
wr224e.SBG_Acceleration.id = hex2dec('00000121');
wr224e.SBG_Acceleration.idext = 'STANDARD';
wr224e.SBG_Acceleration.idinherit = 0;
wr224e.SBG_Acceleration.payload_size = 7;
wr224e.SBG_Acceleration.payload_value = [];
wr224e.SBG_Acceleration.payload_mask = [];
wr224e.SBG_Acceleration.interval = 50;

wr224e.SBG_Acceleration.fields{1}.name = 'SBG_AccelZ';
wr224e.SBG_Acceleration.fields{1}.units = 'm/s2';
wr224e.SBG_Acceleration.fields{1}.start_bit = 24;
wr224e.SBG_Acceleration.fields{1}.bit_length = 16;
wr224e.SBG_Acceleration.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Acceleration.fields{1}.data_type = 'SIGNED';
wr224e.SBG_Acceleration.fields{1}.maximum = 1.000000e+02;
wr224e.SBG_Acceleration.fields{1}.minimum = 0.000000e+00;
wr224e.SBG_Acceleration.fields{1}.scale = 1.000000e-02;
wr224e.SBG_Acceleration.fields{1}.offset = 0.000000e+00;

wr224e.SBG_Acceleration.fields{2}.name = 'SBG_AccelY';
wr224e.SBG_Acceleration.fields{2}.units = 'm/s2';
wr224e.SBG_Acceleration.fields{2}.start_bit = 40;
wr224e.SBG_Acceleration.fields{2}.bit_length = 16;
wr224e.SBG_Acceleration.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Acceleration.fields{2}.data_type = 'SIGNED';
wr224e.SBG_Acceleration.fields{2}.maximum = 1.000000e+02;
wr224e.SBG_Acceleration.fields{2}.minimum = 0.000000e+00;
wr224e.SBG_Acceleration.fields{2}.scale = 1.000000e-02;
wr224e.SBG_Acceleration.fields{2}.offset = 0.000000e+00;

wr224e.SBG_Acceleration.fields{3}.name = 'SBG_AccelX';
wr224e.SBG_Acceleration.fields{3}.units = 'm/s2';
wr224e.SBG_Acceleration.fields{3}.start_bit = 56;
wr224e.SBG_Acceleration.fields{3}.bit_length = 16;
wr224e.SBG_Acceleration.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Acceleration.fields{3}.data_type = 'SIGNED';
wr224e.SBG_Acceleration.fields{3}.maximum = 1.000000e+02;
wr224e.SBG_Acceleration.fields{3}.minimum = 0.000000e+00;
wr224e.SBG_Acceleration.fields{3}.scale = 1.000000e-02;
wr224e.SBG_Acceleration.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Body_Vel'
wr224e.SBG_Body_Vel = struct;
wr224e.SBG_Body_Vel.name = 'SBG_Body_Vel';
wr224e.SBG_Body_Vel.description = 'SBG_Body_Vel';
wr224e.SBG_Body_Vel.protocol = 'wr224e';
wr224e.SBG_Body_Vel.id = hex2dec('00000139');
wr224e.SBG_Body_Vel.idext = 'STANDARD';
wr224e.SBG_Body_Vel.idinherit = 0;
wr224e.SBG_Body_Vel.payload_size = 6;
wr224e.SBG_Body_Vel.payload_value = [];
wr224e.SBG_Body_Vel.payload_mask = [];
wr224e.SBG_Body_Vel.interval = 50;

wr224e.SBG_Body_Vel.fields{1}.name = 'Vel_X';
wr224e.SBG_Body_Vel.fields{1}.units = 'm/s';
wr224e.SBG_Body_Vel.fields{1}.start_bit = 56;
wr224e.SBG_Body_Vel.fields{1}.bit_length = 16;
wr224e.SBG_Body_Vel.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Body_Vel.fields{1}.data_type = 'SIGNED';
wr224e.SBG_Body_Vel.fields{1}.maximum = 3.270000e+02;
wr224e.SBG_Body_Vel.fields{1}.minimum = -3.270000e+02;
wr224e.SBG_Body_Vel.fields{1}.scale = 1.000000e-02;
wr224e.SBG_Body_Vel.fields{1}.offset = 0.000000e+00;

wr224e.SBG_Body_Vel.fields{2}.name = 'Vel_Y';
wr224e.SBG_Body_Vel.fields{2}.units = 'm/s';
wr224e.SBG_Body_Vel.fields{2}.start_bit = 40;
wr224e.SBG_Body_Vel.fields{2}.bit_length = 16;
wr224e.SBG_Body_Vel.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Body_Vel.fields{2}.data_type = 'SIGNED';
wr224e.SBG_Body_Vel.fields{2}.maximum = 3.270000e+02;
wr224e.SBG_Body_Vel.fields{2}.minimum = -3.270000e+02;
wr224e.SBG_Body_Vel.fields{2}.scale = 1.000000e-02;
wr224e.SBG_Body_Vel.fields{2}.offset = 0.000000e+00;

wr224e.SBG_Body_Vel.fields{3}.name = 'Vel_z';
wr224e.SBG_Body_Vel.fields{3}.units = 'm/s';
wr224e.SBG_Body_Vel.fields{3}.start_bit = 24;
wr224e.SBG_Body_Vel.fields{3}.bit_length = 16;
wr224e.SBG_Body_Vel.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Body_Vel.fields{3}.data_type = 'SIGNED';
wr224e.SBG_Body_Vel.fields{3}.maximum = 3.270000e+02;
wr224e.SBG_Body_Vel.fields{3}.minimum = -3.270000e+02;
wr224e.SBG_Body_Vel.fields{3}.scale = 1.000000e-02;
wr224e.SBG_Body_Vel.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFAltitude'
wr224e.SBG_EKFAltitude = struct;
wr224e.SBG_EKFAltitude.name = 'SBG_EKFAltitude';
wr224e.SBG_EKFAltitude.description = 'SBG_EKFAltitude';
wr224e.SBG_EKFAltitude.protocol = 'wr224e';
wr224e.SBG_EKFAltitude.id = hex2dec('00000135');
wr224e.SBG_EKFAltitude.idext = 'STANDARD';
wr224e.SBG_EKFAltitude.idinherit = 0;
wr224e.SBG_EKFAltitude.payload_size = 6;
wr224e.SBG_EKFAltitude.payload_value = [];
wr224e.SBG_EKFAltitude.payload_mask = [];
wr224e.SBG_EKFAltitude.interval = 50;

wr224e.SBG_EKFAltitude.fields{1}.name = 'SBG_Undulation';
wr224e.SBG_EKFAltitude.fields{1}.units = '';
wr224e.SBG_EKFAltitude.fields{1}.start_bit = 24;
wr224e.SBG_EKFAltitude.fields{1}.bit_length = 16;
wr224e.SBG_EKFAltitude.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFAltitude.fields{1}.data_type = 'SIGNED';
wr224e.SBG_EKFAltitude.fields{1}.scale = 5.000000e-03;
wr224e.SBG_EKFAltitude.fields{1}.offset = 0.000000e+00;

wr224e.SBG_EKFAltitude.fields{2}.name = 'SBG_Altitude';
wr224e.SBG_EKFAltitude.fields{2}.units = 'm';
wr224e.SBG_EKFAltitude.fields{2}.start_bit = 56;
wr224e.SBG_EKFAltitude.fields{2}.bit_length = 32;
wr224e.SBG_EKFAltitude.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFAltitude.fields{2}.data_type = 'SIGNED';
wr224e.SBG_EKFAltitude.fields{2}.scale = 1.000000e-03;
wr224e.SBG_EKFAltitude.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFPosition'
wr224e.SBG_EKFPosition = struct;
wr224e.SBG_EKFPosition.name = 'SBG_EKFPosition';
wr224e.SBG_EKFPosition.description = 'SBG_EKFPosition';
wr224e.SBG_EKFPosition.protocol = 'wr224e';
wr224e.SBG_EKFPosition.id = hex2dec('00000134');
wr224e.SBG_EKFPosition.idext = 'STANDARD';
wr224e.SBG_EKFPosition.idinherit = 0;
wr224e.SBG_EKFPosition.payload_size = 8;
wr224e.SBG_EKFPosition.payload_value = [];
wr224e.SBG_EKFPosition.payload_mask = [];
wr224e.SBG_EKFPosition.interval = 50;

wr224e.SBG_EKFPosition.fields{1}.name = 'SBG_EKFLongitude';
wr224e.SBG_EKFPosition.fields{1}.units = '';
wr224e.SBG_EKFPosition.fields{1}.start_bit = 24;
wr224e.SBG_EKFPosition.fields{1}.bit_length = 32;
wr224e.SBG_EKFPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFPosition.fields{1}.data_type = 'SIGNED';
wr224e.SBG_EKFPosition.fields{1}.scale = 1.000000e-07;
wr224e.SBG_EKFPosition.fields{1}.offset = 0.000000e+00;

wr224e.SBG_EKFPosition.fields{2}.name = 'SBG_EKFLatitude';
wr224e.SBG_EKFPosition.fields{2}.units = '';
wr224e.SBG_EKFPosition.fields{2}.start_bit = 56;
wr224e.SBG_EKFPosition.fields{2}.bit_length = 32;
wr224e.SBG_EKFPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFPosition.fields{2}.data_type = 'SIGNED';
wr224e.SBG_EKFPosition.fields{2}.scale = 1.000000e-07;
wr224e.SBG_EKFPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFVelocity'
wr224e.SBG_EKFVelocity = struct;
wr224e.SBG_EKFVelocity.name = 'SBG_EKFVelocity';
wr224e.SBG_EKFVelocity.description = 'SBG_EKFVelocity';
wr224e.SBG_EKFVelocity.protocol = 'wr224e';
wr224e.SBG_EKFVelocity.id = hex2dec('00000137');
wr224e.SBG_EKFVelocity.idext = 'STANDARD';
wr224e.SBG_EKFVelocity.idinherit = 0;
wr224e.SBG_EKFVelocity.payload_size = 6;
wr224e.SBG_EKFVelocity.payload_value = [];
wr224e.SBG_EKFVelocity.payload_mask = [];
wr224e.SBG_EKFVelocity.interval = 50;

wr224e.SBG_EKFVelocity.fields{1}.name = 'SBG_VelocityZ';
wr224e.SBG_EKFVelocity.fields{1}.units = 'm/s';
wr224e.SBG_EKFVelocity.fields{1}.start_bit = 24;
wr224e.SBG_EKFVelocity.fields{1}.bit_length = 16;
wr224e.SBG_EKFVelocity.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFVelocity.fields{1}.data_type = 'SIGNED';
wr224e.SBG_EKFVelocity.fields{1}.maximum = 3.270000e+02;
wr224e.SBG_EKFVelocity.fields{1}.minimum = -3.270000e+02;
wr224e.SBG_EKFVelocity.fields{1}.scale = 1.000000e-02;
wr224e.SBG_EKFVelocity.fields{1}.offset = 0.000000e+00;

wr224e.SBG_EKFVelocity.fields{2}.name = 'SBG_VelocityY';
wr224e.SBG_EKFVelocity.fields{2}.units = 'm/s';
wr224e.SBG_EKFVelocity.fields{2}.start_bit = 40;
wr224e.SBG_EKFVelocity.fields{2}.bit_length = 16;
wr224e.SBG_EKFVelocity.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFVelocity.fields{2}.data_type = 'SIGNED';
wr224e.SBG_EKFVelocity.fields{2}.maximum = 3.270000e+02;
wr224e.SBG_EKFVelocity.fields{2}.minimum = -3.270000e+02;
wr224e.SBG_EKFVelocity.fields{2}.scale = 1.000000e-02;
wr224e.SBG_EKFVelocity.fields{2}.offset = 0.000000e+00;

wr224e.SBG_EKFVelocity.fields{3}.name = 'SBG_VelocityX';
wr224e.SBG_EKFVelocity.fields{3}.units = 'm/s';
wr224e.SBG_EKFVelocity.fields{3}.start_bit = 56;
wr224e.SBG_EKFVelocity.fields{3}.bit_length = 16;
wr224e.SBG_EKFVelocity.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_EKFVelocity.fields{3}.data_type = 'SIGNED';
wr224e.SBG_EKFVelocity.fields{3}.maximum = 3.270000e+02;
wr224e.SBG_EKFVelocity.fields{3}.minimum = -3.270000e+02;
wr224e.SBG_EKFVelocity.fields{3}.scale = 1.000000e-02;
wr224e.SBG_EKFVelocity.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_GPSPosition'
wr224e.SBG_GPSPosition = struct;
wr224e.SBG_GPSPosition.name = 'SBG_GPSPosition';
wr224e.SBG_GPSPosition.description = 'SBG_GPSPosition';
wr224e.SBG_GPSPosition.protocol = 'wr224e';
wr224e.SBG_GPSPosition.id = hex2dec('00000175');
wr224e.SBG_GPSPosition.idext = 'STANDARD';
wr224e.SBG_GPSPosition.idinherit = 0;
wr224e.SBG_GPSPosition.payload_size = 8;
wr224e.SBG_GPSPosition.payload_value = [];
wr224e.SBG_GPSPosition.payload_mask = [];
wr224e.SBG_GPSPosition.interval = 50;

wr224e.SBG_GPSPosition.fields{1}.name = 'SBG_GPSLongitude';
wr224e.SBG_GPSPosition.fields{1}.units = '';
wr224e.SBG_GPSPosition.fields{1}.start_bit = 24;
wr224e.SBG_GPSPosition.fields{1}.bit_length = 32;
wr224e.SBG_GPSPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_GPSPosition.fields{1}.data_type = 'SIGNED';
wr224e.SBG_GPSPosition.fields{1}.scale = 1.000000e-07;
wr224e.SBG_GPSPosition.fields{1}.offset = 0.000000e+00;

wr224e.SBG_GPSPosition.fields{2}.name = 'SBG_GPSLatitude';
wr224e.SBG_GPSPosition.fields{2}.units = '';
wr224e.SBG_GPSPosition.fields{2}.start_bit = 56;
wr224e.SBG_GPSPosition.fields{2}.bit_length = 32;
wr224e.SBG_GPSPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_GPSPosition.fields{2}.data_type = 'SIGNED';
wr224e.SBG_GPSPosition.fields{2}.scale = 1.000000e-07;
wr224e.SBG_GPSPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Gyro'
wr224e.SBG_Gyro = struct;
wr224e.SBG_Gyro.name = 'SBG_Gyro';
wr224e.SBG_Gyro.description = 'SBG_Gyro';
wr224e.SBG_Gyro.protocol = 'wr224e';
wr224e.SBG_Gyro.id = hex2dec('00000122');
wr224e.SBG_Gyro.idext = 'STANDARD';
wr224e.SBG_Gyro.idinherit = 0;
wr224e.SBG_Gyro.payload_size = 7;
wr224e.SBG_Gyro.payload_value = [];
wr224e.SBG_Gyro.payload_mask = [];
wr224e.SBG_Gyro.interval = 50;

wr224e.SBG_Gyro.fields{1}.name = 'SBG_YawRateZ';
wr224e.SBG_Gyro.fields{1}.units = 'deg/s';
wr224e.SBG_Gyro.fields{1}.start_bit = 24;
wr224e.SBG_Gyro.fields{1}.bit_length = 16;
wr224e.SBG_Gyro.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Gyro.fields{1}.data_type = 'SIGNED';
wr224e.SBG_Gyro.fields{1}.maximum = 1.877411e+03;
wr224e.SBG_Gyro.fields{1}.minimum = -1.877469e+03;
wr224e.SBG_Gyro.fields{1}.scale = 1.000000e-03;
wr224e.SBG_Gyro.fields{1}.offset = 0.000000e+00;

wr224e.SBG_Gyro.fields{2}.name = 'SBG_YawRateY';
wr224e.SBG_Gyro.fields{2}.units = 'deg/s';
wr224e.SBG_Gyro.fields{2}.start_bit = 40;
wr224e.SBG_Gyro.fields{2}.bit_length = 16;
wr224e.SBG_Gyro.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Gyro.fields{2}.data_type = 'SIGNED';
wr224e.SBG_Gyro.fields{2}.maximum = 1.877411e+03;
wr224e.SBG_Gyro.fields{2}.minimum = -1.877469e+03;
wr224e.SBG_Gyro.fields{2}.scale = 1.000000e-03;
wr224e.SBG_Gyro.fields{2}.offset = 0.000000e+00;

wr224e.SBG_Gyro.fields{3}.name = 'SBG_YawRateX';
wr224e.SBG_Gyro.fields{3}.units = 'deg/s';
wr224e.SBG_Gyro.fields{3}.start_bit = 56;
wr224e.SBG_Gyro.fields{3}.bit_length = 16;
wr224e.SBG_Gyro.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Gyro.fields{3}.data_type = 'SIGNED';
wr224e.SBG_Gyro.fields{3}.maximum = 1.877411e+03;
wr224e.SBG_Gyro.fields{3}.minimum = -1.877469e+03;
wr224e.SBG_Gyro.fields{3}.scale = 1.000000e-03;
wr224e.SBG_Gyro.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Heading'
wr224e.SBG_Heading = struct;
wr224e.SBG_Heading.name = 'SBG_Heading';
wr224e.SBG_Heading.description = 'SBG_Heading';
wr224e.SBG_Heading.protocol = 'wr224e';
wr224e.SBG_Heading.id = hex2dec('00000173');
wr224e.SBG_Heading.idext = 'STANDARD';
wr224e.SBG_Heading.idinherit = 0;
wr224e.SBG_Heading.payload_size = 3;
wr224e.SBG_Heading.payload_value = [];
wr224e.SBG_Heading.payload_mask = [];
wr224e.SBG_Heading.interval = 50;

wr224e.SBG_Heading.fields{1}.name = 'SBG_Heading';
wr224e.SBG_Heading.fields{1}.units = 'deg';
wr224e.SBG_Heading.fields{1}.start_bit = 56;
wr224e.SBG_Heading.fields{1}.bit_length = 16;
wr224e.SBG_Heading.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_Heading.fields{1}.data_type = 'UNSIGNED';
wr224e.SBG_Heading.fields{1}.maximum = 6.550000e+02;
wr224e.SBG_Heading.fields{1}.minimum = 0.000000e+00;
wr224e.SBG_Heading.fields{1}.scale = 1.000000e-02;
wr224e.SBG_Heading.fields{1}.offset = 0.000000e+00;

%===================================================================
case 'SBG_RollYawPitch'
wr224e.SBG_RollYawPitch = struct;
wr224e.SBG_RollYawPitch.name = 'SBG_RollYawPitch';
wr224e.SBG_RollYawPitch.description = 'SBG_RollYawPitch';
wr224e.SBG_RollYawPitch.protocol = 'wr224e';
wr224e.SBG_RollYawPitch.id = hex2dec('00000132');
wr224e.SBG_RollYawPitch.idext = 'STANDARD';
wr224e.SBG_RollYawPitch.idinherit = 0;
wr224e.SBG_RollYawPitch.payload_size = 7;
wr224e.SBG_RollYawPitch.payload_value = [];
wr224e.SBG_RollYawPitch.payload_mask = [];
wr224e.SBG_RollYawPitch.interval = 50;

wr224e.SBG_RollYawPitch.fields{1}.name = 'SBG_Yaw';
wr224e.SBG_RollYawPitch.fields{1}.units = 'rad';
wr224e.SBG_RollYawPitch.fields{1}.start_bit = 24;
wr224e.SBG_RollYawPitch.fields{1}.bit_length = 16;
wr224e.SBG_RollYawPitch.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_RollYawPitch.fields{1}.data_type = 'SIGNED';
wr224e.SBG_RollYawPitch.fields{1}.maximum = 1.877411e+03;
wr224e.SBG_RollYawPitch.fields{1}.minimum = -1.877469e+03;
wr224e.SBG_RollYawPitch.fields{1}.scale = 1.000000e-04;
wr224e.SBG_RollYawPitch.fields{1}.offset = 0.000000e+00;

wr224e.SBG_RollYawPitch.fields{2}.name = 'SBG_Roll';
wr224e.SBG_RollYawPitch.fields{2}.units = 'rad';
wr224e.SBG_RollYawPitch.fields{2}.start_bit = 56;
wr224e.SBG_RollYawPitch.fields{2}.bit_length = 16;
wr224e.SBG_RollYawPitch.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_RollYawPitch.fields{2}.data_type = 'SIGNED';
wr224e.SBG_RollYawPitch.fields{2}.maximum = 1.877411e+03;
wr224e.SBG_RollYawPitch.fields{2}.minimum = -1.877469e+03;
wr224e.SBG_RollYawPitch.fields{2}.scale = 1.000000e-04;
wr224e.SBG_RollYawPitch.fields{2}.offset = 0.000000e+00;

wr224e.SBG_RollYawPitch.fields{3}.name = 'SBG_Pitch';
wr224e.SBG_RollYawPitch.fields{3}.units = 'rad';
wr224e.SBG_RollYawPitch.fields{3}.start_bit = 40;
wr224e.SBG_RollYawPitch.fields{3}.bit_length = 16;
wr224e.SBG_RollYawPitch.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_RollYawPitch.fields{3}.data_type = 'SIGNED';
wr224e.SBG_RollYawPitch.fields{3}.maximum = 1.877411e+03;
wr224e.SBG_RollYawPitch.fields{3}.minimum = -1.877469e+03;
wr224e.SBG_RollYawPitch.fields{3}.scale = 1.000000e-04;
wr224e.SBG_RollYawPitch.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_VelocityInfo'
wr224e.SBG_VelocityInfo = struct;
wr224e.SBG_VelocityInfo.name = 'SBG_VelocityInfo';
wr224e.SBG_VelocityInfo.description = 'SBG_VelocityInfo';
wr224e.SBG_VelocityInfo.protocol = 'wr224e';
wr224e.SBG_VelocityInfo.id = hex2dec('00000172');
wr224e.SBG_VelocityInfo.idext = 'STANDARD';
wr224e.SBG_VelocityInfo.idinherit = 0;
wr224e.SBG_VelocityInfo.payload_size = 8;
wr224e.SBG_VelocityInfo.payload_value = [];
wr224e.SBG_VelocityInfo.payload_mask = [];
wr224e.SBG_VelocityInfo.interval = 50;

wr224e.SBG_VelocityInfo.fields{1}.name = 'SBG_VelocityType';
wr224e.SBG_VelocityInfo.fields{1}.units = '';
wr224e.SBG_VelocityInfo.fields{1}.start_bit = 30;
wr224e.SBG_VelocityInfo.fields{1}.bit_length = 6;
wr224e.SBG_VelocityInfo.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_VelocityInfo.fields{1}.data_type = 'UNSIGNED';
wr224e.SBG_VelocityInfo.fields{1}.maximum = 6.300000e+01;
wr224e.SBG_VelocityInfo.fields{1}.minimum = 0.000000e+00;
wr224e.SBG_VelocityInfo.fields{1}.scale = 1.000000e+00;
wr224e.SBG_VelocityInfo.fields{1}.offset = 0.000000e+00;

wr224e.SBG_VelocityInfo.fields{2}.name = 'SBG_VelocityStatus';
wr224e.SBG_VelocityInfo.fields{2}.units = '';
wr224e.SBG_VelocityInfo.fields{2}.start_bit = 24;
wr224e.SBG_VelocityInfo.fields{2}.bit_length = 6;
wr224e.SBG_VelocityInfo.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SBG_VelocityInfo.fields{2}.data_type = 'UNSIGNED';
wr224e.SBG_VelocityInfo.fields{2}.maximum = 6.300000e+01;
wr224e.SBG_VelocityInfo.fields{2}.minimum = 0.000000e+00;
wr224e.SBG_VelocityInfo.fields{2}.scale = 1.000000e+00;
wr224e.SBG_VelocityInfo.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Current1'
wr224e.SPDU_Current1 = struct;
wr224e.SPDU_Current1.name = 'SPDU_Current1';
wr224e.SPDU_Current1.description = 'SPDU_Current1';
wr224e.SPDU_Current1.protocol = 'wr224e';
wr224e.SPDU_Current1.id = hex2dec('00000130');
wr224e.SPDU_Current1.idext = 'STANDARD';
wr224e.SPDU_Current1.idinherit = 0;
wr224e.SPDU_Current1.payload_size = 8;
wr224e.SPDU_Current1.payload_value = [];
wr224e.SPDU_Current1.payload_mask = [];
wr224e.SPDU_Current1.interval = 50;

wr224e.SPDU_Current1.fields{1}.name = 'SPDU_Current_eFuse4';
wr224e.SPDU_Current1.fields{1}.units = '';
wr224e.SPDU_Current1.fields{1}.start_bit = 8;
wr224e.SPDU_Current1.fields{1}.bit_length = 16;
wr224e.SPDU_Current1.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current1.fields{1}.data_type = 'SIGNED';
wr224e.SPDU_Current1.fields{1}.scale = 1.000000e-03;
wr224e.SPDU_Current1.fields{1}.offset = 0.000000e+00;

wr224e.SPDU_Current1.fields{2}.name = 'SPDU_Current_eFuse3';
wr224e.SPDU_Current1.fields{2}.units = '';
wr224e.SPDU_Current1.fields{2}.start_bit = 24;
wr224e.SPDU_Current1.fields{2}.bit_length = 16;
wr224e.SPDU_Current1.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current1.fields{2}.data_type = 'SIGNED';
wr224e.SPDU_Current1.fields{2}.scale = 1.000000e-03;
wr224e.SPDU_Current1.fields{2}.offset = 0.000000e+00;

wr224e.SPDU_Current1.fields{3}.name = 'SPDU_Current_eFuse2';
wr224e.SPDU_Current1.fields{3}.units = '';
wr224e.SPDU_Current1.fields{3}.start_bit = 40;
wr224e.SPDU_Current1.fields{3}.bit_length = 16;
wr224e.SPDU_Current1.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current1.fields{3}.data_type = 'SIGNED';
wr224e.SPDU_Current1.fields{3}.scale = 1.000000e-03;
wr224e.SPDU_Current1.fields{3}.offset = 0.000000e+00;

wr224e.SPDU_Current1.fields{4}.name = 'SPDU_Current_eFuse1';
wr224e.SPDU_Current1.fields{4}.units = '';
wr224e.SPDU_Current1.fields{4}.start_bit = 56;
wr224e.SPDU_Current1.fields{4}.bit_length = 16;
wr224e.SPDU_Current1.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current1.fields{4}.data_type = 'SIGNED';
wr224e.SPDU_Current1.fields{4}.scale = 1.000000e-03;
wr224e.SPDU_Current1.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Current2'
wr224e.SPDU_Current2 = struct;
wr224e.SPDU_Current2.name = 'SPDU_Current2';
wr224e.SPDU_Current2.description = 'SPDU_Current2';
wr224e.SPDU_Current2.protocol = 'wr224e';
wr224e.SPDU_Current2.id = hex2dec('00000131');
wr224e.SPDU_Current2.idext = 'STANDARD';
wr224e.SPDU_Current2.idinherit = 0;
wr224e.SPDU_Current2.payload_size = 4;
wr224e.SPDU_Current2.payload_value = [];
wr224e.SPDU_Current2.payload_mask = [];
wr224e.SPDU_Current2.interval = 50;

wr224e.SPDU_Current2.fields{1}.name = 'SPDU_Current_eFuse6';
wr224e.SPDU_Current2.fields{1}.units = '';
wr224e.SPDU_Current2.fields{1}.start_bit = 40;
wr224e.SPDU_Current2.fields{1}.bit_length = 16;
wr224e.SPDU_Current2.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current2.fields{1}.data_type = 'SIGNED';
wr224e.SPDU_Current2.fields{1}.scale = 1.000000e-03;
wr224e.SPDU_Current2.fields{1}.offset = 0.000000e+00;

wr224e.SPDU_Current2.fields{2}.name = 'SPDU_Current_eFuse5';
wr224e.SPDU_Current2.fields{2}.units = '';
wr224e.SPDU_Current2.fields{2}.start_bit = 56;
wr224e.SPDU_Current2.fields{2}.bit_length = 16;
wr224e.SPDU_Current2.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Current2.fields{2}.data_type = 'SIGNED';
wr224e.SPDU_Current2.fields{2}.scale = 1.000000e-03;
wr224e.SPDU_Current2.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Faults'
wr224e.SPDU_Faults = struct;
wr224e.SPDU_Faults.name = 'SPDU_Faults';
wr224e.SPDU_Faults.description = 'SPDU_Faults';
wr224e.SPDU_Faults.protocol = 'wr224e';
wr224e.SPDU_Faults.id = hex2dec('00000033');
wr224e.SPDU_Faults.idext = 'STANDARD';
wr224e.SPDU_Faults.idinherit = 0;
wr224e.SPDU_Faults.payload_size = 1;
wr224e.SPDU_Faults.payload_value = [];
wr224e.SPDU_Faults.payload_mask = [];
wr224e.SPDU_Faults.interval = 50;

wr224e.SPDU_Faults.fields{1}.name = 'SPDU_Fault_eFuse6';
wr224e.SPDU_Faults.fields{1}.units = '';
wr224e.SPDU_Faults.fields{1}.start_bit = 63;
wr224e.SPDU_Faults.fields{1}.bit_length = 1;
wr224e.SPDU_Faults.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{1}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{1}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{1}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{2}.name = 'SPDU_Fault_eFuse5';
wr224e.SPDU_Faults.fields{2}.units = '';
wr224e.SPDU_Faults.fields{2}.start_bit = 62;
wr224e.SPDU_Faults.fields{2}.bit_length = 1;
wr224e.SPDU_Faults.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{2}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{2}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{2}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{3}.name = 'SPDU_Fault_eFuse4';
wr224e.SPDU_Faults.fields{3}.units = '';
wr224e.SPDU_Faults.fields{3}.start_bit = 61;
wr224e.SPDU_Faults.fields{3}.bit_length = 1;
wr224e.SPDU_Faults.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{3}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{3}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{3}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{4}.name = 'SPDU_Fault_eFuse3';
wr224e.SPDU_Faults.fields{4}.units = '';
wr224e.SPDU_Faults.fields{4}.start_bit = 60;
wr224e.SPDU_Faults.fields{4}.bit_length = 1;
wr224e.SPDU_Faults.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{4}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{4}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{4}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{5}.name = 'SPDU_Fault_eFuse2';
wr224e.SPDU_Faults.fields{5}.units = '';
wr224e.SPDU_Faults.fields{5}.start_bit = 59;
wr224e.SPDU_Faults.fields{5}.bit_length = 1;
wr224e.SPDU_Faults.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{5}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{5}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{5}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{6}.name = 'SPDU_Fault_eFuse1';
wr224e.SPDU_Faults.fields{6}.units = '';
wr224e.SPDU_Faults.fields{6}.start_bit = 58;
wr224e.SPDU_Faults.fields{6}.bit_length = 1;
wr224e.SPDU_Faults.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{6}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{6}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{6}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{7}.name = 'SPDU_Fault_AH2';
wr224e.SPDU_Faults.fields{7}.units = '';
wr224e.SPDU_Faults.fields{7}.start_bit = 57;
wr224e.SPDU_Faults.fields{7}.bit_length = 1;
wr224e.SPDU_Faults.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{7}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{7}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{7}.offset = 0.000000e+00;

wr224e.SPDU_Faults.fields{8}.name = 'SPDU_Fault_AH1';
wr224e.SPDU_Faults.fields{8}.units = '';
wr224e.SPDU_Faults.fields{8}.start_bit = 56;
wr224e.SPDU_Faults.fields{8}.bit_length = 1;
wr224e.SPDU_Faults.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.SPDU_Faults.fields{8}.data_type = 'SIGNED';
wr224e.SPDU_Faults.fields{8}.scale = 1.000000e+00;
wr224e.SPDU_Faults.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'SW_Inputs'
wr224e.SW_Inputs = struct;
wr224e.SW_Inputs.name = 'SW_Inputs';
wr224e.SW_Inputs.description = 'SW_Inputs';
wr224e.SW_Inputs.protocol = 'wr224e';
wr224e.SW_Inputs.id = hex2dec('000000D5');
wr224e.SW_Inputs.idext = 'STANDARD';
wr224e.SW_Inputs.idinherit = 0;
wr224e.SW_Inputs.payload_size = 8;
wr224e.SW_Inputs.payload_value = [];
wr224e.SW_Inputs.payload_mask = [];
wr224e.SW_Inputs.interval = 50;

wr224e.SW_Inputs.fields{1}.name = 'SW_AIN_PD1_Analog1';
wr224e.SW_Inputs.fields{1}.units = '';
wr224e.SW_Inputs.fields{1}.start_bit = 32;
wr224e.SW_Inputs.fields{1}.bit_length = 16;
wr224e.SW_Inputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{1}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{1}.maximum = 6.553500e+04;
wr224e.SW_Inputs.fields{1}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{1}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{1}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{2}.name = 'SW_AIN_PD0_Analog0';
wr224e.SW_Inputs.fields{2}.units = '';
wr224e.SW_Inputs.fields{2}.start_bit = 16;
wr224e.SW_Inputs.fields{2}.bit_length = 16;
wr224e.SW_Inputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{2}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{2}.maximum = 6.553500e+04;
wr224e.SW_Inputs.fields{2}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{2}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{2}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{3}.name = 'SW_IN_PD2_Button14';
wr224e.SW_Inputs.fields{3}.units = '';
wr224e.SW_Inputs.fields{3}.start_bit = 40;
wr224e.SW_Inputs.fields{3}.bit_length = 1;
wr224e.SW_Inputs.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{3}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{3}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{3}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{3}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{3}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{4}.name = 'SW_IN_PD4_Button13';
wr224e.SW_Inputs.fields{4}.units = '';
wr224e.SW_Inputs.fields{4}.start_bit = 41;
wr224e.SW_Inputs.fields{4}.bit_length = 1;
wr224e.SW_Inputs.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{4}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{4}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{4}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{4}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{4}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{5}.name = 'SW_IN_PD6_Button12';
wr224e.SW_Inputs.fields{5}.units = '';
wr224e.SW_Inputs.fields{5}.start_bit = 42;
wr224e.SW_Inputs.fields{5}.bit_length = 1;
wr224e.SW_Inputs.fields{5}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{5}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{5}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{5}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{5}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{5}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{6}.name = 'SW_IN_PB7_Button11';
wr224e.SW_Inputs.fields{6}.units = '';
wr224e.SW_Inputs.fields{6}.start_bit = 43;
wr224e.SW_Inputs.fields{6}.bit_length = 1;
wr224e.SW_Inputs.fields{6}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{6}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{6}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{6}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{6}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{6}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{7}.name = 'SW_IN_PB6_Button10';
wr224e.SW_Inputs.fields{7}.units = '';
wr224e.SW_Inputs.fields{7}.start_bit = 44;
wr224e.SW_Inputs.fields{7}.bit_length = 1;
wr224e.SW_Inputs.fields{7}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{7}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{7}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{7}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{7}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{7}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{8}.name = 'SW_IN_PB5_Button9';
wr224e.SW_Inputs.fields{8}.units = '';
wr224e.SW_Inputs.fields{8}.start_bit = 45;
wr224e.SW_Inputs.fields{8}.bit_length = 1;
wr224e.SW_Inputs.fields{8}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{8}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{8}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{8}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{8}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{8}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{9}.name = 'SW_IN_PE2_Button1';
wr224e.SW_Inputs.fields{9}.units = '';
wr224e.SW_Inputs.fields{9}.start_bit = 48;
wr224e.SW_Inputs.fields{9}.bit_length = 1;
wr224e.SW_Inputs.fields{9}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{9}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{9}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{9}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{9}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{9}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{10}.name = 'SW_IN_PE5_Button2';
wr224e.SW_Inputs.fields{10}.units = '';
wr224e.SW_Inputs.fields{10}.start_bit = 49;
wr224e.SW_Inputs.fields{10}.bit_length = 1;
wr224e.SW_Inputs.fields{10}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{10}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{10}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{10}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{10}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{10}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{11}.name = 'SW_IN_PE4_Button3';
wr224e.SW_Inputs.fields{11}.units = '';
wr224e.SW_Inputs.fields{11}.start_bit = 50;
wr224e.SW_Inputs.fields{11}.bit_length = 1;
wr224e.SW_Inputs.fields{11}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{11}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{11}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{11}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{11}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{11}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{12}.name = 'SW_IN_PE1_Mode_Dial_Reserved';
wr224e.SW_Inputs.fields{12}.units = '';
wr224e.SW_Inputs.fields{12}.start_bit = 59;
wr224e.SW_Inputs.fields{12}.bit_length = 1;
wr224e.SW_Inputs.fields{12}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{12}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{12}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{12}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{12}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{12}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{13}.name = 'SW_IN_PD5_Button5';
wr224e.SW_Inputs.fields{13}.units = '';
wr224e.SW_Inputs.fields{13}.start_bit = 52;
wr224e.SW_Inputs.fields{13}.bit_length = 1;
wr224e.SW_Inputs.fields{13}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{13}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{13}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{13}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{13}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{13}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{14}.name = 'SW_IN_PA0_Button6';
wr224e.SW_Inputs.fields{14}.units = '';
wr224e.SW_Inputs.fields{14}.start_bit = 53;
wr224e.SW_Inputs.fields{14}.bit_length = 1;
wr224e.SW_Inputs.fields{14}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{14}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{14}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{14}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{14}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{14}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{15}.name = 'SW_IN_PA1_Button7';
wr224e.SW_Inputs.fields{15}.units = '';
wr224e.SW_Inputs.fields{15}.start_bit = 54;
wr224e.SW_Inputs.fields{15}.bit_length = 1;
wr224e.SW_Inputs.fields{15}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{15}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{15}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{15}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{15}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{15}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{16}.name = 'SW_IN_PB3_Button8';
wr224e.SW_Inputs.fields{16}.units = '';
wr224e.SW_Inputs.fields{16}.start_bit = 55;
wr224e.SW_Inputs.fields{16}.bit_length = 1;
wr224e.SW_Inputs.fields{16}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{16}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{16}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{16}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{16}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{16}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{17}.name = 'SW_IN_PA6_TC_Min';
wr224e.SW_Inputs.fields{17}.units = '';
wr224e.SW_Inputs.fields{17}.start_bit = 56;
wr224e.SW_Inputs.fields{17}.bit_length = 1;
wr224e.SW_Inputs.fields{17}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{17}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{17}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{17}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{17}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{17}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{18}.name = 'SW_IN_PA7_TC_Mid';
wr224e.SW_Inputs.fields{18}.units = '';
wr224e.SW_Inputs.fields{18}.start_bit = 57;
wr224e.SW_Inputs.fields{18}.bit_length = 1;
wr224e.SW_Inputs.fields{18}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{18}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{18}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{18}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{18}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{18}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{19}.name = 'SW_IN_PB2_TC_Max';
wr224e.SW_Inputs.fields{19}.units = '';
wr224e.SW_Inputs.fields{19}.start_bit = 58;
wr224e.SW_Inputs.fields{19}.bit_length = 1;
wr224e.SW_Inputs.fields{19}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{19}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{19}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{19}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{19}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{19}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{20}.name = 'SW_IN_PB1_Button4';
wr224e.SW_Inputs.fields{20}.units = '';
wr224e.SW_Inputs.fields{20}.start_bit = 51;
wr224e.SW_Inputs.fields{20}.bit_length = 1;
wr224e.SW_Inputs.fields{20}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{20}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{20}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{20}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{20}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{20}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{21}.name = 'SW_IN_PB0_Autocross';
wr224e.SW_Inputs.fields{21}.units = '';
wr224e.SW_Inputs.fields{21}.start_bit = 60;
wr224e.SW_Inputs.fields{21}.bit_length = 1;
wr224e.SW_Inputs.fields{21}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{21}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{21}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{21}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{21}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{21}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{22}.name = 'SW_IN_PF1_Endurance';
wr224e.SW_Inputs.fields{22}.units = '';
wr224e.SW_Inputs.fields{22}.start_bit = 61;
wr224e.SW_Inputs.fields{22}.bit_length = 1;
wr224e.SW_Inputs.fields{22}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{22}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{22}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{22}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{22}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{22}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{23}.name = 'SW_IN_PE3_Skidpad';
wr224e.SW_Inputs.fields{23}.units = '';
wr224e.SW_Inputs.fields{23}.start_bit = 62;
wr224e.SW_Inputs.fields{23}.bit_length = 1;
wr224e.SW_Inputs.fields{23}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{23}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{23}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{23}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{23}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{23}.offset = 0.000000e+00;

wr224e.SW_Inputs.fields{24}.name = 'SW_IN_PA3_Accel';
wr224e.SW_Inputs.fields{24}.units = '';
wr224e.SW_Inputs.fields{24}.start_bit = 63;
wr224e.SW_Inputs.fields{24}.bit_length = 1;
wr224e.SW_Inputs.fields{24}.byte_order = 'LITTLE_ENDIAN';
wr224e.SW_Inputs.fields{24}.data_type = 'UNSIGNED';
wr224e.SW_Inputs.fields{24}.maximum = 1.000000e+00;
wr224e.SW_Inputs.fields{24}.minimum = 0.000000e+00;
wr224e.SW_Inputs.fields{24}.scale = 1.000000e+00;
wr224e.SW_Inputs.fields{24}.offset = 0.000000e+00;

%===================================================================
case 'Shock_Pot_Values'
wr224e.Shock_Pot_Values = struct;
wr224e.Shock_Pot_Values.name = 'Shock_Pot_Values';
wr224e.Shock_Pot_Values.description = 'Shock_Pot_Values';
wr224e.Shock_Pot_Values.protocol = 'wr224e';
wr224e.Shock_Pot_Values.id = hex2dec('000000E6');
wr224e.Shock_Pot_Values.idext = 'STANDARD';
wr224e.Shock_Pot_Values.idinherit = 0;
wr224e.Shock_Pot_Values.payload_size = 8;
wr224e.Shock_Pot_Values.payload_value = [];
wr224e.Shock_Pot_Values.payload_mask = [];
wr224e.Shock_Pot_Values.interval = 50;

wr224e.Shock_Pot_Values.fields{1}.name = 'RR_Shock';
wr224e.Shock_Pot_Values.fields{1}.units = 'mm';
wr224e.Shock_Pot_Values.fields{1}.start_bit = 8;
wr224e.Shock_Pot_Values.fields{1}.bit_length = 16;
wr224e.Shock_Pot_Values.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Shock_Pot_Values.fields{1}.data_type = 'SIGNED';
wr224e.Shock_Pot_Values.fields{1}.maximum = 1.000000e+02;
wr224e.Shock_Pot_Values.fields{1}.minimum = 0.000000e+00;
wr224e.Shock_Pot_Values.fields{1}.scale = 1.000000e-01;
wr224e.Shock_Pot_Values.fields{1}.offset = 0.000000e+00;

wr224e.Shock_Pot_Values.fields{2}.name = 'RL_Shock';
wr224e.Shock_Pot_Values.fields{2}.units = 'mm';
wr224e.Shock_Pot_Values.fields{2}.start_bit = 24;
wr224e.Shock_Pot_Values.fields{2}.bit_length = 16;
wr224e.Shock_Pot_Values.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Shock_Pot_Values.fields{2}.data_type = 'SIGNED';
wr224e.Shock_Pot_Values.fields{2}.maximum = 1.000000e+02;
wr224e.Shock_Pot_Values.fields{2}.minimum = 0.000000e+00;
wr224e.Shock_Pot_Values.fields{2}.scale = 1.000000e-01;
wr224e.Shock_Pot_Values.fields{2}.offset = 0.000000e+00;

wr224e.Shock_Pot_Values.fields{3}.name = 'FL_Shock';
wr224e.Shock_Pot_Values.fields{3}.units = 'mm';
wr224e.Shock_Pot_Values.fields{3}.start_bit = 40;
wr224e.Shock_Pot_Values.fields{3}.bit_length = 16;
wr224e.Shock_Pot_Values.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.Shock_Pot_Values.fields{3}.data_type = 'SIGNED';
wr224e.Shock_Pot_Values.fields{3}.maximum = 1.000000e+02;
wr224e.Shock_Pot_Values.fields{3}.minimum = 0.000000e+00;
wr224e.Shock_Pot_Values.fields{3}.scale = 1.000000e-01;
wr224e.Shock_Pot_Values.fields{3}.offset = 0.000000e+00;

wr224e.Shock_Pot_Values.fields{4}.name = 'FR_Shock';
wr224e.Shock_Pot_Values.fields{4}.units = 'mm';
wr224e.Shock_Pot_Values.fields{4}.start_bit = 56;
wr224e.Shock_Pot_Values.fields{4}.bit_length = 16;
wr224e.Shock_Pot_Values.fields{4}.byte_order = 'LITTLE_ENDIAN';
wr224e.Shock_Pot_Values.fields{4}.data_type = 'SIGNED';
wr224e.Shock_Pot_Values.fields{4}.maximum = 1.000000e+02;
wr224e.Shock_Pot_Values.fields{4}.minimum = 0.000000e+00;
wr224e.Shock_Pot_Values.fields{4}.scale = 1.000000e-01;
wr224e.Shock_Pot_Values.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Steering_Wheel_messages'
wr224e.Steering_Wheel_messages = struct;
wr224e.Steering_Wheel_messages.name = 'Steering_Wheel_messages';
wr224e.Steering_Wheel_messages.description = 'Steering_Wheel_messages';
wr224e.Steering_Wheel_messages.protocol = 'wr224e';
wr224e.Steering_Wheel_messages.id = hex2dec('00000337');
wr224e.Steering_Wheel_messages.idext = 'STANDARD';
wr224e.Steering_Wheel_messages.idinherit = 0;
wr224e.Steering_Wheel_messages.payload_size = 8;
wr224e.Steering_Wheel_messages.payload_value = [];
wr224e.Steering_Wheel_messages.payload_mask = [];
wr224e.Steering_Wheel_messages.interval = 50;

wr224e.Steering_Wheel_messages.fields{1}.name = 'LowCellVolt';
wr224e.Steering_Wheel_messages.fields{1}.units = '';
wr224e.Steering_Wheel_messages.fields{1}.start_bit = 56;
wr224e.Steering_Wheel_messages.fields{1}.bit_length = 24;
wr224e.Steering_Wheel_messages.fields{1}.byte_order = 'LITTLE_ENDIAN';
wr224e.Steering_Wheel_messages.fields{1}.data_type = 'UNSIGNED';
wr224e.Steering_Wheel_messages.fields{1}.scale = 1.000000e-04;
wr224e.Steering_Wheel_messages.fields{1}.offset = 0.000000e+00;

wr224e.Steering_Wheel_messages.fields{2}.name = 'HighCellTemp';
wr224e.Steering_Wheel_messages.fields{2}.units = '';
wr224e.Steering_Wheel_messages.fields{2}.start_bit = 32;
wr224e.Steering_Wheel_messages.fields{2}.bit_length = 24;
wr224e.Steering_Wheel_messages.fields{2}.byte_order = 'LITTLE_ENDIAN';
wr224e.Steering_Wheel_messages.fields{2}.data_type = 'SIGNED';
wr224e.Steering_Wheel_messages.fields{2}.scale = 1.000000e+00;
wr224e.Steering_Wheel_messages.fields{2}.offset = 0.000000e+00;

wr224e.Steering_Wheel_messages.fields{3}.name = 'MotorTempRR';
wr224e.Steering_Wheel_messages.fields{3}.units = '';
wr224e.Steering_Wheel_messages.fields{3}.start_bit = 8;
wr224e.Steering_Wheel_messages.fields{3}.bit_length = 8;
wr224e.Steering_Wheel_messages.fields{3}.byte_order = 'LITTLE_ENDIAN';
wr224e.Steering_Wheel_messages.fields{3}.data_type = 'UNSIGNED';
wr224e.Steering_Wheel_messages.fields{3}.scale = 1.000000e+00;
wr224e.Steering_Wheel_messages.fields{3}.offset = 0.000000e+00;

end

try
	msg = wr224e.(msgname);
end