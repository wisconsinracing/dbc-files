import csv


signalList = [
    ["Name", "ID"]
]

car = int(input("\nWhat car?\n1. C Car\n2. E Car\n>>"))
if car == 1:
    dbcFilePath = "CCar/WR224C.dbc"
elif car == 2:
    dbcFilePath = "ECar/WR225E.dbc"
txNode = input("\nTx Node: ")

with open(dbcFilePath) as dbcFile:
    includesTxNode = False
    canID = None
    lines = dbcFile.readlines()
    for line in lines:
        if line.split(" ")[0] == "BO_":
            includesTxNode = txNode == line.split(" ")[-1].strip()
            canID = line.split(" ")[1]
        elif line.strip() == "":
            includesTxNode = False
        elif includesTxNode and line.strip() != "":
            name = line.strip().split(" ")[1]
            signalList.append([
                name,
                canID
            ])

with open('output.csv', mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(signalList)

print("\nDone: output.csv\n")
