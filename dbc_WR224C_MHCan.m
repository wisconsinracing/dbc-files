function msg = dbc_WR224C_MHCan(msgname)
%msg = dbc_WR224C_MHCan(msgname);

msg = struct;

switch msgname

%===================================================================
case 'Bosch_MMU1'
WR224C.Bosch_MMU1 = struct;
WR224C.Bosch_MMU1.name = 'Bosch_MMU1';
WR224C.Bosch_MMU1.description = 'Bosch_MMU1';
WR224C.Bosch_MMU1.protocol = 'WR224C';
WR224C.Bosch_MMU1.id = hex2dec('00000174');
WR224C.Bosch_MMU1.idext = 'STANDARD';
WR224C.Bosch_MMU1.idinherit = 0;
WR224C.Bosch_MMU1.payload_size = 8;
WR224C.Bosch_MMU1.payload_value = [];
WR224C.Bosch_MMU1.payload_mask = [];
WR224C.Bosch_MMU1.interval = -1;

WR224C.Bosch_MMU1.fields{1}.name = 'YRS_YawRate';
WR224C.Bosch_MMU1.fields{1}.units = 'deg/s';
WR224C.Bosch_MMU1.fields{1}.start_bit = 56;
WR224C.Bosch_MMU1.fields{1}.bit_length = 16;
WR224C.Bosch_MMU1.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU1.fields{1}.maximum = 1.600000e+02;
WR224C.Bosch_MMU1.fields{1}.minimum = 4.294967e+09;
WR224C.Bosch_MMU1.fields{1}.scale = 5.000000e-03;
WR224C.Bosch_MMU1.fields{1}.offset = -1.638400e+02;

WR224C.Bosch_MMU1.fields{2}.name = 'YRS_Reserved2_x70';
WR224C.Bosch_MMU1.fields{2}.units = '';
WR224C.Bosch_MMU1.fields{2}.start_bit = 8;
WR224C.Bosch_MMU1.fields{2}.bit_length = 16;
WR224C.Bosch_MMU1.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU1.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU1.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU1.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU1.fields{2}.offset = 3.276800e+04;

WR224C.Bosch_MMU1.fields{3}.name = 'YRS_Reserved1_x70';
WR224C.Bosch_MMU1.fields{3}.units = '';
WR224C.Bosch_MMU1.fields{3}.start_bit = 40;
WR224C.Bosch_MMU1.fields{3}.bit_length = 16;
WR224C.Bosch_MMU1.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{3}.data_type = 'SIGNED';
WR224C.Bosch_MMU1.fields{3}.maximum = 3.481500e+04;
WR224C.Bosch_MMU1.fields{3}.minimum = -3.072000e+04;
WR224C.Bosch_MMU1.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_MMU1.fields{3}.offset = 3.276800e+04;

WR224C.Bosch_MMU1.fields{4}.name = 'YRS_Acc_Y_axis';
WR224C.Bosch_MMU1.fields{4}.units = 'g';
WR224C.Bosch_MMU1.fields{4}.start_bit = 24;
WR224C.Bosch_MMU1.fields{4}.bit_length = 16;
WR224C.Bosch_MMU1.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU1.fields{4}.maximum = 4.200000e+00;
WR224C.Bosch_MMU1.fields{4}.minimum = -4.200000e+00;
WR224C.Bosch_MMU1.fields{4}.scale = 1.240000e-04;
WR224C.Bosch_MMU1.fields{4}.offset = -4.074640e+00;

%===================================================================
case 'Bosch_MMU2'
WR224C.Bosch_MMU2 = struct;
WR224C.Bosch_MMU2.name = 'Bosch_MMU2';
WR224C.Bosch_MMU2.description = 'Bosch_MMU2';
WR224C.Bosch_MMU2.protocol = 'WR224C';
WR224C.Bosch_MMU2.id = hex2dec('00000178');
WR224C.Bosch_MMU2.idext = 'STANDARD';
WR224C.Bosch_MMU2.idinherit = 0;
WR224C.Bosch_MMU2.payload_size = 8;
WR224C.Bosch_MMU2.payload_value = [];
WR224C.Bosch_MMU2.payload_mask = [];
WR224C.Bosch_MMU2.interval = -1;

WR224C.Bosch_MMU2.fields{1}.name = 'YRS_Reserved2_x80';
WR224C.Bosch_MMU2.fields{1}.units = '';
WR224C.Bosch_MMU2.fields{1}.start_bit = 8;
WR224C.Bosch_MMU2.fields{1}.bit_length = 16;
WR224C.Bosch_MMU2.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{1}.data_type = 'SIGNED';
WR224C.Bosch_MMU2.fields{1}.maximum = 3.481500e+04;
WR224C.Bosch_MMU2.fields{1}.minimum = -3.072000e+04;
WR224C.Bosch_MMU2.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_MMU2.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_MMU2.fields{2}.name = 'YRS_Reserved1_x80';
WR224C.Bosch_MMU2.fields{2}.units = '';
WR224C.Bosch_MMU2.fields{2}.start_bit = 40;
WR224C.Bosch_MMU2.fields{2}.bit_length = 16;
WR224C.Bosch_MMU2.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU2.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU2.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU2.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU2.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_MMU2.fields{3}.name = 'YRS_Acc_X_axis';
WR224C.Bosch_MMU2.fields{3}.units = 'g';
WR224C.Bosch_MMU2.fields{3}.start_bit = 24;
WR224C.Bosch_MMU2.fields{3}.bit_length = 16;
WR224C.Bosch_MMU2.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU2.fields{3}.maximum = 4.200000e+00;
WR224C.Bosch_MMU2.fields{3}.minimum = -4.200000e+00;
WR224C.Bosch_MMU2.fields{3}.scale = 1.274000e-04;
WR224C.Bosch_MMU2.fields{3}.offset = -4.174640e+00;

WR224C.Bosch_MMU2.fields{4}.name = 'YRS_Roll_Rate';
WR224C.Bosch_MMU2.fields{4}.units = 'deg/s';
WR224C.Bosch_MMU2.fields{4}.start_bit = 56;
WR224C.Bosch_MMU2.fields{4}.bit_length = 16;
WR224C.Bosch_MMU2.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU2.fields{4}.maximum = 1.630000e+02;
WR224C.Bosch_MMU2.fields{4}.minimum = 4.294967e+09;
WR224C.Bosch_MMU2.fields{4}.scale = 5.000000e-03;
WR224C.Bosch_MMU2.fields{4}.offset = -1.638400e+02;

%===================================================================
case 'Bosch_MMU3'
WR224C.Bosch_MMU3 = struct;
WR224C.Bosch_MMU3.name = 'Bosch_MMU3';
WR224C.Bosch_MMU3.description = 'Bosch_MMU3';
WR224C.Bosch_MMU3.protocol = 'WR224C';
WR224C.Bosch_MMU3.id = hex2dec('0000017C');
WR224C.Bosch_MMU3.idext = 'STANDARD';
WR224C.Bosch_MMU3.idinherit = 0;
WR224C.Bosch_MMU3.payload_size = 8;
WR224C.Bosch_MMU3.payload_value = [];
WR224C.Bosch_MMU3.payload_mask = [];
WR224C.Bosch_MMU3.interval = -1;

WR224C.Bosch_MMU3.fields{1}.name = 'YRS_Acc_Z_axis';
WR224C.Bosch_MMU3.fields{1}.units = 'g';
WR224C.Bosch_MMU3.fields{1}.start_bit = 24;
WR224C.Bosch_MMU3.fields{1}.bit_length = 16;
WR224C.Bosch_MMU3.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU3.fields{1}.maximum = 4.200000e+00;
WR224C.Bosch_MMU3.fields{1}.minimum = -4.200000e+00;
WR224C.Bosch_MMU3.fields{1}.scale = 1.274000e-04;
WR224C.Bosch_MMU3.fields{1}.offset = -4.174650e+00;

WR224C.Bosch_MMU3.fields{2}.name = 'YRS_Reserved1_x90';
WR224C.Bosch_MMU3.fields{2}.units = '';
WR224C.Bosch_MMU3.fields{2}.start_bit = 56;
WR224C.Bosch_MMU3.fields{2}.bit_length = 16;
WR224C.Bosch_MMU3.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{2}.offset = 3.276800e+04;

WR224C.Bosch_MMU3.fields{3}.name = 'YRS_Reserved2_x90';
WR224C.Bosch_MMU3.fields{3}.units = '';
WR224C.Bosch_MMU3.fields{3}.start_bit = 40;
WR224C.Bosch_MMU3.fields{3}.bit_length = 16;
WR224C.Bosch_MMU3.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{3}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{3}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{3}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{3}.offset = 3.276800e+04;

WR224C.Bosch_MMU3.fields{4}.name = 'YRS_Reserved3_x90';
WR224C.Bosch_MMU3.fields{4}.units = '';
WR224C.Bosch_MMU3.fields{4}.start_bit = 8;
WR224C.Bosch_MMU3.fields{4}.bit_length = 16;
WR224C.Bosch_MMU3.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{4}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{4}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{4}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{4}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{4}.offset = 3.276800e+04;

%===================================================================
case 'Bosch_Steering_Angle_CONFIG'
WR224C.Bosch_Steering_Angle_CONFIG = struct;
WR224C.Bosch_Steering_Angle_CONFIG.name = 'Bosch_Steering_Angle_CONFIG';
WR224C.Bosch_Steering_Angle_CONFIG.description = 'Bosch_Steering_Angle_CONFIG';
WR224C.Bosch_Steering_Angle_CONFIG.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_CONFIG.id = hex2dec('000007C0');
WR224C.Bosch_Steering_Angle_CONFIG.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_CONFIG.idinherit = 0;
WR224C.Bosch_Steering_Angle_CONFIG.payload_size = 2;
WR224C.Bosch_Steering_Angle_CONFIG.payload_value = [];
WR224C.Bosch_Steering_Angle_CONFIG.payload_mask = [];
WR224C.Bosch_Steering_Angle_CONFIG.interval = -1;

WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.name = 'RES';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.start_bit = 60;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.bit_length = 12;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.maximum = 4.096000e+03;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.name = 'Codeword';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.start_bit = 56;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.bit_length = 4;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.maximum = 1.500000e+01;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Read'
WR224C.Bosch_Steering_Angle_Read = struct;
WR224C.Bosch_Steering_Angle_Read.name = 'Bosch_Steering_Angle_Read';
WR224C.Bosch_Steering_Angle_Read.description = 'Bosch_Steering_Angle_Read';
WR224C.Bosch_Steering_Angle_Read.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_Read.id = hex2dec('000002B0');
WR224C.Bosch_Steering_Angle_Read.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_Read.idinherit = 0;
WR224C.Bosch_Steering_Angle_Read.payload_size = 5;
WR224C.Bosch_Steering_Angle_Read.payload_value = [];
WR224C.Bosch_Steering_Angle_Read.payload_mask = [];
WR224C.Bosch_Steering_Angle_Read.interval = -1;

WR224C.Bosch_Steering_Angle_Read.fields{1}.name = 'TRIM';
WR224C.Bosch_Steering_Angle_Read.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{1}.start_bit = 34;
WR224C.Bosch_Steering_Angle_Read.fields{1}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{1}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{2}.name = 'Cal';
WR224C.Bosch_Steering_Angle_Read.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{2}.start_bit = 33;
WR224C.Bosch_Steering_Angle_Read.fields{2}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{2}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{3}.name = 'Ok_Status';
WR224C.Bosch_Steering_Angle_Read.fields{3}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{3}.start_bit = 32;
WR224C.Bosch_Steering_Angle_Read.fields{3}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{3}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{4}.name = 'SteeringAngle_Speed';
WR224C.Bosch_Steering_Angle_Read.fields{4}.units = '°/s';
WR224C.Bosch_Steering_Angle_Read.fields{4}.start_bit = 40;
WR224C.Bosch_Steering_Angle_Read.fields{4}.bit_length = 8;
WR224C.Bosch_Steering_Angle_Read.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{4}.maximum = 1.016000e+03;
WR224C.Bosch_Steering_Angle_Read.fields{4}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{4}.scale = 4.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{4}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{5}.name = 'SteeringAngle';
WR224C.Bosch_Steering_Angle_Read.fields{5}.units = '°';
WR224C.Bosch_Steering_Angle_Read.fields{5}.start_bit = 56;
WR224C.Bosch_Steering_Angle_Read.fields{5}.bit_length = 16;
WR224C.Bosch_Steering_Angle_Read.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{5}.data_type = 'SIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{5}.maximum = 7.800000e+02;
WR224C.Bosch_Steering_Angle_Read.fields{5}.minimum = -7.800000e+02;
WR224C.Bosch_Steering_Angle_Read.fields{5}.scale = 1.000000e-01;
WR224C.Bosch_Steering_Angle_Read.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Write'
WR224C.Bosch_Steering_Angle_Write = struct;
WR224C.Bosch_Steering_Angle_Write.name = 'Bosch_Steering_Angle_Write';
WR224C.Bosch_Steering_Angle_Write.description = 'Bosch_Steering_Angle_Write';
WR224C.Bosch_Steering_Angle_Write.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_Write.id = hex2dec('000002B1');
WR224C.Bosch_Steering_Angle_Write.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_Write.idinherit = 0;
WR224C.Bosch_Steering_Angle_Write.payload_size = 8;
WR224C.Bosch_Steering_Angle_Write.payload_value = [];
WR224C.Bosch_Steering_Angle_Write.payload_mask = [];
WR224C.Bosch_Steering_Angle_Write.interval = -1;

WR224C.Bosch_Steering_Angle_Write.fields{1}.name = 'TRIM';
WR224C.Bosch_Steering_Angle_Write.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{1}.start_bit = 34;
WR224C.Bosch_Steering_Angle_Write.fields{1}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{1}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{2}.name = 'Cal';
WR224C.Bosch_Steering_Angle_Write.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{2}.start_bit = 33;
WR224C.Bosch_Steering_Angle_Write.fields{2}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{2}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{3}.name = 'Ok_Status';
WR224C.Bosch_Steering_Angle_Write.fields{3}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{3}.start_bit = 32;
WR224C.Bosch_Steering_Angle_Write.fields{3}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{3}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{4}.name = 'SteeringAngle_Speed';
WR224C.Bosch_Steering_Angle_Write.fields{4}.units = '°/s';
WR224C.Bosch_Steering_Angle_Write.fields{4}.start_bit = 40;
WR224C.Bosch_Steering_Angle_Write.fields{4}.bit_length = 8;
WR224C.Bosch_Steering_Angle_Write.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{4}.maximum = 1.016000e+03;
WR224C.Bosch_Steering_Angle_Write.fields{4}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{4}.scale = 4.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{4}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{5}.name = 'SteeringAngle';
WR224C.Bosch_Steering_Angle_Write.fields{5}.units = '°';
WR224C.Bosch_Steering_Angle_Write.fields{5}.start_bit = 56;
WR224C.Bosch_Steering_Angle_Write.fields{5}.bit_length = 16;
WR224C.Bosch_Steering_Angle_Write.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{5}.data_type = 'SIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{5}.maximum = 7.800000e+02;
WR224C.Bosch_Steering_Angle_Write.fields{5}.minimum = -7.800000e+02;
WR224C.Bosch_Steering_Angle_Write.fields{5}.scale = 1.000000e-01;
WR224C.Bosch_Steering_Angle_Write.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Brake_Rotor_Temp1'
WR224C.Brake_Rotor_Temp1 = struct;
WR224C.Brake_Rotor_Temp1.name = 'Brake_Rotor_Temp1';
WR224C.Brake_Rotor_Temp1.description = 'Brake_Rotor_Temp1';
WR224C.Brake_Rotor_Temp1.protocol = 'WR224C';
WR224C.Brake_Rotor_Temp1.id = hex2dec('000004C4');
WR224C.Brake_Rotor_Temp1.idext = 'STANDARD';
WR224C.Brake_Rotor_Temp1.idinherit = 0;
WR224C.Brake_Rotor_Temp1.payload_size = 8;
WR224C.Brake_Rotor_Temp1.payload_value = [];
WR224C.Brake_Rotor_Temp1.payload_mask = [];
WR224C.Brake_Rotor_Temp1.interval = -1;

WR224C.Brake_Rotor_Temp1.fields{1}.name = 'Rotor_Temp_1';
WR224C.Brake_Rotor_Temp1.fields{1}.units = '';
WR224C.Brake_Rotor_Temp1.fields{1}.start_bit = 48;
WR224C.Brake_Rotor_Temp1.fields{1}.bit_length = 16;
WR224C.Brake_Rotor_Temp1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp1.fields{1}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp1.fields{1}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp1.fields{1}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp1.fields{1}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp1.fields{1}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp1.fields{2}.name = 'Rotor_Temp_2';
WR224C.Brake_Rotor_Temp1.fields{2}.units = '';
WR224C.Brake_Rotor_Temp1.fields{2}.start_bit = 32;
WR224C.Brake_Rotor_Temp1.fields{2}.bit_length = 16;
WR224C.Brake_Rotor_Temp1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp1.fields{2}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp1.fields{2}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp1.fields{2}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp1.fields{2}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp1.fields{2}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp1.fields{3}.name = 'Rotor_Temp_3';
WR224C.Brake_Rotor_Temp1.fields{3}.units = '';
WR224C.Brake_Rotor_Temp1.fields{3}.start_bit = 16;
WR224C.Brake_Rotor_Temp1.fields{3}.bit_length = 16;
WR224C.Brake_Rotor_Temp1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp1.fields{3}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp1.fields{3}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp1.fields{3}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp1.fields{3}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp1.fields{3}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp1.fields{4}.name = 'Rotor_Temp_4';
WR224C.Brake_Rotor_Temp1.fields{4}.units = '';
WR224C.Brake_Rotor_Temp1.fields{4}.start_bit = 0;
WR224C.Brake_Rotor_Temp1.fields{4}.bit_length = 16;
WR224C.Brake_Rotor_Temp1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp1.fields{4}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp1.fields{4}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp1.fields{4}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp1.fields{4}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp1.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp2'
WR224C.Brake_Rotor_Temp2 = struct;
WR224C.Brake_Rotor_Temp2.name = 'Brake_Rotor_Temp2';
WR224C.Brake_Rotor_Temp2.description = 'Brake_Rotor_Temp2';
WR224C.Brake_Rotor_Temp2.protocol = 'WR224C';
WR224C.Brake_Rotor_Temp2.id = hex2dec('000004C5');
WR224C.Brake_Rotor_Temp2.idext = 'STANDARD';
WR224C.Brake_Rotor_Temp2.idinherit = 0;
WR224C.Brake_Rotor_Temp2.payload_size = 8;
WR224C.Brake_Rotor_Temp2.payload_value = [];
WR224C.Brake_Rotor_Temp2.payload_mask = [];
WR224C.Brake_Rotor_Temp2.interval = -1;

WR224C.Brake_Rotor_Temp2.fields{1}.name = 'Rotor_Temp_5';
WR224C.Brake_Rotor_Temp2.fields{1}.units = '';
WR224C.Brake_Rotor_Temp2.fields{1}.start_bit = 48;
WR224C.Brake_Rotor_Temp2.fields{1}.bit_length = 16;
WR224C.Brake_Rotor_Temp2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp2.fields{1}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp2.fields{1}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp2.fields{1}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp2.fields{1}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp2.fields{1}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp2.fields{2}.name = 'Rotor_Temp_6';
WR224C.Brake_Rotor_Temp2.fields{2}.units = '';
WR224C.Brake_Rotor_Temp2.fields{2}.start_bit = 32;
WR224C.Brake_Rotor_Temp2.fields{2}.bit_length = 16;
WR224C.Brake_Rotor_Temp2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp2.fields{2}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp2.fields{2}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp2.fields{2}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp2.fields{2}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp2.fields{2}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp2.fields{3}.name = 'Rotor_Temp_7';
WR224C.Brake_Rotor_Temp2.fields{3}.units = '';
WR224C.Brake_Rotor_Temp2.fields{3}.start_bit = 16;
WR224C.Brake_Rotor_Temp2.fields{3}.bit_length = 16;
WR224C.Brake_Rotor_Temp2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp2.fields{3}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp2.fields{3}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp2.fields{3}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp2.fields{3}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp2.fields{3}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp2.fields{4}.name = 'Rotor_Temp_8';
WR224C.Brake_Rotor_Temp2.fields{4}.units = '';
WR224C.Brake_Rotor_Temp2.fields{4}.start_bit = 0;
WR224C.Brake_Rotor_Temp2.fields{4}.bit_length = 16;
WR224C.Brake_Rotor_Temp2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp2.fields{4}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp2.fields{4}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp2.fields{4}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp2.fields{4}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp2.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'Brake_Rotor_Temp3'
WR224C.Brake_Rotor_Temp3 = struct;
WR224C.Brake_Rotor_Temp3.name = 'Brake_Rotor_Temp3';
WR224C.Brake_Rotor_Temp3.description = 'Brake_Rotor_Temp3';
WR224C.Brake_Rotor_Temp3.protocol = 'WR224C';
WR224C.Brake_Rotor_Temp3.id = hex2dec('000004C6');
WR224C.Brake_Rotor_Temp3.idext = 'STANDARD';
WR224C.Brake_Rotor_Temp3.idinherit = 0;
WR224C.Brake_Rotor_Temp3.payload_size = 8;
WR224C.Brake_Rotor_Temp3.payload_value = [];
WR224C.Brake_Rotor_Temp3.payload_mask = [];
WR224C.Brake_Rotor_Temp3.interval = -1;

WR224C.Brake_Rotor_Temp3.fields{1}.name = 'Rotor_Temp_9';
WR224C.Brake_Rotor_Temp3.fields{1}.units = '';
WR224C.Brake_Rotor_Temp3.fields{1}.start_bit = 48;
WR224C.Brake_Rotor_Temp3.fields{1}.bit_length = 16;
WR224C.Brake_Rotor_Temp3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp3.fields{1}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp3.fields{1}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp3.fields{1}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp3.fields{1}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp3.fields{1}.offset = 0.000000e+00;

WR224C.Brake_Rotor_Temp3.fields{2}.name = 'Rotor_Temp_10';
WR224C.Brake_Rotor_Temp3.fields{2}.units = '';
WR224C.Brake_Rotor_Temp3.fields{2}.start_bit = 32;
WR224C.Brake_Rotor_Temp3.fields{2}.bit_length = 16;
WR224C.Brake_Rotor_Temp3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp3.fields{2}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp3.fields{2}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp3.fields{2}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp3.fields{2}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp3.fields{2}.offset = 0.000000e+00;

WR224C.Brake_Rotor_Temp3.fields{3}.name = 'Rotor_Temp_11';
WR224C.Brake_Rotor_Temp3.fields{3}.units = '';
WR224C.Brake_Rotor_Temp3.fields{3}.start_bit = 16;
WR224C.Brake_Rotor_Temp3.fields{3}.bit_length = 16;
WR224C.Brake_Rotor_Temp3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp3.fields{3}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp3.fields{3}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp3.fields{3}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp3.fields{3}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp3.fields{3}.offset = 0.000000e+00;

WR224C.Brake_Rotor_Temp3.fields{4}.name = 'Rotor_Temp_12';
WR224C.Brake_Rotor_Temp3.fields{4}.units = '';
WR224C.Brake_Rotor_Temp3.fields{4}.start_bit = 0;
WR224C.Brake_Rotor_Temp3.fields{4}.bit_length = 16;
WR224C.Brake_Rotor_Temp3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp3.fields{4}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp3.fields{4}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp3.fields{4}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp3.fields{4}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp3.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'Brake_Rotor_Temp4'
WR224C.Brake_Rotor_Temp4 = struct;
WR224C.Brake_Rotor_Temp4.name = 'Brake_Rotor_Temp4';
WR224C.Brake_Rotor_Temp4.description = 'Brake_Rotor_Temp4';
WR224C.Brake_Rotor_Temp4.protocol = 'WR224C';
WR224C.Brake_Rotor_Temp4.id = hex2dec('000004C7');
WR224C.Brake_Rotor_Temp4.idext = 'STANDARD';
WR224C.Brake_Rotor_Temp4.idinherit = 0;
WR224C.Brake_Rotor_Temp4.payload_size = 8;
WR224C.Brake_Rotor_Temp4.payload_value = [];
WR224C.Brake_Rotor_Temp4.payload_mask = [];
WR224C.Brake_Rotor_Temp4.interval = -1;

WR224C.Brake_Rotor_Temp4.fields{1}.name = 'Rotor_Temp_13';
WR224C.Brake_Rotor_Temp4.fields{1}.units = '';
WR224C.Brake_Rotor_Temp4.fields{1}.start_bit = 48;
WR224C.Brake_Rotor_Temp4.fields{1}.bit_length = 16;
WR224C.Brake_Rotor_Temp4.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp4.fields{1}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp4.fields{1}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp4.fields{1}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp4.fields{1}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp4.fields{1}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp4.fields{2}.name = 'Rotor_Temp_14';
WR224C.Brake_Rotor_Temp4.fields{2}.units = '';
WR224C.Brake_Rotor_Temp4.fields{2}.start_bit = 32;
WR224C.Brake_Rotor_Temp4.fields{2}.bit_length = 16;
WR224C.Brake_Rotor_Temp4.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp4.fields{2}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp4.fields{2}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp4.fields{2}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp4.fields{2}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp4.fields{2}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp4.fields{3}.name = 'Rotor_Temp_15';
WR224C.Brake_Rotor_Temp4.fields{3}.units = '';
WR224C.Brake_Rotor_Temp4.fields{3}.start_bit = 16;
WR224C.Brake_Rotor_Temp4.fields{3}.bit_length = 16;
WR224C.Brake_Rotor_Temp4.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp4.fields{3}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp4.fields{3}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp4.fields{3}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp4.fields{3}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp4.fields{3}.offset = -1.000000e+02;

WR224C.Brake_Rotor_Temp4.fields{4}.name = 'Rotor_Temp_16';
WR224C.Brake_Rotor_Temp4.fields{4}.units = '';
WR224C.Brake_Rotor_Temp4.fields{4}.start_bit = 0;
WR224C.Brake_Rotor_Temp4.fields{4}.bit_length = 16;
WR224C.Brake_Rotor_Temp4.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.Brake_Rotor_Temp4.fields{4}.data_type = 'UNSIGNED';
WR224C.Brake_Rotor_Temp4.fields{4}.maximum = 6.452000e+03;
WR224C.Brake_Rotor_Temp4.fields{4}.minimum = 0.000000e+00;
WR224C.Brake_Rotor_Temp4.fields{4}.scale = 1.000000e-01;
WR224C.Brake_Rotor_Temp4.fields{4}.offset = -1.000000e+02;

%===================================================================
case 'CamPhaseSense'
WR224C.CamPhaseSense = struct;
WR224C.CamPhaseSense.name = 'CamPhaseSense';
WR224C.CamPhaseSense.description = 'CamPhaseSense';
WR224C.CamPhaseSense.protocol = 'WR224C';
WR224C.CamPhaseSense.id = hex2dec('00000005');
WR224C.CamPhaseSense.idext = 'STANDARD';
WR224C.CamPhaseSense.idinherit = 0;
WR224C.CamPhaseSense.payload_size = 8;
WR224C.CamPhaseSense.payload_value = [];
WR224C.CamPhaseSense.payload_mask = [];
WR224C.CamPhaseSense.interval = -1;

WR224C.CamPhaseSense.fields{1}.name = 'DegCA';
WR224C.CamPhaseSense.fields{1}.units = 'Deg';
WR224C.CamPhaseSense.fields{1}.start_bit = 55;
WR224C.CamPhaseSense.fields{1}.bit_length = 9;
WR224C.CamPhaseSense.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{1}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{1}.scale = 2.000000e+00;
WR224C.CamPhaseSense.fields{1}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{2}.name = 'CamPhaseSMState';
WR224C.CamPhaseSense.fields{2}.units = '';
WR224C.CamPhaseSense.fields{2}.start_bit = 51;
WR224C.CamPhaseSense.fields{2}.bit_length = 4;
WR224C.CamPhaseSense.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{2}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{2}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{2}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{3}.name = 'CrankTDCs';
WR224C.CamPhaseSense.fields{3}.units = '';
WR224C.CamPhaseSense.fields{3}.start_bit = 43;
WR224C.CamPhaseSense.fields{3}.bit_length = 8;
WR224C.CamPhaseSense.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{3}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{3}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{3}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{4}.name = 'EngineState';
WR224C.CamPhaseSense.fields{4}.units = '';
WR224C.CamPhaseSense.fields{4}.start_bit = 40;
WR224C.CamPhaseSense.fields{4}.bit_length = 3;
WR224C.CamPhaseSense.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{4}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{4}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{4}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{5}.name = 'CamPhaseChange';
WR224C.CamPhaseSense.fields{5}.units = '';
WR224C.CamPhaseSense.fields{5}.start_bit = 39;
WR224C.CamPhaseSense.fields{5}.bit_length = 1;
WR224C.CamPhaseSense.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{5}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{5}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{5}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{6}.name = 'CamPhaseChangeDone';
WR224C.CamPhaseSense.fields{6}.units = '';
WR224C.CamPhaseSense.fields{6}.start_bit = 38;
WR224C.CamPhaseSense.fields{6}.bit_length = 1;
WR224C.CamPhaseSense.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{6}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{6}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{6}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{7}.name = 'LatchedCamPhaseChange';
WR224C.CamPhaseSense.fields{7}.units = '';
WR224C.CamPhaseSense.fields{7}.start_bit = 37;
WR224C.CamPhaseSense.fields{7}.bit_length = 1;
WR224C.CamPhaseSense.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{7}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{7}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{7}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{8}.name = 'CamBool3';
WR224C.CamPhaseSense.fields{8}.units = '';
WR224C.CamPhaseSense.fields{8}.start_bit = 36;
WR224C.CamPhaseSense.fields{8}.bit_length = 1;
WR224C.CamPhaseSense.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{8}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{8}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{8}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{9}.name = 'ModDegCA';
WR224C.CamPhaseSense.fields{9}.units = 'Deg';
WR224C.CamPhaseSense.fields{9}.start_bit = 26;
WR224C.CamPhaseSense.fields{9}.bit_length = 10;
WR224C.CamPhaseSense.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{9}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{9}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{9}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{10}.name = 'RPM_Inst';
WR224C.CamPhaseSense.fields{10}.units = 'RPM';
WR224C.CamPhaseSense.fields{10}.start_bit = 12;
WR224C.CamPhaseSense.fields{10}.bit_length = 14;
WR224C.CamPhaseSense.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{10}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{10}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{10}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{11}.name = 'TimesThrough';
WR224C.CamPhaseSense.fields{11}.units = 'keer';
WR224C.CamPhaseSense.fields{11}.start_bit = 9;
WR224C.CamPhaseSense.fields{11}.bit_length = 3;
WR224C.CamPhaseSense.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{11}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{11}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{11}.offset = 0.000000e+00;

WR224C.CamPhaseSense.fields{12}.name = 'AvgRPM';
WR224C.CamPhaseSense.fields{12}.units = 'RPM';
WR224C.CamPhaseSense.fields{12}.start_bit = 0;
WR224C.CamPhaseSense.fields{12}.bit_length = 9;
WR224C.CamPhaseSense.fields{12}.byte_order = 'BIG_ENDIAN';
WR224C.CamPhaseSense.fields{12}.data_type = 'UNSIGNED';
WR224C.CamPhaseSense.fields{12}.scale = 1.000000e+00;
WR224C.CamPhaseSense.fields{12}.offset = 0.000000e+00;

%===================================================================
case 'Dyno_Signals'
WR224C.Dyno_Signals = struct;
WR224C.Dyno_Signals.name = 'Dyno_Signals';
WR224C.Dyno_Signals.description = 'Dyno_Signals';
WR224C.Dyno_Signals.protocol = 'WR224C';
WR224C.Dyno_Signals.id = hex2dec('0000002E');
WR224C.Dyno_Signals.idext = 'STANDARD';
WR224C.Dyno_Signals.idinherit = 0;
WR224C.Dyno_Signals.payload_size = 8;
WR224C.Dyno_Signals.payload_value = [];
WR224C.Dyno_Signals.payload_mask = [];
WR224C.Dyno_Signals.interval = -1;

WR224C.Dyno_Signals.fields{1}.name = 'Ambient_Temp';
WR224C.Dyno_Signals.fields{1}.units = '';
WR224C.Dyno_Signals.fields{1}.start_bit = 15;
WR224C.Dyno_Signals.fields{1}.bit_length = 9;
WR224C.Dyno_Signals.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{1}.data_type = 'SIGNED';
WR224C.Dyno_Signals.fields{1}.maximum = 1.000000e+02;
WR224C.Dyno_Signals.fields{1}.minimum = 0.000000e+00;
WR224C.Dyno_Signals.fields{1}.scale = 3.906000e-01;
WR224C.Dyno_Signals.fields{1}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{2}.name = 'EGT1';
WR224C.Dyno_Signals.fields{2}.units = '';
WR224C.Dyno_Signals.fields{2}.start_bit = 21;
WR224C.Dyno_Signals.fields{2}.bit_length = 10;
WR224C.Dyno_Signals.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{2}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{2}.maximum = 1.000000e+03;
WR224C.Dyno_Signals.fields{2}.minimum = 0.000000e+00;
WR224C.Dyno_Signals.fields{2}.scale = 1.000000e+00;
WR224C.Dyno_Signals.fields{2}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{3}.name = 'Dyno_Torque';
WR224C.Dyno_Signals.fields{3}.units = '';
WR224C.Dyno_Signals.fields{3}.start_bit = 43;
WR224C.Dyno_Signals.fields{3}.bit_length = 16;
WR224C.Dyno_Signals.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{3}.data_type = 'SIGNED';
WR224C.Dyno_Signals.fields{3}.scale = 6.103520e-03;
WR224C.Dyno_Signals.fields{3}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{4}.name = 'RPM_Setpoint';
WR224C.Dyno_Signals.fields{4}.units = '';
WR224C.Dyno_Signals.fields{4}.start_bit = 62;
WR224C.Dyno_Signals.fields{4}.bit_length = 13;
WR224C.Dyno_Signals.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{4}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{4}.maximum = 1.200000e+04;
WR224C.Dyno_Signals.fields{4}.minimum = 0.000000e+00;
WR224C.Dyno_Signals.fields{4}.scale = 1.000000e+00;
WR224C.Dyno_Signals.fields{4}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{5}.name = 'EGT2';
WR224C.Dyno_Signals.fields{5}.units = '';
WR224C.Dyno_Signals.fields{5}.start_bit = 27;
WR224C.Dyno_Signals.fields{5}.bit_length = 10;
WR224C.Dyno_Signals.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{5}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{5}.maximum = 1.000000e+03;
WR224C.Dyno_Signals.fields{5}.minimum = 0.000000e+00;
WR224C.Dyno_Signals.fields{5}.scale = 1.000000e+00;
WR224C.Dyno_Signals.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Dyno_Signals_two'
WR224C.Dyno_Signals_two = struct;
WR224C.Dyno_Signals_two.name = 'Dyno_Signals_two';
WR224C.Dyno_Signals_two.description = 'Dyno_Signals_two';
WR224C.Dyno_Signals_two.protocol = 'WR224C';
WR224C.Dyno_Signals_two.id = hex2dec('00000006');
WR224C.Dyno_Signals_two.idext = 'STANDARD';
WR224C.Dyno_Signals_two.idinherit = 0;
WR224C.Dyno_Signals_two.payload_size = 8;
WR224C.Dyno_Signals_two.payload_value = [];
WR224C.Dyno_Signals_two.payload_mask = [];
WR224C.Dyno_Signals_two.interval = -1;

WR224C.Dyno_Signals_two.fields{1}.name = 'DynoPTerm';
WR224C.Dyno_Signals_two.fields{1}.units = '';
WR224C.Dyno_Signals_two.fields{1}.start_bit = 56;
WR224C.Dyno_Signals_two.fields{1}.bit_length = 10;
WR224C.Dyno_Signals_two.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals_two.fields{1}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals_two.fields{1}.scale = 1.000000e-02;
WR224C.Dyno_Signals_two.fields{1}.offset = 0.000000e+00;

WR224C.Dyno_Signals_two.fields{2}.name = 'DynoITerm';
WR224C.Dyno_Signals_two.fields{2}.units = '';
WR224C.Dyno_Signals_two.fields{2}.start_bit = 50;
WR224C.Dyno_Signals_two.fields{2}.bit_length = 10;
WR224C.Dyno_Signals_two.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals_two.fields{2}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals_two.fields{2}.scale = 1.000000e-02;
WR224C.Dyno_Signals_two.fields{2}.offset = 0.000000e+00;

WR224C.Dyno_Signals_two.fields{3}.name = 'DynoDTerm';
WR224C.Dyno_Signals_two.fields{3}.units = '';
WR224C.Dyno_Signals_two.fields{3}.start_bit = 44;
WR224C.Dyno_Signals_two.fields{3}.bit_length = 10;
WR224C.Dyno_Signals_two.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals_two.fields{3}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals_two.fields{3}.scale = 1.000000e-02;
WR224C.Dyno_Signals_two.fields{3}.offset = 0.000000e+00;

WR224C.Dyno_Signals_two.fields{4}.name = 'DynoVoltage';
WR224C.Dyno_Signals_two.fields{4}.units = '';
WR224C.Dyno_Signals_two.fields{4}.start_bit = 38;
WR224C.Dyno_Signals_two.fields{4}.bit_length = 10;
WR224C.Dyno_Signals_two.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals_two.fields{4}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals_two.fields{4}.scale = 1.000000e-02;
WR224C.Dyno_Signals_two.fields{4}.offset = 0.000000e+00;

WR224C.Dyno_Signals_two.fields{5}.name = 'DynoRPMSetpoint';
WR224C.Dyno_Signals_two.fields{5}.units = '';
WR224C.Dyno_Signals_two.fields{5}.start_bit = 16;
WR224C.Dyno_Signals_two.fields{5}.bit_length = 14;
WR224C.Dyno_Signals_two.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals_two.fields{5}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals_two.fields{5}.scale = 1.000000e+00;
WR224C.Dyno_Signals_two.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'EVO_ACC_0x33'
WR224C.EVO_ACC_0x33 = struct;
WR224C.EVO_ACC_0x33.name = 'EVO_ACC_0x33';
WR224C.EVO_ACC_0x33.description = 'EVO_ACC_0x33';
WR224C.EVO_ACC_0x33.protocol = 'WR224C';
WR224C.EVO_ACC_0x33.id = hex2dec('00000033');
WR224C.EVO_ACC_0x33.idext = 'STANDARD';
WR224C.EVO_ACC_0x33.idinherit = 0;
WR224C.EVO_ACC_0x33.payload_size = 8;
WR224C.EVO_ACC_0x33.payload_value = [];
WR224C.EVO_ACC_0x33.payload_mask = [];
WR224C.EVO_ACC_0x33.interval = -1;

WR224C.EVO_ACC_0x33.fields{1}.name = 'EVO_LatAcc';
WR224C.EVO_ACC_0x33.fields{1}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{1}.start_bit = 0;
WR224C.EVO_ACC_0x33.fields{1}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{1}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{1}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{1}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{1}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{1}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{2}.name = 'EVO_VertAcc';
WR224C.EVO_ACC_0x33.fields{2}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{2}.start_bit = 8;
WR224C.EVO_ACC_0x33.fields{2}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{2}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{2}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{2}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{2}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{2}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{3}.name = 'EVO_InlineAcc';
WR224C.EVO_ACC_0x33.fields{3}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{3}.start_bit = 16;
WR224C.EVO_ACC_0x33.fields{3}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{3}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{3}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{3}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{3}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{3}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{4}.name = 'EVO_YawRate';
WR224C.EVO_ACC_0x33.fields{4}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{4}.start_bit = 32;
WR224C.EVO_ACC_0x33.fields{4}.bit_length = 32;
WR224C.EVO_ACC_0x33.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{4}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{4}.maximum = 2.147480e+07;
WR224C.EVO_ACC_0x33.fields{4}.minimum = -2.147480e+07;
WR224C.EVO_ACC_0x33.fields{4}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'EVO_GPS'
WR224C.EVO_GPS = struct;
WR224C.EVO_GPS.name = 'EVO_GPS';
WR224C.EVO_GPS.description = 'EVO_GPS';
WR224C.EVO_GPS.protocol = 'WR224C';
WR224C.EVO_GPS.id = hex2dec('00000032');
WR224C.EVO_GPS.idext = 'STANDARD';
WR224C.EVO_GPS.idinherit = 0;
WR224C.EVO_GPS.payload_size = 8;
WR224C.EVO_GPS.payload_value = [];
WR224C.EVO_GPS.payload_mask = [];
WR224C.EVO_GPS.interval = -1;

WR224C.EVO_GPS.fields{1}.name = 'EVO_GPS_Speed';
WR224C.EVO_GPS.fields{1}.units = 'kmh';
WR224C.EVO_GPS.fields{1}.start_bit = 32;
WR224C.EVO_GPS.fields{1}.bit_length = 32;
WR224C.EVO_GPS.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{1}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{1}.offset = 0.000000e+00;

WR224C.EVO_GPS.fields{2}.name = 'EVO_GPS_Pos_Acc';
WR224C.EVO_GPS.fields{2}.units = '';
WR224C.EVO_GPS.fields{2}.start_bit = 16;
WR224C.EVO_GPS.fields{2}.bit_length = 16;
WR224C.EVO_GPS.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{2}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{2}.offset = 0.000000e+00;

WR224C.EVO_GPS.fields{3}.name = 'EVO_GPS_Speed_Acc';
WR224C.EVO_GPS.fields{3}.units = '';
WR224C.EVO_GPS.fields{3}.start_bit = 0;
WR224C.EVO_GPS.fields{3}.bit_length = 16;
WR224C.EVO_GPS.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{3}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{3}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'EVO_LAP'
WR224C.EVO_LAP = struct;
WR224C.EVO_LAP.name = 'EVO_LAP';
WR224C.EVO_LAP.description = 'EVO_LAP';
WR224C.EVO_LAP.protocol = 'WR224C';
WR224C.EVO_LAP.id = hex2dec('00000034');
WR224C.EVO_LAP.idext = 'STANDARD';
WR224C.EVO_LAP.idinherit = 0;
WR224C.EVO_LAP.payload_size = 8;
WR224C.EVO_LAP.payload_value = [];
WR224C.EVO_LAP.payload_mask = [];
WR224C.EVO_LAP.interval = -1;

WR224C.EVO_LAP.fields{1}.name = 'EVO_LapTime';
WR224C.EVO_LAP.fields{1}.units = '';
WR224C.EVO_LAP.fields{1}.start_bit = 32;
WR224C.EVO_LAP.fields{1}.bit_length = 32;
WR224C.EVO_LAP.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_LAP.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_LAP.fields{1}.scale = 1.000000e+00;
WR224C.EVO_LAP.fields{1}.offset = 0.000000e+00;

WR224C.EVO_LAP.fields{2}.name = 'EVO_Lap_Num';
WR224C.EVO_LAP.fields{2}.units = '';
WR224C.EVO_LAP.fields{2}.start_bit = 0;
WR224C.EVO_LAP.fields{2}.bit_length = 32;
WR224C.EVO_LAP.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_LAP.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_LAP.fields{2}.scale = 1.000000e+00;
WR224C.EVO_LAP.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'EVO_WS_0X35'
WR224C.EVO_WS_0X35 = struct;
WR224C.EVO_WS_0X35.name = 'EVO_WS_0X35';
WR224C.EVO_WS_0X35.description = 'EVO_WS_0X35';
WR224C.EVO_WS_0X35.protocol = 'WR224C';
WR224C.EVO_WS_0X35.id = hex2dec('00000035');
WR224C.EVO_WS_0X35.idext = 'STANDARD';
WR224C.EVO_WS_0X35.idinherit = 0;
WR224C.EVO_WS_0X35.payload_size = 8;
WR224C.EVO_WS_0X35.payload_value = [];
WR224C.EVO_WS_0X35.payload_mask = [];
WR224C.EVO_WS_0X35.interval = -1;

WR224C.EVO_WS_0X35.fields{1}.name = 'EVO_WS_FL';
WR224C.EVO_WS_0X35.fields{1}.units = 'mph';
WR224C.EVO_WS_0X35.fields{1}.start_bit = 48;
WR224C.EVO_WS_0X35.fields{1}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{1}.scale = 1.000000e-01;
WR224C.EVO_WS_0X35.fields{1}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{2}.name = 'EVO_WS_FR';
WR224C.EVO_WS_0X35.fields{2}.units = 'mph';
WR224C.EVO_WS_0X35.fields{2}.start_bit = 32;
WR224C.EVO_WS_0X35.fields{2}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{2}.scale = 1.000000e-01;
WR224C.EVO_WS_0X35.fields{2}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{3}.name = 'EVO_WS_RL';
WR224C.EVO_WS_0X35.fields{3}.units = 'mph';
WR224C.EVO_WS_0X35.fields{3}.start_bit = 16;
WR224C.EVO_WS_0X35.fields{3}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{3}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{3}.scale = 1.000000e-01;
WR224C.EVO_WS_0X35.fields{3}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{4}.name = 'EVO_WS_RR';
WR224C.EVO_WS_0X35.fields{4}.units = 'mph';
WR224C.EVO_WS_0X35.fields{4}.start_bit = 0;
WR224C.EVO_WS_0X35.fields{4}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{4}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{4}.scale = 1.000000e-01;
WR224C.EVO_WS_0X35.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_1'
WR224C.PCM_A_1 = struct;
WR224C.PCM_A_1.name = 'PCM_A_1';
WR224C.PCM_A_1.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_1.protocol = 'WR224C';
WR224C.PCM_A_1.id = hex2dec('000000DC');
WR224C.PCM_A_1.idext = 'STANDARD';
WR224C.PCM_A_1.idinherit = 0;
WR224C.PCM_A_1.payload_size = 8;
WR224C.PCM_A_1.payload_value = [];
WR224C.PCM_A_1.payload_mask = [];
WR224C.PCM_A_1.interval = -1;

WR224C.PCM_A_1.fields{1}.name = 'Is_Real_RPM';
WR224C.PCM_A_1.fields{1}.units = 'bool';
WR224C.PCM_A_1.fields{1}.start_bit = 58;
WR224C.PCM_A_1.fields{1}.bit_length = 1;
WR224C.PCM_A_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{1}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{1}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{2}.name = 'RPM';
WR224C.PCM_A_1.fields{2}.units = 'RPM';
WR224C.PCM_A_1.fields{2}.start_bit = 32;
WR224C.PCM_A_1.fields{2}.bit_length = 16;
WR224C.PCM_A_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{2}.maximum = 1.300000e+04;
WR224C.PCM_A_1.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{2}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{3}.name = 'Plenum_MAP';
WR224C.PCM_A_1.fields{3}.units = 'kPa';
WR224C.PCM_A_1.fields{3}.start_bit = 48;
WR224C.PCM_A_1.fields{3}.bit_length = 10;
WR224C.PCM_A_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{3}.maximum = 1.023000e+02;
WR224C.PCM_A_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{3}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{4}.name = 'VE_Adapt_Enable';
WR224C.PCM_A_1.fields{4}.units = 'bool';
WR224C.PCM_A_1.fields{4}.start_bit = 59;
WR224C.PCM_A_1.fields{4}.bit_length = 1;
WR224C.PCM_A_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{4}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{4}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{5}.name = 'Steady_State_Op';
WR224C.PCM_A_1.fields{5}.units = 'bool';
WR224C.PCM_A_1.fields{5}.start_bit = 60;
WR224C.PCM_A_1.fields{5}.bit_length = 1;
WR224C.PCM_A_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{5}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{5}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{6}.name = 'EStop';
WR224C.PCM_A_1.fields{6}.units = 'bool';
WR224C.PCM_A_1.fields{6}.start_bit = 61;
WR224C.PCM_A_1.fields{6}.bit_length = 1;
WR224C.PCM_A_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{6}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{6}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{7}.name = 'Check_Eng_Light';
WR224C.PCM_A_1.fields{7}.units = 'bool';
WR224C.PCM_A_1.fields{7}.start_bit = 62;
WR224C.PCM_A_1.fields{7}.bit_length = 1;
WR224C.PCM_A_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{7}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{7}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{8}.name = 'TPS2';
WR224C.PCM_A_1.fields{8}.units = '%';
WR224C.PCM_A_1.fields{8}.start_bit = 22;
WR224C.PCM_A_1.fields{8}.bit_length = 10;
WR224C.PCM_A_1.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{8}.maximum = 1.000000e+02;
WR224C.PCM_A_1.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{8}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{8}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{9}.name = 'TPS1';
WR224C.PCM_A_1.fields{9}.units = '%';
WR224C.PCM_A_1.fields{9}.start_bit = 12;
WR224C.PCM_A_1.fields{9}.bit_length = 10;
WR224C.PCM_A_1.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{9}.maximum = 1.000000e+02;
WR224C.PCM_A_1.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{9}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{9}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{10}.name = 'Voltage';
WR224C.PCM_A_1.fields{10}.units = 'V';
WR224C.PCM_A_1.fields{10}.start_bit = 4;
WR224C.PCM_A_1.fields{10}.bit_length = 8;
WR224C.PCM_A_1.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{10}.maximum = 1.600000e+01;
WR224C.PCM_A_1.fields{10}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{10}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{10}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{11}.name = 'Gear';
WR224C.PCM_A_1.fields{11}.units = '#';
WR224C.PCM_A_1.fields{11}.start_bit = 0;
WR224C.PCM_A_1.fields{11}.bit_length = 4;
WR224C.PCM_A_1.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{11}.maximum = 1.000000e+01;
WR224C.PCM_A_1.fields{11}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{11}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{11}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{12}.name = 'Fuel_Pump_Req';
WR224C.PCM_A_1.fields{12}.units = 'bool';
WR224C.PCM_A_1.fields{12}.start_bit = 63;
WR224C.PCM_A_1.fields{12}.bit_length = 1;
WR224C.PCM_A_1.fields{12}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{12}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{12}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{12}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{12}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{12}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_2'
WR224C.PCM_A_2 = struct;
WR224C.PCM_A_2.name = 'PCM_A_2';
WR224C.PCM_A_2.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_2.protocol = 'WR224C';
WR224C.PCM_A_2.id = hex2dec('000000DD');
WR224C.PCM_A_2.idext = 'STANDARD';
WR224C.PCM_A_2.idinherit = 0;
WR224C.PCM_A_2.payload_size = 8;
WR224C.PCM_A_2.payload_value = [];
WR224C.PCM_A_2.payload_mask = [];
WR224C.PCM_A_2.interval = -1;

WR224C.PCM_A_2.fields{1}.name = 'APPS1';
WR224C.PCM_A_2.fields{1}.units = '%';
WR224C.PCM_A_2.fields{1}.start_bit = 54;
WR224C.PCM_A_2.fields{1}.bit_length = 10;
WR224C.PCM_A_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{1}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{2}.name = 'APPS2';
WR224C.PCM_A_2.fields{2}.units = '%';
WR224C.PCM_A_2.fields{2}.start_bit = 44;
WR224C.PCM_A_2.fields{2}.bit_length = 10;
WR224C.PCM_A_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{2}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{3}.name = 'Cyl1_Phi';
WR224C.PCM_A_2.fields{3}.units = 'EqRatio';
WR224C.PCM_A_2.fields{3}.start_bit = 26;
WR224C.PCM_A_2.fields{3}.bit_length = 8;
WR224C.PCM_A_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{3}.maximum = 2.550000e+00;
WR224C.PCM_A_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{3}.scale = 1.000000e-02;
WR224C.PCM_A_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{4}.name = 'Throttle_Req';
WR224C.PCM_A_2.fields{4}.units = '%';
WR224C.PCM_A_2.fields{4}.start_bit = 34;
WR224C.PCM_A_2.fields{4}.bit_length = 10;
WR224C.PCM_A_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{4}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{4}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{5}.name = 'AlphaN_MAF';
WR224C.PCM_A_2.fields{5}.units = 'g/s';
WR224C.PCM_A_2.fields{5}.start_bit = 16;
WR224C.PCM_A_2.fields{5}.bit_length = 10;
WR224C.PCM_A_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{5}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{5}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{6}.name = 'Cyl2_Phi';
WR224C.PCM_A_2.fields{6}.units = 'EqRatio';
WR224C.PCM_A_2.fields{6}.start_bit = 8;
WR224C.PCM_A_2.fields{6}.bit_length = 8;
WR224C.PCM_A_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{6}.maximum = 2.550000e+00;
WR224C.PCM_A_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{6}.scale = 1.000000e-02;
WR224C.PCM_A_2.fields{6}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{7}.name = 'SpkPhiCompDeg';
WR224C.PCM_A_2.fields{7}.units = 'deg';
WR224C.PCM_A_2.fields{7}.start_bit = 3;
WR224C.PCM_A_2.fields{7}.bit_length = 5;
WR224C.PCM_A_2.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{7}.data_type = 'SIGNED';
WR224C.PCM_A_2.fields{7}.maximum = 1.500000e+01;
WR224C.PCM_A_2.fields{7}.minimum = -1.600000e+01;
WR224C.PCM_A_2.fields{7}.scale = 1.000000e+00;
WR224C.PCM_A_2.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_3'
WR224C.PCM_A_3 = struct;
WR224C.PCM_A_3.name = 'PCM_A_3';
WR224C.PCM_A_3.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_3.protocol = 'WR224C';
WR224C.PCM_A_3.id = hex2dec('000000DE');
WR224C.PCM_A_3.idext = 'STANDARD';
WR224C.PCM_A_3.idinherit = 0;
WR224C.PCM_A_3.payload_size = 8;
WR224C.PCM_A_3.payload_value = [];
WR224C.PCM_A_3.payload_mask = [];
WR224C.PCM_A_3.interval = -1;

WR224C.PCM_A_3.fields{1}.name = 'APC';
WR224C.PCM_A_3.fields{1}.units = 'mg';
WR224C.PCM_A_3.fields{1}.start_bit = 52;
WR224C.PCM_A_3.fields{1}.bit_length = 12;
WR224C.PCM_A_3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{1}.maximum = 4.000000e+02;
WR224C.PCM_A_3.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{1}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{2}.name = 'Load';
WR224C.PCM_A_3.fields{2}.units = '%';
WR224C.PCM_A_3.fields{2}.start_bit = 10;
WR224C.PCM_A_3.fields{2}.bit_length = 10;
WR224C.PCM_A_3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{2}.maximum = 1.329900e+02;
WR224C.PCM_A_3.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{2}.scale = 1.300000e-01;
WR224C.PCM_A_3.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{3}.name = 'Cyl1_FPC';
WR224C.PCM_A_3.fields{3}.units = 'mg';
WR224C.PCM_A_3.fields{3}.start_bit = 31;
WR224C.PCM_A_3.fields{3}.bit_length = 10;
WR224C.PCM_A_3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{3}.maximum = 1.023000e+02;
WR224C.PCM_A_3.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{3}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{4}.name = 'Cyl2_FPC';
WR224C.PCM_A_3.fields{4}.units = 'mg';
WR224C.PCM_A_3.fields{4}.start_bit = 0;
WR224C.PCM_A_3.fields{4}.bit_length = 10;
WR224C.PCM_A_3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{4}.maximum = 1.023000e+02;
WR224C.PCM_A_3.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{4}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{5}.name = 'Cyl1_Spark_Adv';
WR224C.PCM_A_3.fields{5}.units = 'deg';
WR224C.PCM_A_3.fields{5}.start_bit = 20;
WR224C.PCM_A_3.fields{5}.bit_length = 11;
WR224C.PCM_A_3.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{5}.data_type = 'SIGNED';
WR224C.PCM_A_3.fields{5}.maximum = 1.020000e+02;
WR224C.PCM_A_3.fields{5}.minimum = -1.020000e+02;
WR224C.PCM_A_3.fields{5}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{6}.name = 'Cyl2_Spark_Adv';
WR224C.PCM_A_3.fields{6}.units = 'deg';
WR224C.PCM_A_3.fields{6}.start_bit = 41;
WR224C.PCM_A_3.fields{6}.bit_length = 11;
WR224C.PCM_A_3.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{6}.data_type = 'SIGNED';
WR224C.PCM_A_3.fields{6}.maximum = 1.020000e+02;
WR224C.PCM_A_3.fields{6}.minimum = -1.020000e+02;
WR224C.PCM_A_3.fields{6}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_4'
WR224C.PCM_A_4 = struct;
WR224C.PCM_A_4.name = 'PCM_A_4';
WR224C.PCM_A_4.description = 'Torque Model';
WR224C.PCM_A_4.protocol = 'WR224C';
WR224C.PCM_A_4.id = hex2dec('00000007');
WR224C.PCM_A_4.idext = 'STANDARD';
WR224C.PCM_A_4.idinherit = 0;
WR224C.PCM_A_4.payload_size = 8;
WR224C.PCM_A_4.payload_value = [];
WR224C.PCM_A_4.payload_mask = [];
WR224C.PCM_A_4.interval = -1;

WR224C.PCM_A_4.fields{1}.name = 'VirtualEGT';
WR224C.PCM_A_4.fields{1}.units = 'C';
WR224C.PCM_A_4.fields{1}.start_bit = 56;
WR224C.PCM_A_4.fields{1}.bit_length = 8;
WR224C.PCM_A_4.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{1}.maximum = 1.280000e+03;
WR224C.PCM_A_4.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_4.fields{1}.scale = 5.000000e+00;
WR224C.PCM_A_4.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{2}.name = 'TqModelLeadTqReq';
WR224C.PCM_A_4.fields{2}.units = '%';
WR224C.PCM_A_4.fields{2}.start_bit = 46;
WR224C.PCM_A_4.fields{2}.bit_length = 10;
WR224C.PCM_A_4.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_A_4.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_4.fields{2}.scale = 1.000000e-01;
WR224C.PCM_A_4.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{3}.name = 'TqModelSetCutPct';
WR224C.PCM_A_4.fields{3}.units = '%';
WR224C.PCM_A_4.fields{3}.start_bit = 36;
WR224C.PCM_A_4.fields{3}.bit_length = 10;
WR224C.PCM_A_4.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{3}.maximum = 1.000000e+02;
WR224C.PCM_A_4.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_4.fields{3}.scale = 1.000000e-01;
WR224C.PCM_A_4.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{4}.name = 'TqModelAirPTerm';
WR224C.PCM_A_4.fields{4}.units = '%';
WR224C.PCM_A_4.fields{4}.start_bit = 27;
WR224C.PCM_A_4.fields{4}.bit_length = 9;
WR224C.PCM_A_4.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{4}.data_type = 'SIGNED';
WR224C.PCM_A_4.fields{4}.maximum = 2.500000e+01;
WR224C.PCM_A_4.fields{4}.minimum = -2.500000e+01;
WR224C.PCM_A_4.fields{4}.scale = 1.000000e-01;
WR224C.PCM_A_4.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{5}.name = 'TqModelAirITerm';
WR224C.PCM_A_4.fields{5}.units = '%';
WR224C.PCM_A_4.fields{5}.start_bit = 18;
WR224C.PCM_A_4.fields{5}.bit_length = 9;
WR224C.PCM_A_4.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{5}.data_type = 'SIGNED';
WR224C.PCM_A_4.fields{5}.maximum = 2.500000e+01;
WR224C.PCM_A_4.fields{5}.minimum = -2.500000e+01;
WR224C.PCM_A_4.fields{5}.scale = 1.000000e-01;
WR224C.PCM_A_4.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{6}.name = 'CumulatedFallingAir';
WR224C.PCM_A_4.fields{6}.units = '%';
WR224C.PCM_A_4.fields{6}.start_bit = 11;
WR224C.PCM_A_4.fields{6}.bit_length = 7;
WR224C.PCM_A_4.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{6}.maximum = 1.000000e+02;
WR224C.PCM_A_4.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_A_4.fields{6}.scale = 1.000000e+00;
WR224C.PCM_A_4.fields{6}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{7}.name = 'SetPathActive';
WR224C.PCM_A_4.fields{7}.units = '';
WR224C.PCM_A_4.fields{7}.start_bit = 10;
WR224C.PCM_A_4.fields{7}.bit_length = 1;
WR224C.PCM_A_4.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{7}.scale = 1.000000e+00;
WR224C.PCM_A_4.fields{7}.offset = 0.000000e+00;

WR224C.PCM_A_4.fields{8}.name = 'SparkCutDeg';
WR224C.PCM_A_4.fields{8}.units = '';
WR224C.PCM_A_4.fields{8}.start_bit = 4;
WR224C.PCM_A_4.fields{8}.bit_length = 6;
WR224C.PCM_A_4.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_4.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_A_4.fields{8}.maximum = 6.400000e+01;
WR224C.PCM_A_4.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_A_4.fields{8}.scale = 1.000000e+00;
WR224C.PCM_A_4.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_1'
WR224C.PCM_B_1 = struct;
WR224C.PCM_B_1.name = 'PCM_B_1';
WR224C.PCM_B_1.description = 'Traction Control. Should have same frequency/interval as other B frames.';
WR224C.PCM_B_1.protocol = 'WR224C';
WR224C.PCM_B_1.id = hex2dec('000000E6');
WR224C.PCM_B_1.idext = 'STANDARD';
WR224C.PCM_B_1.idinherit = 0;
WR224C.PCM_B_1.payload_size = 8;
WR224C.PCM_B_1.payload_value = [];
WR224C.PCM_B_1.payload_mask = [];
WR224C.PCM_B_1.interval = -1;

WR224C.PCM_B_1.fields{1}.name = 'Wheel_Slip';
WR224C.PCM_B_1.fields{1}.units = '%';
WR224C.PCM_B_1.fields{1}.start_bit = 47;
WR224C.PCM_B_1.fields{1}.bit_length = 10;
WR224C.PCM_B_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_B_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{1}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{2}.name = 'TC_Retard';
WR224C.PCM_B_1.fields{2}.units = 'deg';
WR224C.PCM_B_1.fields{2}.start_bit = 37;
WR224C.PCM_B_1.fields{2}.bit_length = 10;
WR224C.PCM_B_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{2}.data_type = 'SIGNED';
WR224C.PCM_B_1.fields{2}.maximum = 5.000000e+01;
WR224C.PCM_B_1.fields{2}.minimum = -5.000000e+01;
WR224C.PCM_B_1.fields{2}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{3}.name = 'TC_Active';
WR224C.PCM_B_1.fields{3}.units = 'bool';
WR224C.PCM_B_1.fields{3}.start_bit = 29;
WR224C.PCM_B_1.fields{3}.bit_length = 1;
WR224C.PCM_B_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{3}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{4}.name = 'TC_TorqueCutPct';
WR224C.PCM_B_1.fields{4}.units = '';
WR224C.PCM_B_1.fields{4}.start_bit = 30;
WR224C.PCM_B_1.fields{4}.bit_length = 7;
WR224C.PCM_B_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{4}.maximum = 1.270000e+02;
WR224C.PCM_B_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{4}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{5}.name = 'TC_UndersteerPct';
WR224C.PCM_B_1.fields{5}.units = '';
WR224C.PCM_B_1.fields{5}.start_bit = 57;
WR224C.PCM_B_1.fields{5}.bit_length = 7;
WR224C.PCM_B_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{5}.maximum = 1.270000e+02;
WR224C.PCM_B_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{5}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{6}.name = 'Wheel_Slip_Error';
WR224C.PCM_B_1.fields{6}.units = '%';
WR224C.PCM_B_1.fields{6}.start_bit = 20;
WR224C.PCM_B_1.fields{6}.bit_length = 9;
WR224C.PCM_B_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{6}.maximum = 5.000000e+01;
WR224C.PCM_B_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{6}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{7}.name = 'FilteredVehicleSpeed';
WR224C.PCM_B_1.fields{7}.units = 'mph';
WR224C.PCM_B_1.fields{7}.start_bit = 10;
WR224C.PCM_B_1.fields{7}.bit_length = 10;
WR224C.PCM_B_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{7}.maximum = 1.023000e+02;
WR224C.PCM_B_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{7}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_2'
WR224C.PCM_B_2 = struct;
WR224C.PCM_B_2.name = 'PCM_B_2';
WR224C.PCM_B_2.description = 'Should have same frequency/interval as other B frames.';
WR224C.PCM_B_2.protocol = 'WR224C';
WR224C.PCM_B_2.id = hex2dec('000000DF');
WR224C.PCM_B_2.idext = 'STANDARD';
WR224C.PCM_B_2.idinherit = 0;
WR224C.PCM_B_2.payload_size = 8;
WR224C.PCM_B_2.payload_value = [];
WR224C.PCM_B_2.payload_mask = [];
WR224C.PCM_B_2.interval = -1;

WR224C.PCM_B_2.fields{1}.name = 'FuelFlowRate';
WR224C.PCM_B_2.fields{1}.units = 'ml/s';
WR224C.PCM_B_2.fields{1}.start_bit = 56;
WR224C.PCM_B_2.fields{1}.bit_length = 8;
WR224C.PCM_B_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{1}.maximum = 2.550000e+01;
WR224C.PCM_B_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{1}.scale = 1.000000e-01;
WR224C.PCM_B_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{2}.name = 'Crank_Req';
WR224C.PCM_B_2.fields{2}.units = 'bool';
WR224C.PCM_B_2.fields{2}.start_bit = 48;
WR224C.PCM_B_2.fields{2}.bit_length = 1;
WR224C.PCM_B_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{2}.maximum = 1.000000e+00;
WR224C.PCM_B_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{2}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{3}.name = 'Fuel_Pct_Remaining';
WR224C.PCM_B_2.fields{3}.units = '%';
WR224C.PCM_B_2.fields{3}.start_bit = 49;
WR224C.PCM_B_2.fields{3}.bit_length = 7;
WR224C.PCM_B_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{3}.maximum = 1.000000e+02;
WR224C.PCM_B_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{4}.name = 'Last_Lap_Time';
WR224C.PCM_B_2.fields{4}.units = 's';
WR224C.PCM_B_2.fields{4}.start_bit = 35;
WR224C.PCM_B_2.fields{4}.bit_length = 13;
WR224C.PCM_B_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{4}.maximum = 8.100000e+01;
WR224C.PCM_B_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{4}.scale = 1.000000e-02;
WR224C.PCM_B_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{5}.name = 'StartingFuel';
WR224C.PCM_B_2.fields{5}.units = 'mL';
WR224C.PCM_B_2.fields{5}.start_bit = 25;
WR224C.PCM_B_2.fields{5}.bit_length = 10;
WR224C.PCM_B_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{5}.maximum = 1.000000e+04;
WR224C.PCM_B_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{5}.scale = 1.000000e+01;
WR224C.PCM_B_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{6}.name = 'FuelUsed';
WR224C.PCM_B_2.fields{6}.units = 'mL';
WR224C.PCM_B_2.fields{6}.start_bit = 15;
WR224C.PCM_B_2.fields{6}.bit_length = 10;
WR224C.PCM_B_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{6}.maximum = 1.000000e+04;
WR224C.PCM_B_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{6}.scale = 1.000000e+01;
WR224C.PCM_B_2.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{7}.name = 'LapTimerTrigger';
WR224C.PCM_B_2.fields{7}.units = '';
WR224C.PCM_B_2.fields{7}.start_bit = 13;
WR224C.PCM_B_2.fields{7}.bit_length = 1;
WR224C.PCM_B_2.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_B_2.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{7}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{8}.name = 'TransientFuelingActive';
WR224C.PCM_B_2.fields{8}.units = '';
WR224C.PCM_B_2.fields{8}.start_bit = 14;
WR224C.PCM_B_2.fields{8}.bit_length = 1;
WR224C.PCM_B_2.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{8}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{9}.name = 'FuelPumpPWM';
WR224C.PCM_B_2.fields{9}.units = '%';
WR224C.PCM_B_2.fields{9}.start_bit = 3;
WR224C.PCM_B_2.fields{9}.bit_length = 10;
WR224C.PCM_B_2.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{9}.maximum = 1.000000e+02;
WR224C.PCM_B_2.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{9}.scale = 1.000000e-01;
WR224C.PCM_B_2.fields{9}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_3'
WR224C.PCM_B_3 = struct;
WR224C.PCM_B_3.name = 'PCM_B_3';
WR224C.PCM_B_3.description = 'PCM_B_3';
WR224C.PCM_B_3.protocol = 'WR224C';
WR224C.PCM_B_3.id = hex2dec('000000E3');
WR224C.PCM_B_3.idext = 'STANDARD';
WR224C.PCM_B_3.idinherit = 0;
WR224C.PCM_B_3.payload_size = 8;
WR224C.PCM_B_3.payload_value = [];
WR224C.PCM_B_3.payload_mask = [];
WR224C.PCM_B_3.interval = -1;

WR224C.PCM_B_3.fields{1}.name = 'Cyl1_MPW';
WR224C.PCM_B_3.fields{1}.units = 'ms';
WR224C.PCM_B_3.fields{1}.start_bit = 47;
WR224C.PCM_B_3.fields{1}.bit_length = 9;
WR224C.PCM_B_3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{1}.maximum = 5.110000e+01;
WR224C.PCM_B_3.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{1}.scale = 1.000000e-01;
WR224C.PCM_B_3.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{2}.name = 'Cyl2_MPW';
WR224C.PCM_B_3.fields{2}.units = '';
WR224C.PCM_B_3.fields{2}.start_bit = 38;
WR224C.PCM_B_3.fields{2}.bit_length = 9;
WR224C.PCM_B_3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{2}.maximum = 5.110000e+01;
WR224C.PCM_B_3.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{2}.scale = 1.000000e-01;
WR224C.PCM_B_3.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{3}.name = 'Inj_Enabled';
WR224C.PCM_B_3.fields{3}.units = '';
WR224C.PCM_B_3.fields{3}.start_bit = 18;
WR224C.PCM_B_3.fields{3}.bit_length = 1;
WR224C.PCM_B_3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{3}.maximum = 1.000000e+00;
WR224C.PCM_B_3.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{4}.name = 'MPRD';
WR224C.PCM_B_3.fields{4}.units = '';
WR224C.PCM_B_3.fields{4}.start_bit = 19;
WR224C.PCM_B_3.fields{4}.bit_length = 1;
WR224C.PCM_B_3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{4}.maximum = 1.000000e+00;
WR224C.PCM_B_3.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{4}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{5}.name = 'VE';
WR224C.PCM_B_3.fields{5}.units = '';
WR224C.PCM_B_3.fields{5}.start_bit = 28;
WR224C.PCM_B_3.fields{5}.bit_length = 10;
WR224C.PCM_B_3.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{5}.maximum = 1.023000e+00;
WR224C.PCM_B_3.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{5}.scale = 1.000000e-03;
WR224C.PCM_B_3.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{6}.name = 'IdleSparkAdv';
WR224C.PCM_B_3.fields{6}.units = '';
WR224C.PCM_B_3.fields{6}.start_bit = 56;
WR224C.PCM_B_3.fields{6}.bit_length = 8;
WR224C.PCM_B_3.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{6}.data_type = 'SIGNED';
WR224C.PCM_B_3.fields{6}.maximum = 1.200000e+02;
WR224C.PCM_B_3.fields{6}.minimum = -1.200000e+02;
WR224C.PCM_B_3.fields{6}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{7}.name = 'SparkEnabled';
WR224C.PCM_B_3.fields{7}.units = '';
WR224C.PCM_B_3.fields{7}.start_bit = 17;
WR224C.PCM_B_3.fields{7}.bit_length = 1;
WR224C.PCM_B_3.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_B_3.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{7}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{8}.name = 'DeltaAPC';
WR224C.PCM_B_3.fields{8}.units = 'mg';
WR224C.PCM_B_3.fields{8}.start_bit = 20;
WR224C.PCM_B_3.fields{8}.bit_length = 8;
WR224C.PCM_B_3.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{8}.data_type = 'SIGNED';
WR224C.PCM_B_3.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{8}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{9}.name = 'DriverTrqReq';
WR224C.PCM_B_3.fields{9}.units = '%';
WR224C.PCM_B_3.fields{9}.start_bit = 7;
WR224C.PCM_B_3.fields{9}.bit_length = 10;
WR224C.PCM_B_3.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{9}.maximum = 1.000000e+02;
WR224C.PCM_B_3.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_B_3.fields{9}.scale = 1.000000e-01;
WR224C.PCM_B_3.fields{9}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{10}.name = 'Cyl1Enabled';
WR224C.PCM_B_3.fields{10}.units = 'bool';
WR224C.PCM_B_3.fields{10}.start_bit = 6;
WR224C.PCM_B_3.fields{10}.bit_length = 1;
WR224C.PCM_B_3.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{10}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{10}.offset = 0.000000e+00;

WR224C.PCM_B_3.fields{11}.name = 'Cyl2Enabled';
WR224C.PCM_B_3.fields{11}.units = 'bool';
WR224C.PCM_B_3.fields{11}.start_bit = 5;
WR224C.PCM_B_3.fields{11}.bit_length = 1;
WR224C.PCM_B_3.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_3.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_B_3.fields{11}.scale = 1.000000e+00;
WR224C.PCM_B_3.fields{11}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_4'
WR224C.PCM_B_4 = struct;
WR224C.PCM_B_4.name = 'PCM_B_4';
WR224C.PCM_B_4.description = 'Tau X';
WR224C.PCM_B_4.protocol = 'WR224C';
WR224C.PCM_B_4.id = hex2dec('00000004');
WR224C.PCM_B_4.idext = 'STANDARD';
WR224C.PCM_B_4.idinherit = 0;
WR224C.PCM_B_4.payload_size = 8;
WR224C.PCM_B_4.payload_value = [];
WR224C.PCM_B_4.payload_mask = [];
WR224C.PCM_B_4.interval = -1;

WR224C.PCM_B_4.fields{1}.name = 'PuddleMass';
WR224C.PCM_B_4.fields{1}.units = 'mg';
WR224C.PCM_B_4.fields{1}.start_bit = 56;
WR224C.PCM_B_4.fields{1}.bit_length = 8;
WR224C.PCM_B_4.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{1}.maximum = 2.550000e+02;
WR224C.PCM_B_4.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{1}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{2}.name = 'FuelVapFactor';
WR224C.PCM_B_4.fields{2}.units = '';
WR224C.PCM_B_4.fields{2}.start_bit = 49;
WR224C.PCM_B_4.fields{2}.bit_length = 7;
WR224C.PCM_B_4.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_B_4.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{2}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{3}.name = 'DeltaMAF';
WR224C.PCM_B_4.fields{3}.units = '';
WR224C.PCM_B_4.fields{3}.start_bit = 39;
WR224C.PCM_B_4.fields{3}.bit_length = 10;
WR224C.PCM_B_4.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{3}.data_type = 'SIGNED';
WR224C.PCM_B_4.fields{3}.maximum = 5.000000e+01;
WR224C.PCM_B_4.fields{3}.minimum = -5.000000e+01;
WR224C.PCM_B_4.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{4}.name = 'Tau';
WR224C.PCM_B_4.fields{4}.units = '';
WR224C.PCM_B_4.fields{4}.start_bit = 32;
WR224C.PCM_B_4.fields{4}.bit_length = 7;
WR224C.PCM_B_4.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{4}.maximum = 1.000000e+00;
WR224C.PCM_B_4.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{4}.scale = 1.000000e-02;
WR224C.PCM_B_4.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{5}.name = 'X';
WR224C.PCM_B_4.fields{5}.units = '';
WR224C.PCM_B_4.fields{5}.start_bit = 25;
WR224C.PCM_B_4.fields{5}.bit_length = 7;
WR224C.PCM_B_4.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{5}.maximum = 1.000000e+00;
WR224C.PCM_B_4.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{5}.scale = 1.000000e-02;
WR224C.PCM_B_4.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{6}.name = 'MBTBestPhiTorque';
WR224C.PCM_B_4.fields{6}.units = '%';
WR224C.PCM_B_4.fields{6}.start_bit = 18;
WR224C.PCM_B_4.fields{6}.bit_length = 7;
WR224C.PCM_B_4.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{6}.maximum = 1.000000e+02;
WR224C.PCM_B_4.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{6}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{7}.name = 'TorqueError';
WR224C.PCM_B_4.fields{7}.units = '%';
WR224C.PCM_B_4.fields{7}.start_bit = 10;
WR224C.PCM_B_4.fields{7}.bit_length = 8;
WR224C.PCM_B_4.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{7}.data_type = 'SIGNED';
WR224C.PCM_B_4.fields{7}.maximum = 1.000000e+02;
WR224C.PCM_B_4.fields{7}.minimum = -1.000000e+02;
WR224C.PCM_B_4.fields{7}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_4.fields{8}.name = 'MBTCurrPhiTorque';
WR224C.PCM_B_4.fields{8}.units = '%';
WR224C.PCM_B_4.fields{8}.start_bit = 3;
WR224C.PCM_B_4.fields{8}.bit_length = 7;
WR224C.PCM_B_4.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_4.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_B_4.fields{8}.maximum = 1.000000e+02;
WR224C.PCM_B_4.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_B_4.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_4.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_5'
WR224C.PCM_B_5 = struct;
WR224C.PCM_B_5.name = 'PCM_B_5';
WR224C.PCM_B_5.description = 'Fuel factors';
WR224C.PCM_B_5.protocol = 'WR224C';
WR224C.PCM_B_5.id = hex2dec('00000003');
WR224C.PCM_B_5.idext = 'STANDARD';
WR224C.PCM_B_5.idinherit = 0;
WR224C.PCM_B_5.payload_size = 8;
WR224C.PCM_B_5.payload_value = [];
WR224C.PCM_B_5.payload_mask = [];
WR224C.PCM_B_5.interval = -1;

WR224C.PCM_B_5.fields{1}.name = 'OffDecelFF';
WR224C.PCM_B_5.fields{1}.units = '';
WR224C.PCM_B_5.fields{1}.start_bit = 56;
WR224C.PCM_B_5.fields{1}.bit_length = 8;
WR224C.PCM_B_5.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{1}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{2}.name = 'EnrichEnleanFF';
WR224C.PCM_B_5.fields{2}.units = '';
WR224C.PCM_B_5.fields{2}.start_bit = 48;
WR224C.PCM_B_5.fields{2}.bit_length = 8;
WR224C.PCM_B_5.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{2}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{3}.name = 'WarmupFF';
WR224C.PCM_B_5.fields{3}.units = '';
WR224C.PCM_B_5.fields{3}.start_bit = 41;
WR224C.PCM_B_5.fields{3}.bit_length = 7;
WR224C.PCM_B_5.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{3}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{4}.name = 'CLFCyl1FF';
WR224C.PCM_B_5.fields{4}.units = '';
WR224C.PCM_B_5.fields{4}.start_bit = 34;
WR224C.PCM_B_5.fields{4}.bit_length = 7;
WR224C.PCM_B_5.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{4}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{5}.name = 'CLFCyl2FF';
WR224C.PCM_B_5.fields{5}.units = '';
WR224C.PCM_B_5.fields{5}.start_bit = 27;
WR224C.PCM_B_5.fields{5}.bit_length = 7;
WR224C.PCM_B_5.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{5}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{6}.name = 'TotalFF';
WR224C.PCM_B_5.fields{6}.units = '';
WR224C.PCM_B_5.fields{6}.start_bit = 19;
WR224C.PCM_B_5.fields{6}.bit_length = 8;
WR224C.PCM_B_5.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{6}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{7}.name = 'TauXFF';
WR224C.PCM_B_5.fields{7}.units = '';
WR224C.PCM_B_5.fields{7}.start_bit = 11;
WR224C.PCM_B_5.fields{7}.bit_length = 8;
WR224C.PCM_B_5.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_5.fields{7}.scale = 1.000000e-02;
WR224C.PCM_B_5.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_5.fields{8}.name = 'DeltaMAP';
WR224C.PCM_B_5.fields{8}.units = 'kPa/s';
WR224C.PCM_B_5.fields{8}.start_bit = 0;
WR224C.PCM_B_5.fields{8}.bit_length = 11;
WR224C.PCM_B_5.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_5.fields{8}.data_type = 'SIGNED';
WR224C.PCM_B_5.fields{8}.maximum = 1.000000e+03;
WR224C.PCM_B_5.fields{8}.minimum = -1.000000e+03;
WR224C.PCM_B_5.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_5.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_1'
WR224C.PCM_C_1 = struct;
WR224C.PCM_C_1.name = 'PCM_C_1';
WR224C.PCM_C_1.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_1.protocol = 'WR224C';
WR224C.PCM_C_1.id = hex2dec('000000E0');
WR224C.PCM_C_1.idext = 'STANDARD';
WR224C.PCM_C_1.idinherit = 0;
WR224C.PCM_C_1.payload_size = 8;
WR224C.PCM_C_1.payload_value = [];
WR224C.PCM_C_1.payload_mask = [];
WR224C.PCM_C_1.interval = -1;

WR224C.PCM_C_1.fields{1}.name = 'APPS_Arb';
WR224C.PCM_C_1.fields{1}.units = '%';
WR224C.PCM_C_1.fields{1}.start_bit = 33;
WR224C.PCM_C_1.fields{1}.bit_length = 10;
WR224C.PCM_C_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{1}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{2}.name = 'TPS_Arb';
WR224C.PCM_C_1.fields{2}.units = '%';
WR224C.PCM_C_1.fields{2}.start_bit = 43;
WR224C.PCM_C_1.fields{2}.bit_length = 10;
WR224C.PCM_C_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{2}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{3}.name = 'SpeedDensity_MAF';
WR224C.PCM_C_1.fields{3}.units = 'g/s';
WR224C.PCM_C_1.fields{3}.start_bit = 53;
WR224C.PCM_C_1.fields{3}.bit_length = 11;
WR224C.PCM_C_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{3}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{3}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{4}.name = 'Rad_Fan';
WR224C.PCM_C_1.fields{4}.units = '%';
WR224C.PCM_C_1.fields{4}.start_bit = 25;
WR224C.PCM_C_1.fields{4}.bit_length = 8;
WR224C.PCM_C_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{4}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{4}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{5}.name = 'Target_Phi';
WR224C.PCM_C_1.fields{5}.units = 'EqRatio';
WR224C.PCM_C_1.fields{5}.start_bit = 17;
WR224C.PCM_C_1.fields{5}.bit_length = 8;
WR224C.PCM_C_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{5}.maximum = 2.000000e+00;
WR224C.PCM_C_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{5}.scale = 1.000000e-02;
WR224C.PCM_C_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{6}.name = 'PCMFault2';
WR224C.PCM_C_1.fields{6}.units = 'bool';
WR224C.PCM_C_1.fields{6}.start_bit = 12;
WR224C.PCM_C_1.fields{6}.bit_length = 1;
WR224C.PCM_C_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{6}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{6}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{7}.name = 'PCMFault3';
WR224C.PCM_C_1.fields{7}.units = 'bool';
WR224C.PCM_C_1.fields{7}.start_bit = 13;
WR224C.PCM_C_1.fields{7}.bit_length = 1;
WR224C.PCM_C_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{7}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{7}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{8}.name = 'PCMFault4';
WR224C.PCM_C_1.fields{8}.units = 'bool';
WR224C.PCM_C_1.fields{8}.start_bit = 14;
WR224C.PCM_C_1.fields{8}.bit_length = 1;
WR224C.PCM_C_1.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{8}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{8}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{8}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{9}.name = 'PCMFault5';
WR224C.PCM_C_1.fields{9}.units = 'bool';
WR224C.PCM_C_1.fields{9}.start_bit = 15;
WR224C.PCM_C_1.fields{9}.bit_length = 1;
WR224C.PCM_C_1.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{9}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{9}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{9}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{10}.name = 'PCMFault6';
WR224C.PCM_C_1.fields{10}.units = 'bool';
WR224C.PCM_C_1.fields{10}.start_bit = 16;
WR224C.PCM_C_1.fields{10}.bit_length = 1;
WR224C.PCM_C_1.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{10}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{10}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{10}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{10}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_2'
WR224C.PCM_C_2 = struct;
WR224C.PCM_C_2.name = 'PCM_C_2';
WR224C.PCM_C_2.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_2.protocol = 'WR224C';
WR224C.PCM_C_2.id = hex2dec('000000E1');
WR224C.PCM_C_2.idext = 'STANDARD';
WR224C.PCM_C_2.idinherit = 0;
WR224C.PCM_C_2.payload_size = 8;
WR224C.PCM_C_2.payload_value = [];
WR224C.PCM_C_2.payload_mask = [];
WR224C.PCM_C_2.interval = -1;

WR224C.PCM_C_2.fields{1}.name = 'Rev_Limiter';
WR224C.PCM_C_2.fields{1}.units = 'rpm';
WR224C.PCM_C_2.fields{1}.start_bit = 48;
WR224C.PCM_C_2.fields{1}.bit_length = 16;
WR224C.PCM_C_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{1}.maximum = 1.300000e+04;
WR224C.PCM_C_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{1}.scale = 1.000000e+00;
WR224C.PCM_C_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{2}.name = 'Spark_Energy_Pct';
WR224C.PCM_C_2.fields{2}.units = '%';
WR224C.PCM_C_2.fields{2}.start_bit = 40;
WR224C.PCM_C_2.fields{2}.bit_length = 8;
WR224C.PCM_C_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_C_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{2}.scale = 1.000000e+00;
WR224C.PCM_C_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{3}.name = 'Oil_Pressure';
WR224C.PCM_C_2.fields{3}.units = 'kPa';
WR224C.PCM_C_2.fields{3}.start_bit = 31;
WR224C.PCM_C_2.fields{3}.bit_length = 9;
WR224C.PCM_C_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{3}.maximum = 6.000000e+02;
WR224C.PCM_C_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{3}.scale = 2.000000e+00;
WR224C.PCM_C_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{4}.name = 'Fuel_Pressure';
WR224C.PCM_C_2.fields{4}.units = 'kPa';
WR224C.PCM_C_2.fields{4}.start_bit = 22;
WR224C.PCM_C_2.fields{4}.bit_length = 9;
WR224C.PCM_C_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{4}.maximum = 1.023000e+03;
WR224C.PCM_C_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{4}.scale = 2.000000e+00;
WR224C.PCM_C_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{5}.name = 'Coolant_Temp';
WR224C.PCM_C_2.fields{5}.units = 'C';
WR224C.PCM_C_2.fields{5}.start_bit = 11;
WR224C.PCM_C_2.fields{5}.bit_length = 11;
WR224C.PCM_C_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{5}.maximum = 1.500000e+02;
WR224C.PCM_C_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{5}.scale = 1.000000e-01;
WR224C.PCM_C_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{6}.name = 'Oil_Temp';
WR224C.PCM_C_2.fields{6}.units = 'C';
WR224C.PCM_C_2.fields{6}.start_bit = 0;
WR224C.PCM_C_2.fields{6}.bit_length = 11;
WR224C.PCM_C_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{6}.maximum = 1.500000e+02;
WR224C.PCM_C_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{6}.scale = 1.000000e-01;
WR224C.PCM_C_2.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_3'
WR224C.PCM_C_3 = struct;
WR224C.PCM_C_3.name = 'PCM_C_3';
WR224C.PCM_C_3.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_3.protocol = 'WR224C';
WR224C.PCM_C_3.id = hex2dec('000000E5');
WR224C.PCM_C_3.idext = 'STANDARD';
WR224C.PCM_C_3.idinherit = 0;
WR224C.PCM_C_3.payload_size = 8;
WR224C.PCM_C_3.payload_value = [];
WR224C.PCM_C_3.payload_mask = [];
WR224C.PCM_C_3.interval = -1;

WR224C.PCM_C_3.fields{1}.name = 'Plenum_Air_Temp';
WR224C.PCM_C_3.fields{1}.units = 'C';
WR224C.PCM_C_3.fields{1}.start_bit = 54;
WR224C.PCM_C_3.fields{1}.bit_length = 10;
WR224C.PCM_C_3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{1}.data_type = 'SIGNED';
WR224C.PCM_C_3.fields{1}.maximum = 5.000000e+01;
WR224C.PCM_C_3.fields{1}.minimum = -5.000000e+01;
WR224C.PCM_C_3.fields{1}.scale = 1.000000e-01;
WR224C.PCM_C_3.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{2}.name = 'F_Brake_Pressure';
WR224C.PCM_C_3.fields{2}.units = 'bar';
WR224C.PCM_C_3.fields{2}.start_bit = 43;
WR224C.PCM_C_3.fields{2}.bit_length = 11;
WR224C.PCM_C_3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{2}.maximum = 2.000000e+02;
WR224C.PCM_C_3.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{2}.scale = 1.000000e-01;
WR224C.PCM_C_3.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{3}.name = 'R_Brake_Pressure';
WR224C.PCM_C_3.fields{3}.units = 'bar';
WR224C.PCM_C_3.fields{3}.start_bit = 32;
WR224C.PCM_C_3.fields{3}.bit_length = 11;
WR224C.PCM_C_3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{3}.maximum = 2.000000e+02;
WR224C.PCM_C_3.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{3}.scale = 1.000000e-01;
WR224C.PCM_C_3.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{4}.name = 'Gear_Granular';
WR224C.PCM_C_3.fields{4}.units = '';
WR224C.PCM_C_3.fields{4}.start_bit = 23;
WR224C.PCM_C_3.fields{4}.bit_length = 9;
WR224C.PCM_C_3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{4}.maximum = 6.000000e+00;
WR224C.PCM_C_3.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{4}.scale = 1.175000e-02;
WR224C.PCM_C_3.fields{4}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{5}.name = 'Cyl1SOI';
WR224C.PCM_C_3.fields{5}.units = 'deg';
WR224C.PCM_C_3.fields{5}.start_bit = 16;
WR224C.PCM_C_3.fields{5}.bit_length = 7;
WR224C.PCM_C_3.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{5}.maximum = 7.200000e+02;
WR224C.PCM_C_3.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{5}.scale = 5.700000e+00;
WR224C.PCM_C_3.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'PCM_SteeringWheelInputs'
WR224C.PCM_SteeringWheelInputs = struct;
WR224C.PCM_SteeringWheelInputs.name = 'PCM_SteeringWheelInputs';
WR224C.PCM_SteeringWheelInputs.description = 'PCM_SteeringWheelInputs';
WR224C.PCM_SteeringWheelInputs.protocol = 'WR224C';
WR224C.PCM_SteeringWheelInputs.id = hex2dec('00000010');
WR224C.PCM_SteeringWheelInputs.idext = 'STANDARD';
WR224C.PCM_SteeringWheelInputs.idinherit = 0;
WR224C.PCM_SteeringWheelInputs.payload_size = 8;
WR224C.PCM_SteeringWheelInputs.payload_value = [];
WR224C.PCM_SteeringWheelInputs.payload_mask = [];
WR224C.PCM_SteeringWheelInputs.interval = -1;

WR224C.PCM_SteeringWheelInputs.fields{1}.name = 'SW_Button1';
WR224C.PCM_SteeringWheelInputs.fields{1}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{1}.start_bit = 56;
WR224C.PCM_SteeringWheelInputs.fields{1}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{1}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{1}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{2}.name = 'SW_Button3';
WR224C.PCM_SteeringWheelInputs.fields{2}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{2}.start_bit = 58;
WR224C.PCM_SteeringWheelInputs.fields{2}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{2}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{2}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{3}.name = 'SW_Button2';
WR224C.PCM_SteeringWheelInputs.fields{3}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{3}.start_bit = 57;
WR224C.PCM_SteeringWheelInputs.fields{3}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{3}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{3}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{4}.name = 'SW_Button4';
WR224C.PCM_SteeringWheelInputs.fields{4}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{4}.start_bit = 59;
WR224C.PCM_SteeringWheelInputs.fields{4}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{4}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{4}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{5}.name = 'SW_Button5';
WR224C.PCM_SteeringWheelInputs.fields{5}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{5}.start_bit = 60;
WR224C.PCM_SteeringWheelInputs.fields{5}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{5}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{5}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{6}.name = 'SW_Button6';
WR224C.PCM_SteeringWheelInputs.fields{6}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{6}.start_bit = 61;
WR224C.PCM_SteeringWheelInputs.fields{6}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{6}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{6}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{7}.name = 'SW_DialLeft';
WR224C.PCM_SteeringWheelInputs.fields{7}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{7}.start_bit = 51;
WR224C.PCM_SteeringWheelInputs.fields{7}.bit_length = 4;
WR224C.PCM_SteeringWheelInputs.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{7}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{7}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{8}.name = 'SW_DialRight';
WR224C.PCM_SteeringWheelInputs.fields{8}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{8}.start_bit = 40;
WR224C.PCM_SteeringWheelInputs.fields{8}.bit_length = 4;
WR224C.PCM_SteeringWheelInputs.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{8}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{8}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{9}.name = 'SW_JoystickCENTRE';
WR224C.PCM_SteeringWheelInputs.fields{9}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{9}.start_bit = 62;
WR224C.PCM_SteeringWheelInputs.fields{9}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{9}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{9}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{9}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{10}.name = 'SW_JoystickDOWN';
WR224C.PCM_SteeringWheelInputs.fields{10}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{10}.start_bit = 63;
WR224C.PCM_SteeringWheelInputs.fields{10}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{10}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{10}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{10}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{11}.name = 'SW_JoystickLEFT';
WR224C.PCM_SteeringWheelInputs.fields{11}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{11}.start_bit = 48;
WR224C.PCM_SteeringWheelInputs.fields{11}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{11}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{11}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{11}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{12}.name = 'SW_JoystickRIGHT';
WR224C.PCM_SteeringWheelInputs.fields{12}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{12}.start_bit = 49;
WR224C.PCM_SteeringWheelInputs.fields{12}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{12}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{12}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{12}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{12}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{13}.name = 'SW_JoystickUP';
WR224C.PCM_SteeringWheelInputs.fields{13}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{13}.start_bit = 50;
WR224C.PCM_SteeringWheelInputs.fields{13}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{13}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{13}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{13}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{13}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Body_Vel'
WR224C.SBG_Body_Vel = struct;
WR224C.SBG_Body_Vel.name = 'SBG_Body_Vel';
WR224C.SBG_Body_Vel.description = 'SBG_Body_Vel';
WR224C.SBG_Body_Vel.protocol = 'WR224C';
WR224C.SBG_Body_Vel.id = hex2dec('00000139');
WR224C.SBG_Body_Vel.idext = 'STANDARD';
WR224C.SBG_Body_Vel.idinherit = 0;
WR224C.SBG_Body_Vel.payload_size = 6;
WR224C.SBG_Body_Vel.payload_value = [];
WR224C.SBG_Body_Vel.payload_mask = [];
WR224C.SBG_Body_Vel.interval = -1;

WR224C.SBG_Body_Vel.fields{1}.name = 'Vel_X';
WR224C.SBG_Body_Vel.fields{1}.units = 'm/s';
WR224C.SBG_Body_Vel.fields{1}.start_bit = 56;
WR224C.SBG_Body_Vel.fields{1}.bit_length = 16;
WR224C.SBG_Body_Vel.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Body_Vel.fields{1}.data_type = 'SIGNED';
WR224C.SBG_Body_Vel.fields{1}.maximum = 3.270000e+02;
WR224C.SBG_Body_Vel.fields{1}.minimum = -3.270000e+02;
WR224C.SBG_Body_Vel.fields{1}.scale = 1.000000e-02;
WR224C.SBG_Body_Vel.fields{1}.offset = 0.000000e+00;

WR224C.SBG_Body_Vel.fields{2}.name = 'Vel_Y';
WR224C.SBG_Body_Vel.fields{2}.units = 'm/s';
WR224C.SBG_Body_Vel.fields{2}.start_bit = 40;
WR224C.SBG_Body_Vel.fields{2}.bit_length = 16;
WR224C.SBG_Body_Vel.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Body_Vel.fields{2}.data_type = 'SIGNED';
WR224C.SBG_Body_Vel.fields{2}.maximum = 3.270000e+02;
WR224C.SBG_Body_Vel.fields{2}.minimum = -3.270000e+02;
WR224C.SBG_Body_Vel.fields{2}.scale = 1.000000e-02;
WR224C.SBG_Body_Vel.fields{2}.offset = 0.000000e+00;

WR224C.SBG_Body_Vel.fields{3}.name = 'Vel_z';
WR224C.SBG_Body_Vel.fields{3}.units = 'm/s';
WR224C.SBG_Body_Vel.fields{3}.start_bit = 24;
WR224C.SBG_Body_Vel.fields{3}.bit_length = 16;
WR224C.SBG_Body_Vel.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Body_Vel.fields{3}.data_type = 'SIGNED';
WR224C.SBG_Body_Vel.fields{3}.maximum = 3.270000e+02;
WR224C.SBG_Body_Vel.fields{3}.minimum = -3.270000e+02;
WR224C.SBG_Body_Vel.fields{3}.scale = 1.000000e-02;
WR224C.SBG_Body_Vel.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_ECAN_MSG_IMU_ACCEL'
WR224C.SBG_ECAN_MSG_IMU_ACCEL = struct;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.name = 'SBG_ECAN_MSG_IMU_ACCEL';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.description = 'SBG_ECAN_MSG_IMU_ACCEL';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.protocol = 'WR224C';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.id = hex2dec('00000121');
WR224C.SBG_ECAN_MSG_IMU_ACCEL.idext = 'STANDARD';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.idinherit = 0;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.payload_size = 6;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.payload_value = [];
WR224C.SBG_ECAN_MSG_IMU_ACCEL.payload_mask = [];
WR224C.SBG_ECAN_MSG_IMU_ACCEL.interval = -1;

WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.name = 'ACCEL_Z';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.units = 'm.s-2';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.start_bit = 24;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.bit_length = 16;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.data_type = 'SIGNED';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.maximum = 3.276700e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.minimum = -3.276800e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.scale = 1.000000e-02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{1}.offset = 0.000000e+00;

WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.name = 'ACCEL_Y';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.units = 'm.s-2';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.start_bit = 40;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.bit_length = 16;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.data_type = 'SIGNED';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.maximum = 3.276700e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.minimum = -3.276800e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.scale = 1.000000e-02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{2}.offset = 0.000000e+00;

WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.name = 'ACCEL_X';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.units = 'm.s-2';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.start_bit = 56;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.bit_length = 16;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.data_type = 'SIGNED';
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.maximum = 3.276700e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.minimum = -3.276800e+02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.scale = 1.000000e-02;
WR224C.SBG_ECAN_MSG_IMU_ACCEL.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFAltitude'
WR224C.SBG_EKFAltitude = struct;
WR224C.SBG_EKFAltitude.name = 'SBG_EKFAltitude';
WR224C.SBG_EKFAltitude.description = 'SBG_EKFAltitude';
WR224C.SBG_EKFAltitude.protocol = 'WR224C';
WR224C.SBG_EKFAltitude.id = hex2dec('00000135');
WR224C.SBG_EKFAltitude.idext = 'STANDARD';
WR224C.SBG_EKFAltitude.idinherit = 0;
WR224C.SBG_EKFAltitude.payload_size = 6;
WR224C.SBG_EKFAltitude.payload_value = [];
WR224C.SBG_EKFAltitude.payload_mask = [];
WR224C.SBG_EKFAltitude.interval = -1;

WR224C.SBG_EKFAltitude.fields{1}.name = 'SBG_Undulation';
WR224C.SBG_EKFAltitude.fields{1}.units = '';
WR224C.SBG_EKFAltitude.fields{1}.start_bit = 24;
WR224C.SBG_EKFAltitude.fields{1}.bit_length = 16;
WR224C.SBG_EKFAltitude.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFAltitude.fields{1}.data_type = 'SIGNED';
WR224C.SBG_EKFAltitude.fields{1}.scale = 5.000000e-03;
WR224C.SBG_EKFAltitude.fields{1}.offset = 0.000000e+00;

WR224C.SBG_EKFAltitude.fields{2}.name = 'SBG_Altitude';
WR224C.SBG_EKFAltitude.fields{2}.units = 'm';
WR224C.SBG_EKFAltitude.fields{2}.start_bit = 56;
WR224C.SBG_EKFAltitude.fields{2}.bit_length = 32;
WR224C.SBG_EKFAltitude.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFAltitude.fields{2}.data_type = 'SIGNED';
WR224C.SBG_EKFAltitude.fields{2}.scale = 1.000000e-03;
WR224C.SBG_EKFAltitude.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFPosition'
WR224C.SBG_EKFPosition = struct;
WR224C.SBG_EKFPosition.name = 'SBG_EKFPosition';
WR224C.SBG_EKFPosition.description = 'SBG_EKFPosition';
WR224C.SBG_EKFPosition.protocol = 'WR224C';
WR224C.SBG_EKFPosition.id = hex2dec('00000134');
WR224C.SBG_EKFPosition.idext = 'STANDARD';
WR224C.SBG_EKFPosition.idinherit = 0;
WR224C.SBG_EKFPosition.payload_size = 8;
WR224C.SBG_EKFPosition.payload_value = [];
WR224C.SBG_EKFPosition.payload_mask = [];
WR224C.SBG_EKFPosition.interval = -1;

WR224C.SBG_EKFPosition.fields{1}.name = 'SBG_EKFLongitude';
WR224C.SBG_EKFPosition.fields{1}.units = '';
WR224C.SBG_EKFPosition.fields{1}.start_bit = 24;
WR224C.SBG_EKFPosition.fields{1}.bit_length = 32;
WR224C.SBG_EKFPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFPosition.fields{1}.data_type = 'SIGNED';
WR224C.SBG_EKFPosition.fields{1}.scale = 1.000000e-07;
WR224C.SBG_EKFPosition.fields{1}.offset = 0.000000e+00;

WR224C.SBG_EKFPosition.fields{2}.name = 'SBG_EKFLatitude';
WR224C.SBG_EKFPosition.fields{2}.units = '';
WR224C.SBG_EKFPosition.fields{2}.start_bit = 56;
WR224C.SBG_EKFPosition.fields{2}.bit_length = 32;
WR224C.SBG_EKFPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFPosition.fields{2}.data_type = 'SIGNED';
WR224C.SBG_EKFPosition.fields{2}.scale = 1.000000e-07;
WR224C.SBG_EKFPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_EKFVelocity'
WR224C.SBG_EKFVelocity = struct;
WR224C.SBG_EKFVelocity.name = 'SBG_EKFVelocity';
WR224C.SBG_EKFVelocity.description = 'SBG_EKFVelocity';
WR224C.SBG_EKFVelocity.protocol = 'WR224C';
WR224C.SBG_EKFVelocity.id = hex2dec('00000137');
WR224C.SBG_EKFVelocity.idext = 'STANDARD';
WR224C.SBG_EKFVelocity.idinherit = 0;
WR224C.SBG_EKFVelocity.payload_size = 6;
WR224C.SBG_EKFVelocity.payload_value = [];
WR224C.SBG_EKFVelocity.payload_mask = [];
WR224C.SBG_EKFVelocity.interval = -1;

WR224C.SBG_EKFVelocity.fields{1}.name = 'SBG_VelocityZ';
WR224C.SBG_EKFVelocity.fields{1}.units = 'm/s';
WR224C.SBG_EKFVelocity.fields{1}.start_bit = 24;
WR224C.SBG_EKFVelocity.fields{1}.bit_length = 16;
WR224C.SBG_EKFVelocity.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFVelocity.fields{1}.data_type = 'SIGNED';
WR224C.SBG_EKFVelocity.fields{1}.maximum = 3.270000e+02;
WR224C.SBG_EKFVelocity.fields{1}.minimum = -3.270000e+02;
WR224C.SBG_EKFVelocity.fields{1}.scale = 1.000000e-02;
WR224C.SBG_EKFVelocity.fields{1}.offset = 0.000000e+00;

WR224C.SBG_EKFVelocity.fields{2}.name = 'SBG_VelocityY';
WR224C.SBG_EKFVelocity.fields{2}.units = 'm/s';
WR224C.SBG_EKFVelocity.fields{2}.start_bit = 40;
WR224C.SBG_EKFVelocity.fields{2}.bit_length = 16;
WR224C.SBG_EKFVelocity.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFVelocity.fields{2}.data_type = 'SIGNED';
WR224C.SBG_EKFVelocity.fields{2}.maximum = 3.270000e+02;
WR224C.SBG_EKFVelocity.fields{2}.minimum = -3.270000e+02;
WR224C.SBG_EKFVelocity.fields{2}.scale = 1.000000e-02;
WR224C.SBG_EKFVelocity.fields{2}.offset = 0.000000e+00;

WR224C.SBG_EKFVelocity.fields{3}.name = 'SBG_VelocityX';
WR224C.SBG_EKFVelocity.fields{3}.units = 'm/s';
WR224C.SBG_EKFVelocity.fields{3}.start_bit = 56;
WR224C.SBG_EKFVelocity.fields{3}.bit_length = 16;
WR224C.SBG_EKFVelocity.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_EKFVelocity.fields{3}.data_type = 'SIGNED';
WR224C.SBG_EKFVelocity.fields{3}.maximum = 3.270000e+02;
WR224C.SBG_EKFVelocity.fields{3}.minimum = -3.270000e+02;
WR224C.SBG_EKFVelocity.fields{3}.scale = 1.000000e-02;
WR224C.SBG_EKFVelocity.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Euler'
WR224C.SBG_Euler = struct;
WR224C.SBG_Euler.name = 'SBG_Euler';
WR224C.SBG_Euler.description = 'SBG_Euler';
WR224C.SBG_Euler.protocol = 'WR224C';
WR224C.SBG_Euler.id = hex2dec('00000132');
WR224C.SBG_Euler.idext = 'STANDARD';
WR224C.SBG_Euler.idinherit = 0;
WR224C.SBG_Euler.payload_size = 6;
WR224C.SBG_Euler.payload_value = [];
WR224C.SBG_Euler.payload_mask = [];
WR224C.SBG_Euler.interval = -1;

WR224C.SBG_Euler.fields{1}.name = 'YAW';
WR224C.SBG_Euler.fields{1}.units = 'rad';
WR224C.SBG_Euler.fields{1}.start_bit = 24;
WR224C.SBG_Euler.fields{1}.bit_length = 16;
WR224C.SBG_Euler.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Euler.fields{1}.data_type = 'SIGNED';
WR224C.SBG_Euler.fields{1}.maximum = 3.276700e+00;
WR224C.SBG_Euler.fields{1}.minimum = -3.276800e+00;
WR224C.SBG_Euler.fields{1}.scale = 1.000000e-04;
WR224C.SBG_Euler.fields{1}.offset = 0.000000e+00;

WR224C.SBG_Euler.fields{2}.name = 'PITCH';
WR224C.SBG_Euler.fields{2}.units = 'rad';
WR224C.SBG_Euler.fields{2}.start_bit = 40;
WR224C.SBG_Euler.fields{2}.bit_length = 16;
WR224C.SBG_Euler.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Euler.fields{2}.data_type = 'SIGNED';
WR224C.SBG_Euler.fields{2}.maximum = 3.276700e+00;
WR224C.SBG_Euler.fields{2}.minimum = -3.276800e+00;
WR224C.SBG_Euler.fields{2}.scale = 1.000000e-04;
WR224C.SBG_Euler.fields{2}.offset = 0.000000e+00;

WR224C.SBG_Euler.fields{3}.name = 'ROLL';
WR224C.SBG_Euler.fields{3}.units = 'rad';
WR224C.SBG_Euler.fields{3}.start_bit = 56;
WR224C.SBG_Euler.fields{3}.bit_length = 16;
WR224C.SBG_Euler.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Euler.fields{3}.data_type = 'SIGNED';
WR224C.SBG_Euler.fields{3}.maximum = 3.276700e+00;
WR224C.SBG_Euler.fields{3}.minimum = -3.276800e+00;
WR224C.SBG_Euler.fields{3}.scale = 1.000000e-04;
WR224C.SBG_Euler.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_GPSPosition'
WR224C.SBG_GPSPosition = struct;
WR224C.SBG_GPSPosition.name = 'SBG_GPSPosition';
WR224C.SBG_GPSPosition.description = 'SBG_GPSPosition';
WR224C.SBG_GPSPosition.protocol = 'WR224C';
WR224C.SBG_GPSPosition.id = hex2dec('00000175');
WR224C.SBG_GPSPosition.idext = 'STANDARD';
WR224C.SBG_GPSPosition.idinherit = 0;
WR224C.SBG_GPSPosition.payload_size = 8;
WR224C.SBG_GPSPosition.payload_value = [];
WR224C.SBG_GPSPosition.payload_mask = [];
WR224C.SBG_GPSPosition.interval = -1;

WR224C.SBG_GPSPosition.fields{1}.name = 'SBG_GPSLongitude';
WR224C.SBG_GPSPosition.fields{1}.units = '';
WR224C.SBG_GPSPosition.fields{1}.start_bit = 24;
WR224C.SBG_GPSPosition.fields{1}.bit_length = 32;
WR224C.SBG_GPSPosition.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_GPSPosition.fields{1}.data_type = 'SIGNED';
WR224C.SBG_GPSPosition.fields{1}.scale = 1.000000e-07;
WR224C.SBG_GPSPosition.fields{1}.offset = 0.000000e+00;

WR224C.SBG_GPSPosition.fields{2}.name = 'SBG_GPSLatitude';
WR224C.SBG_GPSPosition.fields{2}.units = '';
WR224C.SBG_GPSPosition.fields{2}.start_bit = 56;
WR224C.SBG_GPSPosition.fields{2}.bit_length = 32;
WR224C.SBG_GPSPosition.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_GPSPosition.fields{2}.data_type = 'SIGNED';
WR224C.SBG_GPSPosition.fields{2}.scale = 1.000000e-07;
WR224C.SBG_GPSPosition.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'SBG_GyroRates'
WR224C.SBG_GyroRates = struct;
WR224C.SBG_GyroRates.name = 'SBG_GyroRates';
WR224C.SBG_GyroRates.description = 'SBG_GyroRates';
WR224C.SBG_GyroRates.protocol = 'WR224C';
WR224C.SBG_GyroRates.id = hex2dec('00000122');
WR224C.SBG_GyroRates.idext = 'STANDARD';
WR224C.SBG_GyroRates.idinherit = 0;
WR224C.SBG_GyroRates.payload_size = 6;
WR224C.SBG_GyroRates.payload_value = [];
WR224C.SBG_GyroRates.payload_mask = [];
WR224C.SBG_GyroRates.interval = -1;

WR224C.SBG_GyroRates.fields{1}.name = 'GyroRate_Z';
WR224C.SBG_GyroRates.fields{1}.units = 'rad.s-1';
WR224C.SBG_GyroRates.fields{1}.start_bit = 24;
WR224C.SBG_GyroRates.fields{1}.bit_length = 16;
WR224C.SBG_GyroRates.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_GyroRates.fields{1}.data_type = 'SIGNED';
WR224C.SBG_GyroRates.fields{1}.maximum = 3.276700e+01;
WR224C.SBG_GyroRates.fields{1}.minimum = -3.276800e+01;
WR224C.SBG_GyroRates.fields{1}.scale = 1.000000e-03;
WR224C.SBG_GyroRates.fields{1}.offset = 0.000000e+00;

WR224C.SBG_GyroRates.fields{2}.name = 'GyroRate_Y';
WR224C.SBG_GyroRates.fields{2}.units = 'rad.s-1';
WR224C.SBG_GyroRates.fields{2}.start_bit = 40;
WR224C.SBG_GyroRates.fields{2}.bit_length = 16;
WR224C.SBG_GyroRates.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_GyroRates.fields{2}.data_type = 'SIGNED';
WR224C.SBG_GyroRates.fields{2}.maximum = 3.276700e+01;
WR224C.SBG_GyroRates.fields{2}.minimum = -3.276800e+01;
WR224C.SBG_GyroRates.fields{2}.scale = 1.000000e-03;
WR224C.SBG_GyroRates.fields{2}.offset = 0.000000e+00;

WR224C.SBG_GyroRates.fields{3}.name = 'GyroRate_X';
WR224C.SBG_GyroRates.fields{3}.units = 'rad.s-1';
WR224C.SBG_GyroRates.fields{3}.start_bit = 56;
WR224C.SBG_GyroRates.fields{3}.bit_length = 16;
WR224C.SBG_GyroRates.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_GyroRates.fields{3}.data_type = 'SIGNED';
WR224C.SBG_GyroRates.fields{3}.maximum = 3.276700e+01;
WR224C.SBG_GyroRates.fields{3}.minimum = -3.276800e+01;
WR224C.SBG_GyroRates.fields{3}.scale = 1.000000e-03;
WR224C.SBG_GyroRates.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'SBG_Heading'
WR224C.SBG_Heading = struct;
WR224C.SBG_Heading.name = 'SBG_Heading';
WR224C.SBG_Heading.description = 'SBG_Heading';
WR224C.SBG_Heading.protocol = 'WR224C';
WR224C.SBG_Heading.id = hex2dec('00000173');
WR224C.SBG_Heading.idext = 'STANDARD';
WR224C.SBG_Heading.idinherit = 0;
WR224C.SBG_Heading.payload_size = 3;
WR224C.SBG_Heading.payload_value = [];
WR224C.SBG_Heading.payload_mask = [];
WR224C.SBG_Heading.interval = -1;

WR224C.SBG_Heading.fields{1}.name = 'SBG_Heading';
WR224C.SBG_Heading.fields{1}.units = 'deg';
WR224C.SBG_Heading.fields{1}.start_bit = 56;
WR224C.SBG_Heading.fields{1}.bit_length = 16;
WR224C.SBG_Heading.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_Heading.fields{1}.data_type = 'UNSIGNED';
WR224C.SBG_Heading.fields{1}.maximum = 6.550000e+02;
WR224C.SBG_Heading.fields{1}.minimum = 0.000000e+00;
WR224C.SBG_Heading.fields{1}.scale = 1.000000e-02;
WR224C.SBG_Heading.fields{1}.offset = 0.000000e+00;

%===================================================================
case 'SBG_VelocityInfo'
WR224C.SBG_VelocityInfo = struct;
WR224C.SBG_VelocityInfo.name = 'SBG_VelocityInfo';
WR224C.SBG_VelocityInfo.description = 'SBG_VelocityInfo';
WR224C.SBG_VelocityInfo.protocol = 'WR224C';
WR224C.SBG_VelocityInfo.id = hex2dec('00000172');
WR224C.SBG_VelocityInfo.idext = 'STANDARD';
WR224C.SBG_VelocityInfo.idinherit = 0;
WR224C.SBG_VelocityInfo.payload_size = 8;
WR224C.SBG_VelocityInfo.payload_value = [];
WR224C.SBG_VelocityInfo.payload_mask = [];
WR224C.SBG_VelocityInfo.interval = -1;

WR224C.SBG_VelocityInfo.fields{1}.name = 'SBG_VelocityType';
WR224C.SBG_VelocityInfo.fields{1}.units = '';
WR224C.SBG_VelocityInfo.fields{1}.start_bit = 30;
WR224C.SBG_VelocityInfo.fields{1}.bit_length = 6;
WR224C.SBG_VelocityInfo.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_VelocityInfo.fields{1}.data_type = 'UNSIGNED';
WR224C.SBG_VelocityInfo.fields{1}.maximum = 6.300000e+01;
WR224C.SBG_VelocityInfo.fields{1}.minimum = 0.000000e+00;
WR224C.SBG_VelocityInfo.fields{1}.scale = 1.000000e+00;
WR224C.SBG_VelocityInfo.fields{1}.offset = 0.000000e+00;

WR224C.SBG_VelocityInfo.fields{2}.name = 'SBG_VelocityStatus';
WR224C.SBG_VelocityInfo.fields{2}.units = '';
WR224C.SBG_VelocityInfo.fields{2}.start_bit = 24;
WR224C.SBG_VelocityInfo.fields{2}.bit_length = 6;
WR224C.SBG_VelocityInfo.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SBG_VelocityInfo.fields{2}.data_type = 'UNSIGNED';
WR224C.SBG_VelocityInfo.fields{2}.maximum = 6.300000e+01;
WR224C.SBG_VelocityInfo.fields{2}.minimum = 0.000000e+00;
WR224C.SBG_VelocityInfo.fields{2}.scale = 1.000000e+00;
WR224C.SBG_VelocityInfo.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'Shifting'
WR224C.Shifting = struct;
WR224C.Shifting.name = 'Shifting';
WR224C.Shifting.description = 'Shifting';
WR224C.Shifting.protocol = 'WR224C';
WR224C.Shifting.id = hex2dec('000000E2');
WR224C.Shifting.idext = 'STANDARD';
WR224C.Shifting.idinherit = 0;
WR224C.Shifting.payload_size = 8;
WR224C.Shifting.payload_value = [];
WR224C.Shifting.payload_mask = [];
WR224C.Shifting.interval = -1;

WR224C.Shifting.fields{1}.name = 'ShiftUpPaddle';
WR224C.Shifting.fields{1}.units = '';
WR224C.Shifting.fields{1}.start_bit = 55;
WR224C.Shifting.fields{1}.bit_length = 1;
WR224C.Shifting.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{1}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{1}.maximum = 1.000000e+00;
WR224C.Shifting.fields{1}.minimum = 0.000000e+00;
WR224C.Shifting.fields{1}.scale = 1.000000e+00;
WR224C.Shifting.fields{1}.offset = 0.000000e+00;

WR224C.Shifting.fields{2}.name = 'ShiftDownPaddle';
WR224C.Shifting.fields{2}.units = '';
WR224C.Shifting.fields{2}.start_bit = 47;
WR224C.Shifting.fields{2}.bit_length = 1;
WR224C.Shifting.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{2}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{2}.maximum = 1.000000e+00;
WR224C.Shifting.fields{2}.minimum = 0.000000e+00;
WR224C.Shifting.fields{2}.scale = 1.000000e+00;
WR224C.Shifting.fields{2}.offset = 0.000000e+00;

WR224C.Shifting.fields{3}.name = 'ShiftUpValid';
WR224C.Shifting.fields{3}.units = '';
WR224C.Shifting.fields{3}.start_bit = 46;
WR224C.Shifting.fields{3}.bit_length = 1;
WR224C.Shifting.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{3}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{3}.maximum = 1.000000e+00;
WR224C.Shifting.fields{3}.minimum = 0.000000e+00;
WR224C.Shifting.fields{3}.scale = 1.000000e+00;
WR224C.Shifting.fields{3}.offset = 0.000000e+00;

WR224C.Shifting.fields{4}.name = 'ShiftDownValid';
WR224C.Shifting.fields{4}.units = '';
WR224C.Shifting.fields{4}.start_bit = 45;
WR224C.Shifting.fields{4}.bit_length = 1;
WR224C.Shifting.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{4}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{4}.maximum = 1.000000e+00;
WR224C.Shifting.fields{4}.minimum = 0.000000e+00;
WR224C.Shifting.fields{4}.scale = 1.000000e+00;
WR224C.Shifting.fields{4}.offset = 0.000000e+00;

WR224C.Shifting.fields{5}.name = 'ShiftUpToSM';
WR224C.Shifting.fields{5}.units = '';
WR224C.Shifting.fields{5}.start_bit = 44;
WR224C.Shifting.fields{5}.bit_length = 1;
WR224C.Shifting.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{5}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{5}.maximum = 1.000000e+00;
WR224C.Shifting.fields{5}.minimum = 0.000000e+00;
WR224C.Shifting.fields{5}.scale = 1.000000e+00;
WR224C.Shifting.fields{5}.offset = 0.000000e+00;

WR224C.Shifting.fields{6}.name = 'ShiftDownToSM';
WR224C.Shifting.fields{6}.units = '';
WR224C.Shifting.fields{6}.start_bit = 43;
WR224C.Shifting.fields{6}.bit_length = 1;
WR224C.Shifting.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{6}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{6}.maximum = 1.000000e+00;
WR224C.Shifting.fields{6}.minimum = 0.000000e+00;
WR224C.Shifting.fields{6}.scale = 1.000000e+00;
WR224C.Shifting.fields{6}.offset = 0.000000e+00;

WR224C.Shifting.fields{7}.name = 'ShiftingSparkReq';
WR224C.Shifting.fields{7}.units = '';
WR224C.Shifting.fields{7}.start_bit = 42;
WR224C.Shifting.fields{7}.bit_length = 1;
WR224C.Shifting.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{7}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{7}.maximum = 1.000000e+00;
WR224C.Shifting.fields{7}.minimum = 0.000000e+00;
WR224C.Shifting.fields{7}.scale = 1.000000e+00;
WR224C.Shifting.fields{7}.offset = 0.000000e+00;

WR224C.Shifting.fields{8}.name = 'AutoShiftUpReady';
WR224C.Shifting.fields{8}.units = '';
WR224C.Shifting.fields{8}.start_bit = 41;
WR224C.Shifting.fields{8}.bit_length = 1;
WR224C.Shifting.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{8}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{8}.maximum = 1.000000e+00;
WR224C.Shifting.fields{8}.minimum = 0.000000e+00;
WR224C.Shifting.fields{8}.scale = 1.000000e+00;
WR224C.Shifting.fields{8}.offset = 0.000000e+00;

WR224C.Shifting.fields{9}.name = 'AutoshiftUpReady';
WR224C.Shifting.fields{9}.units = '';
WR224C.Shifting.fields{9}.start_bit = 40;
WR224C.Shifting.fields{9}.bit_length = 1;
WR224C.Shifting.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{9}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{9}.maximum = 1.000000e+00;
WR224C.Shifting.fields{9}.minimum = 0.000000e+00;
WR224C.Shifting.fields{9}.scale = 1.000000e+00;
WR224C.Shifting.fields{9}.offset = 0.000000e+00;

WR224C.Shifting.fields{10}.name = 'CurrentlyShifting';
WR224C.Shifting.fields{10}.units = '';
WR224C.Shifting.fields{10}.start_bit = 39;
WR224C.Shifting.fields{10}.bit_length = 1;
WR224C.Shifting.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{10}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{10}.maximum = 1.000000e+00;
WR224C.Shifting.fields{10}.minimum = 0.000000e+00;
WR224C.Shifting.fields{10}.scale = 1.000000e+00;
WR224C.Shifting.fields{10}.offset = 0.000000e+00;

WR224C.Shifting.fields{11}.name = 'ShiftingSparkRetard';
WR224C.Shifting.fields{11}.units = 'deg';
WR224C.Shifting.fields{11}.start_bit = 48;
WR224C.Shifting.fields{11}.bit_length = 7;
WR224C.Shifting.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{11}.data_type = 'SIGNED';
WR224C.Shifting.fields{11}.maximum = 1.270000e+02;
WR224C.Shifting.fields{11}.minimum = -1.280000e+02;
WR224C.Shifting.fields{11}.scale = 1.000000e+00;
WR224C.Shifting.fields{11}.offset = 0.000000e+00;

WR224C.Shifting.fields{12}.name = 'LoadState';
WR224C.Shifting.fields{12}.units = '';
WR224C.Shifting.fields{12}.start_bit = 36;
WR224C.Shifting.fields{12}.bit_length = 3;
WR224C.Shifting.fields{12}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{12}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{12}.maximum = 7.000000e+00;
WR224C.Shifting.fields{12}.minimum = 0.000000e+00;
WR224C.Shifting.fields{12}.scale = 1.000000e+00;
WR224C.Shifting.fields{12}.offset = 0.000000e+00;

WR224C.Shifting.fields{13}.name = 'ShiftUpSol';
WR224C.Shifting.fields{13}.units = '';
WR224C.Shifting.fields{13}.start_bit = 35;
WR224C.Shifting.fields{13}.bit_length = 1;
WR224C.Shifting.fields{13}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{13}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{13}.maximum = 1.000000e+00;
WR224C.Shifting.fields{13}.minimum = 0.000000e+00;
WR224C.Shifting.fields{13}.scale = 1.000000e+00;
WR224C.Shifting.fields{13}.offset = 0.000000e+00;

WR224C.Shifting.fields{14}.name = 'ShiftDownSol';
WR224C.Shifting.fields{14}.units = '';
WR224C.Shifting.fields{14}.start_bit = 34;
WR224C.Shifting.fields{14}.bit_length = 1;
WR224C.Shifting.fields{14}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{14}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{14}.maximum = 1.000000e+00;
WR224C.Shifting.fields{14}.minimum = 0.000000e+00;
WR224C.Shifting.fields{14}.scale = 1.000000e+00;
WR224C.Shifting.fields{14}.offset = 0.000000e+00;

WR224C.Shifting.fields{15}.name = 'ExtendedFiretime';
WR224C.Shifting.fields{15}.units = '';
WR224C.Shifting.fields{15}.start_bit = 33;
WR224C.Shifting.fields{15}.bit_length = 1;
WR224C.Shifting.fields{15}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{15}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{15}.maximum = 1.000000e+00;
WR224C.Shifting.fields{15}.minimum = 0.000000e+00;
WR224C.Shifting.fields{15}.scale = 1.000000e+00;
WR224C.Shifting.fields{15}.offset = 0.000000e+00;

WR224C.Shifting.fields{16}.name = 'DownshiftOverrev';
WR224C.Shifting.fields{16}.units = '';
WR224C.Shifting.fields{16}.start_bit = 32;
WR224C.Shifting.fields{16}.bit_length = 1;
WR224C.Shifting.fields{16}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{16}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{16}.maximum = 1.000000e+00;
WR224C.Shifting.fields{16}.minimum = 0.000000e+00;
WR224C.Shifting.fields{16}.scale = 1.000000e+00;
WR224C.Shifting.fields{16}.offset = 0.000000e+00;

WR224C.Shifting.fields{17}.name = 'ShftingDecel';
WR224C.Shifting.fields{17}.units = '';
WR224C.Shifting.fields{17}.start_bit = 31;
WR224C.Shifting.fields{17}.bit_length = 1;
WR224C.Shifting.fields{17}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{17}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{17}.maximum = 1.000000e+00;
WR224C.Shifting.fields{17}.minimum = 0.000000e+00;
WR224C.Shifting.fields{17}.scale = 1.000000e+00;
WR224C.Shifting.fields{17}.offset = 0.000000e+00;

WR224C.Shifting.fields{18}.name = 'ClutchSlipping';
WR224C.Shifting.fields{18}.units = '';
WR224C.Shifting.fields{18}.start_bit = 30;
WR224C.Shifting.fields{18}.bit_length = 1;
WR224C.Shifting.fields{18}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{18}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{18}.maximum = 1.000000e+00;
WR224C.Shifting.fields{18}.minimum = 0.000000e+00;
WR224C.Shifting.fields{18}.scale = 1.000000e+00;
WR224C.Shifting.fields{18}.offset = 0.000000e+00;

WR224C.Shifting.fields{19}.name = 'ShiftUpActReq';
WR224C.Shifting.fields{19}.units = '';
WR224C.Shifting.fields{19}.start_bit = 56;
WR224C.Shifting.fields{19}.bit_length = 1;
WR224C.Shifting.fields{19}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{19}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{19}.maximum = 1.000000e+00;
WR224C.Shifting.fields{19}.minimum = 0.000000e+00;
WR224C.Shifting.fields{19}.scale = 1.000000e+00;
WR224C.Shifting.fields{19}.offset = 0.000000e+00;

WR224C.Shifting.fields{20}.name = 'ShiftDownActReq';
WR224C.Shifting.fields{20}.units = '';
WR224C.Shifting.fields{20}.start_bit = 57;
WR224C.Shifting.fields{20}.bit_length = 1;
WR224C.Shifting.fields{20}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{20}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{20}.maximum = 1.000000e+00;
WR224C.Shifting.fields{20}.minimum = 0.000000e+00;
WR224C.Shifting.fields{20}.scale = 1.000000e+00;
WR224C.Shifting.fields{20}.offset = 0.000000e+00;

WR224C.Shifting.fields{21}.name = 'PaddleLogicState';
WR224C.Shifting.fields{21}.units = '';
WR224C.Shifting.fields{21}.start_bit = 26;
WR224C.Shifting.fields{21}.bit_length = 4;
WR224C.Shifting.fields{21}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{21}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{21}.maximum = 1.500000e+01;
WR224C.Shifting.fields{21}.minimum = 0.000000e+00;
WR224C.Shifting.fields{21}.scale = 1.000000e+00;
WR224C.Shifting.fields{21}.offset = 0.000000e+00;

WR224C.Shifting.fields{22}.name = 'ActuatorLogicState';
WR224C.Shifting.fields{22}.units = '';
WR224C.Shifting.fields{22}.start_bit = 58;
WR224C.Shifting.fields{22}.bit_length = 5;
WR224C.Shifting.fields{22}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{22}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{22}.maximum = 3.100000e+01;
WR224C.Shifting.fields{22}.minimum = 0.000000e+00;
WR224C.Shifting.fields{22}.scale = 1.000000e+00;
WR224C.Shifting.fields{22}.offset = 0.000000e+00;

WR224C.Shifting.fields{23}.name = 'BlipActive';
WR224C.Shifting.fields{23}.units = '';
WR224C.Shifting.fields{23}.start_bit = 63;
WR224C.Shifting.fields{23}.bit_length = 1;
WR224C.Shifting.fields{23}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{23}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{23}.maximum = 1.000000e+00;
WR224C.Shifting.fields{23}.minimum = 0.000000e+00;
WR224C.Shifting.fields{23}.scale = 1.000000e+00;
WR224C.Shifting.fields{23}.offset = 0.000000e+00;

WR224C.Shifting.fields{24}.name = 'LastShiftingFiretime';
WR224C.Shifting.fields{24}.units = 'ms';
WR224C.Shifting.fields{24}.start_bit = 18;
WR224C.Shifting.fields{24}.bit_length = 8;
WR224C.Shifting.fields{24}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{24}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{24}.maximum = 2.550000e+02;
WR224C.Shifting.fields{24}.minimum = 0.000000e+00;
WR224C.Shifting.fields{24}.scale = 1.000000e+00;
WR224C.Shifting.fields{24}.offset = 0.000000e+00;

WR224C.Shifting.fields{25}.name = 'EngineDecel';
WR224C.Shifting.fields{25}.units = '';
WR224C.Shifting.fields{25}.start_bit = 17;
WR224C.Shifting.fields{25}.bit_length = 1;
WR224C.Shifting.fields{25}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{25}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{25}.scale = 1.000000e+00;
WR224C.Shifting.fields{25}.offset = 0.000000e+00;

WR224C.Shifting.fields{26}.name = 'FuelingDecel';
WR224C.Shifting.fields{26}.units = '';
WR224C.Shifting.fields{26}.start_bit = 16;
WR224C.Shifting.fields{26}.bit_length = 1;
WR224C.Shifting.fields{26}.byte_order = 'BIG_ENDIAN';
WR224C.Shifting.fields{26}.data_type = 'UNSIGNED';
WR224C.Shifting.fields{26}.scale = 1.000000e+00;
WR224C.Shifting.fields{26}.offset = 0.000000e+00;

%===================================================================
case 'TestDayParams'
WR224C.TestDayParams = struct;
WR224C.TestDayParams.name = 'TestDayParams';
WR224C.TestDayParams.description = 'TestDayParams';
WR224C.TestDayParams.protocol = 'WR224C';
WR224C.TestDayParams.id = hex2dec('00000002');
WR224C.TestDayParams.idext = 'STANDARD';
WR224C.TestDayParams.idinherit = 0;
WR224C.TestDayParams.payload_size = 8;
WR224C.TestDayParams.payload_value = [];
WR224C.TestDayParams.payload_mask = [];
WR224C.TestDayParams.interval = -1;

WR224C.TestDayParams.fields{1}.name = 'EventMode';
WR224C.TestDayParams.fields{1}.units = '';
WR224C.TestDayParams.fields{1}.start_bit = 56;
WR224C.TestDayParams.fields{1}.bit_length = 3;
WR224C.TestDayParams.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{1}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{1}.maximum = 8.000000e+00;
WR224C.TestDayParams.fields{1}.minimum = 0.000000e+00;
WR224C.TestDayParams.fields{1}.scale = 1.000000e+00;
WR224C.TestDayParams.fields{1}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{2}.name = 'ChassisChanges';
WR224C.TestDayParams.fields{2}.units = '';
WR224C.TestDayParams.fields{2}.start_bit = 59;
WR224C.TestDayParams.fields{2}.bit_length = 5;
WR224C.TestDayParams.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{2}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{2}.scale = 1.000000e+00;
WR224C.TestDayParams.fields{2}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{3}.name = 'AeroChanges';
WR224C.TestDayParams.fields{3}.units = '';
WR224C.TestDayParams.fields{3}.start_bit = 51;
WR224C.TestDayParams.fields{3}.bit_length = 5;
WR224C.TestDayParams.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{3}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{3}.scale = 1.000000e+00;
WR224C.TestDayParams.fields{3}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{4}.name = 'PreRadHotSide';
WR224C.TestDayParams.fields{4}.units = '';
WR224C.TestDayParams.fields{4}.start_bit = 31;
WR224C.TestDayParams.fields{4}.bit_length = 10;
WR224C.TestDayParams.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{4}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{4}.maximum = 1.200000e+02;
WR224C.TestDayParams.fields{4}.minimum = 0.000000e+00;
WR224C.TestDayParams.fields{4}.scale = 1.170000e-01;
WR224C.TestDayParams.fields{4}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{5}.name = 'PostRadHotSide';
WR224C.TestDayParams.fields{5}.units = '';
WR224C.TestDayParams.fields{5}.start_bit = 21;
WR224C.TestDayParams.fields{5}.bit_length = 10;
WR224C.TestDayParams.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{5}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{5}.maximum = 1.200000e+02;
WR224C.TestDayParams.fields{5}.minimum = 0.000000e+00;
WR224C.TestDayParams.fields{5}.scale = 1.170000e-01;
WR224C.TestDayParams.fields{5}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{6}.name = 'PreRadColdSide';
WR224C.TestDayParams.fields{6}.units = '';
WR224C.TestDayParams.fields{6}.start_bit = 11;
WR224C.TestDayParams.fields{6}.bit_length = 10;
WR224C.TestDayParams.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{6}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{6}.maximum = 1.200000e+02;
WR224C.TestDayParams.fields{6}.minimum = 0.000000e+00;
WR224C.TestDayParams.fields{6}.scale = 1.170000e-01;
WR224C.TestDayParams.fields{6}.offset = 0.000000e+00;

WR224C.TestDayParams.fields{7}.name = 'PostRadColdSide';
WR224C.TestDayParams.fields{7}.units = '';
WR224C.TestDayParams.fields{7}.start_bit = 41;
WR224C.TestDayParams.fields{7}.bit_length = 10;
WR224C.TestDayParams.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.TestDayParams.fields{7}.data_type = 'UNSIGNED';
WR224C.TestDayParams.fields{7}.maximum = 1.200000e+02;
WR224C.TestDayParams.fields{7}.minimum = 0.000000e+00;
WR224C.TestDayParams.fields{7}.scale = 1.170000e-01;
WR224C.TestDayParams.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'TireTemp_FL'
WR224C.TireTemp_FL = struct;
WR224C.TireTemp_FL.name = 'TireTemp_FL';
WR224C.TireTemp_FL.description = 'TireTemp_FL';
WR224C.TireTemp_FL.protocol = 'WR224C';
WR224C.TireTemp_FL.id = hex2dec('00000001');
WR224C.TireTemp_FL.idext = 'STANDARD';
WR224C.TireTemp_FL.idinherit = 0;
WR224C.TireTemp_FL.payload_size = 8;
WR224C.TireTemp_FL.payload_value = [];
WR224C.TireTemp_FL.payload_mask = [];
WR224C.TireTemp_FL.interval = -1;

WR224C.TireTemp_FL.fields{1}.name = 'FL_ZoneA';
WR224C.TireTemp_FL.fields{1}.units = '';
WR224C.TireTemp_FL.fields{1}.start_bit = 56;
WR224C.TireTemp_FL.fields{1}.bit_length = 8;
WR224C.TireTemp_FL.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{1}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{1}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{1}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{2}.name = 'FL_ZoneB';
WR224C.TireTemp_FL.fields{2}.units = '';
WR224C.TireTemp_FL.fields{2}.start_bit = 48;
WR224C.TireTemp_FL.fields{2}.bit_length = 8;
WR224C.TireTemp_FL.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{2}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{2}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{2}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{3}.name = 'FL_Zone_C';
WR224C.TireTemp_FL.fields{3}.units = '';
WR224C.TireTemp_FL.fields{3}.start_bit = 40;
WR224C.TireTemp_FL.fields{3}.bit_length = 8;
WR224C.TireTemp_FL.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{3}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{3}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{3}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{4}.name = 'FL_Zone_D';
WR224C.TireTemp_FL.fields{4}.units = '';
WR224C.TireTemp_FL.fields{4}.start_bit = 32;
WR224C.TireTemp_FL.fields{4}.bit_length = 8;
WR224C.TireTemp_FL.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{4}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{4}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{4}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{5}.name = 'FL_Zone_E';
WR224C.TireTemp_FL.fields{5}.units = '';
WR224C.TireTemp_FL.fields{5}.start_bit = 24;
WR224C.TireTemp_FL.fields{5}.bit_length = 8;
WR224C.TireTemp_FL.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{5}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{5}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{5}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{6}.name = 'FL_Zone_F';
WR224C.TireTemp_FL.fields{6}.units = '';
WR224C.TireTemp_FL.fields{6}.start_bit = 16;
WR224C.TireTemp_FL.fields{6}.bit_length = 8;
WR224C.TireTemp_FL.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{6}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{6}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{6}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{7}.name = 'FL_ZoneG';
WR224C.TireTemp_FL.fields{7}.units = '';
WR224C.TireTemp_FL.fields{7}.start_bit = 8;
WR224C.TireTemp_FL.fields{7}.bit_length = 8;
WR224C.TireTemp_FL.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{7}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{7}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{7}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{8}.name = 'FL_ZoneH';
WR224C.TireTemp_FL.fields{8}.units = '';
WR224C.TireTemp_FL.fields{8}.start_bit = 0;
WR224C.TireTemp_FL.fields{8}.bit_length = 8;
WR224C.TireTemp_FL.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.TireTemp_FL.fields{8}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{8}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{8}.offset = 0.000000e+00;

end

try
	msg = WR224C.(msgname);
end