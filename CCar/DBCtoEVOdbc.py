def remove_lines(input_file, output_file, lines_to_remove):
    with open(input_file, 'r') as f_in, open(output_file, 'w') as f_out:
        for line in f_in:
            if line not in lines_to_remove:
                f_out.write(line)

# Lines to remove
lines_to_remove = [
    "BO_ 50 EVO_GPS: 8 AIMEvo5\n",
    " SG_ EVO_GPS_Speed : 7|32@0+ (1,0) [0|0] \"kmh\" Vector__XXX\n",
    " SG_ EVO_GPS_Pos_Acc : 39|16@0+ (1,0) [0|0] \"\" Vector__XXX\n",
    " SG_ EVO_GPS_Speed_Acc : 55|16@0+ (1,0) [0|0] \"\" Vector__XXX\n",
    "\n",
    "BO_ 52 EVO_LAP: 8 AIMEvo5\n",
    " SG_ EVO_LapTime : 7|32@0+ (1,0) [0|0] \"\" Vector__XXX\n",
    " SG_ EVO_Lap_Num : 39|32@0+ (1,0) [0|0] \"\" Vector__XXX\n",
    "\n",
    "BO_ 53 EVO_WS_0X35: 8 AIMEvo5\n",
    " SG_ EVO_WS_FL : 39|16@0+ (1,0) [0|0] \"mph\" Vector__XXX\n",
    " SG_ EVO_WS_FR : 55|16@0+ (1,0) [0|0] \"mph\" Vector__XXX\n",
    " SG_ EVO_WS_RL : 23|16@0+ (1,0) [0|0] \"mph\" Vector__XXX\n",
    " SG_ EVO_WS_RR : 7|16@0+ (1,0) [0|0] \"mph\" Vector__XXX\n",
    "\n",
    "BO_ 51 EVO_ACC_0x33: 8 AIMEvo5\n",
    " SG_ EVO_LatAcc : 63|8@0- (0.01,0) [-1.28|1.27] \"m/s^2\" Vector__XXX\n",
    " SG_ EVO_VertAcc : 55|8@0- (0.01,0) [-1.28|1.27] \"m/s^2\" Vector__XXX\n",
    " SG_ EVO_InlineAcc : 47|8@0- (0.01,0) [-1.28|1.27] \"m/s^2\" Vector__XXX\n",
    " SG_ EVO_YawRate : 7|32@0- (0.01,0) [-21474800|21474800] \"m/s^2\" Vector__XXX\n",
]


# Remove messages
remove_lines('C:\dbc-files\CCar\WR224C.dbc', 'C:\dbc-files\CCar\EVOWR224C.dbc', lines_to_remove)