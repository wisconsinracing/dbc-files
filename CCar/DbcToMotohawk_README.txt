1. Make the dbc you want. ensure it follows the naming convention: WR_2XX.dbc, where XX is the last two digits of the year.
2. If you have an old .m file with interval information, make sure it is in this directory. The intervals will not be tranferred to the new file otherwise.
3. Run the python script by double clicking it. If that doesn't work, try running it in jupyter notebook or vs code.
4. Take the .m file it produced and put it in C:\ECUFiles\Models\PCM112_224C_CP2\CAN

Done! Open matlab and you should see it.  