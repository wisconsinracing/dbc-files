#!/usr/bin/env python
# coding: utf-8
# %%
from decimal import Decimal


# %%

# takes in a dbc filepath and returns a list of CAN frames, represented as a dict. Eacht frame dict has a 'sigs' 
# key that points to a list of signal dicts, with each signal being one of the signals in that frame.
def dbc_to_python(dbc_file, matlab_file_path):
    filename = dbc_file.split('.')[0]
    frame_dicts = []
    skipping = False
    begin_skip = True
    with open(dbc_file, 'r') as dbc:
         
        for line in dbc:
            tokens = line.split()
            # skip the first several lines
            if begin_skip == True:
                try:
                    first = tokens[0]
                    second = tokens[1]
                    if first == 'BO_':
                        begin_skip = False
                    else:
                        continue
                except Exception as e:
                    continue
            # skip signals under VECTOR__INDEPENDENT_SIG_MSG
            if skipping:
                try: 
                    a = tokens[0]
                    continue
                except Exception as e:
                    skipping = False   
            
            # start reading the meat of the file
            try:
                first = tokens[0]
            except IndexError as e:
                # ignore blank lines
                continue
            # deal with making frames
            if first == 'BO_':
                # New message definition
                msg_name = tokens[2].split(":")[0]
                if msg_name == 'VECTOR__INDEPENDENT_SIG_MSG':
                    skipping = True
                    continue
                frame_dicts.append(parse_frame(tokens, filename))
            # makes signal dicts
            elif first == 'SG_':
                # Signal definition
                signal = parse_signal(tokens)
                frame_dicts[-1]['sigs'].append(signal)
            # add the descriptions from the bottom of the dbc
            elif first == 'CM_' and tokens[1] == 'BO_':
                find = convert_hex(int(tokens[2]))
                for frame in frame_dicts:
                    if frame['id'] == find:
                        frame['description'] = ' '.join(tokens[3:])[1:-2]
            try:
                intervals = parse_matlab(matlab_file_path)
                for frame in frame_dicts:
                    if frame['name'] in intervals:
                        frame['interval'] = intervals[frame['name']]
            except:
                pass
                
        return frame_dicts


# %%
#convert the start bit given in the dbc to the start bit used by matlab, for motorola signals (big endian)
def motorola_start_bit(bit, length):
    length = length-1
    if length == 0:
        return intel_start_bit(bit)
    hor_pos = bit % 8
    vert_pos = bit // 8
    while (length > 0):
        length -= 1
        if hor_pos == 0:
            vert_pos += 1
            hor_pos = 7
        else:
            hor_pos -= 1
    endbit = vert_pos * 8 + hor_pos
    return intel_start_bit(endbit)


# %%
#convert the start bit given in the dbc to the start bit used by matlab, for intel signals (little endian)
def intel_start_bit(dbc_start_bit):
    x = dbc_start_bit%8
    y = 8 * (dbc_start_bit//8)
    fin = 32 + (32 - y -8) + x
    return fin
    


# %%
def parse_matlab(matlab_file_path):
    message_intervals = {}
    with open(matlab_file_path, 'r') as matlab:
         
        for line in matlab:
            tokens = line.split()
            if len(tokens) > 0 and 'interval' in tokens[0]:
                message_intervals[tokens[0].split('.')[1]] = tokens[2].split(';')[0]
    return message_intervals
    


# %%
# return a dictionary containing relevant infor about a signal by parsing its line in the dbc
def parse_signal(tokens):
    signal_name = tokens[1]
    start_bit = int(tokens[3].split('|')[0])
    bit_length = int(tokens[3].split('|')[1].split('@')[0])
    byte_order = 'BIG_ENDIAN' if tokens[3].split('@')[1][0] == '0' else 'LITTLE_ENDIAN'
    data_type = 'UNSIGNED' if tokens[3].split('@')[1][1] == '+' else 'SIGNED'
    scale, offset = map(float, tokens[4][1:-1].split(','))
    minimum = float(tokens[5].split('|')[0][1:])
    maximum = float(tokens[5].split('|')[1][:-1])
    units = tokens[6][1:-1]
    ans = {
        'name': signal_name,
        'start_bit' : motorola_start_bit(start_bit, bit_length) if byte_order == 'BIG_ENDIAN' else intel_start_bit(start_bit),
        'bit_length': bit_length,
        'byte_order': byte_order,
        'data_type': data_type,
        'scale': convert_sci(Decimal(scale)),
        'offset': convert_sci(Decimal(offset)),
        'units': units
    }
    # if max and min were specified
    if not (minimum == 0.0 and maximum == 0.0):
        ans['minimum'] = convert_sci(Decimal(minimum))
        ans['maximum'] = convert_sci(Decimal(maximum))
    return ans


# %%
# converts a decimal float to a hex string
def convert_hex(dec):
    prelim = hex(dec).upper()[2:]
    
    num_zeros = 8-len(prelim)
    try:
        ans = "0"*num_zeros + prelim
        return ans
    except Exception as e:
        print(e)
        print("Your id may be too long")


# %%
# converts a decimal float to a scientific notation string
def convert_sci(n):
    return '%e' % n


# %%
# creates a frame dictionary
def parse_frame(line, filename):

    return {
    "name" : line[2][:-1],
    "description" : line[2][:-1],
    "protocol" : filename,
    "id" : convert_hex(int(line[1])),
    "idext" : "STANDARD", # note the hardcoding
    "idinherit" : 0, #hardcoded
    "payload_size" : line[3],
    'interval' : -1, #hardcoded, changeable in simulink
    'sigs':[]
    }


# %%

# once frames and signals are calculated, write all the data into a .m file in the form of a function with a
# big switch statement
def write_file(matlab_file_path, dbc_file_pathframes, filename):
    frames = dbc_to_python(dbc_file_path, matlab_file_path)
    frames = sorted(frames,  key = lambda frame : frame['name'])
    with open(matlab_file_path, 'w') as matlab_script:
        matlab_script.write('function msg = '+ matlab_file_path.split('.')[0] + "(msgname)\n")
        matlab_script.write('%msg = '+ matlab_file_path.split('.')[0] + "(msgname);\n\n")
        matlab_script.write('msg = struct;\n\n')
        matlab_script.write('switch msgname\n\n')
        for frame in frames:
            base_str = f"{filename}.{frame['name']}"
            matlab_script.write("%===================================================================\n")
            matlab_script.write(f"case \'{frame['name']}\'\n")
            matlab_script.write(base_str +" = struct;\n")
            matlab_script.write(base_str + f".name = \'{frame['name']}\';\n")
            matlab_script.write(base_str + f".description = \'{frame['description']}\';\n")
            matlab_script.write(base_str +f".protocol = \'{filename}\';\n")
            matlab_script.write(base_str +f".id = hex2dec(\'{frame['id']}\');\n")
            matlab_script.write(base_str +f".idext = \'{frame['idext']}\';\n")
            matlab_script.write(base_str +f".idinherit = {frame['idinherit']};\n")
            matlab_script.write(base_str +f".payload_size = {frame['payload_size']};\n")
            matlab_script.write(base_str +f".payload_value = [];\n") # hardcoded
            matlab_script.write(base_str +f".payload_mask = [];\n")
            matlab_script.write(base_str +f".interval = {frame['interval']};\n\n") # FIX LATER
            sig_num = 1
            base_str = base_str + ".fields{" + str(sig_num) + "}"
            for signal in frame['sigs']:
                first_half = base_str.split("{")[0]
                second_half = base_str.split('}')[1]
                base_str = first_half + '{' + str(sig_num) + '}' + second_half
                
                matlab_script.write(base_str + f".name = \'{signal['name']}\';\n")
                matlab_script.write(base_str + f".units = \'{signal['units']}\';\n")
                matlab_script.write(base_str +f".start_bit = {signal['start_bit']};\n")
                matlab_script.write(base_str +f".bit_length = {signal['bit_length']};\n")
                matlab_script.write(base_str +f".byte_order = \'{signal['byte_order']}\';\n")
                matlab_script.write(base_str +f".data_type = \'{signal['data_type']}\';\n")
                try:
                    matlab_script.write(base_str +f".maximum = {signal['maximum']};\n")
                    matlab_script.write(base_str +f".minimum = {signal['minimum']};\n")
                except KeyError as e:
                    pass
                matlab_script.write(base_str +f".scale = {signal['scale']};\n")
                matlab_script.write(base_str +f".offset = {signal['offset']};\n\n") # hardcoded
                sig_num += 1
        matlab_script.write("end\n\n")
        matlab_script.write("try\n\t")
        matlab_script.write(f"msg = {filename}.(msgname);\n")
        matlab_script.write("end")
        


# %%
# Example usage:
dbc_file_path = 'WR224C.dbc'
filename = dbc_file_path.split('.')[0]
matlab_file_path = "dbc_"+dbc_file_path.split('.')[0]+"_MHCan.m"
write_file(matlab_file_path, dbc_file_path, filename)


# %%
