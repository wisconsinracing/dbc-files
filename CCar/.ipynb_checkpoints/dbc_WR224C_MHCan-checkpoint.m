% -*- coding: utf-8 -*-
function msg = dbc_WR224C_MHCan(msgname)
%msg = dbc_WR224C_MHCan(msgname);

msg = struct;

switch msgname

%===================================================================
case 'Bosch_MMU1'
WR224C.Bosch_MMU1 = struct;
WR224C.Bosch_MMU1.name = 'Bosch_MMU1';
WR224C.Bosch_MMU1.description = 'Bosch_MMU1';
WR224C.Bosch_MMU1.protocol = 'WR224C';
WR224C.Bosch_MMU1.id = hex2dec('00000174');
WR224C.Bosch_MMU1.idext = 'STANDARD';
WR224C.Bosch_MMU1.idinherit = 0;
WR224C.Bosch_MMU1.payload_size = 8;
WR224C.Bosch_MMU1.payload_value = [];
WR224C.Bosch_MMU1.payload_mask = [];
WR224C.Bosch_MMU1.interval = -1;

WR224C.Bosch_MMU1.fields{1}.name = 'YRS_YawRate';
WR224C.Bosch_MMU1.fields{1}.units = 'deg/s';
WR224C.Bosch_MMU1.fields{1}.start_bit = 56;
WR224C.Bosch_MMU1.fields{1}.bit_length = 16;
WR224C.Bosch_MMU1.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU1.fields{1}.maximum = 1.600000e+02;
WR224C.Bosch_MMU1.fields{1}.minimum = -1.600000e+02;
WR224C.Bosch_MMU1.fields{1}.scale = 5.000000e-03;
WR224C.Bosch_MMU1.fields{1}.offset = -1.638400e+02;

WR224C.Bosch_MMU1.fields{2}.name = 'YRS_Reserved2_x70';
WR224C.Bosch_MMU1.fields{2}.units = '';
WR224C.Bosch_MMU1.fields{2}.start_bit = 8;
WR224C.Bosch_MMU1.fields{2}.bit_length = 16;
WR224C.Bosch_MMU1.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU1.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU1.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU1.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU1.fields{2}.offset = 3.276800e+04;

WR224C.Bosch_MMU1.fields{3}.name = 'YRS_Reserved1_x70';
WR224C.Bosch_MMU1.fields{3}.units = '';
WR224C.Bosch_MMU1.fields{3}.start_bit = 40;
WR224C.Bosch_MMU1.fields{3}.bit_length = 16;
WR224C.Bosch_MMU1.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{3}.data_type = 'SIGNED';
WR224C.Bosch_MMU1.fields{3}.maximum = 3.481500e+04;
WR224C.Bosch_MMU1.fields{3}.minimum = -3.072000e+04;
WR224C.Bosch_MMU1.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_MMU1.fields{3}.offset = 3.276800e+04;

WR224C.Bosch_MMU1.fields{4}.name = 'YRS_Acc_Y_axis';
WR224C.Bosch_MMU1.fields{4}.units = 'g';
WR224C.Bosch_MMU1.fields{4}.start_bit = 24;
WR224C.Bosch_MMU1.fields{4}.bit_length = 16;
WR224C.Bosch_MMU1.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU1.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU1.fields{4}.maximum = 4.200000e+00;
WR224C.Bosch_MMU1.fields{4}.minimum = -4.200000e+00;
WR224C.Bosch_MMU1.fields{4}.scale = 1.240000e-04;
WR224C.Bosch_MMU1.fields{4}.offset = -4.074640e+00;

%===================================================================
case 'Bosch_MMU2'
WR224C.Bosch_MMU2 = struct;
WR224C.Bosch_MMU2.name = 'Bosch_MMU2';
WR224C.Bosch_MMU2.description = 'Bosch_MMU2';
WR224C.Bosch_MMU2.protocol = 'WR224C';
WR224C.Bosch_MMU2.id = hex2dec('00000178');
WR224C.Bosch_MMU2.idext = 'STANDARD';
WR224C.Bosch_MMU2.idinherit = 0;
WR224C.Bosch_MMU2.payload_size = 8;
WR224C.Bosch_MMU2.payload_value = [];
WR224C.Bosch_MMU2.payload_mask = [];
WR224C.Bosch_MMU2.interval = -1;

WR224C.Bosch_MMU2.fields{1}.name = 'YRS_Reserved2_x80';
WR224C.Bosch_MMU2.fields{1}.units = '';
WR224C.Bosch_MMU2.fields{1}.start_bit = 8;
WR224C.Bosch_MMU2.fields{1}.bit_length = 16;
WR224C.Bosch_MMU2.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{1}.data_type = 'SIGNED';
WR224C.Bosch_MMU2.fields{1}.maximum = 3.481500e+04;
WR224C.Bosch_MMU2.fields{1}.minimum = -3.072000e+04;
WR224C.Bosch_MMU2.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_MMU2.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_MMU2.fields{2}.name = 'YRS_Reserved1_x80';
WR224C.Bosch_MMU2.fields{2}.units = '';
WR224C.Bosch_MMU2.fields{2}.start_bit = 40;
WR224C.Bosch_MMU2.fields{2}.bit_length = 16;
WR224C.Bosch_MMU2.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU2.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU2.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU2.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU2.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_MMU2.fields{3}.name = 'YRS_Acc_X_axis';
WR224C.Bosch_MMU2.fields{3}.units = 'g';
WR224C.Bosch_MMU2.fields{3}.start_bit = 24;
WR224C.Bosch_MMU2.fields{3}.bit_length = 16;
WR224C.Bosch_MMU2.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU2.fields{3}.maximum = 4.200000e+00;
WR224C.Bosch_MMU2.fields{3}.minimum = -4.200000e+00;
WR224C.Bosch_MMU2.fields{3}.scale = 1.274000e-04;
WR224C.Bosch_MMU2.fields{3}.offset = -4.174640e+00;

WR224C.Bosch_MMU2.fields{4}.name = 'YRS_Roll_Rate';
WR224C.Bosch_MMU2.fields{4}.units = 'deg/s';
WR224C.Bosch_MMU2.fields{4}.start_bit = 56;
WR224C.Bosch_MMU2.fields{4}.bit_length = 16;
WR224C.Bosch_MMU2.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU2.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU2.fields{4}.maximum = 1.630000e+02;
WR224C.Bosch_MMU2.fields{4}.minimum = -1.630000e+02;
WR224C.Bosch_MMU2.fields{4}.scale = 5.000000e-03;
WR224C.Bosch_MMU2.fields{4}.offset = -1.638400e+02;

%===================================================================
case 'Bosch_MMU3'
WR224C.Bosch_MMU3 = struct;
WR224C.Bosch_MMU3.name = 'Bosch_MMU3';
WR224C.Bosch_MMU3.description = 'Bosch_MMU3';
WR224C.Bosch_MMU3.protocol = 'WR224C';
WR224C.Bosch_MMU3.id = hex2dec('0000017C');
WR224C.Bosch_MMU3.idext = 'STANDARD';
WR224C.Bosch_MMU3.idinherit = 0;
WR224C.Bosch_MMU3.payload_size = 8;
WR224C.Bosch_MMU3.payload_value = [];
WR224C.Bosch_MMU3.payload_mask = [];
WR224C.Bosch_MMU3.interval = -1;

WR224C.Bosch_MMU3.fields{1}.name = 'YRS_Acc_Z_axis';
WR224C.Bosch_MMU3.fields{1}.units = 'g';
WR224C.Bosch_MMU3.fields{1}.start_bit = 24;
WR224C.Bosch_MMU3.fields{1}.bit_length = 16;
WR224C.Bosch_MMU3.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_MMU3.fields{1}.maximum = 4.200000e+00;
WR224C.Bosch_MMU3.fields{1}.minimum = -4.200000e+00;
WR224C.Bosch_MMU3.fields{1}.scale = 1.274000e-04;
WR224C.Bosch_MMU3.fields{1}.offset = -4.174646e+00;

WR224C.Bosch_MMU3.fields{2}.name = 'YRS_Reserved1_x90';
WR224C.Bosch_MMU3.fields{2}.units = '';
WR224C.Bosch_MMU3.fields{2}.start_bit = 56;
WR224C.Bosch_MMU3.fields{2}.bit_length = 16;
WR224C.Bosch_MMU3.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{2}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{2}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{2}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{2}.offset = 3.276800e+04;

WR224C.Bosch_MMU3.fields{3}.name = 'YRS_Reserved2_x90';
WR224C.Bosch_MMU3.fields{3}.units = '';
WR224C.Bosch_MMU3.fields{3}.start_bit = 40;
WR224C.Bosch_MMU3.fields{3}.bit_length = 16;
WR224C.Bosch_MMU3.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{3}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{3}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{3}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{3}.offset = 3.276800e+04;

WR224C.Bosch_MMU3.fields{4}.name = 'YRS_Reserved3_x90';
WR224C.Bosch_MMU3.fields{4}.units = '';
WR224C.Bosch_MMU3.fields{4}.start_bit = 8;
WR224C.Bosch_MMU3.fields{4}.bit_length = 16;
WR224C.Bosch_MMU3.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_MMU3.fields{4}.data_type = 'SIGNED';
WR224C.Bosch_MMU3.fields{4}.maximum = 3.481500e+04;
WR224C.Bosch_MMU3.fields{4}.minimum = -3.072000e+04;
WR224C.Bosch_MMU3.fields{4}.scale = 1.000000e+00;
WR224C.Bosch_MMU3.fields{4}.offset = 3.276800e+04;

%===================================================================
case 'Bosch_Steering_Angle_CONFIG'
WR224C.Bosch_Steering_Angle_CONFIG = struct;
WR224C.Bosch_Steering_Angle_CONFIG.name = 'Bosch_Steering_Angle_CONFIG';
WR224C.Bosch_Steering_Angle_CONFIG.description = 'Bosch_Steering_Angle_CONFIG';
WR224C.Bosch_Steering_Angle_CONFIG.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_CONFIG.id = hex2dec('000007C0');
WR224C.Bosch_Steering_Angle_CONFIG.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_CONFIG.idinherit = 0;
WR224C.Bosch_Steering_Angle_CONFIG.payload_size = 2;
WR224C.Bosch_Steering_Angle_CONFIG.payload_value = [];
WR224C.Bosch_Steering_Angle_CONFIG.payload_mask = [];
WR224C.Bosch_Steering_Angle_CONFIG.interval = -1;

WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.name = 'RES';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.start_bit = 60;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.bit_length = 12;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.maximum = 4.095000e+03;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.name = 'Codeword';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.start_bit = 56;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.bit_length = 4;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.maximum = 1.500000e+01;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_CONFIG.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Read'
WR224C.Bosch_Steering_Angle_Read = struct;
WR224C.Bosch_Steering_Angle_Read.name = 'Bosch_Steering_Angle_Read';
WR224C.Bosch_Steering_Angle_Read.description = 'Bosch_Steering_Angle_Read';
WR224C.Bosch_Steering_Angle_Read.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_Read.id = hex2dec('000002B0');
WR224C.Bosch_Steering_Angle_Read.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_Read.idinherit = 0;
WR224C.Bosch_Steering_Angle_Read.payload_size = 8;
WR224C.Bosch_Steering_Angle_Read.payload_value = [];
WR224C.Bosch_Steering_Angle_Read.payload_mask = [];
WR224C.Bosch_Steering_Angle_Read.interval = -1;

WR224C.Bosch_Steering_Angle_Read.fields{1}.name = 'TRIM';
WR224C.Bosch_Steering_Angle_Read.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{1}.start_bit = 34;
WR224C.Bosch_Steering_Angle_Read.fields{1}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{1}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{2}.name = 'Cal';
WR224C.Bosch_Steering_Angle_Read.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{2}.start_bit = 33;
WR224C.Bosch_Steering_Angle_Read.fields{2}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{2}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{3}.name = 'Ok_Status';
WR224C.Bosch_Steering_Angle_Read.fields{3}.units = '';
WR224C.Bosch_Steering_Angle_Read.fields{3}.start_bit = 32;
WR224C.Bosch_Steering_Angle_Read.fields{3}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Read.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{3}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{3}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{4}.name = 'SteeringAngle_Speed';
WR224C.Bosch_Steering_Angle_Read.fields{4}.units = '°/s';
WR224C.Bosch_Steering_Angle_Read.fields{4}.start_bit = 40;
WR224C.Bosch_Steering_Angle_Read.fields{4}.bit_length = 8;
WR224C.Bosch_Steering_Angle_Read.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{4}.maximum = 1.016000e+03;
WR224C.Bosch_Steering_Angle_Read.fields{4}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{4}.scale = 4.000000e+00;
WR224C.Bosch_Steering_Angle_Read.fields{4}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Read.fields{5}.name = 'SteeringAngle';
WR224C.Bosch_Steering_Angle_Read.fields{5}.units = '°';
WR224C.Bosch_Steering_Angle_Read.fields{5}.start_bit = 56;
WR224C.Bosch_Steering_Angle_Read.fields{5}.bit_length = 16;
WR224C.Bosch_Steering_Angle_Read.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Read.fields{5}.data_type = 'SIGNED';
WR224C.Bosch_Steering_Angle_Read.fields{5}.maximum = 7.800000e+02;
WR224C.Bosch_Steering_Angle_Read.fields{5}.minimum = -7.800000e+02;
WR224C.Bosch_Steering_Angle_Read.fields{5}.scale = 1.000000e-01;
WR224C.Bosch_Steering_Angle_Read.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Bosch_Steering_Angle_Write'
WR224C.Bosch_Steering_Angle_Write = struct;
WR224C.Bosch_Steering_Angle_Write.name = 'Bosch_Steering_Angle_Write';
WR224C.Bosch_Steering_Angle_Write.description = 'Bosch_Steering_Angle_Write';
WR224C.Bosch_Steering_Angle_Write.protocol = 'WR224C';
WR224C.Bosch_Steering_Angle_Write.id = hex2dec('000002B1');
WR224C.Bosch_Steering_Angle_Write.idext = 'STANDARD';
WR224C.Bosch_Steering_Angle_Write.idinherit = 0;
WR224C.Bosch_Steering_Angle_Write.payload_size = 8;
WR224C.Bosch_Steering_Angle_Write.payload_value = [];
WR224C.Bosch_Steering_Angle_Write.payload_mask = [];
WR224C.Bosch_Steering_Angle_Write.interval = -1;

WR224C.Bosch_Steering_Angle_Write.fields{1}.name = 'TRIM';
WR224C.Bosch_Steering_Angle_Write.fields{1}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{1}.start_bit = 34;
WR224C.Bosch_Steering_Angle_Write.fields{1}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{1}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{1}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{1}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{2}.name = 'Cal';
WR224C.Bosch_Steering_Angle_Write.fields{2}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{2}.start_bit = 33;
WR224C.Bosch_Steering_Angle_Write.fields{2}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{2}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{2}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{2}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{3}.name = 'Ok_Status';
WR224C.Bosch_Steering_Angle_Write.fields{3}.units = '';
WR224C.Bosch_Steering_Angle_Write.fields{3}.start_bit = 32;
WR224C.Bosch_Steering_Angle_Write.fields{3}.bit_length = 1;
WR224C.Bosch_Steering_Angle_Write.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{3}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{3}.maximum = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.scale = 1.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{3}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{4}.name = 'SteeringAngle_Speed';
WR224C.Bosch_Steering_Angle_Write.fields{4}.units = '°/s';
WR224C.Bosch_Steering_Angle_Write.fields{4}.start_bit = 40;
WR224C.Bosch_Steering_Angle_Write.fields{4}.bit_length = 8;
WR224C.Bosch_Steering_Angle_Write.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{4}.data_type = 'UNSIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{4}.maximum = 1.016000e+03;
WR224C.Bosch_Steering_Angle_Write.fields{4}.minimum = 0.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{4}.scale = 4.000000e+00;
WR224C.Bosch_Steering_Angle_Write.fields{4}.offset = 0.000000e+00;

WR224C.Bosch_Steering_Angle_Write.fields{5}.name = 'SteeringAngle';
WR224C.Bosch_Steering_Angle_Write.fields{5}.units = '°';
WR224C.Bosch_Steering_Angle_Write.fields{5}.start_bit = 56;
WR224C.Bosch_Steering_Angle_Write.fields{5}.bit_length = 16;
WR224C.Bosch_Steering_Angle_Write.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Bosch_Steering_Angle_Write.fields{5}.data_type = 'SIGNED';
WR224C.Bosch_Steering_Angle_Write.fields{5}.maximum = 7.800000e+02;
WR224C.Bosch_Steering_Angle_Write.fields{5}.minimum = -7.800000e+02;
WR224C.Bosch_Steering_Angle_Write.fields{5}.scale = 1.000000e-01;
WR224C.Bosch_Steering_Angle_Write.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'Dyno_Signals'
WR224C.Dyno_Signals = struct;
WR224C.Dyno_Signals.name = 'Dyno_Signals';
WR224C.Dyno_Signals.description = 'Dyno_Signals';
WR224C.Dyno_Signals.protocol = 'WR224C';
WR224C.Dyno_Signals.id = hex2dec('0000002E');
WR224C.Dyno_Signals.idext = 'STANDARD';
WR224C.Dyno_Signals.idinherit = 0;
WR224C.Dyno_Signals.payload_size = 8;
WR224C.Dyno_Signals.payload_value = [];
WR224C.Dyno_Signals.payload_mask = [];
WR224C.Dyno_Signals.interval = -1;

WR224C.Dyno_Signals.fields{1}.name = 'Ambient_Temp';
WR224C.Dyno_Signals.fields{1}.units = '';
WR224C.Dyno_Signals.fields{1}.start_bit = 15;
WR224C.Dyno_Signals.fields{1}.bit_length = 9;
WR224C.Dyno_Signals.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{1}.data_type = 'SIGNED';
WR224C.Dyno_Signals.fields{1}.maximum = 1.000000e+02;
WR224C.Dyno_Signals.fields{1}.minimum = 0.000000e+00;
WR224C.Dyno_Signals.fields{1}.scale = 3.906000e-01;
WR224C.Dyno_Signals.fields{1}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{2}.name = 'EGT1';
WR224C.Dyno_Signals.fields{2}.units = '';
WR224C.Dyno_Signals.fields{2}.start_bit = 21;
WR224C.Dyno_Signals.fields{2}.bit_length = 10;
WR224C.Dyno_Signals.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{2}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{2}.scale = 9.765630e-01;
WR224C.Dyno_Signals.fields{2}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{3}.name = 'Dyno_Torque';
WR224C.Dyno_Signals.fields{3}.units = '';
WR224C.Dyno_Signals.fields{3}.start_bit = 43;
WR224C.Dyno_Signals.fields{3}.bit_length = 16;
WR224C.Dyno_Signals.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{3}.data_type = 'SIGNED';
WR224C.Dyno_Signals.fields{3}.scale = 6.103520e-03;
WR224C.Dyno_Signals.fields{3}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{4}.name = 'RPM_Setpoint';
WR224C.Dyno_Signals.fields{4}.units = '';
WR224C.Dyno_Signals.fields{4}.start_bit = 62;
WR224C.Dyno_Signals.fields{4}.bit_length = 13;
WR224C.Dyno_Signals.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{4}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{4}.scale = 1.000000e+00;
WR224C.Dyno_Signals.fields{4}.offset = 0.000000e+00;

WR224C.Dyno_Signals.fields{5}.name = 'EGT2';
WR224C.Dyno_Signals.fields{5}.units = '';
WR224C.Dyno_Signals.fields{5}.start_bit = 27;
WR224C.Dyno_Signals.fields{5}.bit_length = 10;
WR224C.Dyno_Signals.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.Dyno_Signals.fields{5}.data_type = 'UNSIGNED';
WR224C.Dyno_Signals.fields{5}.scale = 6.103520e-03;
WR224C.Dyno_Signals.fields{5}.offset = 0.000000e+00;

%===================================================================
case 'EVO_ACC_0x33'
WR224C.EVO_ACC_0x33 = struct;
WR224C.EVO_ACC_0x33.name = 'EVO_ACC_0x33';
WR224C.EVO_ACC_0x33.description = 'EVO_ACC_0x33';
WR224C.EVO_ACC_0x33.protocol = 'WR224C';
WR224C.EVO_ACC_0x33.id = hex2dec('00000033');
WR224C.EVO_ACC_0x33.idext = 'STANDARD';
WR224C.EVO_ACC_0x33.idinherit = 0;
WR224C.EVO_ACC_0x33.payload_size = 8;
WR224C.EVO_ACC_0x33.payload_value = [];
WR224C.EVO_ACC_0x33.payload_mask = [];
WR224C.EVO_ACC_0x33.interval = -1;

WR224C.EVO_ACC_0x33.fields{1}.name = 'EVO_LatAcc';
WR224C.EVO_ACC_0x33.fields{1}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{1}.start_bit = 0;
WR224C.EVO_ACC_0x33.fields{1}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{1}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{1}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{1}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{1}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{1}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{2}.name = 'EVO_VertAcc';
WR224C.EVO_ACC_0x33.fields{2}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{2}.start_bit = 8;
WR224C.EVO_ACC_0x33.fields{2}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{2}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{2}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{2}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{2}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{2}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{3}.name = 'EVO_InlineAcc';
WR224C.EVO_ACC_0x33.fields{3}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{3}.start_bit = 16;
WR224C.EVO_ACC_0x33.fields{3}.bit_length = 8;
WR224C.EVO_ACC_0x33.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{3}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{3}.maximum = 1.270000e+00;
WR224C.EVO_ACC_0x33.fields{3}.minimum = -1.280000e+00;
WR224C.EVO_ACC_0x33.fields{3}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{3}.offset = 0.000000e+00;

WR224C.EVO_ACC_0x33.fields{4}.name = 'EVO_YawRate';
WR224C.EVO_ACC_0x33.fields{4}.units = 'm/s^2';
WR224C.EVO_ACC_0x33.fields{4}.start_bit = 32;
WR224C.EVO_ACC_0x33.fields{4}.bit_length = 32;
WR224C.EVO_ACC_0x33.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_ACC_0x33.fields{4}.data_type = 'SIGNED';
WR224C.EVO_ACC_0x33.fields{4}.maximum = 2.147480e+07;
WR224C.EVO_ACC_0x33.fields{4}.minimum = -2.147480e+07;
WR224C.EVO_ACC_0x33.fields{4}.scale = 1.000000e-02;
WR224C.EVO_ACC_0x33.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'EVO_GPS'
WR224C.EVO_GPS = struct;
WR224C.EVO_GPS.name = 'EVO_GPS';
WR224C.EVO_GPS.description = 'EVO_GPS';
WR224C.EVO_GPS.protocol = 'WR224C';
WR224C.EVO_GPS.id = hex2dec('00000032');
WR224C.EVO_GPS.idext = 'STANDARD';
WR224C.EVO_GPS.idinherit = 0;
WR224C.EVO_GPS.payload_size = 8;
WR224C.EVO_GPS.payload_value = [];
WR224C.EVO_GPS.payload_mask = [];
WR224C.EVO_GPS.interval = -1;

WR224C.EVO_GPS.fields{1}.name = 'EVO_GPS_Speed';
WR224C.EVO_GPS.fields{1}.units = 'kmh';
WR224C.EVO_GPS.fields{1}.start_bit = 32;
WR224C.EVO_GPS.fields{1}.bit_length = 32;
WR224C.EVO_GPS.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{1}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{1}.offset = 0.000000e+00;

WR224C.EVO_GPS.fields{2}.name = 'EVO_GPS_Pos_Acc';
WR224C.EVO_GPS.fields{2}.units = '';
WR224C.EVO_GPS.fields{2}.start_bit = 16;
WR224C.EVO_GPS.fields{2}.bit_length = 16;
WR224C.EVO_GPS.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{2}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{2}.offset = 0.000000e+00;

WR224C.EVO_GPS.fields{3}.name = 'EVO_GPS_Speed_Acc';
WR224C.EVO_GPS.fields{3}.units = '';
WR224C.EVO_GPS.fields{3}.start_bit = 0;
WR224C.EVO_GPS.fields{3}.bit_length = 16;
WR224C.EVO_GPS.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_GPS.fields{3}.data_type = 'UNSIGNED';
WR224C.EVO_GPS.fields{3}.scale = 1.000000e+00;
WR224C.EVO_GPS.fields{3}.offset = 0.000000e+00;

%===================================================================
case 'EVO_LAP'
WR224C.EVO_LAP = struct;
WR224C.EVO_LAP.name = 'EVO_LAP';
WR224C.EVO_LAP.description = 'EVO_LAP';
WR224C.EVO_LAP.protocol = 'WR224C';
WR224C.EVO_LAP.id = hex2dec('00000034');
WR224C.EVO_LAP.idext = 'STANDARD';
WR224C.EVO_LAP.idinherit = 0;
WR224C.EVO_LAP.payload_size = 8;
WR224C.EVO_LAP.payload_value = [];
WR224C.EVO_LAP.payload_mask = [];
WR224C.EVO_LAP.interval = -1;

WR224C.EVO_LAP.fields{1}.name = 'EVO_LapTime';
WR224C.EVO_LAP.fields{1}.units = '';
WR224C.EVO_LAP.fields{1}.start_bit = 32;
WR224C.EVO_LAP.fields{1}.bit_length = 32;
WR224C.EVO_LAP.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_LAP.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_LAP.fields{1}.scale = 1.000000e+00;
WR224C.EVO_LAP.fields{1}.offset = 0.000000e+00;

WR224C.EVO_LAP.fields{2}.name = 'EVO_Lap_Num';
WR224C.EVO_LAP.fields{2}.units = '';
WR224C.EVO_LAP.fields{2}.start_bit = 0;
WR224C.EVO_LAP.fields{2}.bit_length = 32;
WR224C.EVO_LAP.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_LAP.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_LAP.fields{2}.scale = 1.000000e+00;
WR224C.EVO_LAP.fields{2}.offset = 0.000000e+00;

%===================================================================
case 'EVO_WS_0X35'
WR224C.EVO_WS_0X35 = struct;
WR224C.EVO_WS_0X35.name = 'EVO_WS_0X35';
WR224C.EVO_WS_0X35.description = 'EVO_WS_0X35';
WR224C.EVO_WS_0X35.protocol = 'WR224C';
WR224C.EVO_WS_0X35.id = hex2dec('00000035');
WR224C.EVO_WS_0X35.idext = 'STANDARD';
WR224C.EVO_WS_0X35.idinherit = 0;
WR224C.EVO_WS_0X35.payload_size = 8;
WR224C.EVO_WS_0X35.payload_value = [];
WR224C.EVO_WS_0X35.payload_mask = [];
WR224C.EVO_WS_0X35.interval = -1;

WR224C.EVO_WS_0X35.fields{1}.name = 'EVO_WS_FL';
WR224C.EVO_WS_0X35.fields{1}.units = 'mph';
WR224C.EVO_WS_0X35.fields{1}.start_bit = 16;
WR224C.EVO_WS_0X35.fields{1}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{1}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{1}.scale = 1.000000e+00;
WR224C.EVO_WS_0X35.fields{1}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{2}.name = 'EVO_WS_FR';
WR224C.EVO_WS_0X35.fields{2}.units = 'mph';
WR224C.EVO_WS_0X35.fields{2}.start_bit = 0;
WR224C.EVO_WS_0X35.fields{2}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{2}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{2}.scale = 1.000000e+00;
WR224C.EVO_WS_0X35.fields{2}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{3}.name = 'EVO_WS_RL';
WR224C.EVO_WS_0X35.fields{3}.units = 'mph';
WR224C.EVO_WS_0X35.fields{3}.start_bit = 32;
WR224C.EVO_WS_0X35.fields{3}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{3}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{3}.scale = 1.000000e+00;
WR224C.EVO_WS_0X35.fields{3}.offset = 0.000000e+00;

WR224C.EVO_WS_0X35.fields{4}.name = 'EVO_WS_RR';
WR224C.EVO_WS_0X35.fields{4}.units = 'mph';
WR224C.EVO_WS_0X35.fields{4}.start_bit = 48;
WR224C.EVO_WS_0X35.fields{4}.bit_length = 16;
WR224C.EVO_WS_0X35.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.EVO_WS_0X35.fields{4}.data_type = 'UNSIGNED';
WR224C.EVO_WS_0X35.fields{4}.scale = 1.000000e+00;
WR224C.EVO_WS_0X35.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_1'
WR224C.PCM_A_1 = struct;
WR224C.PCM_A_1.name = 'PCM_A_1';
WR224C.PCM_A_1.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_1.protocol = 'WR224C';
WR224C.PCM_A_1.id = hex2dec('000000DC');
WR224C.PCM_A_1.idext = 'STANDARD';
WR224C.PCM_A_1.idinherit = 0;
WR224C.PCM_A_1.payload_size = 8;
WR224C.PCM_A_1.payload_value = [];
WR224C.PCM_A_1.payload_mask = [];
WR224C.PCM_A_1.interval = -1;

WR224C.PCM_A_1.fields{1}.name = 'Is_Real_RPM';
WR224C.PCM_A_1.fields{1}.units = 'bool';
WR224C.PCM_A_1.fields{1}.start_bit = 58;
WR224C.PCM_A_1.fields{1}.bit_length = 1;
WR224C.PCM_A_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{1}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{1}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{2}.name = 'RPM';
WR224C.PCM_A_1.fields{2}.units = 'RPM';
WR224C.PCM_A_1.fields{2}.start_bit = 32;
WR224C.PCM_A_1.fields{2}.bit_length = 16;
WR224C.PCM_A_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{2}.maximum = 1.300000e+04;
WR224C.PCM_A_1.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{2}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{3}.name = 'Plenum_MAP';
WR224C.PCM_A_1.fields{3}.units = 'kPa';
WR224C.PCM_A_1.fields{3}.start_bit = 48;
WR224C.PCM_A_1.fields{3}.bit_length = 10;
WR224C.PCM_A_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{3}.maximum = 1.023000e+02;
WR224C.PCM_A_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{3}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{4}.name = 'VE_Adapt_Enable';
WR224C.PCM_A_1.fields{4}.units = 'bool';
WR224C.PCM_A_1.fields{4}.start_bit = 59;
WR224C.PCM_A_1.fields{4}.bit_length = 1;
WR224C.PCM_A_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{4}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{4}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{5}.name = 'Steady_State_Op';
WR224C.PCM_A_1.fields{5}.units = 'bool';
WR224C.PCM_A_1.fields{5}.start_bit = 60;
WR224C.PCM_A_1.fields{5}.bit_length = 1;
WR224C.PCM_A_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{5}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{5}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{6}.name = 'EStop';
WR224C.PCM_A_1.fields{6}.units = 'bool';
WR224C.PCM_A_1.fields{6}.start_bit = 61;
WR224C.PCM_A_1.fields{6}.bit_length = 1;
WR224C.PCM_A_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{6}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{6}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{7}.name = 'Check_Eng_Light';
WR224C.PCM_A_1.fields{7}.units = 'bool';
WR224C.PCM_A_1.fields{7}.start_bit = 62;
WR224C.PCM_A_1.fields{7}.bit_length = 1;
WR224C.PCM_A_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{7}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{7}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{8}.name = 'TPS2';
WR224C.PCM_A_1.fields{8}.units = '%';
WR224C.PCM_A_1.fields{8}.start_bit = 22;
WR224C.PCM_A_1.fields{8}.bit_length = 10;
WR224C.PCM_A_1.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{8}.maximum = 1.000000e+02;
WR224C.PCM_A_1.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{8}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{8}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{9}.name = 'TPS1';
WR224C.PCM_A_1.fields{9}.units = '%';
WR224C.PCM_A_1.fields{9}.start_bit = 12;
WR224C.PCM_A_1.fields{9}.bit_length = 10;
WR224C.PCM_A_1.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{9}.maximum = 1.000000e+02;
WR224C.PCM_A_1.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{9}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{9}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{10}.name = 'Voltage';
WR224C.PCM_A_1.fields{10}.units = 'V';
WR224C.PCM_A_1.fields{10}.start_bit = 4;
WR224C.PCM_A_1.fields{10}.bit_length = 8;
WR224C.PCM_A_1.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{10}.maximum = 1.600000e+01;
WR224C.PCM_A_1.fields{10}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{10}.scale = 1.000000e-01;
WR224C.PCM_A_1.fields{10}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{11}.name = 'Gear';
WR224C.PCM_A_1.fields{11}.units = '#';
WR224C.PCM_A_1.fields{11}.start_bit = 0;
WR224C.PCM_A_1.fields{11}.bit_length = 4;
WR224C.PCM_A_1.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{11}.maximum = 1.000000e+01;
WR224C.PCM_A_1.fields{11}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{11}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{11}.offset = 0.000000e+00;

WR224C.PCM_A_1.fields{12}.name = 'Fuel_Pump_Req';
WR224C.PCM_A_1.fields{12}.units = 'bool';
WR224C.PCM_A_1.fields{12}.start_bit = 63;
WR224C.PCM_A_1.fields{12}.bit_length = 1;
WR224C.PCM_A_1.fields{12}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_1.fields{12}.data_type = 'UNSIGNED';
WR224C.PCM_A_1.fields{12}.maximum = 1.000000e+00;
WR224C.PCM_A_1.fields{12}.minimum = 0.000000e+00;
WR224C.PCM_A_1.fields{12}.scale = 1.000000e+00;
WR224C.PCM_A_1.fields{12}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_2'
WR224C.PCM_A_2 = struct;
WR224C.PCM_A_2.name = 'PCM_A_2';
WR224C.PCM_A_2.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_2.protocol = 'WR224C';
WR224C.PCM_A_2.id = hex2dec('000000DD');
WR224C.PCM_A_2.idext = 'STANDARD';
WR224C.PCM_A_2.idinherit = 0;
WR224C.PCM_A_2.payload_size = 8;
WR224C.PCM_A_2.payload_value = [];
WR224C.PCM_A_2.payload_mask = [];
WR224C.PCM_A_2.interval = -1;

WR224C.PCM_A_2.fields{1}.name = 'APPS1';
WR224C.PCM_A_2.fields{1}.units = '%';
WR224C.PCM_A_2.fields{1}.start_bit = 54;
WR224C.PCM_A_2.fields{1}.bit_length = 10;
WR224C.PCM_A_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{1}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{2}.name = 'APPS2';
WR224C.PCM_A_2.fields{2}.units = '%';
WR224C.PCM_A_2.fields{2}.start_bit = 44;
WR224C.PCM_A_2.fields{2}.bit_length = 10;
WR224C.PCM_A_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{2}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{3}.name = 'Cyl1_Phi';
WR224C.PCM_A_2.fields{3}.units = 'EqRatio';
WR224C.PCM_A_2.fields{3}.start_bit = 26;
WR224C.PCM_A_2.fields{3}.bit_length = 8;
WR224C.PCM_A_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{3}.maximum = 2.550000e+00;
WR224C.PCM_A_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{3}.scale = 1.000000e-02;
WR224C.PCM_A_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{4}.name = 'Throttle_Req';
WR224C.PCM_A_2.fields{4}.units = '%';
WR224C.PCM_A_2.fields{4}.start_bit = 34;
WR224C.PCM_A_2.fields{4}.bit_length = 10;
WR224C.PCM_A_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{4}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{4}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{5}.name = 'HW_MAF';
WR224C.PCM_A_2.fields{5}.units = 'g/s';
WR224C.PCM_A_2.fields{5}.start_bit = 16;
WR224C.PCM_A_2.fields{5}.bit_length = 10;
WR224C.PCM_A_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{5}.maximum = 1.000000e+02;
WR224C.PCM_A_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{5}.scale = 1.000000e-01;
WR224C.PCM_A_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{6}.name = 'Cyl2_Phi';
WR224C.PCM_A_2.fields{6}.units = 'EqRatio';
WR224C.PCM_A_2.fields{6}.start_bit = 8;
WR224C.PCM_A_2.fields{6}.bit_length = 8;
WR224C.PCM_A_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{6}.maximum = 2.550000e+00;
WR224C.PCM_A_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{6}.scale = 1.000000e-02;
WR224C.PCM_A_2.fields{6}.offset = 0.000000e+00;

WR224C.PCM_A_2.fields{7}.name = 'IMEP';
WR224C.PCM_A_2.fields{7}.units = 'kPa';
WR224C.PCM_A_2.fields{7}.start_bit = 0;
WR224C.PCM_A_2.fields{7}.bit_length = 8;
WR224C.PCM_A_2.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_2.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_A_2.fields{7}.maximum = 1.500000e+03;
WR224C.PCM_A_2.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_A_2.fields{7}.scale = 1.000000e+01;
WR224C.PCM_A_2.fields{7}.offset = 0.000000e+00;

%===================================================================
case 'PCM_A_3'
WR224C.PCM_A_3 = struct;
WR224C.PCM_A_3.name = 'PCM_A_3';
WR224C.PCM_A_3.description = 'Should have same frequency/interval as other A frames.';
WR224C.PCM_A_3.protocol = 'WR224C';
WR224C.PCM_A_3.id = hex2dec('000000DE');
WR224C.PCM_A_3.idext = 'STANDARD';
WR224C.PCM_A_3.idinherit = 0;
WR224C.PCM_A_3.payload_size = 8;
WR224C.PCM_A_3.payload_value = [];
WR224C.PCM_A_3.payload_mask = [];
WR224C.PCM_A_3.interval = 10000;

WR224C.PCM_A_3.fields{1}.name = 'APC';
WR224C.PCM_A_3.fields{1}.units = 'mg';
WR224C.PCM_A_3.fields{1}.start_bit = 52;
WR224C.PCM_A_3.fields{1}.bit_length = 12;
WR224C.PCM_A_3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{1}.maximum = 4.000000e+02;
WR224C.PCM_A_3.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{1}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{1}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{2}.name = 'Load';
WR224C.PCM_A_3.fields{2}.units = '%';
WR224C.PCM_A_3.fields{2}.start_bit = 10;
WR224C.PCM_A_3.fields{2}.bit_length = 10;
WR224C.PCM_A_3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_A_3.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{2}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{2}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{3}.name = 'Cyl1_FPC';
WR224C.PCM_A_3.fields{3}.units = 'mg';
WR224C.PCM_A_3.fields{3}.start_bit = 31;
WR224C.PCM_A_3.fields{3}.bit_length = 10;
WR224C.PCM_A_3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{3}.maximum = 1.023000e+02;
WR224C.PCM_A_3.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{3}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{3}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{4}.name = 'Cyl2_FPC';
WR224C.PCM_A_3.fields{4}.units = 'mg';
WR224C.PCM_A_3.fields{4}.start_bit = 0;
WR224C.PCM_A_3.fields{4}.bit_length = 10;
WR224C.PCM_A_3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_A_3.fields{4}.maximum = 1.023000e+02;
WR224C.PCM_A_3.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_A_3.fields{4}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{4}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{5}.name = 'Cyl1_Spark_Adv';
WR224C.PCM_A_3.fields{5}.units = 'deg';
WR224C.PCM_A_3.fields{5}.start_bit = 20;
WR224C.PCM_A_3.fields{5}.bit_length = 11;
WR224C.PCM_A_3.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{5}.data_type = 'SIGNED';
WR224C.PCM_A_3.fields{5}.maximum = 1.020000e+02;
WR224C.PCM_A_3.fields{5}.minimum = -1.020000e+02;
WR224C.PCM_A_3.fields{5}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{5}.offset = 0.000000e+00;

WR224C.PCM_A_3.fields{6}.name = 'Cyl2_Spark_Adv';
WR224C.PCM_A_3.fields{6}.units = 'deg';
WR224C.PCM_A_3.fields{6}.start_bit = 41;
WR224C.PCM_A_3.fields{6}.bit_length = 11;
WR224C.PCM_A_3.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_A_3.fields{6}.data_type = 'SIGNED';
WR224C.PCM_A_3.fields{6}.maximum = 1.020000e+02;
WR224C.PCM_A_3.fields{6}.minimum = -1.020000e+02;
WR224C.PCM_A_3.fields{6}.scale = 1.000000e-01;
WR224C.PCM_A_3.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_1'
WR224C.PCM_B_1 = struct;
WR224C.PCM_B_1.name = 'PCM_B_1';
WR224C.PCM_B_1.description = 'Should have same frequency/interval as other B frames.';
WR224C.PCM_B_1.protocol = 'WR224C';
WR224C.PCM_B_1.id = hex2dec('000000E6');
WR224C.PCM_B_1.idext = 'STANDARD';
WR224C.PCM_B_1.idinherit = 0;
WR224C.PCM_B_1.payload_size = 8;
WR224C.PCM_B_1.payload_value = [];
WR224C.PCM_B_1.payload_mask = [];
WR224C.PCM_B_1.interval = -1;

WR224C.PCM_B_1.fields{1}.name = 'Wheel_Slip';
WR224C.PCM_B_1.fields{1}.units = '%';
WR224C.PCM_B_1.fields{1}.start_bit = 30;
WR224C.PCM_B_1.fields{1}.bit_length = 10;
WR224C.PCM_B_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_B_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{1}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{2}.name = 'TC_Retard';
WR224C.PCM_B_1.fields{2}.units = 'deg';
WR224C.PCM_B_1.fields{2}.start_bit = 19;
WR224C.PCM_B_1.fields{2}.bit_length = 11;
WR224C.PCM_B_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{2}.data_type = 'SIGNED';
WR224C.PCM_B_1.fields{2}.maximum = 6.000000e+01;
WR224C.PCM_B_1.fields{2}.minimum = -6.000000e+01;
WR224C.PCM_B_1.fields{2}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{3}.name = 'TC_Active';
WR224C.PCM_B_1.fields{3}.units = 'bool';
WR224C.PCM_B_1.fields{3}.start_bit = 0;
WR224C.PCM_B_1.fields{3}.bit_length = 1;
WR224C.PCM_B_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{3}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{4}.name = 'TC_P_Term';
WR224C.PCM_B_1.fields{4}.units = '';
WR224C.PCM_B_1.fields{4}.start_bit = 7;
WR224C.PCM_B_1.fields{4}.bit_length = 12;
WR224C.PCM_B_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{4}.maximum = 2.550000e+02;
WR224C.PCM_B_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{4}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{5}.name = 'TC_I_Term';
WR224C.PCM_B_1.fields{5}.units = '';
WR224C.PCM_B_1.fields{5}.start_bit = 52;
WR224C.PCM_B_1.fields{5}.bit_length = 12;
WR224C.PCM_B_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{5}.maximum = 2.550000e+02;
WR224C.PCM_B_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{5}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{6}.name = 'TC_D_Term';
WR224C.PCM_B_1.fields{6}.units = '';
WR224C.PCM_B_1.fields{6}.start_bit = 40;
WR224C.PCM_B_1.fields{6}.bit_length = 12;
WR224C.PCM_B_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{6}.maximum = 2.550000e+02;
WR224C.PCM_B_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{6}.scale = 1.000000e-01;
WR224C.PCM_B_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{7}.name = 'Shift_Down_Sol';
WR224C.PCM_B_1.fields{7}.units = 'bool';
WR224C.PCM_B_1.fields{7}.start_bit = 5;
WR224C.PCM_B_1.fields{7}.bit_length = 1;
WR224C.PCM_B_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{7}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{8}.name = 'Shift_Up_Sol';
WR224C.PCM_B_1.fields{8}.units = 'bool';
WR224C.PCM_B_1.fields{8}.start_bit = 4;
WR224C.PCM_B_1.fields{8}.bit_length = 1;
WR224C.PCM_B_1.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{8}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{8}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{9}.name = 'Clutch_Engage_Sol';
WR224C.PCM_B_1.fields{9}.units = 'bool';
WR224C.PCM_B_1.fields{9}.start_bit = 3;
WR224C.PCM_B_1.fields{9}.bit_length = 1;
WR224C.PCM_B_1.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{9}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{9}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{9}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{10}.name = 'Clutch_Disengage_Sol';
WR224C.PCM_B_1.fields{10}.units = 'bool';
WR224C.PCM_B_1.fields{10}.start_bit = 2;
WR224C.PCM_B_1.fields{10}.bit_length = 1;
WR224C.PCM_B_1.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{10}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{10}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{10}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{10}.offset = 0.000000e+00;

WR224C.PCM_B_1.fields{11}.name = 'Clutch_Slowdrop_Sol';
WR224C.PCM_B_1.fields{11}.units = 'bool';
WR224C.PCM_B_1.fields{11}.start_bit = 1;
WR224C.PCM_B_1.fields{11}.bit_length = 1;
WR224C.PCM_B_1.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_1.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_B_1.fields{11}.maximum = 1.000000e+00;
WR224C.PCM_B_1.fields{11}.minimum = 0.000000e+00;
WR224C.PCM_B_1.fields{11}.scale = 1.000000e+00;
WR224C.PCM_B_1.fields{11}.offset = 0.000000e+00;

%===================================================================
case 'PCM_B_2'
WR224C.PCM_B_2 = struct;
WR224C.PCM_B_2.name = 'PCM_B_2';
WR224C.PCM_B_2.description = 'Should have same frequency/interval as other B frames.';
WR224C.PCM_B_2.protocol = 'WR224C';
WR224C.PCM_B_2.id = hex2dec('000000DF');
WR224C.PCM_B_2.idext = 'STANDARD';
WR224C.PCM_B_2.idinherit = 0;
WR224C.PCM_B_2.payload_size = 8;
WR224C.PCM_B_2.payload_value = [];
WR224C.PCM_B_2.payload_mask = [];
WR224C.PCM_B_2.interval = -1;

WR224C.PCM_B_2.fields{1}.name = 'Wheel_Slip_Error';
WR224C.PCM_B_2.fields{1}.units = '%';
WR224C.PCM_B_2.fields{1}.start_bit = 54;
WR224C.PCM_B_2.fields{1}.bit_length = 10;
WR224C.PCM_B_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_B_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{1}.scale = 1.000000e-01;
WR224C.PCM_B_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{2}.name = 'Cyl1_Pulse_Width';
WR224C.PCM_B_2.fields{2}.units = 'ms';
WR224C.PCM_B_2.fields{2}.start_bit = 45;
WR224C.PCM_B_2.fields{2}.bit_length = 9;
WR224C.PCM_B_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{2}.maximum = 4.500000e+01;
WR224C.PCM_B_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{2}.scale = 1.000000e-01;
WR224C.PCM_B_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{3}.name = 'Air_Tank_Pressure';
WR224C.PCM_B_2.fields{3}.units = 'kPa';
WR224C.PCM_B_2.fields{3}.start_bit = 33;
WR224C.PCM_B_2.fields{3}.bit_length = 12;
WR224C.PCM_B_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{3}.maximum = 4.000000e+03;
WR224C.PCM_B_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{3}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{4}.name = 'Cyl2_Pulse_Width';
WR224C.PCM_B_2.fields{4}.units = 'ms';
WR224C.PCM_B_2.fields{4}.start_bit = 24;
WR224C.PCM_B_2.fields{4}.bit_length = 9;
WR224C.PCM_B_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{4}.maximum = 4.500000e+01;
WR224C.PCM_B_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{4}.scale = 1.000000e-01;
WR224C.PCM_B_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{5}.name = 'Shifts_Remaining';
WR224C.PCM_B_2.fields{5}.units = 'bool';
WR224C.PCM_B_2.fields{5}.start_bit = 14;
WR224C.PCM_B_2.fields{5}.bit_length = 10;
WR224C.PCM_B_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{5}.maximum = 1.023000e+03;
WR224C.PCM_B_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{5}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{6}.name = 'Crank_Req';
WR224C.PCM_B_2.fields{6}.units = 'bool';
WR224C.PCM_B_2.fields{6}.start_bit = 11;
WR224C.PCM_B_2.fields{6}.bit_length = 1;
WR224C.PCM_B_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{6}.maximum = 1.000000e+00;
WR224C.PCM_B_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{6}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{6}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{7}.name = 'Shift_Up_Paddle';
WR224C.PCM_B_2.fields{7}.units = 'bool';
WR224C.PCM_B_2.fields{7}.start_bit = 12;
WR224C.PCM_B_2.fields{7}.bit_length = 1;
WR224C.PCM_B_2.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_B_2.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{7}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{7}.offset = 0.000000e+00;

WR224C.PCM_B_2.fields{8}.name = 'Shift_Down_Paddle';
WR224C.PCM_B_2.fields{8}.units = 'bool';
WR224C.PCM_B_2.fields{8}.start_bit = 13;
WR224C.PCM_B_2.fields{8}.bit_length = 1;
WR224C.PCM_B_2.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_B_2.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_B_2.fields{8}.maximum = 1.000000e+00;
WR224C.PCM_B_2.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_B_2.fields{8}.scale = 1.000000e+00;
WR224C.PCM_B_2.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_1'
WR224C.PCM_C_1 = struct;
WR224C.PCM_C_1.name = 'PCM_C_1';
WR224C.PCM_C_1.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_1.protocol = 'WR224C';
WR224C.PCM_C_1.id = hex2dec('000000E0');
WR224C.PCM_C_1.idext = 'STANDARD';
WR224C.PCM_C_1.idinherit = 0;
WR224C.PCM_C_1.payload_size = 8;
WR224C.PCM_C_1.payload_value = [];
WR224C.PCM_C_1.payload_mask = [];
WR224C.PCM_C_1.interval = -1;

WR224C.PCM_C_1.fields{1}.name = 'APPS_Arb';
WR224C.PCM_C_1.fields{1}.units = '%';
WR224C.PCM_C_1.fields{1}.start_bit = 24;
WR224C.PCM_C_1.fields{1}.bit_length = 10;
WR224C.PCM_C_1.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{1}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{1}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{2}.name = 'TPS_Arb';
WR224C.PCM_C_1.fields{2}.units = '%';
WR224C.PCM_C_1.fields{2}.start_bit = 34;
WR224C.PCM_C_1.fields{2}.bit_length = 10;
WR224C.PCM_C_1.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{2}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{3}.name = 'Ind_Torque';
WR224C.PCM_C_1.fields{3}.units = 'Nm';
WR224C.PCM_C_1.fields{3}.start_bit = 44;
WR224C.PCM_C_1.fields{3}.bit_length = 10;
WR224C.PCM_C_1.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{3}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{3}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{4}.name = 'Virtual_MAF';
WR224C.PCM_C_1.fields{4}.units = 'g/s';
WR224C.PCM_C_1.fields{4}.start_bit = 54;
WR224C.PCM_C_1.fields{4}.bit_length = 10;
WR224C.PCM_C_1.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{4}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{4}.scale = 1.000000e-01;
WR224C.PCM_C_1.fields{4}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{5}.name = 'Rad_Fan';
WR224C.PCM_C_1.fields{5}.units = '%';
WR224C.PCM_C_1.fields{5}.start_bit = 16;
WR224C.PCM_C_1.fields{5}.bit_length = 8;
WR224C.PCM_C_1.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{5}.maximum = 1.000000e+02;
WR224C.PCM_C_1.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{5}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{5}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{6}.name = 'Target_Phi';
WR224C.PCM_C_1.fields{6}.units = 'EqRatio';
WR224C.PCM_C_1.fields{6}.start_bit = 8;
WR224C.PCM_C_1.fields{6}.bit_length = 8;
WR224C.PCM_C_1.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{6}.maximum = 2.000000e+00;
WR224C.PCM_C_1.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{6}.scale = 1.000000e-02;
WR224C.PCM_C_1.fields{6}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{7}.name = 'DownShiftOverspeedWarning';
WR224C.PCM_C_1.fields{7}.units = 'bool';
WR224C.PCM_C_1.fields{7}.start_bit = 0;
WR224C.PCM_C_1.fields{7}.bit_length = 1;
WR224C.PCM_C_1.fields{7}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{7}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{7}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{7}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{7}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{8}.name = 'ShiftingSolenoidExtendedFireTime';
WR224C.PCM_C_1.fields{8}.units = 'bool';
WR224C.PCM_C_1.fields{8}.start_bit = 1;
WR224C.PCM_C_1.fields{8}.bit_length = 1;
WR224C.PCM_C_1.fields{8}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{8}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{8}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{8}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{8}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{9}.name = 'PCMFault1';
WR224C.PCM_C_1.fields{9}.units = 'bool';
WR224C.PCM_C_1.fields{9}.start_bit = 2;
WR224C.PCM_C_1.fields{9}.bit_length = 1;
WR224C.PCM_C_1.fields{9}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{9}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{9}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{9}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{9}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{10}.name = 'PCMFault2';
WR224C.PCM_C_1.fields{10}.units = 'bool';
WR224C.PCM_C_1.fields{10}.start_bit = 3;
WR224C.PCM_C_1.fields{10}.bit_length = 1;
WR224C.PCM_C_1.fields{10}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{10}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{10}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{10}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{10}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{11}.name = 'PCMFault3';
WR224C.PCM_C_1.fields{11}.units = 'bool';
WR224C.PCM_C_1.fields{11}.start_bit = 4;
WR224C.PCM_C_1.fields{11}.bit_length = 1;
WR224C.PCM_C_1.fields{11}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{11}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{11}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{11}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{11}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{12}.name = 'PCMFault4';
WR224C.PCM_C_1.fields{12}.units = 'bool';
WR224C.PCM_C_1.fields{12}.start_bit = 5;
WR224C.PCM_C_1.fields{12}.bit_length = 1;
WR224C.PCM_C_1.fields{12}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{12}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{12}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{12}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{12}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{12}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{13}.name = 'PCMFault5';
WR224C.PCM_C_1.fields{13}.units = 'bool';
WR224C.PCM_C_1.fields{13}.start_bit = 6;
WR224C.PCM_C_1.fields{13}.bit_length = 1;
WR224C.PCM_C_1.fields{13}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{13}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{13}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{13}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{13}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{13}.offset = 0.000000e+00;

WR224C.PCM_C_1.fields{14}.name = 'PCMFault6';
WR224C.PCM_C_1.fields{14}.units = 'bool';
WR224C.PCM_C_1.fields{14}.start_bit = 7;
WR224C.PCM_C_1.fields{14}.bit_length = 1;
WR224C.PCM_C_1.fields{14}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_1.fields{14}.data_type = 'UNSIGNED';
WR224C.PCM_C_1.fields{14}.maximum = 1.000000e+00;
WR224C.PCM_C_1.fields{14}.minimum = 0.000000e+00;
WR224C.PCM_C_1.fields{14}.scale = 1.000000e+00;
WR224C.PCM_C_1.fields{14}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_2'
WR224C.PCM_C_2 = struct;
WR224C.PCM_C_2.name = 'PCM_C_2';
WR224C.PCM_C_2.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_2.protocol = 'WR224C';
WR224C.PCM_C_2.id = hex2dec('000000E1');
WR224C.PCM_C_2.idext = 'STANDARD';
WR224C.PCM_C_2.idinherit = 0;
WR224C.PCM_C_2.payload_size = 8;
WR224C.PCM_C_2.payload_value = [];
WR224C.PCM_C_2.payload_mask = [];
WR224C.PCM_C_2.interval = -1;

WR224C.PCM_C_2.fields{1}.name = 'Rev_Limiter';
WR224C.PCM_C_2.fields{1}.units = 'rpm';
WR224C.PCM_C_2.fields{1}.start_bit = 48;
WR224C.PCM_C_2.fields{1}.bit_length = 16;
WR224C.PCM_C_2.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{1}.maximum = 1.300000e+04;
WR224C.PCM_C_2.fields{1}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{1}.scale = 1.000000e+00;
WR224C.PCM_C_2.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{2}.name = 'Spark_Energy_Pct';
WR224C.PCM_C_2.fields{2}.units = '%';
WR224C.PCM_C_2.fields{2}.start_bit = 40;
WR224C.PCM_C_2.fields{2}.bit_length = 8;
WR224C.PCM_C_2.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_C_2.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{2}.scale = 1.000000e+00;
WR224C.PCM_C_2.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{3}.name = 'Oil_Pressure';
WR224C.PCM_C_2.fields{3}.units = 'kPa';
WR224C.PCM_C_2.fields{3}.start_bit = 31;
WR224C.PCM_C_2.fields{3}.bit_length = 9;
WR224C.PCM_C_2.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{3}.maximum = 6.000000e+02;
WR224C.PCM_C_2.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{3}.scale = 2.000000e+00;
WR224C.PCM_C_2.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{4}.name = 'Fuel_Pressure';
WR224C.PCM_C_2.fields{4}.units = 'kPa';
WR224C.PCM_C_2.fields{4}.start_bit = 22;
WR224C.PCM_C_2.fields{4}.bit_length = 9;
WR224C.PCM_C_2.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{4}.maximum = 3.500000e+02;
WR224C.PCM_C_2.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{4}.scale = 1.000000e+00;
WR224C.PCM_C_2.fields{4}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{5}.name = 'Coolant_Temp';
WR224C.PCM_C_2.fields{5}.units = 'C';
WR224C.PCM_C_2.fields{5}.start_bit = 11;
WR224C.PCM_C_2.fields{5}.bit_length = 11;
WR224C.PCM_C_2.fields{5}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{5}.maximum = 1.200000e+02;
WR224C.PCM_C_2.fields{5}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{5}.scale = 1.000000e-01;
WR224C.PCM_C_2.fields{5}.offset = 0.000000e+00;

WR224C.PCM_C_2.fields{6}.name = 'Oil_Temp';
WR224C.PCM_C_2.fields{6}.units = 'C';
WR224C.PCM_C_2.fields{6}.start_bit = 0;
WR224C.PCM_C_2.fields{6}.bit_length = 11;
WR224C.PCM_C_2.fields{6}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_2.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_C_2.fields{6}.maximum = 1.300000e+02;
WR224C.PCM_C_2.fields{6}.minimum = 0.000000e+00;
WR224C.PCM_C_2.fields{6}.scale = 1.000000e-01;
WR224C.PCM_C_2.fields{6}.offset = 0.000000e+00;

%===================================================================
case 'PCM_C_3'
WR224C.PCM_C_3 = struct;
WR224C.PCM_C_3.name = 'PCM_C_3';
WR224C.PCM_C_3.description = 'Variables in C frames do not have to be sent at high frequencies/lower intervals, and not all C variables need to be sent at the same rate.';
WR224C.PCM_C_3.protocol = 'WR224C';
WR224C.PCM_C_3.id = hex2dec('000000E5');
WR224C.PCM_C_3.idext = 'STANDARD';
WR224C.PCM_C_3.idinherit = 0;
WR224C.PCM_C_3.payload_size = 8;
WR224C.PCM_C_3.payload_value = [];
WR224C.PCM_C_3.payload_mask = [];
WR224C.PCM_C_3.interval = -1;

WR224C.PCM_C_3.fields{1}.name = 'Plenum_Air_Temp';
WR224C.PCM_C_3.fields{1}.units = 'C';
WR224C.PCM_C_3.fields{1}.start_bit = 53;
WR224C.PCM_C_3.fields{1}.bit_length = 11;
WR224C.PCM_C_3.fields{1}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{1}.data_type = 'SIGNED';
WR224C.PCM_C_3.fields{1}.maximum = 8.000000e+01;
WR224C.PCM_C_3.fields{1}.minimum = -1.000000e+01;
WR224C.PCM_C_3.fields{1}.scale = 1.000000e-01;
WR224C.PCM_C_3.fields{1}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{2}.name = 'CAN_Throttle_Req';
WR224C.PCM_C_3.fields{2}.units = '%';
WR224C.PCM_C_3.fields{2}.start_bit = 42;
WR224C.PCM_C_3.fields{2}.bit_length = 11;
WR224C.PCM_C_3.fields{2}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{2}.maximum = 1.000000e+02;
WR224C.PCM_C_3.fields{2}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{2}.scale = 1.000000e-01;
WR224C.PCM_C_3.fields{2}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{3}.name = 'F_Brake_Pressure';
WR224C.PCM_C_3.fields{3}.units = 'kPa';
WR224C.PCM_C_3.fields{3}.start_bit = 33;
WR224C.PCM_C_3.fields{3}.bit_length = 9;
WR224C.PCM_C_3.fields{3}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{3}.maximum = 3.000000e+03;
WR224C.PCM_C_3.fields{3}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{3}.scale = 1.000000e+01;
WR224C.PCM_C_3.fields{3}.offset = 0.000000e+00;

WR224C.PCM_C_3.fields{4}.name = 'R_Brake_Pressure';
WR224C.PCM_C_3.fields{4}.units = 'kPa';
WR224C.PCM_C_3.fields{4}.start_bit = 24;
WR224C.PCM_C_3.fields{4}.bit_length = 9;
WR224C.PCM_C_3.fields{4}.byte_order = 'BIG_ENDIAN';
WR224C.PCM_C_3.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_C_3.fields{4}.maximum = 3.000000e+03;
WR224C.PCM_C_3.fields{4}.minimum = 0.000000e+00;
WR224C.PCM_C_3.fields{4}.scale = 1.000000e+01;
WR224C.PCM_C_3.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'PCM_SteeringWheelInputs'
WR224C.PCM_SteeringWheelInputs = struct;
WR224C.PCM_SteeringWheelInputs.name = 'PCM_SteeringWheelInputs';
WR224C.PCM_SteeringWheelInputs.description = 'PCM_SteeringWheelInputs';
WR224C.PCM_SteeringWheelInputs.protocol = 'WR224C';
WR224C.PCM_SteeringWheelInputs.id = hex2dec('00000010');
WR224C.PCM_SteeringWheelInputs.idext = 'STANDARD';
WR224C.PCM_SteeringWheelInputs.idinherit = 0;
WR224C.PCM_SteeringWheelInputs.payload_size = 8;
WR224C.PCM_SteeringWheelInputs.payload_value = [];
WR224C.PCM_SteeringWheelInputs.payload_mask = [];
WR224C.PCM_SteeringWheelInputs.interval = -1;

WR224C.PCM_SteeringWheelInputs.fields{1}.name = 'SW_Button1';
WR224C.PCM_SteeringWheelInputs.fields{1}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{1}.start_bit = 56;
WR224C.PCM_SteeringWheelInputs.fields{1}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{1}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{1}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{1}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{2}.name = 'SW_Button3';
WR224C.PCM_SteeringWheelInputs.fields{2}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{2}.start_bit = 58;
WR224C.PCM_SteeringWheelInputs.fields{2}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{2}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{2}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{2}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{3}.name = 'SW_Button2';
WR224C.PCM_SteeringWheelInputs.fields{3}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{3}.start_bit = 57;
WR224C.PCM_SteeringWheelInputs.fields{3}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{3}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{3}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{3}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{4}.name = 'SW_Button4';
WR224C.PCM_SteeringWheelInputs.fields{4}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{4}.start_bit = 59;
WR224C.PCM_SteeringWheelInputs.fields{4}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{4}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{4}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{4}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{5}.name = 'SW_Button5';
WR224C.PCM_SteeringWheelInputs.fields{5}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{5}.start_bit = 60;
WR224C.PCM_SteeringWheelInputs.fields{5}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{5}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{5}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{5}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{6}.name = 'SW_Button6';
WR224C.PCM_SteeringWheelInputs.fields{6}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{6}.start_bit = 61;
WR224C.PCM_SteeringWheelInputs.fields{6}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{6}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{6}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{6}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{7}.name = 'SW_DialLeft';
WR224C.PCM_SteeringWheelInputs.fields{7}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{7}.start_bit = 51;
WR224C.PCM_SteeringWheelInputs.fields{7}.bit_length = 4;
WR224C.PCM_SteeringWheelInputs.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{7}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{7}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{7}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{8}.name = 'SW_DialRight';
WR224C.PCM_SteeringWheelInputs.fields{8}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{8}.start_bit = 40;
WR224C.PCM_SteeringWheelInputs.fields{8}.bit_length = 4;
WR224C.PCM_SteeringWheelInputs.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{8}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{8}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{8}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{9}.name = 'SW_JoystickCENTRE';
WR224C.PCM_SteeringWheelInputs.fields{9}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{9}.start_bit = 62;
WR224C.PCM_SteeringWheelInputs.fields{9}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{9}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{9}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{9}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{9}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{10}.name = 'SW_JoystickDOWN';
WR224C.PCM_SteeringWheelInputs.fields{10}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{10}.start_bit = 63;
WR224C.PCM_SteeringWheelInputs.fields{10}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{10}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{10}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{10}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{10}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{11}.name = 'SW_JoystickLEFT';
WR224C.PCM_SteeringWheelInputs.fields{11}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{11}.start_bit = 48;
WR224C.PCM_SteeringWheelInputs.fields{11}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{11}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{11}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{11}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{11}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{12}.name = 'SW_JoystickRIGHT';
WR224C.PCM_SteeringWheelInputs.fields{12}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{12}.start_bit = 49;
WR224C.PCM_SteeringWheelInputs.fields{12}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{12}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{12}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{12}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{12}.offset = 0.000000e+00;

WR224C.PCM_SteeringWheelInputs.fields{13}.name = 'SW_JoystickUP';
WR224C.PCM_SteeringWheelInputs.fields{13}.units = '';
WR224C.PCM_SteeringWheelInputs.fields{13}.start_bit = 50;
WR224C.PCM_SteeringWheelInputs.fields{13}.bit_length = 1;
WR224C.PCM_SteeringWheelInputs.fields{13}.byte_order = 'LITTLE_ENDIAN';
WR224C.PCM_SteeringWheelInputs.fields{13}.data_type = 'UNSIGNED';
WR224C.PCM_SteeringWheelInputs.fields{13}.scale = 1.000000e+00;
WR224C.PCM_SteeringWheelInputs.fields{13}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Current1'
WR224C.SPDU_Current1 = struct;
WR224C.SPDU_Current1.name = 'SPDU_Current1';
WR224C.SPDU_Current1.description = 'SPDU_Current1';
WR224C.SPDU_Current1.protocol = 'WR224C';
WR224C.SPDU_Current1.id = hex2dec('00000130');
WR224C.SPDU_Current1.idext = 'STANDARD';
WR224C.SPDU_Current1.idinherit = 0;
WR224C.SPDU_Current1.payload_size = 8;
WR224C.SPDU_Current1.payload_value = [];
WR224C.SPDU_Current1.payload_mask = [];
WR224C.SPDU_Current1.interval = -1;

WR224C.SPDU_Current1.fields{1}.name = 'SPDU_Current_eFuse2';
WR224C.SPDU_Current1.fields{1}.units = '';
WR224C.SPDU_Current1.fields{1}.start_bit = 8;
WR224C.SPDU_Current1.fields{1}.bit_length = 16;
WR224C.SPDU_Current1.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current1.fields{1}.data_type = 'SIGNED';
WR224C.SPDU_Current1.fields{1}.scale = 1.000000e-03;
WR224C.SPDU_Current1.fields{1}.offset = 0.000000e+00;

WR224C.SPDU_Current1.fields{2}.name = 'SPDU_Current_eFuse1';
WR224C.SPDU_Current1.fields{2}.units = '';
WR224C.SPDU_Current1.fields{2}.start_bit = 24;
WR224C.SPDU_Current1.fields{2}.bit_length = 16;
WR224C.SPDU_Current1.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current1.fields{2}.data_type = 'SIGNED';
WR224C.SPDU_Current1.fields{2}.scale = 1.000000e-03;
WR224C.SPDU_Current1.fields{2}.offset = 0.000000e+00;

WR224C.SPDU_Current1.fields{3}.name = 'SPDU_Current_AH2';
WR224C.SPDU_Current1.fields{3}.units = '';
WR224C.SPDU_Current1.fields{3}.start_bit = 40;
WR224C.SPDU_Current1.fields{3}.bit_length = 16;
WR224C.SPDU_Current1.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current1.fields{3}.data_type = 'SIGNED';
WR224C.SPDU_Current1.fields{3}.scale = 1.000000e-03;
WR224C.SPDU_Current1.fields{3}.offset = 0.000000e+00;

WR224C.SPDU_Current1.fields{4}.name = 'SPDU_Current_AH1';
WR224C.SPDU_Current1.fields{4}.units = '';
WR224C.SPDU_Current1.fields{4}.start_bit = 56;
WR224C.SPDU_Current1.fields{4}.bit_length = 16;
WR224C.SPDU_Current1.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current1.fields{4}.data_type = 'SIGNED';
WR224C.SPDU_Current1.fields{4}.scale = 1.000000e-03;
WR224C.SPDU_Current1.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Current2'
WR224C.SPDU_Current2 = struct;
WR224C.SPDU_Current2.name = 'SPDU_Current2';
WR224C.SPDU_Current2.description = 'SPDU_Current2';
WR224C.SPDU_Current2.protocol = 'WR224C';
WR224C.SPDU_Current2.id = hex2dec('00000131');
WR224C.SPDU_Current2.idext = 'STANDARD';
WR224C.SPDU_Current2.idinherit = 0;
WR224C.SPDU_Current2.payload_size = 8;
WR224C.SPDU_Current2.payload_value = [];
WR224C.SPDU_Current2.payload_mask = [];
WR224C.SPDU_Current2.interval = -1;

WR224C.SPDU_Current2.fields{1}.name = 'SPDU_Current_eFuse6';
WR224C.SPDU_Current2.fields{1}.units = '';
WR224C.SPDU_Current2.fields{1}.start_bit = 8;
WR224C.SPDU_Current2.fields{1}.bit_length = 16;
WR224C.SPDU_Current2.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current2.fields{1}.data_type = 'SIGNED';
WR224C.SPDU_Current2.fields{1}.scale = 1.000000e-03;
WR224C.SPDU_Current2.fields{1}.offset = 0.000000e+00;

WR224C.SPDU_Current2.fields{2}.name = 'SPDU_Current_eFuse5';
WR224C.SPDU_Current2.fields{2}.units = '';
WR224C.SPDU_Current2.fields{2}.start_bit = 24;
WR224C.SPDU_Current2.fields{2}.bit_length = 16;
WR224C.SPDU_Current2.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current2.fields{2}.data_type = 'SIGNED';
WR224C.SPDU_Current2.fields{2}.scale = 1.000000e-03;
WR224C.SPDU_Current2.fields{2}.offset = 0.000000e+00;

WR224C.SPDU_Current2.fields{3}.name = 'SPDU_Current_eFuse4';
WR224C.SPDU_Current2.fields{3}.units = '';
WR224C.SPDU_Current2.fields{3}.start_bit = 40;
WR224C.SPDU_Current2.fields{3}.bit_length = 16;
WR224C.SPDU_Current2.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current2.fields{3}.data_type = 'SIGNED';
WR224C.SPDU_Current2.fields{3}.scale = 1.000000e-03;
WR224C.SPDU_Current2.fields{3}.offset = 0.000000e+00;

WR224C.SPDU_Current2.fields{4}.name = 'SPDU_Current_eFuse3';
WR224C.SPDU_Current2.fields{4}.units = '';
WR224C.SPDU_Current2.fields{4}.start_bit = 56;
WR224C.SPDU_Current2.fields{4}.bit_length = 16;
WR224C.SPDU_Current2.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Current2.fields{4}.data_type = 'SIGNED';
WR224C.SPDU_Current2.fields{4}.scale = 1.000000e-03;
WR224C.SPDU_Current2.fields{4}.offset = 0.000000e+00;

%===================================================================
case 'SPDU_Faults'
WR224C.SPDU_Faults = struct;
WR224C.SPDU_Faults.name = 'SPDU_Faults';
WR224C.SPDU_Faults.description = 'SPDU_Faults';
WR224C.SPDU_Faults.protocol = 'WR224C';
WR224C.SPDU_Faults.id = hex2dec('00000030');
WR224C.SPDU_Faults.idext = 'STANDARD';
WR224C.SPDU_Faults.idinherit = 0;
WR224C.SPDU_Faults.payload_size = 1;
WR224C.SPDU_Faults.payload_value = [];
WR224C.SPDU_Faults.payload_mask = [];
WR224C.SPDU_Faults.interval = -1;

WR224C.SPDU_Faults.fields{1}.name = 'SPDU_Fault_eFuse6';
WR224C.SPDU_Faults.fields{1}.units = '';
WR224C.SPDU_Faults.fields{1}.start_bit = 63;
WR224C.SPDU_Faults.fields{1}.bit_length = 1;
WR224C.SPDU_Faults.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{1}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{1}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{1}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{2}.name = 'SPDU_Fault_eFuse5';
WR224C.SPDU_Faults.fields{2}.units = '';
WR224C.SPDU_Faults.fields{2}.start_bit = 62;
WR224C.SPDU_Faults.fields{2}.bit_length = 1;
WR224C.SPDU_Faults.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{2}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{2}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{2}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{3}.name = 'SPDU_Fault_eFuse4';
WR224C.SPDU_Faults.fields{3}.units = '';
WR224C.SPDU_Faults.fields{3}.start_bit = 61;
WR224C.SPDU_Faults.fields{3}.bit_length = 1;
WR224C.SPDU_Faults.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{3}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{3}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{3}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{4}.name = 'SPDU_Fault_eFuse3';
WR224C.SPDU_Faults.fields{4}.units = '';
WR224C.SPDU_Faults.fields{4}.start_bit = 60;
WR224C.SPDU_Faults.fields{4}.bit_length = 1;
WR224C.SPDU_Faults.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{4}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{4}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{4}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{5}.name = 'SPDU_Fault_eFuse2';
WR224C.SPDU_Faults.fields{5}.units = '';
WR224C.SPDU_Faults.fields{5}.start_bit = 59;
WR224C.SPDU_Faults.fields{5}.bit_length = 1;
WR224C.SPDU_Faults.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{5}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{5}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{5}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{6}.name = 'SPDU_Fault_eFuse1';
WR224C.SPDU_Faults.fields{6}.units = '';
WR224C.SPDU_Faults.fields{6}.start_bit = 58;
WR224C.SPDU_Faults.fields{6}.bit_length = 1;
WR224C.SPDU_Faults.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{6}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{6}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{6}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{7}.name = 'SPDU_Fault_AH2';
WR224C.SPDU_Faults.fields{7}.units = '';
WR224C.SPDU_Faults.fields{7}.start_bit = 57;
WR224C.SPDU_Faults.fields{7}.bit_length = 1;
WR224C.SPDU_Faults.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{7}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{7}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{7}.offset = 0.000000e+00;

WR224C.SPDU_Faults.fields{8}.name = 'SPDU_Fault_AH1';
WR224C.SPDU_Faults.fields{8}.units = '';
WR224C.SPDU_Faults.fields{8}.start_bit = 56;
WR224C.SPDU_Faults.fields{8}.bit_length = 1;
WR224C.SPDU_Faults.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR224C.SPDU_Faults.fields{8}.data_type = 'SIGNED';
WR224C.SPDU_Faults.fields{8}.scale = 1.000000e+00;
WR224C.SPDU_Faults.fields{8}.offset = 0.000000e+00;

%===================================================================
case 'TireTemp_FL'
WR224C.TireTemp_FL = struct;
WR224C.TireTemp_FL.name = 'TireTemp_FL';
WR224C.TireTemp_FL.description = 'TireTemp_FL';
WR224C.TireTemp_FL.protocol = 'WR224C';
WR224C.TireTemp_FL.id = hex2dec('00000001');
WR224C.TireTemp_FL.idext = 'STANDARD';
WR224C.TireTemp_FL.idinherit = 0;
WR224C.TireTemp_FL.payload_size = 8;
WR224C.TireTemp_FL.payload_value = [];
WR224C.TireTemp_FL.payload_mask = [];
WR224C.TireTemp_FL.interval = -1;

WR224C.TireTemp_FL.fields{1}.name = 'FL_ZoneA';
WR224C.TireTemp_FL.fields{1}.units = '';
WR224C.TireTemp_FL.fields{1}.start_bit = 56;
WR224C.TireTemp_FL.fields{1}.bit_length = 8;
WR224C.TireTemp_FL.fields{1}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{1}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{1}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{1}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{2}.name = 'FL_ZoneB';
WR224C.TireTemp_FL.fields{2}.units = '';
WR224C.TireTemp_FL.fields{2}.start_bit = 48;
WR224C.TireTemp_FL.fields{2}.bit_length = 8;
WR224C.TireTemp_FL.fields{2}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{2}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{2}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{2}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{3}.name = 'FL_Zone_C';
WR224C.TireTemp_FL.fields{3}.units = '';
WR224C.TireTemp_FL.fields{3}.start_bit = 40;
WR224C.TireTemp_FL.fields{3}.bit_length = 8;
WR224C.TireTemp_FL.fields{3}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{3}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{3}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{3}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{4}.name = 'FL_Zone_D';
WR224C.TireTemp_FL.fields{4}.units = '';
WR224C.TireTemp_FL.fields{4}.start_bit = 32;
WR224C.TireTemp_FL.fields{4}.bit_length = 8;
WR224C.TireTemp_FL.fields{4}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{4}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{4}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{4}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{5}.name = 'FL_Zone_E';
WR224C.TireTemp_FL.fields{5}.units = '';
WR224C.TireTemp_FL.fields{5}.start_bit = 24;
WR224C.TireTemp_FL.fields{5}.bit_length = 8;
WR224C.TireTemp_FL.fields{5}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{5}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{5}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{5}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{6}.name = 'FL_Zone_F';
WR224C.TireTemp_FL.fields{6}.units = '';
WR224C.TireTemp_FL.fields{6}.start_bit = 16;
WR224C.TireTemp_FL.fields{6}.bit_length = 8;
WR224C.TireTemp_FL.fields{6}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{6}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{6}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{6}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{7}.name = 'FL_ZoneG';
WR224C.TireTemp_FL.fields{7}.units = '';
WR224C.TireTemp_FL.fields{7}.start_bit = 8;
WR224C.TireTemp_FL.fields{7}.bit_length = 8;
WR224C.TireTemp_FL.fields{7}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{7}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{7}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{7}.offset = 0.000000e+00;

WR224C.TireTemp_FL.fields{8}.name = 'FL_ZoneH';
WR224C.TireTemp_FL.fields{8}.units = '';
WR224C.TireTemp_FL.fields{8}.start_bit = 0;
WR224C.TireTemp_FL.fields{8}.bit_length = 8;
WR224C.TireTemp_FL.fields{8}.byte_order = 'LITTLE_ENDIAN';
WR224C.TireTemp_FL.fields{8}.data_type = 'UNSIGNED';
WR224C.TireTemp_FL.fields{8}.scale = 1.000000e+00;
WR224C.TireTemp_FL.fields{8}.offset = 0.000000e+00;

end

try
	msg = WR224C.(msgname);
end
